<?PHP
class Parentsapimodel extends CI_Model
{
	function insertData($tbl_name,$data_array,$sendid = NULL)
	{
	 	$this->db->insert($tbl_name,$data_array);
	 	$result_id = $this->db->insert_id();
	 	
	 	/*echo $result_id;
	 	exit;*/
	 	
	 	if($sendid == 1)
	 	{
	 		//return id
	 		return $result_id;
	 	}
	}
	
	function insertBatchData($tbl_name,$data_array,$sendid = NULL)
	{
	 	$this->db->insert_batch($tbl_name,$data_array);
	 	$result_id = $this->db->insert_id();
	 	
	 	/*echo $result_id;
	 	exit;*/
	 	
	 	if($sendid == 1)
	 	{
	 		//return id
	 		return $result_id;
	 	}
	}
  
  
  
  
	function update_insert_fcm_token($user_id=null, $user_name=null,$user_type=null,$fcm_token=null,$device_id=null,$app_type){
		if($user_id != null && $user_name  != null && $user_type  != null && $fcm_token != null && $device_id != null){
			$sql = $this->db->query("select * 
						from tbl_fcm_user_token 
						where user_id = ".$this->db->escape($user_id).'
						And user_name = '.$this->db->escape($user_name).'
						And user_type = '.$this->db->escape($user_type).'
						And app_type = '.$this->db->escape($app_type).'
						'  
					);
			
			$data = array(
						'user_id'=>$user_id,
						'user_name'=>$user_name,
						'user_type'=>$user_type,
						'fcm_token'=>$fcm_token,
						'device_id'=>$device_id,
						'app_type'=>$app_type,
					);
					
			$res = false;
			if($sql->num_rows() > 0){
				$result = $sql->result_array();
				$fcm_user_token_id = $result[0]['fcm_user_token_id'];
				$res = $this->updateRecord('tbl_fcm_user_token', $data, 'fcm_user_token_id = '.$this->db->escape($fcm_user_token_id));
			}else{
				$res = $this->insertData('tbl_fcm_user_token', $data,1);
			}
			
			return $res;
		}
	}
		 
	
	function deleteuser_fcm_details($user_id,$user_type)
	{
		$this->db->where('user_id', $user_id);
		$this->db->where('user_type', $user_type);
		$this->db->delete('tbl_fcm_user_token');
		if($this->db->affected_rows() >= 1)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
  
  
	function fetch_otp($mobile,$country_code){
		$sql = $this->db->query("select * from tbl_otp_check where mobile = ".$this->db->escape($mobile).' And country_code ='.$this->db->escape($country_code));
		if($sql->num_rows()>0){
			return $sql->result();
		}else{
			return false;
		}
	}
  
	function generate_otp($mobile,$country_code,$otp){
		$sql = $this->db->query("select * from tbl_otp_check where mobile = ".$this->db->escape($mobile).' And country_code ='.$this->db->escape($country_code) );
		
		$data = array(
               "mobile"=>$mobile,
               "country_code"=>$country_code,
               "otp" => $otp,
               "time_stamp"=>date('Y-m-d H:i:s')
		);
    
		if($sql->num_rows()>0){
			$this->updatedataotp($data, "tbl_otp_check", "mobile", $mobile,"country_code", $country_code);
		}else{
			$this->insertData("tbl_otp_check",$data,1);
		}
		
		return true;
	}
  
	function updatedataotp($data,$table,$column1,$value1,$column2,$value2){
		$this->db->where($column1,$value1);
		$this->db->where($column2,$value2);
		$this->db->update($table,$data);
	}
	
	function getdata($table, $fields, $condition = '1=1'){
		//echo "Select $fields from $table where $condition";exit;
		$sql = $this->db->query("Select $fields from $table where $condition");
		if($sql->num_rows() > 0){
			return $sql->result_array();
		}else{
			return false;
		}
	}
	
	function getdata_orderby($table, $fields, $condition = '1=1', $order_by){
		//echo "Select $fields from $table where $condition";exit;
		$sql = $this->db->query("Select $fields from $table where $condition $order_by");
		if($sql->num_rows() > 0){
			return $sql->result_array();
		}else{
			return false;
		}
	}
	
	function getdata_orderby_limit($table, $fields, $condition = '1=1', $order_by, $page){
	
		$rows = 10;
		$page = $rows * $page;
		$limit = $page.",".$rows;
		
		$sql = $this->db->query("Select $fields from $table where $condition $order_by limit $limit");
		//print_r($this->db->last_query());
		//exit;
		
		if($sql->num_rows() > 0){
			return $sql->result_array();
		}else{
			return false;
		}
	}
	
	function getdata_orderby1($table, $fields, $condition = '1=1'){
		//echo "Select $fields from $table where $condition";exit;
		$sql = $this->db->query("Select $fields from $table where $condition order by id desc limit 1");
		if($sql->num_rows() > 0){
			return $sql->result_array();
		}else{
			return false;
		}
	}
	
	function getdata_group_order_by($table, $fields, $condition = '1=1', $group_by="", $order_by=""){
		//echo "Select $fields from $table where $condition";exit;
		$sql = $this->db->query("Select $fields from $table where $condition $group_by $order_by");
		if($sql->num_rows() > 0){
			return $sql->result_array();
		}else{
			return false;
		}
	}
	
	function getdata_orderby_limit1($table, $fields, $condition = '1=1', $order_by){
		//echo "Select $fields from $table where $condition";exit;
		$sql = $this->db->query("Select $fields from $table where $condition $order_by limit 1");
		if($sql->num_rows() > 0){
			return $sql->result_array();
		}else{
			return false;
		}
	}
    
    
    
	function updateRecord($tbl_name,$datar,$condition)
	{
		//$this -> db -> where($comp_col, $eid);
		$this->db->where("($condition)");
		$this -> db -> update($tbl_name,$datar);
		 
		if ($this->db->affected_rows() > 0){
			return true;
		}else{
			return true;
		} 
	}
	 
	function getFormdata($ID)
	{
		$this -> db -> select('i.*');
		$this -> db -> from('tbl_categories as i');
		$this -> db -> where('i.category_id', $ID);
	
		$query = $this -> db -> get();
	   
		//print_r($this->db->last_query());
		//exit;
	   
		if($query -> num_rows() >= 1){
			return $query->result();
		}else{
			return false;
		}
	}
	
	
	function delrecord($tbl_name,$tbl_id,$record_id)
	{
		$this->db->where($tbl_id, $record_id);
	    $this->db->delete($tbl_name);
		if($this->db->affected_rows() >= 1)
		{
			return true;
	    }
	    else
	    {
			return false;
	    }
	}
	
	function delrecord_condition($tbl_name,$condition)
	{
		//$this->db->where($tbl_id, $record_id);
		$this->db->where("($condition)");
		$this->db->delete($tbl_name);
		if($this->db->affected_rows() >= 1)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function checkEmail($email_id){
		$condition = "1=1 && i.email='".$email_id."' ";
		
		$this -> db -> select('i.professional_id');
		$this -> db -> from('tbl_professional as i');
		$this -> db -> where("($condition)");
		$query = $this -> db -> get();
		
		if($query -> num_rows() >= 1){
			return $query -> result_array();
		}else{
			return false;
		}
	}
	
	function get_fcm_data($user_id,$user_type){
		$query = $this->db->query('Select * from tbl_fcm_user_token where user_id = '.$this->db->escape($user_id).' And user_type='.$this->db->escape($user_type).' ');
		//echo 'Select * from tbl_fcm_user_token where user_id = '.$this->db->escape($user_id).' And user_type='.$this->db->escape($user_type).' ';
		//exit;
	    //print_r($this->db->last_query());
		//exit;
		if($query -> num_rows() >= 1){
				return $query->result_array();
			}else{
				return false;
			}
	}
	
	function get_email_data($eid){
		$query = $this->db->query('Select * from tbl_emailcontents where eid = '.$this->db->escape($eid));
	    
		if($query -> num_rows() >= 1){
			return $query->result_array();
		}else{
			return false;
		}
	}
	
	function check_guest_utoken($device_id) {
		$this -> db -> select('u.*');
		$this -> db -> from('tbl_admin_users as u');	
		$this -> db -> where('u.device_id',$device_id);
		
		$query = $this -> db -> get();
	   
		//print_r($this->db->last_query());
		//exit;
	   
		if($query -> num_rows() >= 1)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
			
	}
	
	function check_utoken($user_id) {
		$this -> db -> select('u.*');
		$this -> db -> from('tbl_student_master as u');
		$this -> db -> where('u.student_id',$user_id);
		
		$query = $this -> db -> get();
	   
		//print_r($this->db->last_query());
		//exit;
	   
		if($query -> num_rows() >= 1)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
	}
	
	function check_registered_utoken($utoken) {
		$this -> db -> select('u.*');
		$this -> db -> from('tbl_users as u');	
		$this -> db -> where('u.email',$utoken);
		$query = $this -> db -> get();
	
		if($query -> num_rows() >= 1)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
		
	}
	
	
	function getRecords($tbl_name, $condition, $page, $default_sort_column=null, $default_sort_order=null, $group_by=null){
		//echo "here...<br/>";
		//print_r($_REQUEST);
		//exit;
		
		$table = $tbl_name;
		$default_sort_column = $default_sort_column;
		$default_sort_order = $default_sort_order;
		
		
		//$rows = 100;
		$rows = 10;
		$page = $rows * $page;
		
		// sort order by column
		$sort = $default_sort_column;  
		$order = $default_sort_order;

		$this -> db -> select('*');
		$this -> db -> from($tbl_name);
		
		$this->db->where("($condition)");
		if(!empty($group_by)){
			$this->db->group_by($group_by);
		}
		$this->db->order_by($sort, $order);
		$this->db->limit($rows,$page);
		
		$query = $this -> db -> get();
		
		//print_r($this->db->last_query());
		//exit;
		
		
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
		
		
		//exit;
	}
	
	function login_check($condition) {
		$this -> db -> select('student_id, enrollment_no, zone_id, center_id, student_first_name, student_last_name, dob, profile_pic');
		$this -> db -> from('tbl_student_master');
		$this -> db -> where("($condition)");
		
		$query = $this -> db -> get();
		//print_r($this->db->last_query());
		//exit;
	 
		if($query -> num_rows() >= 1)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
	}
	
	function getCenterUserNewsletters($condition, $page){
		
		$rows = 10;
		$page = $rows * $page;

		$this -> db -> select('n.*');
		$this -> db -> from('tbl_assign_newsletters as i');
		$this -> db -> join('tbl_newsletters as n', 'i.newsletter_id  = n.newsletter_id', 'left');
		
		$this->db->where("($condition)");
		$this->db->order_by("n.newsletter_id", "desc");
		$this->db->limit($rows,$page);
		$query = $this -> db -> get();
		
		$this -> db -> select('n.*');
		$this -> db -> from('tbl_assign_newsletters as i');
		$this -> db -> join('tbl_newsletters as n', 'i.newsletter_id  = n.newsletter_id', 'left');
		
		$this->db->where("($condition)");
		$this->db->order_by("n.newsletter_id", "desc");	
		$query1 = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			$totcount = $query1 -> num_rows();
			return array("query_result" => $query->result(),"totalRecords"=>$totcount);
		}
		else
		{
			//return false;
			return array("totalRecords"=>0);
		}
		
	}
	
	function getStudentProfile($condition){
		
		$this -> db -> select('s.student_id, s.enrollment_no, s.student_first_name, s.student_last_name, s.dob, s.nationality, s.religion, s.father_name, s.mother_name, s.present_address, s.mother_home_contact_no, s.mother_mobile_contact_no, s.father_home_contact_no, s.father_mobile_contact_no, s.profile_pic, a.academic_year_master_name, ct.categoy_name, cr.course_name, b.batch_name, z.zone_name, cn.center_name');
		
		$this -> db -> from('tbl_student_details as d');
		$this -> db -> join('tbl_student_master as s', 'd.student_id  = s.student_id', 'left');
		$this -> db -> join('tbl_academic_year_master as a', 'd.academic_year_id  = a.academic_year_master_id', 'left');
		
		$this -> db -> join('tbl_categories as ct', 'd.category_id  = ct.category_id', 'left');
		$this -> db -> join('tbl_courses as cr', 'd.course_id  = cr.course_id', 'left');
		$this -> db -> join('tbl_batch_master as b', 'd.batch_id  = b.batch_id', 'left');
		
		$this -> db -> join('tbl_zones as z', 's.zone_id  = z.zone_id', 'left');
		$this -> db -> join('tbl_centers as cn', 's.center_id  = cn.center_id', 'left');
		
		$this->db->where("($condition)");
		
		$query = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
		
	}
	
	function getUserCategories($condition){
		
		$this -> db -> select('c.category_id,c.categoy_name');
		$this -> db -> from('tbl_student_details as i');
		$this -> db -> join('tbl_categories as c', 'i.category_id  = c.category_id', 'left');
		$this->db->where("($condition)");
		$this->db->order_by('c.categoy_name', 'asc');	
		$this->db->group_by('i.category_id');	
		$query = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
		
	}
	
	function getUserCourses($condition){
		
		$this -> db -> select('c.course_id,c.course_name');
		$this -> db -> from('tbl_student_details as i');
		$this -> db -> join('tbl_courses as c', 'i.course_id  = c.course_id', 'left');
		$this->db->where("($condition)");
		$this->db->order_by('c.course_name', 'asc');	
		$this->db->group_by('i.course_id');	
		$query = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
		
	}
	
	function getUserBatches($condition){
		
		$this -> db -> select('b.batch_id,b.batch_name');
		$this -> db -> from('tbl_student_details as i');
		$this -> db -> join('tbl_batch_master as b', 'i.batch_id  = b.batch_id', 'left');
		$this->db->where("($condition)");
		$this->db->order_by('b.batch_name', 'asc');	
		$this->db->group_by('i.batch_id');	
		$query = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
		
	}
	
	function getCamaraList($condition){
		
		$this -> db -> select('c.camera_id,c.camera_name, c.url, c.start_time, c.end_time');
		$this -> db -> from('tbl_assign_camera as i');
		$this -> db -> join('tbl_camera as c', 'i.camera_id  = c.camera_id', 'left');
		$this->db->where("($condition)");
		$this->db->order_by('c.camera_name', 'asc');	
		$this->db->group_by('i.camera_id');	
		$query = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
		
	}
	
	 //ravi code start 14-02-2018
	 
	 
	public function getvideotime()
     {

		$this -> db -> select('videotime');
		$this -> db -> from('videotime as i');
		$query = $this -> db -> get();
	         
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return 0;
		}	
	}
	  
	
	//ravi code end 14-02-2018
	
	/* arul start */
	function getUserGroups($student_id){
		$condition = " sg.student_id = ".$student_id ;
		$this -> db -> select('gm.group_master_id,gm.group_master_name');
		$this -> db -> from('tbl_student_group as sg');
		$this -> db -> join('tbl_group_master as gm', 'gm.group_master_id  = sg.group_id', 'left');
		$this->db->where("($condition)");
		$this->db->order_by('gm.group_master_name', 'ASC');	
		$this->db->group_by('gm.group_master_id');
		$query = $this -> db -> get();
		if($query -> num_rows() >= 1){
			return $query->result_array();
		}else{	
			return false;
		}
	}
	/* arul end */

	//aakashi code 29-06-2019
	function getStudentRoutine($condition){
		
		$this -> db -> select('ct.categoy_name, cr.course_name, cn.center_name,r.routine_date,s.student_first_name, s.student_last_name ,r.routine_type,subr.subroutine_name,ra.routine_action,r.sleep_start_time,r.sleep_end_time,r.routine_comments');
		
		$this -> db -> from('tbl_student_daily_routine_data as r');
		
		$this -> db -> join('tbl_categories as ct', 'r.category_id  = ct.category_id', 'left');
		$this -> db -> join('tbl_courses as cr', 'r.course_id  = cr.course_id', 'left');
		$this -> db -> join('tbl_centers as cn', 'r.center_id  = cn.center_id', 'left');
		$this -> db -> join('tbl_student_master as s', 'r.student_id  = s.student_id', 'left');
		$this -> db -> join('tbl_subroutine_master as subr', 'r.subroutine_id  = subr.subroutine_id', 'left');
		$this -> db -> join('tbl_routine_actions_master as ra', 'r.routine_action_id  = ra.routine_action_id', 'left');
		
		$this->db->where("($condition)");
		
		$query = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
		
	}
	//aakashi code end
}
?>