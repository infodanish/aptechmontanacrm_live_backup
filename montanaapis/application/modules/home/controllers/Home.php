<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Home extends CI_Controller {

 function __construct()
 {
   parent::__construct();
   $this->load->model('homemodel','',TRUE);
 }
 
 function index()
 {
//   echo '<pre>';
//   print_r($_SESSION["webadmin"]);
//   exit;
   //if(!empty($_SESSION["webadmin"]))
	//{   
	
     $result = array();
          /*echo "segment 1: ".$this->uri->segment(1);
		echo "segment 2: ".$this->uri->segment(2);
		echo "segment 3: ".$this->uri->segment(3);
		echo "segment 4: ".$this->uri->segment(4);
		exit;*/
		$newsletter_id = $this->uri->segment(3);
		$condition = "newsletter_id='".$newsletter_id."' ";
		$getDetails = $this->homemodel->getdata("tbl_newsletters", $condition);
		//echo "<pre>".$getDetails[0]['newsletter_description'];
		//print_r($getDetails);
		//exit;
		$result['newsletter_description'] = (!empty($getDetails[0]['newsletter_description'])) ? $getDetails[0]['newsletter_description'] : '';
		//$this->load->view('template/header.php');
		$this->load->view('home/index',$result);
		//$this->load->view('template/footer.php');
	//}
	/*else
	{
		//If no session, redirect to login page
		redirect('login', 'refresh');
	}*/
 }
 
 
 
function addEdit($id=NULL)
 {
   if($this->session->userdata('logged_in'))
   {
     $session_data = $this->session->userdata('logged_in');
	 //$comp_details = comp_details();
	 
	 //$data['name'] = $session_data['name'];
	 
     //echo $id;
     $resulte['roles'] = $this->customermodel->getDropdown("roles","RoleId,Role");     
     $resulte['suppliers'] = $this->customermodel->getDropdown("vendor_master","VendorId,Name");
     
     $resulte['users'] = $this->customermodel->getUsersdata($id);
     $resulte['warehouse'] = $this->customermodel->getUserswarehouse($id);
     
     $sel_roles = $this->customermodel->getDropdownSelval("test_user_roles","UserId","RoleId",$id); 
     $sel_suppliers = $this->customermodel->getDropdownSelval("test_customer_supplier","UserId","supplier_id",$id);
     //echo "<pre>";
     //print_r($resulte['sel_suppliers']);
     
     $roles_ids = array();
     $supplier_ids = array();
	 
     if(!empty($sel_roles)){
	     foreach ($sel_roles as $key => $value) {
		    $roles_ids[] = $value->RoleId;
		 }
		 $resulte['selusers'] = $roles_ids;	
     }
   	 
	 if(!empty($sel_suppliers)){
		 foreach ($sel_suppliers as $key => $value) {
		    $supplier_ids[] = $value->supplier_id;
		 }
		 $resulte['selsuppliers'] = $supplier_ids;	
	 }
     
	 
	 
	 
	 //print_r($supplier_ids);
     
     
     
     
     //exit;
     
     //print_r($resulte);
     //print_r($resulte['warehouse']);
     //print_r($resulte['suppliers']);
     
     
	 $this->load->view('template/header.php');
     $this->load->view('customer_addEdit_view',$resulte);
     $this->load->view('template/footer.php');
   }
   else
   {
     //If no session, redirect to login page
     redirect('auth/login', 'refresh');
   }
 }
 
 
 
 
//For file upload

function uploadimage()
 {
	 
	/*echo "in condition";
	exit;*/ 
 	$path = "images/customer_logo/";
	 
	 $valid_formats = array("jpg", "JPG", "JPEG", "jpeg", "png", "PNG", "gif", "GIF", "bmp", "BMP");
	 
	 $name = $_FILES['customer_logo']['name'];
			$size = $_FILES['customer_logo']['size'];
			
			if(strlen($name))
				{
					list($txt, $ext) = explode(".", $name);
					if(in_array($ext,$valid_formats))
					{
					if($size<(1024*1024))
						{
							//$actual_image_name = time().substr(str_replace(" ", "_", $txt), 5).".".$ext;
							$rndname = uniqid();
							$actual_image_name = time().$rndname.".".$ext;
							$tmp = $_FILES['customer_logo']['tmp_name'];
							if(move_uploaded_file($tmp, $path.$actual_image_name))
								{
									$imgpath = base_url()."images/customer_logo/".$actual_image_name;
									return $actual_image_name;
								
									
								}
							else
								echo '<div class="alert alert-danger fade in"><strong>File Upload failed!!!</strong></div>';
						}
						else
						echo '<div class="alert alert-danger fade in"><strong>Image file size max 1 MB!!!</strong></div>';					
						}
						else
						echo '<div class="alert alert-danger fade in"><strong>Invalid file format..</strong></div>';	
				}
				
			else
				echo '<div class="alert alert-warning fade in"><strong>Please select a file to upload..!</strong></div>';
	 
 }
	
//End file upload	
	
	
	
	

 function logout()
 {
   //$this->session->unset_userdata('logged_in');
   /*$session_id = $_SESSION["webadmin"][0]->session_id;
   $session_data = array(
                          'last_active' => date('Y-m-d H:i:s')
   );*/
   session_destroy();
   redirect('login', 'refresh');
 }

}

?>
