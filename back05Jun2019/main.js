(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./node_modules/moment/locale sync recursive ^\\.\\/.*$":
/*!**************************************************!*\
  !*** ./node_modules/moment/locale sync ^\.\/.*$ ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "./node_modules/moment/locale/af.js",
	"./af.js": "./node_modules/moment/locale/af.js",
	"./ar": "./node_modules/moment/locale/ar.js",
	"./ar-dz": "./node_modules/moment/locale/ar-dz.js",
	"./ar-dz.js": "./node_modules/moment/locale/ar-dz.js",
	"./ar-kw": "./node_modules/moment/locale/ar-kw.js",
	"./ar-kw.js": "./node_modules/moment/locale/ar-kw.js",
	"./ar-ly": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ly.js": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ma": "./node_modules/moment/locale/ar-ma.js",
	"./ar-ma.js": "./node_modules/moment/locale/ar-ma.js",
	"./ar-sa": "./node_modules/moment/locale/ar-sa.js",
	"./ar-sa.js": "./node_modules/moment/locale/ar-sa.js",
	"./ar-tn": "./node_modules/moment/locale/ar-tn.js",
	"./ar-tn.js": "./node_modules/moment/locale/ar-tn.js",
	"./ar.js": "./node_modules/moment/locale/ar.js",
	"./az": "./node_modules/moment/locale/az.js",
	"./az.js": "./node_modules/moment/locale/az.js",
	"./be": "./node_modules/moment/locale/be.js",
	"./be.js": "./node_modules/moment/locale/be.js",
	"./bg": "./node_modules/moment/locale/bg.js",
	"./bg.js": "./node_modules/moment/locale/bg.js",
	"./bm": "./node_modules/moment/locale/bm.js",
	"./bm.js": "./node_modules/moment/locale/bm.js",
	"./bn": "./node_modules/moment/locale/bn.js",
	"./bn.js": "./node_modules/moment/locale/bn.js",
	"./bo": "./node_modules/moment/locale/bo.js",
	"./bo.js": "./node_modules/moment/locale/bo.js",
	"./br": "./node_modules/moment/locale/br.js",
	"./br.js": "./node_modules/moment/locale/br.js",
	"./bs": "./node_modules/moment/locale/bs.js",
	"./bs.js": "./node_modules/moment/locale/bs.js",
	"./ca": "./node_modules/moment/locale/ca.js",
	"./ca.js": "./node_modules/moment/locale/ca.js",
	"./cs": "./node_modules/moment/locale/cs.js",
	"./cs.js": "./node_modules/moment/locale/cs.js",
	"./cv": "./node_modules/moment/locale/cv.js",
	"./cv.js": "./node_modules/moment/locale/cv.js",
	"./cy": "./node_modules/moment/locale/cy.js",
	"./cy.js": "./node_modules/moment/locale/cy.js",
	"./da": "./node_modules/moment/locale/da.js",
	"./da.js": "./node_modules/moment/locale/da.js",
	"./de": "./node_modules/moment/locale/de.js",
	"./de-at": "./node_modules/moment/locale/de-at.js",
	"./de-at.js": "./node_modules/moment/locale/de-at.js",
	"./de-ch": "./node_modules/moment/locale/de-ch.js",
	"./de-ch.js": "./node_modules/moment/locale/de-ch.js",
	"./de.js": "./node_modules/moment/locale/de.js",
	"./dv": "./node_modules/moment/locale/dv.js",
	"./dv.js": "./node_modules/moment/locale/dv.js",
	"./el": "./node_modules/moment/locale/el.js",
	"./el.js": "./node_modules/moment/locale/el.js",
	"./en-SG": "./node_modules/moment/locale/en-SG.js",
	"./en-SG.js": "./node_modules/moment/locale/en-SG.js",
	"./en-au": "./node_modules/moment/locale/en-au.js",
	"./en-au.js": "./node_modules/moment/locale/en-au.js",
	"./en-ca": "./node_modules/moment/locale/en-ca.js",
	"./en-ca.js": "./node_modules/moment/locale/en-ca.js",
	"./en-gb": "./node_modules/moment/locale/en-gb.js",
	"./en-gb.js": "./node_modules/moment/locale/en-gb.js",
	"./en-ie": "./node_modules/moment/locale/en-ie.js",
	"./en-ie.js": "./node_modules/moment/locale/en-ie.js",
	"./en-il": "./node_modules/moment/locale/en-il.js",
	"./en-il.js": "./node_modules/moment/locale/en-il.js",
	"./en-nz": "./node_modules/moment/locale/en-nz.js",
	"./en-nz.js": "./node_modules/moment/locale/en-nz.js",
	"./eo": "./node_modules/moment/locale/eo.js",
	"./eo.js": "./node_modules/moment/locale/eo.js",
	"./es": "./node_modules/moment/locale/es.js",
	"./es-do": "./node_modules/moment/locale/es-do.js",
	"./es-do.js": "./node_modules/moment/locale/es-do.js",
	"./es-us": "./node_modules/moment/locale/es-us.js",
	"./es-us.js": "./node_modules/moment/locale/es-us.js",
	"./es.js": "./node_modules/moment/locale/es.js",
	"./et": "./node_modules/moment/locale/et.js",
	"./et.js": "./node_modules/moment/locale/et.js",
	"./eu": "./node_modules/moment/locale/eu.js",
	"./eu.js": "./node_modules/moment/locale/eu.js",
	"./fa": "./node_modules/moment/locale/fa.js",
	"./fa.js": "./node_modules/moment/locale/fa.js",
	"./fi": "./node_modules/moment/locale/fi.js",
	"./fi.js": "./node_modules/moment/locale/fi.js",
	"./fo": "./node_modules/moment/locale/fo.js",
	"./fo.js": "./node_modules/moment/locale/fo.js",
	"./fr": "./node_modules/moment/locale/fr.js",
	"./fr-ca": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ca.js": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ch": "./node_modules/moment/locale/fr-ch.js",
	"./fr-ch.js": "./node_modules/moment/locale/fr-ch.js",
	"./fr.js": "./node_modules/moment/locale/fr.js",
	"./fy": "./node_modules/moment/locale/fy.js",
	"./fy.js": "./node_modules/moment/locale/fy.js",
	"./ga": "./node_modules/moment/locale/ga.js",
	"./ga.js": "./node_modules/moment/locale/ga.js",
	"./gd": "./node_modules/moment/locale/gd.js",
	"./gd.js": "./node_modules/moment/locale/gd.js",
	"./gl": "./node_modules/moment/locale/gl.js",
	"./gl.js": "./node_modules/moment/locale/gl.js",
	"./gom-latn": "./node_modules/moment/locale/gom-latn.js",
	"./gom-latn.js": "./node_modules/moment/locale/gom-latn.js",
	"./gu": "./node_modules/moment/locale/gu.js",
	"./gu.js": "./node_modules/moment/locale/gu.js",
	"./he": "./node_modules/moment/locale/he.js",
	"./he.js": "./node_modules/moment/locale/he.js",
	"./hi": "./node_modules/moment/locale/hi.js",
	"./hi.js": "./node_modules/moment/locale/hi.js",
	"./hr": "./node_modules/moment/locale/hr.js",
	"./hr.js": "./node_modules/moment/locale/hr.js",
	"./hu": "./node_modules/moment/locale/hu.js",
	"./hu.js": "./node_modules/moment/locale/hu.js",
	"./hy-am": "./node_modules/moment/locale/hy-am.js",
	"./hy-am.js": "./node_modules/moment/locale/hy-am.js",
	"./id": "./node_modules/moment/locale/id.js",
	"./id.js": "./node_modules/moment/locale/id.js",
	"./is": "./node_modules/moment/locale/is.js",
	"./is.js": "./node_modules/moment/locale/is.js",
	"./it": "./node_modules/moment/locale/it.js",
	"./it-ch": "./node_modules/moment/locale/it-ch.js",
	"./it-ch.js": "./node_modules/moment/locale/it-ch.js",
	"./it.js": "./node_modules/moment/locale/it.js",
	"./ja": "./node_modules/moment/locale/ja.js",
	"./ja.js": "./node_modules/moment/locale/ja.js",
	"./jv": "./node_modules/moment/locale/jv.js",
	"./jv.js": "./node_modules/moment/locale/jv.js",
	"./ka": "./node_modules/moment/locale/ka.js",
	"./ka.js": "./node_modules/moment/locale/ka.js",
	"./kk": "./node_modules/moment/locale/kk.js",
	"./kk.js": "./node_modules/moment/locale/kk.js",
	"./km": "./node_modules/moment/locale/km.js",
	"./km.js": "./node_modules/moment/locale/km.js",
	"./kn": "./node_modules/moment/locale/kn.js",
	"./kn.js": "./node_modules/moment/locale/kn.js",
	"./ko": "./node_modules/moment/locale/ko.js",
	"./ko.js": "./node_modules/moment/locale/ko.js",
	"./ku": "./node_modules/moment/locale/ku.js",
	"./ku.js": "./node_modules/moment/locale/ku.js",
	"./ky": "./node_modules/moment/locale/ky.js",
	"./ky.js": "./node_modules/moment/locale/ky.js",
	"./lb": "./node_modules/moment/locale/lb.js",
	"./lb.js": "./node_modules/moment/locale/lb.js",
	"./lo": "./node_modules/moment/locale/lo.js",
	"./lo.js": "./node_modules/moment/locale/lo.js",
	"./lt": "./node_modules/moment/locale/lt.js",
	"./lt.js": "./node_modules/moment/locale/lt.js",
	"./lv": "./node_modules/moment/locale/lv.js",
	"./lv.js": "./node_modules/moment/locale/lv.js",
	"./me": "./node_modules/moment/locale/me.js",
	"./me.js": "./node_modules/moment/locale/me.js",
	"./mi": "./node_modules/moment/locale/mi.js",
	"./mi.js": "./node_modules/moment/locale/mi.js",
	"./mk": "./node_modules/moment/locale/mk.js",
	"./mk.js": "./node_modules/moment/locale/mk.js",
	"./ml": "./node_modules/moment/locale/ml.js",
	"./ml.js": "./node_modules/moment/locale/ml.js",
	"./mn": "./node_modules/moment/locale/mn.js",
	"./mn.js": "./node_modules/moment/locale/mn.js",
	"./mr": "./node_modules/moment/locale/mr.js",
	"./mr.js": "./node_modules/moment/locale/mr.js",
	"./ms": "./node_modules/moment/locale/ms.js",
	"./ms-my": "./node_modules/moment/locale/ms-my.js",
	"./ms-my.js": "./node_modules/moment/locale/ms-my.js",
	"./ms.js": "./node_modules/moment/locale/ms.js",
	"./mt": "./node_modules/moment/locale/mt.js",
	"./mt.js": "./node_modules/moment/locale/mt.js",
	"./my": "./node_modules/moment/locale/my.js",
	"./my.js": "./node_modules/moment/locale/my.js",
	"./nb": "./node_modules/moment/locale/nb.js",
	"./nb.js": "./node_modules/moment/locale/nb.js",
	"./ne": "./node_modules/moment/locale/ne.js",
	"./ne.js": "./node_modules/moment/locale/ne.js",
	"./nl": "./node_modules/moment/locale/nl.js",
	"./nl-be": "./node_modules/moment/locale/nl-be.js",
	"./nl-be.js": "./node_modules/moment/locale/nl-be.js",
	"./nl.js": "./node_modules/moment/locale/nl.js",
	"./nn": "./node_modules/moment/locale/nn.js",
	"./nn.js": "./node_modules/moment/locale/nn.js",
	"./pa-in": "./node_modules/moment/locale/pa-in.js",
	"./pa-in.js": "./node_modules/moment/locale/pa-in.js",
	"./pl": "./node_modules/moment/locale/pl.js",
	"./pl.js": "./node_modules/moment/locale/pl.js",
	"./pt": "./node_modules/moment/locale/pt.js",
	"./pt-br": "./node_modules/moment/locale/pt-br.js",
	"./pt-br.js": "./node_modules/moment/locale/pt-br.js",
	"./pt.js": "./node_modules/moment/locale/pt.js",
	"./ro": "./node_modules/moment/locale/ro.js",
	"./ro.js": "./node_modules/moment/locale/ro.js",
	"./ru": "./node_modules/moment/locale/ru.js",
	"./ru.js": "./node_modules/moment/locale/ru.js",
	"./sd": "./node_modules/moment/locale/sd.js",
	"./sd.js": "./node_modules/moment/locale/sd.js",
	"./se": "./node_modules/moment/locale/se.js",
	"./se.js": "./node_modules/moment/locale/se.js",
	"./si": "./node_modules/moment/locale/si.js",
	"./si.js": "./node_modules/moment/locale/si.js",
	"./sk": "./node_modules/moment/locale/sk.js",
	"./sk.js": "./node_modules/moment/locale/sk.js",
	"./sl": "./node_modules/moment/locale/sl.js",
	"./sl.js": "./node_modules/moment/locale/sl.js",
	"./sq": "./node_modules/moment/locale/sq.js",
	"./sq.js": "./node_modules/moment/locale/sq.js",
	"./sr": "./node_modules/moment/locale/sr.js",
	"./sr-cyrl": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr-cyrl.js": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr.js": "./node_modules/moment/locale/sr.js",
	"./ss": "./node_modules/moment/locale/ss.js",
	"./ss.js": "./node_modules/moment/locale/ss.js",
	"./sv": "./node_modules/moment/locale/sv.js",
	"./sv.js": "./node_modules/moment/locale/sv.js",
	"./sw": "./node_modules/moment/locale/sw.js",
	"./sw.js": "./node_modules/moment/locale/sw.js",
	"./ta": "./node_modules/moment/locale/ta.js",
	"./ta.js": "./node_modules/moment/locale/ta.js",
	"./te": "./node_modules/moment/locale/te.js",
	"./te.js": "./node_modules/moment/locale/te.js",
	"./tet": "./node_modules/moment/locale/tet.js",
	"./tet.js": "./node_modules/moment/locale/tet.js",
	"./tg": "./node_modules/moment/locale/tg.js",
	"./tg.js": "./node_modules/moment/locale/tg.js",
	"./th": "./node_modules/moment/locale/th.js",
	"./th.js": "./node_modules/moment/locale/th.js",
	"./tl-ph": "./node_modules/moment/locale/tl-ph.js",
	"./tl-ph.js": "./node_modules/moment/locale/tl-ph.js",
	"./tlh": "./node_modules/moment/locale/tlh.js",
	"./tlh.js": "./node_modules/moment/locale/tlh.js",
	"./tr": "./node_modules/moment/locale/tr.js",
	"./tr.js": "./node_modules/moment/locale/tr.js",
	"./tzl": "./node_modules/moment/locale/tzl.js",
	"./tzl.js": "./node_modules/moment/locale/tzl.js",
	"./tzm": "./node_modules/moment/locale/tzm.js",
	"./tzm-latn": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm-latn.js": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm.js": "./node_modules/moment/locale/tzm.js",
	"./ug-cn": "./node_modules/moment/locale/ug-cn.js",
	"./ug-cn.js": "./node_modules/moment/locale/ug-cn.js",
	"./uk": "./node_modules/moment/locale/uk.js",
	"./uk.js": "./node_modules/moment/locale/uk.js",
	"./ur": "./node_modules/moment/locale/ur.js",
	"./ur.js": "./node_modules/moment/locale/ur.js",
	"./uz": "./node_modules/moment/locale/uz.js",
	"./uz-latn": "./node_modules/moment/locale/uz-latn.js",
	"./uz-latn.js": "./node_modules/moment/locale/uz-latn.js",
	"./uz.js": "./node_modules/moment/locale/uz.js",
	"./vi": "./node_modules/moment/locale/vi.js",
	"./vi.js": "./node_modules/moment/locale/vi.js",
	"./x-pseudo": "./node_modules/moment/locale/x-pseudo.js",
	"./x-pseudo.js": "./node_modules/moment/locale/x-pseudo.js",
	"./yo": "./node_modules/moment/locale/yo.js",
	"./yo.js": "./node_modules/moment/locale/yo.js",
	"./zh-cn": "./node_modules/moment/locale/zh-cn.js",
	"./zh-cn.js": "./node_modules/moment/locale/zh-cn.js",
	"./zh-hk": "./node_modules/moment/locale/zh-hk.js",
	"./zh-hk.js": "./node_modules/moment/locale/zh-hk.js",
	"./zh-tw": "./node_modules/moment/locale/zh-tw.js",
	"./zh-tw.js": "./node_modules/moment/locale/zh-tw.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) { // check for number or string
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return id;
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./node_modules/moment/locale sync recursive ^\\.\\/.*$";

/***/ }),

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");



let AppComponent = class AppComponent {
    constructor(router) {
        this.router = router;
        this.title = 'aptechmontanacrm-angular';
        this.showHead = false;
        router.events.forEach((event) => {
            if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_1__["NavigationStart"]) {
                if (event['url'] == '/login') {
                    this.showHead = false;
                }
                else {
                    this.showHead = true;
                }
            }
        });
    }
    ngOnInit() {
    }
};
AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
        selector: 'app-root',
        template: __webpack_require__(/*! ./components/common/layout/layout.component.html */ "./src/app/components/common/layout/layout.component.html"),
        styles: [__webpack_require__(/*! ./components/common/layout/layout.component.css */ "./src/app/components/common/layout/layout.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
], AppComponent);



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _auth_auth_guard__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./auth/auth.guard */ "./src/app/auth/auth.guard.ts");
/* harmony import */ var ng2_pdf_viewer__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ng2-pdf-viewer */ "./node_modules/ng2-pdf-viewer/ng2-pdf-viewer.js");
/* harmony import */ var _routing_app_routing_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./routing/app-routing.module */ "./src/app/routing/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _components_common_header_header_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./components/common/header/header.component */ "./src/app/components/common/header/header.component.ts");
/* harmony import */ var _components_common_footer_footer_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./components/common/footer/footer.component */ "./src/app/components/common/footer/footer.component.ts");
/* harmony import */ var _components_login_login_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./components/login/login.component */ "./src/app/components/login/login.component.ts");
/* harmony import */ var _components_common_layout_layout_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./components/common/layout/layout.component */ "./src/app/components/common/layout/layout.component.ts");
/* harmony import */ var _components_dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./components/dashboard/dashboard.component */ "./src/app/components/dashboard/dashboard.component.ts");
/* harmony import */ var _components_common_side_nav_side_nav_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./components/common/side-nav/side-nav.component */ "./src/app/components/common/side-nav/side-nav.component.ts");
/* harmony import */ var _components_common_top_nav_top_nav_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./components/common/top-nav/top-nav.component */ "./src/app/components/common/top-nav/top-nav.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _components_constant_constant_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./components/constant/constant.component */ "./src/app/components/constant/constant.component.ts");
/* harmony import */ var _components_enquiry_enquirylist_enquirylist_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./components/enquiry/enquirylist/enquirylist.component */ "./src/app/components/enquiry/enquirylist/enquirylist.component.ts");
/* harmony import */ var _components_enquiry_addeditenquiry_addeditenquiry_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./components/enquiry/addeditenquiry/addeditenquiry.component */ "./src/app/components/enquiry/addeditenquiry/addeditenquiry.component.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");
/* harmony import */ var ngx_pagination__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ngx-pagination */ "./node_modules/ngx-pagination/dist/ngx-pagination.js");
/* harmony import */ var ngx_mydatepicker__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ngx-mydatepicker */ "./node_modules/ngx-mydatepicker/index.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm2015/animations.js");
/* harmony import */ var _components_admission_admissiontab1_admissiontab1_component__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./components/admission/admissiontab1/admissiontab1.component */ "./src/app/components/admission/admissiontab1/admissiontab1.component.ts");
/* harmony import */ var _components_admission_admissionmaster_admissionmaster_component__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./components/admission/admissionmaster/admissionmaster.component */ "./src/app/components/admission/admissionmaster/admissionmaster.component.ts");
/* harmony import */ var _angular_material_tabs__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! @angular/material/tabs */ "./node_modules/@angular/material/esm2015/tabs.js");
/* harmony import */ var _components_album_albumlist_albumlist_component__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ./components/album/albumlist/albumlist.component */ "./src/app/components/album/albumlist/albumlist.component.ts");
/* harmony import */ var _components_album_albumimages_albumimages_component__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ./components/album/albumimages/albumimages.component */ "./src/app/components/album/albumimages/albumimages.component.ts");
/* harmony import */ var _components_timetable_timetablemaster_timetablemaster_component__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! ./components/timetable/timetablemaster/timetablemaster.component */ "./src/app/components/timetable/timetablemaster/timetablemaster.component.ts");
/* harmony import */ var _components_timetable_timetabledetails_timetabledetails_component__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! ./components/timetable/timetabledetails/timetabledetails.component */ "./src/app/components/timetable/timetabledetails/timetabledetails.component.ts");
/* harmony import */ var _components_timetable_pdfview_pdfview_component__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! ./components/timetable/pdfview/pdfview.component */ "./src/app/components/timetable/pdfview/pdfview.component.ts");
/* harmony import */ var _components_folderdocuments_folderdocument_folderdocument_component__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! ./components/folderdocuments/folderdocument/folderdocument.component */ "./src/app/components/folderdocuments/folderdocument/folderdocument.component.ts");
/* harmony import */ var _components_folderdocuments_documentdetails_documentdetails_component__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! ./components/folderdocuments/documentdetails/documentdetails.component */ "./src/app/components/folderdocuments/documentdetails/documentdetails.component.ts");
/* harmony import */ var _components_folderdocuments_pdfdocview_pdfdocview_component__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(/*! ./components/folderdocuments/pdfdocview/pdfdocview.component */ "./src/app/components/folderdocuments/pdfdocview/pdfdocview.component.ts");
/* harmony import */ var _components_remainingfees_feesdetails_feesdetails_component__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(/*! ./components/remainingfees/feesdetails/feesdetails.component */ "./src/app/components/remainingfees/feesdetails/feesdetails.component.ts");
/* harmony import */ var ngx_select2__WEBPACK_IMPORTED_MODULE_36__ = __webpack_require__(/*! ngx-select2 */ "./node_modules/ngx-select2/fesm2015/ngx-select2.js");
/* harmony import */ var _directives_select2_directive__WEBPACK_IMPORTED_MODULE_37__ = __webpack_require__(/*! ./directives/select2.directive */ "./src/app/directives/select2.directive.ts");
/* harmony import */ var _angular_material_autocomplete__WEBPACK_IMPORTED_MODULE_38__ = __webpack_require__(/*! @angular/material/autocomplete */ "./node_modules/@angular/material/esm2015/autocomplete.js");
/* harmony import */ var ng2_select2__WEBPACK_IMPORTED_MODULE_39__ = __webpack_require__(/*! ng2-select2 */ "./node_modules/ng2-select2/ng2-select2.js");
/* harmony import */ var ng2_select2__WEBPACK_IMPORTED_MODULE_39___default = /*#__PURE__*/__webpack_require__.n(ng2_select2__WEBPACK_IMPORTED_MODULE_39__);
/* harmony import */ var _components_admission_remainingfees_remainingfees_component__WEBPACK_IMPORTED_MODULE_40__ = __webpack_require__(/*! ./components/admission/remainingfees/remainingfees.component */ "./src/app/components/admission/remainingfees/remainingfees.component.ts");
/* harmony import */ var ngx_bootstrap_datepicker__WEBPACK_IMPORTED_MODULE_41__ = __webpack_require__(/*! ngx-bootstrap/datepicker */ "./node_modules/ngx-bootstrap/datepicker/fesm2015/ngx-bootstrap-datepicker.js");
/* harmony import */ var _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_42__ = __webpack_require__(/*! @angular/material/datepicker */ "./node_modules/@angular/material/esm2015/datepicker.js");
/* harmony import */ var _components_attendence_markattendence_markattendence_component__WEBPACK_IMPORTED_MODULE_43__ = __webpack_require__(/*! ./components/attendence/markattendence/markattendence.component */ "./src/app/components/attendence/markattendence/markattendence.component.ts");
/* harmony import */ var ng2_datepicker__WEBPACK_IMPORTED_MODULE_44__ = __webpack_require__(/*! ng2-datepicker */ "./node_modules/ng2-datepicker/dist/bundles/ng2-datepicker.umd.js");
/* harmony import */ var ng2_datepicker__WEBPACK_IMPORTED_MODULE_44___default = /*#__PURE__*/__webpack_require__.n(ng2_datepicker__WEBPACK_IMPORTED_MODULE_44__);














































let AppModule = class AppModule {
};
AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        declarations: [
            _app_component__WEBPACK_IMPORTED_MODULE_8__["AppComponent"],
            _components_common_header_header_component__WEBPACK_IMPORTED_MODULE_9__["HeaderComponent"],
            _components_common_footer_footer_component__WEBPACK_IMPORTED_MODULE_10__["FooterComponent"],
            _components_login_login_component__WEBPACK_IMPORTED_MODULE_11__["LoginComponent"],
            _components_common_layout_layout_component__WEBPACK_IMPORTED_MODULE_12__["LayoutComponent"],
            _components_dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_13__["DashboardComponent"],
            _components_common_side_nav_side_nav_component__WEBPACK_IMPORTED_MODULE_14__["SideNavComponent"],
            _components_common_top_nav_top_nav_component__WEBPACK_IMPORTED_MODULE_15__["TopNavComponent"],
            _components_constant_constant_component__WEBPACK_IMPORTED_MODULE_17__["ConstantComponent"],
            _components_enquiry_enquirylist_enquirylist_component__WEBPACK_IMPORTED_MODULE_18__["EnquirylistComponent"],
            _components_enquiry_addeditenquiry_addeditenquiry_component__WEBPACK_IMPORTED_MODULE_19__["AddeditenquiryComponent"],
            _components_admission_admissiontab1_admissiontab1_component__WEBPACK_IMPORTED_MODULE_24__["Admissiontab1Component"],
            _components_admission_admissionmaster_admissionmaster_component__WEBPACK_IMPORTED_MODULE_25__["AdmissionmasterComponent"],
            _components_album_albumlist_albumlist_component__WEBPACK_IMPORTED_MODULE_27__["AlbumlistComponent"],
            _components_album_albumimages_albumimages_component__WEBPACK_IMPORTED_MODULE_28__["AlbumimagesComponent"],
            _components_timetable_timetablemaster_timetablemaster_component__WEBPACK_IMPORTED_MODULE_29__["TimetablemasterComponent"],
            _components_timetable_timetabledetails_timetabledetails_component__WEBPACK_IMPORTED_MODULE_30__["TimetabledetailsComponent"],
            ng2_pdf_viewer__WEBPACK_IMPORTED_MODULE_6__["PdfViewerComponent"],
            _components_timetable_pdfview_pdfview_component__WEBPACK_IMPORTED_MODULE_31__["PdfviewComponent"],
            _components_folderdocuments_folderdocument_folderdocument_component__WEBPACK_IMPORTED_MODULE_32__["FolderdocumentComponent"],
            _components_folderdocuments_documentdetails_documentdetails_component__WEBPACK_IMPORTED_MODULE_33__["DocumentdetailsComponent"],
            _components_folderdocuments_pdfdocview_pdfdocview_component__WEBPACK_IMPORTED_MODULE_34__["PdfdocviewComponent"],
            _components_remainingfees_feesdetails_feesdetails_component__WEBPACK_IMPORTED_MODULE_35__["FeesdetailsComponent"],
            _directives_select2_directive__WEBPACK_IMPORTED_MODULE_37__["Select2Directive"],
            _components_admission_remainingfees_remainingfees_component__WEBPACK_IMPORTED_MODULE_40__["RemainingfeesComponent"],
            _components_attendence_markattendence_markattendence_component__WEBPACK_IMPORTED_MODULE_43__["MarkattendenceComponent"],
        ],
        imports: [
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
            _routing_app_routing_module__WEBPACK_IMPORTED_MODULE_7__["AppRoutingModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_16__["FormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_16__["ReactiveFormsModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClientModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_20__["MatSnackBarModule"],
            ngx_pagination__WEBPACK_IMPORTED_MODULE_21__["NgxPaginationModule"],
            _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_23__["BrowserAnimationsModule"],
            ngx_mydatepicker__WEBPACK_IMPORTED_MODULE_22__["NgxMyDatePickerModule"].forRoot(),
            _angular_material_tabs__WEBPACK_IMPORTED_MODULE_26__["MatTabsModule"],
            ngx_select2__WEBPACK_IMPORTED_MODULE_36__["LSelect2Module"],
            _angular_material_autocomplete__WEBPACK_IMPORTED_MODULE_38__["MatAutocompleteModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_20__["MatInputModule"],
            ng2_select2__WEBPACK_IMPORTED_MODULE_39__["Select2Module"],
            ngx_bootstrap_datepicker__WEBPACK_IMPORTED_MODULE_41__["BsDatepickerModule"].forRoot(),
            _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_42__["MatDatepickerModule"],
            ng2_datepicker__WEBPACK_IMPORTED_MODULE_44__["NgDatepickerModule"]
        ],
        providers: [_services_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"], _auth_auth_guard__WEBPACK_IMPORTED_MODULE_5__["AuthGuard"]],
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_8__["AppComponent"]]
    })
], AppModule);



/***/ }),

/***/ "./src/app/auth/auth.guard.ts":
/*!************************************!*\
  !*** ./src/app/auth/auth.guard.ts ***!
  \************************************/
/*! exports provided: AuthGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthGuard", function() { return AuthGuard; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");




let AuthGuard = class AuthGuard {
    constructor(auth, myRoute) {
        this.auth = auth;
        this.myRoute = myRoute;
    }
    canActivate(next, state) {
        if (this.auth.isLoggedIn()) {
            return true;
        }
        else {
            console.log(2);
            this.myRoute.navigate(["login"]);
            return false;
        }
    }
};
AuthGuard = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"],
        _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
], AuthGuard);



/***/ }),

/***/ "./src/app/components/admission/admissionmaster/admissionmaster.component.css":
/*!************************************************************************************!*\
  !*** ./src/app/components/admission/admissionmaster/admissionmaster.component.css ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvYWRtaXNzaW9uL2FkbWlzc2lvbm1hc3Rlci9hZG1pc3Npb25tYXN0ZXIuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/components/admission/admissionmaster/admissionmaster.component.html":
/*!*************************************************************************************!*\
  !*** ./src/app/components/admission/admissionmaster/admissionmaster.component.html ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"contents\">\n\t<div class=\"loader\" *ngIf=\"isLoading\">\n\t\t<img src=\"assets/images/loader.gif\">\n\t</div>\n\t<div class=\"container-fluid\">\n\t\t<section class=\"content\">\n\t\t<div id=\"main-form-content\">\n\t\t\t<form name=\"form\" (ngSubmit)=\"onSubmit()\" #f=\"ngForm\" class=\"form-inline\"> \n\t\t\t<div class=\"col-sm-4 col-lg-2\">\n\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t<input [(ngModel)]=\"admissionFilter.enrollmentno\" placeholder=\"enrollment no\" name=\"enrollmentno\"   \n\t\t\t\t\t#enrollmentno=\"ngModel\" class=\"form-control w100\">\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"col-sm-12 col-lg-2\">\n\t\t\t\t<button class=\"btn btn-info\">Search</button>\n\t\t\t\t<button (click)=\"clearfilter()\" class=\"btn btn-info\">Clear</button>\n\t\t\t</div>\n\t\t\t</form>\n\t\t</div>\n\t\t<div class=\"clearfix\"></div>\n\t\t<div class=\"col-md-12 load_table\">\n\t\t\t<div  class=\"qtraq-subtitle-wrapper mt20\">\n\t\t\t\t<h1> Admission Details</h1>\n\t\t\t\t<button class=\"btn btn-danger\" routerLink=\"addeditadmission\"> Add Admission</button>\n\t\t\t</div>\n\t\t\t<table class=\"table table-striped\">\n\t\t\t\t<thead>\n\t\t\t\t\t<tr>\n\t\t\t\t\t\t<th class=\"hidden\">Id</th>\n\t\t\t\t\t\t<th>Student Name</th>\n\t\t\t\t\t\t<th>Course Name</th>\n\t\t\t\t\t\t<th>Enrollment No</th>\n\t\t\t\t\t\t<th>Action</th>\n\t\t\t\t\t</tr>\n\t\t\t\t</thead>\n\t\t\t\t<tbody>\n\t\t\t\t\t<tr *ngFor=\"let admission of admissionList| paginate: { itemsPerPage: this.size, \n\t  currentPage: this.page, totalItems: this.totalRecords }\">\n\t\t\t\t\t\t<td class=\"hidden\">1</td>\n\t\t\t\t\t\t<td>{{admission.student_first_name + \" \" +admission.student_last_name}}</td>\n\t\t\t\t\t\t<td>{{admission.course_name}}</td>\n\t\t\t\t\t\t<td>{{admission.enrollment_no}}</td>\n\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t<button class=\"btn btn-danger\" routerLink=\"addeditadmission/{{admission.student_id}}\"> Edit</button>\n\t\t\t\t\t\t</td>\n\t\t\t\t\t</tr>\n\t\t\t\t</tbody>\n\t\t\t</table>\n\t\t\t<pagination-controls  (pageChange)=\"pageChanged($event)\"></pagination-controls>\n\t\t</div>\n\t\t<div class=\"clearfix\"></div>\n\t\t</section>\n\t</div>\n</div>\n"

/***/ }),

/***/ "./src/app/components/admission/admissionmaster/admissionmaster.component.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/components/admission/admissionmaster/admissionmaster.component.ts ***!
  \***********************************************************************************/
/*! exports provided: AdmissionmasterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdmissionmasterComponent", function() { return AdmissionmasterComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");
/* harmony import */ var _services_common_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/common.service */ "./src/app/services/common.service.ts");




let AdmissionmasterComponent = class AdmissionmasterComponent {
    constructor(commonService, snackbar) {
        this.commonService = commonService;
        this.snackbar = snackbar;
        this.page = 0;
        this.admissionList = [];
        this.totalRecords = 0;
        this.size = 10;
        this.admissionFilter = {
            enrollmentno: "",
        };
        this.isLoading = true;
    }
    ngOnInit() {
        localStorage.removeItem('studentid');
        localStorage.removeItem('tabindex');
        localStorage.removeItem('enquiryNO');
        localStorage.removeItem('docsave');
        localStorage.removeItem('category');
        localStorage.removeItem('course');
        this.listAdmissions(1, this.admissionFilter);
    }
    listAdmissions(page, admissionFilter) {
        this.commonService.listAdmissions((page - 1), this.size, admissionFilter).subscribe((response) => {
            this.isLoading = false;
            if (response['status_code'] == 200) {
                this.admissionList = response['body']['query_result'];
                this.totalRecords = response['body']['totalRecords'];
                this.page = page;
            }
            else {
                this.snackbar.open(response['msg'], '', {
                    duration: 500,
                    verticalPosition: "top",
                    panelClass: ['bg-red']
                });
                this.admissionList = [];
                this.totalRecords = 0;
            }
        }, (err) => console.log("error", err), () => console.log("getCustomersPage() retrieved customers for page:", +page));
    }
    clearfilter() {
        this.admissionFilter.enrollmentno = "";
        this.listAdmissions(this.page, this.admissionFilter);
    }
    onSubmit() {
        this.listAdmissions(this.page, this.admissionFilter);
    }
    pageChanged(event) {
        console.log(1);
        this.listAdmissions(event, this.admissionFilter);
    }
};
AdmissionmasterComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-admissionmaster',
        template: __webpack_require__(/*! ./admissionmaster.component.html */ "./src/app/components/admission/admissionmaster/admissionmaster.component.html"),
        styles: [__webpack_require__(/*! ./admissionmaster.component.css */ "./src/app/components/admission/admissionmaster/admissionmaster.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_common_service__WEBPACK_IMPORTED_MODULE_3__["CommonService"],
        _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSnackBar"]])
], AdmissionmasterComponent);



/***/ }),

/***/ "./src/app/components/admission/admissiontab1/admissiontab1.component.css":
/*!********************************************************************************!*\
  !*** ./src/app/components/admission/admissiontab1/admissiontab1.component.css ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvYWRtaXNzaW9uL2FkbWlzc2lvbnRhYjEvYWRtaXNzaW9udGFiMS5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/components/admission/admissiontab1/admissiontab1.component.html":
/*!*********************************************************************************!*\
  !*** ./src/app/components/admission/admissiontab1/admissiontab1.component.html ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"contents\">\n\t<div class=\"loader\" *ngIf=\"isLoading\">\n\t\t<img src=\"assets/images/loader.gif\">\n\t</div>\n\t<div class=\"container-fluid\">\n\t  <section class=\"content\">\n\t    <div id=\"main-form-content\" class=\"load_table\">\n\t\t\t<mat-tab-group #tabGroup disableRipple (selectedTabChange)=\"admissionform1($event)\" [selectedIndex]=\"selectedindex\">\n\t\t\t\t<mat-tab onclick=\"admissionform(admission)\">\n\t\t\t\t\t<ng-template mat-tab-label>\n\t\t\t\t\t\t<span class=\"label-text\">Admission</span>\n\t\t\t\t\t</ng-template>\n\t\t\t\t    <form [formGroup]=\"AdmissionTabForm\" (ngSubmit)=\"FormOnSubmit(AdmissionTabForm.value)\" class=\" org-form all-com-form\" enctype=\"multipart/form-data\">\n\t\t\t\t        <div class=\"qtraq-subtitle-wrapper\">\n\t\t\t\t          <h1>Admission Process</h1>\n\t\t\t\t        </div>\n\t\t\t\t        \n\t\t\t\t        <div class=\"row\">\n\t\t\t\t          \t<div class=\"col-sm-6\" *ngIf= \"Enquirydisable\">\n\t\t\t\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\"> Enquiry<span class=\"text-danger\">*</span> </label><br>\n\t\t\t\t\t\t\t\t\t<select2 [cssImport]=true  [options]=\"optionsSelect\"  (valueChanged)=\"getInquiryDetails($event)\">\n\t\t\t\t\t\t\t\t\t\t<option>Select Enquiry</option>\n\t\t\t\t\t\t\t\t\t\t<option *ngFor=\"let inquiry of inquiries\"[value]=\"inquiry.inquiry_master_id\"> {{ inquiry.inquiry_no +' ('+inquiry.student_first_name +' '+ inquiry.student_last_name +')' }}</option>\n\t\t\t\t\t\t\t\t\t</select2>\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t\t        <div class=\"col-sm-6\" *ngIf= \"EnquirydisableCreate\">\n\t\t\t\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\"> Enquiry </label><br>\n\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"inquiryid\" [(ngModel)]=\"inquiryId\" [value]=\"inquiries1\" formControlName=\"inquiryid\" readonly=\"readonly\">\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\n\t\t\t\t\t        <div class=\"col-sm-6\">\n\t\t\t\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\"> Admission Date<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t\t\t\t<input ngx-mydatepicker name=\"admissiondate\" (dateChanged)=\"onadmissionDateChanged($event)\" [options]=\"myOptions\" [(ngModel)]=\"ngxadmissiondate\"\n\t\t\t\t\t\t\t        #dp1=\"ngx-mydatepicker\" formControlName=\"admissiondate\" (click)=\"dp1.toggleCalendar()\" class=\"form-control w100\" placeholder=\"Select Admission Date\" [attr.disabled]=\"admissiondateDisable ? '' : null\">\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"AdmissionFormSubmitted && \n\t\t\t            \t\t\t\tAdmissionTabForm.controls.admissiondate.errors\">\n\t\t\t                \t\t\t<span *ngIf=\"AdmissionTabForm.controls.admissiondate.errors.required\">Select Admission Date</span>\n\t\t\t              \t\t\t</div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t        </div>\n\n\t\t\t\t        <div class=\"row\">\n\t\t\t\t\t        <div class=\"col-sm-6\">\n\t\t\t\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\"> Academic Year<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t\t\t\t<select class=\"form-control\" name=\"academicyearid\" formControlName=\"academicyearid\" [(ngModel)]=\"academicyearid\">\n\t\t\t\t\t\t                <option value=\"\" selected=\"selected\">Select Academic Year</option>\n\t\t\t\t\t\t                <option [selected]=\" academicyearid == academy.academic_year_master_id\" [value]=\"academy.academic_year_master_id\"\n\t\t\t\t\t\t                  *ngFor=\"let academy of academicyears\">{{academy.academic_year_master_name}}</option>\n\t\t\t\t\t              \t</select>\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"AdmissionFormSubmitted && \n\t\t\t            \t\t\t\tAdmissionTabForm.controls.academicyearid.errors\">\n\t\t\t                \t\t\t<span *ngIf=\"AdmissionTabForm.controls.academicyearid.errors.required\">Select Academic Year</span>\n\t\t\t              \t\t\t</div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t        \t<div class=\"col-sm-6\">\n\t\t\t\t\t            <div class=\"form-group mt10\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\"> Enrollment No </label>\n\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"enrollmentno\" [(ngModel)]=\"enrollmentno\" formControlName=\"enrollmentno\" readonly=\"readonly\">\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t        </div>\n\n\t\t\t\t        <div class=\"row\">\n\t\t\t\t        \t<div class=\"col-sm-6\">\n\t\t\t\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\"> Category<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t\t\t\t<select class=\"form-control\" name=\"categoryid\" formControlName=\"categoryid\" [(ngModel)]=\"categoryId\" (change)=\"getCourses(categoryId)\">\n\t\t\t\t\t\t                <option value=\"\" selected=\"selected\">Select Category</option>\n\t\t\t\t\t\t                <option [selected]=\" categoryId == cat.category_id\" [value]=\"cat.category_id\"\n\t\t\t\t\t\t                  *ngFor=\"let cat of category\">{{cat.categoy_name}}</option>\n\t\t\t\t\t              \t</select>\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"AdmissionFormSubmitted && \n\t\t\t            \t\t\t\tAdmissionTabForm.controls.categoryid.errors\">\n\t\t\t                \t\t\t<span *ngIf=\"AdmissionTabForm.controls.categoryid.errors.required\">Select Category</span>\n\t\t\t              \t\t\t</div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t\t        <div class=\"col-sm-6\">\n\t\t\t\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\"> Course<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t\t\t\t<select class=\"form-control\" name=\"courseid\" formControlName=\"courseid\" [(ngModel)]=\"courseId\" (change)=\"getCenterUserBatches(categoryId,courseId)\">\n\t\t\t\t\t\t                <option value=\"\">Select Course</option>\n\t\t\t\t\t\t                <option [selected]=\" courseId == course.course_id\" [value]=\"course.course_id\"\n\t\t\t\t\t\t                  *ngFor=\"let course of courses\">{{course.course_name}}</option>\n\t\t\t\t\t              \t</select>\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"AdmissionFormSubmitted && \n\t\t\t            \t\t\t\tAdmissionTabForm.controls.courseid.errors\">\n\t\t\t                \t\t\t<span *ngIf=\"AdmissionTabForm.controls.courseid.errors.required\">Select Course</span>\n\t\t\t              \t\t\t</div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t        </div>\n\n\t\t\t\t        <div class=\"row\">\n\t\t\t\t        \t<div class=\"col-sm-6\">\n\t\t\t\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\"> Batch</label>\n\t\t\t\t\t\t\t\t\t<select class=\"form-control\" name=\"batchid\" formControlName=\"batchid\" [(ngModel)]=\"batchId\">\n\t\t\t\t\t\t                <option value=\"\" selected=\"selected\">Select Batch</option>\n\t\t\t\t\t\t                <option [selected]=\" batchId == batch.batch_id\" [value]=\"batch.batch_id\"\n\t\t\t\t\t\t                  *ngFor=\"let batch of batches\">{{batch.batch_name}}</option>\n\t\t\t\t\t              \t</select>\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"AdmissionFormSubmitted && \n\t\t\t            \t\t\t\tAdmissionTabForm.controls.batchid.errors\">\n\t\t\t                \t\t\t<span *ngIf=\"AdmissionTabForm.controls.batchid.errors.required\">Select Batch</span>\n\t\t\t              \t\t\t</div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t\t        <div class=\"col-sm-6\">\n\t\t\t\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\"> Program Start Date<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t\t\t\t<input ngx-mydatepicker name=\"programmestartdate\" (dateChanged)=\"onprogramStartDateChanged($event)\" [options]=\"myOptions\" [(ngModel)]=\"ngxprogramstartdate\"\n\t\t\t\t\t\t\t        #dp2=\"ngx-mydatepicker\" formControlName=\"programmestartdate\" (click)=\"dp2.toggleCalendar()\" class=\"form-control w100\" placeholder=\"Select Program Start Date\">\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"AdmissionFormSubmitted && \n\t\t\t            \t\t\t\tAdmissionTabForm.controls.programmestartdate.errors\">\n\t\t\t                \t\t\t<span *ngIf=\"AdmissionTabForm.controls.programmestartdate.errors.required\">Select Program Start Date</span>\n\t\t\t              \t\t\t</div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t        </div>\n\t\t\t\t        <div class=\"row\">\n\t\t\t\t        \t<div class=\"col-sm-6\">\n\t\t\t\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\"> Program End Date<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t\t\t\t<input ngx-mydatepicker name=\"programmeenddate\" (dateChanged)=\"onprogramEndDateChanged($event)\" [options]=\"myOptions\" [(ngModel)]=\"ngxprogramenddate\"\n\t\t\t\t\t\t\t        #dpenddate=\"ngx-mydatepicker\" formControlName=\"programmeenddate\" (click)=\"dpenddate.toggleCalendar()\" class=\"form-control w100\" placeholder=\"Select Program End Date\">\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"AdmissionFormSubmitted && \n\t\t\t            \t\t\t\tAdmissionTabForm.controls.programmeenddate.errors\">\n\t\t\t                \t\t\t<span *ngIf=\"AdmissionTabForm.controls.programmeenddate.errors.required\">Select Program End Date</span>\n\t\t\t              \t\t\t</div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t        </div>\n\t\t\t\t        <h3 style=\"background-color: red;color: white;\">CHILD INFORMATION</h3>\n\n\t\t\t\t        <div class=\"row\">\n\t\t\t\t          \t<div class=\"col-sm-6\">\n\t\t\t\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\"> First Name<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"studentfirstname\" [(ngModel)]=\"studentfirstname\" formControlName=\"studentfirstname\"\n\t\t\t\t\t\t\t\t\tplaceholder=\"Enter First Name\">\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"AdmissionFormSubmitted && \n\t\t\t            \t\t\t\tAdmissionTabForm.controls.studentfirstname.errors\">\n\t\t\t                \t\t\t<span *ngIf=\"AdmissionTabForm.controls.studentfirstname.errors.required\">Please Enter Student First Name</span>\n\t\t\t              \t\t\t</div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t\t        <div class=\"col-sm-6\">\n\t\t\t\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\"> Last Name<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"studentlastname\"[(ngModel)]=\"studentlastname\" formControlName=\"studentlastname\"\n\t\t\t\t\t\t\t\t\tplaceholder=\"Enter Last Name\">\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"AdmissionFormSubmitted && \n\t\t\t            \t\t\t\tAdmissionTabForm.controls.studentlastname.errors\">\n\t\t\t                \t\t\t<span *ngIf=\"AdmissionTabForm.controls.studentlastname.errors.required\">Please Enter Student Last Name</span>\n\t\t\t              \t\t\t</div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t        </div>\n\n\t\t\t\t        <div class=\"row\">\n\t\t\t\t          \t<div class=\"col-sm-3\">\n\t\t\t\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\"> Date Of Birth<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t\t\t\t<input ngx-mydatepicker name=\"dob\" (dateChanged)=\"onDobChanged($event)\" [options]=\"myOptions\" [(ngModel)]=\"ngxstudentdob\"\n\t\t\t\t\t\t\t        #dp3=\"ngx-mydatepicker\" formControlName=\"dob\" (click)=\"dp3.toggleCalendar()\" class=\"form-control w100\" placeholder=\"Select Birth Date\">\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"AdmissionFormSubmitted && \n\t\t\t            \t\t\t\tAdmissionTabForm.controls.dob.errors\">\n\t\t\t                \t\t\t<span *ngIf=\"AdmissionTabForm.controls.dob.errors.required\">Select Date Of Birth</span>\n\t\t\t              \t\t\t</div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t\t        <div class=\"col-sm-3\">\n\t\t\t\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\"> Age As On</label>\n\t\t\t\t\t\t\t\t\t<input type=\"text\"formControlName=\"ageon\" name=\"ageon\" [(ngModel)]=\"ageon\" class=\"form-control\" value=\"{{this.programmestartdate}}\" placeholder=\"\" disabled=\"disabled\">\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t          \t<div class=\"col-sm-3\">\n\t\t\t\t\t            <div class=\"form-group mt10\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\"> Years </label>\n\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"dobyear\" [(ngModel)]=\"dobyear\" formControlName=\"dobyear\" readonly=\"readonly\">\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t\t        <div class=\"col-sm-3\">\n\t\t\t\t\t            <div class=\"form-group mt10\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\"> Months </label>\n\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"dobmonth\" [(ngModel)]=\"dobmonth\" formControlName=\"dobmonth\" readonly=\"readonly\">\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t\t    </div>\n\n\t\t\t\t\t    <div class=\"row\">\n\t\t\t\t          \t<div class=\"col-sm-6\">\n\t\t\t\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\"> Nationality<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"nationality\" [(ngModel)]=\"nationality\" formControlName=\"nationality\"\n\t\t\t\t\t\t\t\t\tplaceholder=\"Enter Nationality\">\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"AdmissionFormSubmitted && \n\t\t\t            \t\t\t\tAdmissionTabForm.controls.nationality.errors\">\n\t\t\t                \t\t\t<span *ngIf=\"AdmissionTabForm.controls.nationality.errors.required\">Please Enter Nationality</span>\n\t\t\t              \t\t\t</div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t\t        <div class=\"col-sm-6\">\n\t\t\t\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\"> Religion<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"religion\"[(ngModel)]=\"religion\" formControlName=\"religion\"\n\t\t\t\t\t\t\t\t\tplaceholder=\"Enter Religion\">\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"AdmissionFormSubmitted && \n\t\t\t            \t\t\t\tAdmissionTabForm.controls.religion.errors\">\n\t\t\t                \t\t\t<span *ngIf=\"AdmissionTabForm.controls.religion.errors.required\">Please Enter Religion</span>\n\t\t\t              \t\t\t</div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t        </div>\n\n\t\t\t\t        <div class=\"row\">\n\t\t\t\t          \t<div class=\"col-sm-6\">\n\t\t\t\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\"> Mother Tongue<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"mothertongue\" [(ngModel)]=\"mothertongue\" formControlName=\"mothertongue\"\n\t\t\t\t\t\t\t\t\tplaceholder=\"Enter Mother Tongue\">\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"AdmissionFormSubmitted && \n\t\t\t            \t\t\t\tAdmissionTabForm.controls.mothertongue.errors\">\n\t\t\t                \t\t\t<span *ngIf=\"AdmissionTabForm.controls.mothertongue.errors.required\">Please Enter Mother Tongue</span>\n\t\t\t              \t\t\t</div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t\t        <div class=\"col-sm-6\">\n\t\t\t\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\"> Other Language(s) </label>\n\t\t\t\t\t\t\t\t\t<textarea class=\"form-control textarea-50\" name=\"otherlanguages\" [(ngModel)]=\"otherlanguages\" formControlName=\"otherlanguages\"\n\t\t\t\t\t\t\t\t\tplaceholder=\"Enter Other Languages\"></textarea>\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t        </div>\n\n\t\t\t\t        <div class=\"row\">\n\t\t\t\t         \t<div class=\"col-sm-6\">\n\t\t\t\t\t            <div class=\"form-group mt10\">\n\t\t\t\t         \t\t\t<label>Has Child attended preschool before?</label>\n\t\t\t\t         \t\t\t<br>\n\t\t\t\t\t\t\t\t\t<input type=\"radio\" name=\"attendedpreschoolbefore\" [value]=\"true\" [(ngModel)]=\"attendedpreschoolbefore\" formControlName=\"attendedpreschoolbefore\"[checked]=\"attendedpreschoolbefore\">Yes\n\t\t\t\t\t\t\t\t\t<input type=\"radio\" name=\"attendedpreschoolbefore\" [value]=\"false\" [(ngModel)]=\"attendedpreschoolbefore\" formControlName=\"attendedpreschoolbefore\" [checked]=\"!attendedpreschoolbefore\">No\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t          \t<div class=\"col-sm-6\" *ngIf=\"attendedpreschoolbefore\">\n\t\t\t\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\"> Preschool Name<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"preschoolname\" [(ngModel)]=\"preschoolname\" formControlName=\"preschoolname\"\n\t\t\t\t\t\t\t\t\tplaceholder=\"Enter Preschool Name\">\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"AdmissionFormSubmitted && \n\t\t\t            \t\t\t\tAdmissionTabForm.controls.preschoolname.errors\">\n\t\t\t                \t\t\t<span *ngIf=\"AdmissionTabForm.controls.preschoolname.errors.required\">Please Enter Preschool Name</span>\n\t\t\t              \t\t\t</div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t        </div>\n\n\t\t\t            <div class=\"form-group\">\n\t\t\t\t\t\t\t<button type=\"submit\" class=\"btn btn-primary \" style=\"margin-right: 10px\">Save</button>\n\t\t\t\t\t\t\t<button type=\"button\" routerLink=\"/admissionlist\" class=\"btn btn-primary \">Cancel</button>\n\t\t\t            </div>\n\t\t\t\t      </form>\n\t\t\t\t</mat-tab>\n\n\t\t\t\t<mat-tab disabled=\"{{admissionForm1disable}}\">\n\t\t\t\t\t<ng-template mat-tab-label>\n\t\t\t\t\t\t<span class=\"label-text\">Family Information</span>\n\t\t\t\t\t</ng-template>\n\t\t\t\t    <form [formGroup]=\"AdmissionTab1Form\" (ngSubmit)=\"Form1OnSubmit(AdmissionTab1Form.value)\" class=\" org-form all-com-form\"\n\t\t\t\t        enctype=\"multipart/form-data\">\n\t\t\t\t        <div class=\"qtraq-subtitle-wrapper\">\n\t\t\t\t          <h1>Admission Process</h1>\n\t\t\t\t        </div>\n\t\t\t\t        \n\t\t\t\t        <div class=\"row\">\n\t\t\t\t          \t<div class=\"col-sm-6\">\n\t\t\t\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\"> Father's Name </label>\n\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"fathername\" [(ngModel)]=\"fathername\" formControlName=\"fathername\"\n\t\t\t\t\t\t\t\t\tplaceholder=\"Enter Father Name\">\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"AdmissionForm1Submitted && \n\t\t\t            \t\t\t\tAdmissionTab1Form.controls.fathername.errors\">\n\t\t\t                \t\t\t<span *ngIf=\"AdmissionTab1Form.controls.fathername.errors.required\">Please Enter Father Name</span>\n\t\t\t              \t\t\t</div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t\t        <div class=\"col-sm-6\">\n\t\t\t\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\"> Mother's Name<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"mothername\" [(ngModel)]=\"mothername\" formControlName=\"mothername\"\n\t\t\t\t\t\t\t\t\tplaceholder=\"Enter Mother Name\">\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"AdmissionForm1Submitted && \n\t\t\t            \t\t\t\tAdmissionTab1Form.controls.mothername.errors\">\n\t\t\t                \t\t\t<span *ngIf=\"AdmissionTab1Form.controls.mothername.errors.required\">Please Enter Student Mother Name</span>\n\t\t\t              \t\t\t</div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t        </div>\n\n\t\t\t\t        <div class=\"row\">\n\t\t\t\t\t        <div class=\"col-sm-6\">\n\t\t\t\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\"> Father's Profession<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"fatherprof\" [(ngModel)]=\"fatherprof\" formControlName=\"fatherprof\" placeholder=\"Enter Father Profession\">\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"AdmissionForm1Submitted && \n\t\t\t            \t\t\t\tAdmissionTab1Form.controls.fatherprof.errors\">\n\t\t\t                \t\t\t<span *ngIf=\"AdmissionTab1Form.controls.fatherprof.errors.required\">Please Enter  Father Profession</span>\n\t\t\t              \t\t\t</div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t\t        <div class=\"col-sm-6\">\n\t\t\t\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\"> Mother Profession<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"motherprof\" [(ngModel)]=\"motherprof\" formControlName=\"motherprof\" placeholder=\"Enter Mother Profession\">\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"AdmissionForm1Submitted && \n\t\t\t            \t\t\t\tAdmissionTab1Form.controls.motherprof.errors\">\n\t\t\t                \t\t\t<span *ngIf=\"AdmissionTab1Form.controls.motherprof.errors.required\">Please Enter  Mother Profession</span>\n\t\t\t              \t\t\t</div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t        </div>\n\n\t\t\t\t        <div class=\"row\">\n\t\t\t\t\t        <div class=\"col-sm-6\">\n\t\t\t\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\"> Father Language(s)<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t\t\t\t<textarea class=\"form-control textarea-50\" name=\"fatherlanguages\" [(ngModel)]=\"fatherlanguages\" formControlName=\"fatherlanguages\" placeholder=\"Enter Father Languages\"></textarea>\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"AdmissionForm1Submitted && \n\t\t\t            \t\t\t\tAdmissionTab1Form.controls.fatherlanguages.errors\">\n\t\t\t                \t\t\t<span *ngIf=\"AdmissionTab1Form.controls.fatherlanguages.errors.required\">Please Enter  Father Profession</span>\n\t\t\t              \t\t\t</div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t\t        <div class=\"col-sm-6\">\n\t\t\t\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\"> Mother Language(s)<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t\t\t\t<textarea class=\"form-control textarea-50\" name=\"motherlanguages\" [(ngModel)]=\"motherlanguages\" formControlName=\"motherlanguages\" placeholder=\"Enter Mother Languages\"></textarea>\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"AdmissionForm1Submitted && \n\t\t\t            \t\t\t\tAdmissionTab1Form.controls.motherlanguages.errors\">\n\t\t\t                \t\t\t<span *ngIf=\"AdmissionTab1Form.controls.motherlanguages.errors.required\">Please Enter  Mother Profession</span>\n\t\t\t              \t\t\t</div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t        </div>\n\n\t\t\t\t        <div class=\"row\">\n\t\t\t\t\t        <div class=\"col-sm-6\">\n\t\t\t\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\"> Father Nationality<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"fathernationality\" [(ngModel)]=\"fathernationality\" formControlName=\"fathernationality\" placeholder=\"Enter Father Nationality\">\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"AdmissionForm1Submitted && \n\t\t\t            \t\t\t\tAdmissionTab1Form.controls.fathernationality.errors\">\n\t\t\t                \t\t\t<span *ngIf=\"AdmissionTab1Form.controls.fathernationality.errors.required\">Please Enter  Father Nationality</span>\n\t\t\t              \t\t\t</div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t\t        <div class=\"col-sm-6\">\n\t\t\t\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\"> Mother Nationality<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"mothernationality\" [(ngModel)]=\"mothernationality\" formControlName=\"mothernationality\" placeholder=\"Enter Mother Nationality\">\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"AdmissionForm1Submitted && \n\t\t\t            \t\t\t\tAdmissionTab1Form.controls.mothernationality.errors\">\n\t\t\t                \t\t\t<span *ngIf=\"AdmissionTab1Form.controls.mothernationality.errors.required\">Please Enter  Mother Nationality</span>\n\t\t\t              \t\t\t</div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t        </div>\n\n\t\t\t\t        <h3 style=\"color: red;\">Siblings Name and date of Birth :</h3>\n\n\t\t\t\t        <div class=\"row\">\n\t\t\t\t          \t<div class=\"col-sm-6\">\n\t\t\t\t\t            <div class=\"form-group mt10\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\"> Enter Enrollment No</label>\n\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"sibling1enrollment\" [(ngModel)]=\"sibling1enrollment\" formControlName=\"sibling1enrollment\"\n\t\t\t\t\t\t\t\t\tplaceholder=\"Enter Sibling Enrollment No\" (keyup)=\"sibling1enrollChnage(sibling1enrollment)\">\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t\t        <div class=\"col-sm-6\">\n\t\t\t\t\t            <div class=\"form-group mt10\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\"> Enter Enrollment No</label>\n\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"sibling2enrollment\" [(ngModel)]=\"sibling2enrollment\" formControlName=\"sibling2enrollment\"\n\t\t\t\t\t\t\t\t\tplaceholder=\"Enter Sibling Enrollment No\" (keyup)=\"sibling2enrollChnage(sibling2enrollment)\">\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t        </div>\n\n\t\t\t\t        <div class=\"row\">\n\t\t\t\t\t        <div class=\"col-sm-6\">\n\t\t\t\t\t            <div class=\"form-group mt10\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\">Sibling Name </label>\n\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"sibling1name\" [(ngModel)]=\"sibling1name\" formControlName=\"sibling1name\" disabled=\"disabled\">\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t\t\t\t<input type=\"hidden\" name=\"sibling1id\" [(ngModel)]=\"sibling1id\" formControlName=\"sibling1id\">\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t\t        <div class=\"col-sm-6\">\n\t\t\t\t\t            <div class=\"form-group mt10\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\">Sibling Name </label>\n\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"sibling2name\" [(ngModel)]=\"sibling2name\" formControlName=\"sibling2name\" disabled=\"disabled\">\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t\t\t\t<input type=\"hidden\" name=\"sibling2id\" [(ngModel)]=\"sibling2id\" formControlName=\"sibling2id\">\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t\t    </div>\n\n\t\t\t\t\t    <!-- <div class=\"row\">\n\t\t\t\t          \t<div class=\"col-sm-6\">\n\t\t\t\t\t            <div class=\"form-group mt10\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\"> Which School do the sibling attend? </label>\n\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"sibling1school\" [(ngModel)]=\"sibling1school\" formControlName=\"sibling1school\"\n\t\t\t\t\t\t\t\t\tplaceholder=\"Enter Sibling School\">\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"AdmissionForm1Submitted && \n\t\t\t            \t\t\t\tAdmissionTab1Form.controls.sibling1school.errors\">\n\t\t\t                \t\t\t<span *ngIf=\"AdmissionTab1Form.controls.sibling1school.errors.required\">Please Enter School Name</span>\n\t\t\t              \t\t\t</div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t\t        <div class=\"col-sm-6\">\n\t\t\t\t\t            <div class=\"form-group mt10\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\">Which School do the sibling attend? </label>\n\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"sibling2school\" [(ngModel)]=\"sibling2school\" formControlName=\"sibling2school\"\n\t\t\t\t\t\t\t\t\tplaceholder=\"Enter Sibling School\">\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"AdmissionForm1Submitted && \n\t\t\t            \t\t\t\tAdmissionTab1Form.controls.sibling2school.errors\">\n\t\t\t                \t\t\t<span *ngIf=\"AdmissionTab1Form.controls.sibling2school.errors.required\">Please Enter School Name</span>\n\t\t\t              \t\t\t</div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t        </div> -->\n\n\t\t\t            <div class=\"form-group\">\n\t\t\t\t\t\t\t<button type=\"submit\" class=\"btn btn-primary \">Save</button>\n\t\t\t\t\t\t\t<button type=\"button\" routerLink=\"/admissionlist\" class=\"btn btn-primary \">Cancel</button>\n\t\t\t            </div>\n\t\t\t\t      </form>\n\t\t\t\t</mat-tab>\n\n\t\t\t\t<mat-tab disabled=\"{{admissionForm2disable}}\">\n\t\t\t\t\t<ng-template mat-tab-label>\n\t\t\t\t\t\t<span class=\"label-text\">Contact Information</span>\n\t\t\t\t\t</ng-template>\n\t\t\t\t    <form [formGroup]=\"AdmissionTab2Form\" (ngSubmit)=\"Form2OnSubmit(AdmissionTab2Form.value)\" class=\" org-form all-com-form\"\n\t\t\t\t        enctype=\"multipart/form-data\">\n\t\t\t\t        <div class=\"qtraq-subtitle-wrapper\">\n\t\t\t\t          <h1>Admission Process</h1>\n\t\t\t\t        </div>\n\n\t\t\t\t        <div class=\"row\">\n\t\t\t\t          \t<div class=\"col-sm-12\">\n\t\t\t\t\t            <div class=\"form-group mt10\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\">Present Address<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t\t\t\t<textarea type=\"text\" placeholder=\"Present Address\" class=\"form-control textarea-100\" name=\"presentaddress\" [(ngModel)]=\"presentaddress\" formControlName=\"presentaddress\" required></textarea>\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"AdmissionForm2Submitted && \n\t\t\t            \t\t\t\tAdmissionTab2Form.controls.presentaddress.errors\">\n\t\t\t                \t\t\t<span *ngIf=\"AdmissionTab2Form.controls.presentaddress.errors.required\">Please Enter Present Address</span>\n\t\t\t              \t\t\t</div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t        </div>\n\n\t\t\t\t        <div class=\"row\">\n\t\t\t\t          \t<div class=\"col-sm-6\">\n\t\t\t\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\"> Country<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t\t\t\t<select class=\"form-control\" name=\"countryid\" formControlName=\"countryid\" [(ngModel)]=\"countryId\" (change)=\"getState(countryId)\">\n\t\t\t\t\t\t                <option value=\"\" selected=\"selected\">Select Country</option>\n\t\t\t\t\t\t                <option [selected]=\" countryId == country.country_id\" [value]=\"country.country_id\"\n\t\t\t\t\t\t                  *ngFor=\"let country of countries\">{{country.country_name}}</option>\n\t\t\t\t\t              \t</select>\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"AdmissionForm2Submitted && \n\t\t\t            \t\t\t\tAdmissionTab2Form.controls.countryid.errors\">\n\t\t\t                \t\t\t<span *ngIf=\"AdmissionTab2Form.controls.countryid.errors.required\">Select Country</span>\n\t\t\t              \t\t\t</div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t\t        <div class=\"col-sm-6\">\n\t\t\t\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\"> State<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t\t\t\t<select class=\"form-control\" name=\"stateid\" formControlName=\"stateid\" [(ngModel)]=\"stateId\" (change)=\"getCity(stateId)\">\n\t\t\t\t\t\t                <option value=\"\">Select State</option>\n\t\t\t\t\t\t                <option [selected]=\" stateId == state.state_id\" [value]=\"state.state_id\"\n\t\t\t\t\t\t                  *ngFor=\"let state of states\">{{state.state_name}}</option>\n\t\t\t\t\t              \t</select>\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"AdmissionForm2Submitted && \n\t\t\t            \t\t\t\tAdmissionTab2Form.controls.stateid.errors\">\n\t\t\t                \t\t\t<span *ngIf=\"AdmissionTab2Form.controls.stateid.errors.required\">Select State</span>\n\t\t\t              \t\t\t</div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t        </div>\n\n\t\t\t\t        <div class=\"row\">\n\t\t\t\t          \t<div class=\"col-sm-6\">\n\t\t\t\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\"> City<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t\t\t\t<select class=\"form-control\" name=\"cityid\" formControlName=\"cityid\" [(ngModel)]=\"cityId\">\n\t\t\t\t\t\t                <option value=\"\" selected=\"selected\">Select City</option>\n\t\t\t\t\t\t                <option [selected]=\" cityId == city.city_id\" [value]=\"city.city_id\"\n\t\t\t\t\t\t                  *ngFor=\"let city of cities\">{{city.city_name}}</option>\n\t\t\t\t\t              \t</select>\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"AdmissionForm2Submitted && \n\t\t\t            \t\t\t\tAdmissionTab2Form.controls.cityid.errors\">\n\t\t\t                \t\t\t<span *ngIf=\"AdmissionTab2Form.controls.cityid.errors.required\">Select City</span>\n\t\t\t              \t\t\t</div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t\t        <div class=\"col-sm-6\">\n\t\t\t\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\"> Pincode<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t\t\t\t<input type=\"number\" class=\"form-control\" name=\"pincode\" [(ngModel)]=\"pincode\" formControlName=\"pincode\"\n\t\t\t\t\t\t\t\t\tplaceholder=\"Enter Pincode\">\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"AdmissionForm2Submitted && \n\t\t\t            \t\t\t\tAdmissionTab2Form.controls.pincode.errors\">\n\t\t\t                \t\t\t<span *ngIf=\"AdmissionTab2Form.controls.pincode.errors.required\">Please Enter Pincode</span>\n\t\t\t              \t\t\t</div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t        </div>\n\n\t\t\t\t        <div class=\"row\">\n\t\t\t\t          \t<div class=\"col-sm-6\">\n\t\t\t\t\t            <div class=\"form-group mt10\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\"> Father's Email Id<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"fatheremailid\" [(ngModel)]=\"fatheremailid\" formControlName=\"fatheremailid\"\n\t\t\t\t\t\t\t\t\tplaceholder=\"Enter Father Email Id\">\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"AdmissionForm2Submitted && \n\t\t\t            \t\t\t\tAdmissionTab2Form.controls.fatheremailid.errors\">\n\t\t\t                \t\t\t<span *ngIf=\"AdmissionTab2Form.controls.fatheremailid.errors.required\">Please Enter Father Email Id</span>\n\t\t\t              \t\t\t</div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t\t        <div class=\"col-sm-6\">\n\t\t\t\t\t            <div class=\"form-group mt10\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\"> Mother Email Id<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"motheremailid\" [(ngModel)]=\"motheremailid\" formControlName=\"motheremailid\"\n\t\t\t\t\t\t\t\t\tplaceholder=\"Enter Mother Email Id\">\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"AdmissionForm2Submitted && \n\t\t\t            \t\t\t\tAdmissionTab2Form.controls.motheremailid.errors\">\n\t\t\t                \t\t\t<span *ngIf=\"AdmissionTab2Form.controls.motheremailid.errors.required\">Please Enter Mother Email Id</span>\n\t\t\t              \t\t\t</div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t        </div>\n\n\t\t\t\t        <div class=\"row\">\n\t\t\t\t          \t<div class=\"col-sm-6\">\n\t\t\t\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\">Father Mobile Contact Number<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t\t\t\t<input type=\"number\" class=\"form-control\" name=\"fathermobilecontactno\" [(ngModel)]=\"fathermobilecontactno\" formControlName=\"fathermobilecontactno\"\n\t\t\t\t\t\t\t\t\tplaceholder=\"Enter Father Mobile Contact No\">\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"AdmissionForm2Submitted && \n\t\t\t            \t\t\t\tAdmissionTab2Form.controls.fathermobilecontactno.errors\">\n\t\t\t                \t\t\t<span *ngIf=\"AdmissionTab2Form.controls.fathermobilecontactno.errors.required\">Please Enter Father Mobile Contact No</span>\n\t\t\t              \t\t\t</div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t\t        <div class=\"col-sm-6\">\n\t\t\t\t\t            <div class=\"form-group mt10\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\">Mother Mobile Contact Number<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t\t\t\t<input type=\"number\" class=\"form-control\" name=\"mothermobilecontactno\" [(ngModel)]=\"mothermobilecontactno\" formControlName=\"mothermobilecontactno\"\n\t\t\t\t\t\t\t\t\tplaceholder=\"Enter Mother Mobile Contact No\">\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"AdmissionForm2Submitted && \n\t\t\t            \t\t\t\tAdmissionTab2Form.controls.mothermobilecontactno.errors\">\n\t\t\t                \t\t\t<span *ngIf=\"AdmissionTab2Form.controls.mothermobilecontactno.errors.required\">Please Enter Mother Mobile Contact No</span>\n\t\t\t              \t\t\t</div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t        </div>\n\n\t\t\t\t        <div class=\"row\">\n\t\t\t\t          \t<div class=\"col-sm-6\">\n\t\t\t\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\">Father Home Contact Number </label>\n\t\t\t\t\t\t\t\t\t<input type=\"number\" class=\"form-control\" name=\"fatherhomecontactno\" [(ngModel)]=\"fatherhomecontactno\" formControlName=\"fatherhomecontactno\"\n\t\t\t\t\t\t\t\t\tplaceholder=\"Enter Father Home Contact No\">\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t\t        <div class=\"col-sm-6\">\n\t\t\t\t\t            <div class=\"form-group mt10\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\">Mother Home Contact Number </label>\n\t\t\t\t\t\t\t\t\t<input type=\"number\" class=\"form-control\" name=\"motherhomecontactno\" [(ngModel)]=\"motherhomecontactno\" formControlName=\"motherhomecontactno\"\n\t\t\t\t\t\t\t\t\tplaceholder=\"Enter Mother Home Contact No\">\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t        </div>\n\n\t\t\t\t        <div class=\"row\">\n\t\t\t\t          \t<div class=\"col-sm-6\">\n\t\t\t\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\">Father Office Contact Number </label>\n\t\t\t\t\t\t\t\t\t<input type=\"number\" class=\"form-control\" name=\"fatherofficecontactno\" [(ngModel)]=\"fatherofficecontactno\" formControlName=\"fatherofficecontactno\"\n\t\t\t\t\t\t\t\t\tplaceholder=\"Enter Father Office Contact No\">\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t\t        <div class=\"col-sm-6\">\n\t\t\t\t\t            <div class=\"form-group mt10\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\">Mother Office Contact Number </label>\n\t\t\t\t\t\t\t\t\t<input type=\"number\" class=\"form-control\" name=\"motherofficecontactno\" [(ngModel)]=\"motherofficecontactno\" formControlName=\"motherofficecontactno\"\n\t\t\t\t\t\t\t\t\tplaceholder=\"Enter Mother Office Contact No\">\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t        </div>\n\t\t\t\t        <h3 style=\"color: red;\">Emergency Contact :</h3>\n\t\t\t\t        <div class=\"row\">\n\t\t\t\t          \t<div class=\"col-sm-6\">\n\t\t\t\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\"> Name<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"emergencycontactname\" [(ngModel)]=\"emergencycontactname\" formControlName=\"emergencycontactname\"\n\t\t\t\t\t\t\t\t\tplaceholder=\"Enter Name\">\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"AdmissionForm2Submitted && \n\t\t\t            \t\t\t\tAdmissionTab2Form.controls.emergencycontactname.errors\">\n\t\t\t                \t\t\t<span *ngIf=\"AdmissionTab2Form.controls.emergencycontactname.errors.required\">Please Enter Name</span>\n\t\t\t              \t\t\t</div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t\t        <div class=\"col-sm-6\">\n\t\t\t\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\"> Relationship<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"emergencycontactrelationship\" [(ngModel)]=\"emergencycontactrelationship\" formControlName=\"emergencycontactrelationship\"\n\t\t\t\t\t\t\t\t\tplaceholder=\"Enter Relationship\">\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"AdmissionForm2Submitted && \n\t\t\t            \t\t\t\tAdmissionTab2Form.controls.emergencycontactrelationship.errors\">\n\t\t\t                \t\t\t<span *ngIf=\"AdmissionTab2Form.controls.emergencycontactrelationship.errors.required\">Please Enter Relationship</span>\n\t\t\t              \t\t\t</div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t        </div>\n\n\t\t\t\t        <div class=\"row\">\n\t\t\t\t          \t<div class=\"col-sm-6\">\n\t\t\t\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\"> Mobile<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t\t\t\t<input type=\"number\" class=\"form-control\" name=\"emergencycontactmobileno\" [(ngModel)]=\"emergencycontactmobileno\" formControlName=\"emergencycontactmobileno\"\n\t\t\t\t\t\t\t\t\tplaceholder=\"Enter Mobile No\">\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"AdmissionForm2Submitted && \n\t\t\t            \t\t\t\tAdmissionTab2Form.controls.emergencycontactmobileno.errors\">\n\t\t\t                \t\t\t<span *ngIf=\"AdmissionTab2Form.controls.emergencycontactmobileno.errors.required\">Please Enter Mobile No</span>\n\t\t\t              \t\t\t</div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t\t        <div class=\"col-sm-6\">\n\t\t\t\t\t            <div class=\"form-group mt10\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\">Home Telephone </label>\n\t\t\t\t\t\t\t\t\t<input type=\"number\" class=\"form-control\" name=\"emergencycontacttelno\" [(ngModel)]=\"emergencycontacttelno\" formControlName=\"emergencycontacttelno\"\n\t\t\t\t\t\t\t\t\tplaceholder=\"Enter Home Telephone No\">\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t        </div>\n\n\n\t\t\t            <div class=\"form-group\">\n\t\t\t\t\t\t\t<button type=\"submit\" class=\"btn btn-primary \" style=\"margin-right: 10px\">Save</button>\n\t\t\t\t\t\t\t<button type=\"button\" routerLink=\"/admissionlist\" class=\"btn btn-primary \">Cancel</button>\n\t\t\t            </div>\n\t\t\t\t      </form>\n\t\t\t\t</mat-tab>\n\n\t\t\t\t<mat-tab disabled=\"{{admissionForm3disable}}\">\n\t\t\t\t\t<ng-template mat-tab-label>\n\t\t\t\t\t\t<span class=\"label-text\">Authorised Person</span>\n\t\t\t\t\t</ng-template>\n\t\t\t\t    <form [formGroup]=\"AdmissionTab3Form\" (ngSubmit)=\"Form3OnSubmit(AdmissionTab3Form.value)\" class=\" org-form all-com-form\"\n\t\t\t\t        enctype=\"multipart/form-data\">\n\t\t\t\t        <div class=\"qtraq-subtitle-wrapper\">\n\t\t\t\t          <h1>Authorised to pick & Drop</h1>\n\t\t\t\t        </div>\n\n\t\t\t\t        <div class=\"row\">\n\t\t\t\t          \t<div class=\"col-sm-4\">\n\t\t\t\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\"> Name<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"authperson1\" [(ngModel)]=\"authperson1\" formControlName=\"authperson1\"\n\t\t\t\t\t\t\t\t\tplaceholder=\"Enter Name\">\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"AdmissionForm3Submitted && \n\t\t\t            \t\t\t\tAdmissionTab3Form.controls.authperson1.errors\">\n\t\t\t                \t\t\t<span *ngIf=\"AdmissionTab3Form.controls.authperson1.errors.required\">Please Enter Name</span>\n\t\t\t              \t\t\t</div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t\t        <div class=\"col-sm-4\">\n\t\t\t\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\"> Relationship<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"authperson1relation\" [(ngModel)]=\"authperson1relation\" formControlName=\"authperson1relation\"\n\t\t\t\t\t\t\t\t\tplaceholder=\"Enter Relationship\">\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"AdmissionForm3Submitted && \n\t\t\t            \t\t\t\tAdmissionTab3Form.controls.authperson1relation.errors\">\n\t\t\t                \t\t\t<span *ngIf=\"AdmissionTab3Form.controls.authperson1relation.errors.required\">Please Enter Relationship</span>\n\t\t\t              \t\t\t</div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t\t        <div class=\"col-sm-4\">\n\t\t\t\t\t            <div class=\"form-group mt10\">\n\t\t\t\t\t            \t<label>Gender</label>\n\t\t\t\t         \t\t\t<br>\n\t\t\t\t\t\t\t\t\t<input type=\"radio\" [(ngModel)]=\"authperson1gender\" formControlName=\"authperson1gender\" name=\"authperson1gender\" [value]=\"true\" [checked]=\"authperson1gender\">Male\n\t\t\t\t\t\t\t\t\t<input type=\"radio\" [(ngModel)]=\"authperson1gender\" formControlName=\"authperson1gender\" name=\"authperson1gender\" [value]=\"false\"[checked]=\"!authperson1gender\">Female\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t        </div>\n\n\t\t\t\t        <div class=\"row\">\n\t\t\t\t          \t<div class=\"col-sm-4\">\n\t\t\t\t\t            <div class=\"form-group mt10\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\"> Name </label>\n\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"authperson2\" [(ngModel)]=\"authperson2\" formControlName=\"authperson2\"\n\t\t\t\t\t\t\t\t\tplaceholder=\"Enter Name\">\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t\t        <div class=\"col-sm-4\">\n\t\t\t\t\t            <div class=\"form-group mt10\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\"> Relationship </label>\n\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"authperson2relation\" [(ngModel)]=\"authperson2relation\" formControlName=\"authperson2relation\"\n\t\t\t\t\t\t\t\t\tplaceholder=\"Enter Relationship\">\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t\t        <div class=\"col-sm-4\">\n\t\t\t\t\t            <div class=\"form-group mt10\">\n\t\t\t\t\t            \t<label>Gender</label>\n\t\t\t\t         \t\t\t<br>\n\t\t\t\t\t\t\t\t\t<input type=\"radio\" [(ngModel)]=\"authperson2gender\" formControlName=\"authperson2gender\" name=\"authperson2gender\" [value]=\"true\" [checked]=\"authperson2gender\">Male\n\t\t\t\t\t\t\t\t\t<input type=\"radio\" [(ngModel)]=\"authperson2gender\" formControlName=\"authperson2gender\" name=\"authperson2gender\" [value]=\"false\"[checked]=\"!authperson2gender\">Female\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t        </div>\n\n\t\t\t\t        <div class=\"row\">\n\t\t\t\t          \t<div class=\"col-sm-4\">\n\t\t\t\t\t            <div class=\"form-group mt10\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\"> Name </label>\n\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"authperson3\" [(ngModel)]=\"authperson3\" formControlName=\"authperson3\"\n\t\t\t\t\t\t\t\t\tplaceholder=\"Enter Name\">\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t\t        <div class=\"col-sm-4\">\n\t\t\t\t\t            <div class=\"form-group mt10\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\"> Relationship </label>\n\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"authperson3relation\" [(ngModel)]=\"authperson3relation\" formControlName=\"authperson3relation\"\n\t\t\t\t\t\t\t\t\tplaceholder=\"Enter Relationship\">\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t\t        <div class=\"col-sm-4\">\n\t\t\t\t\t            <div class=\"form-group mt10\">\n\t\t\t\t\t            \t<label>Gender</label>\n\t\t\t\t         \t\t\t<br>\n\t\t\t\t\t\t\t\t\t<input type=\"radio\" [(ngModel)]=\"authperson3gender\" formControlName=\"authperson3gender\" name=\"authperson3gender\" [value]=\"true\" [checked]=\"authperson3gender\">Male\n\t\t\t\t\t\t\t\t\t<input type=\"radio\" [(ngModel)]=\"authperson3gender\" formControlName=\"authperson3gender\" name=\"authperson3gender\" [value]=\"false\"[checked]=\"!authperson3gender\">Female\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t        </div>\n\n\t\t\t            <div class=\"form-group\">\n\t\t\t\t\t\t\t<button type=\"submit\" class=\"btn btn-primary \" style=\"margin-right: 10px\">Save</button>\n\t\t\t\t\t\t\t<button type=\"button\" routerLink=\"/admissionlist\" class=\"btn btn-primary \">Cancel</button>\n\t\t\t            </div>\n\t\t\t\t      </form>\n\t\t\t\t</mat-tab>\n\n\t\t\t\t<mat-tab disabled=\"{{admissionForm4disable}}\">\n\t\t\t\t\t<ng-template mat-tab-label>\n\t\t\t\t\t\t<span class=\"label-text\">Documents</span>\n\t\t\t\t\t</ng-template>\n\t\t\t\t    <form [formGroup]=\"AdmissionTab4Form\" (ngSubmit)=\"Form4OnSubmit(AdmissionTab4Form.value)\" class=\" org-form all-com-form\"\n\t\t\t\t        enctype=\"multipart/form-data\">\n\t\t\t\t        <div class=\"qtraq-subtitle-wrapper\">\n\t\t\t\t          <h1>Admission Process</h1>\n\t\t\t\t        </div>\n\n\t\t\t\t        <div class=\"row\">\n\t\t\t\t          \t<div class=\"col-sm-6\">\n\t\t\t\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\"> Immunization Certificate<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t\t\t\t<div *ngIf=\"admission_form_path\">\n\t\t\t\t\t                 \t<a href=\"{{admission_form_path}}\" target=\"_blank\">View Admisson Form Detail </a>\n\t\t\t\t\t                </div>\n\t\t\t\t\t\t\t\t\t<div class=\"inputfile-box\">\n\t\t\t\t                     \t<input class=\"inputfile\" id=\"file\" type=\"file\" (change)=\"onadmissionFormUpload($event)\" name=\"duly_filled_admission_form\" formControlName=\"duly_filled_admission_form\">\n\t\t\t\t                     \t<label for=\"file\"><span class=\"file-box\" id=\"admissionForm\"></span><span class=\"file-button\">Upload</span></label>\n\t\t\t\t                    </div>\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"AdmissionForm4Submitted && \n\t\t\t            \t\t\t\tAdmissionTab4Form.controls.duly_filled_admission_form.errors && admission_form_path == ''\">\n\t\t\t                \t\t\t<span *ngIf=\"AdmissionTab4Form.controls.duly_filled_admission_form.errors.required\">Please Upload Admission Form</span>\n\t\t\t              \t\t\t</div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t          \t<div class=\"col-sm-6\">\n\t\t\t\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\"> Birth Certificate<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t\t\t\t<div *ngIf=\"birth_certificate_path\">\n\t\t\t\t\t                 \t<a href=\"{{birth_certificate_path}}\" target=\"_blank\">View Birth Certificate Detail </a>\n\t\t\t\t\t                </div>\n\t\t\t\t\t\t\t\t\t<div class=\"inputfile-box\">\n\t\t\t\t                     \t<input class=\"inputfile\" id=\"file1\" type=\"file\" (change)=\"onbirthCertyUpload($event)\" name=\"birth_certificate\" formControlName=\"birth_certificate\">\n\t\t\t\t                     \t<label for=\"file1\"><span class=\"file-box\" id=\"birthcerti\"></span><span class=\"file-button\">Upload</span></label>\n\t\t\t\t                    </div>\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"AdmissionForm4Submitted && \n\t\t\t            \t\t\t\tAdmissionTab4Form.controls.birth_certificate.errors && birth_certificate_path == ''\">\n\t\t\t                \t\t\t<span *ngIf=\"AdmissionTab4Form.controls.birth_certificate.errors.required\">Please Upload Birth Certificate</span>\n\t\t\t              \t\t\t</div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t        </div>\n\n\t\t\t\t        <div class=\"row\">\n\t\t\t\t          \t<div class=\"col-sm-6\">\n\t\t\t\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\"> Child Photo<span class=\"text-danger\">*</span> </label>\n\t\t\t\t                    <div *ngIf=\"profile_pic_path\">\n\t\t\t\t\t                 \t<img height=\"200px\" src=\"{{profile_pic_path}}\" alt=\"img\">\n\t\t\t\t\t                </div>\n\t\t\t\t\t\t\t\t\t<div class=\"inputfile-box\">\n\t\t\t\t                     \t<input class=\"inputfile\" id=\"file2\" type=\"file\" (change)=\"onprofilePicUpload($event)\" name=\"profile_pic\" formControlName=\"profile_pic\">\n\t\t\t\t                     \t<label for=\"file2\"><span class=\"file-box\" id=\"profilePic\"></span><span class=\"file-button\">Upload</span></label>\n\t\t\t\t                    </div>\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"AdmissionForm4Submitted && \n\t\t\t            \t\t\t\tAdmissionTab4Form.controls.profile_pic.errors && profile_pic_path == ''\">\n\t\t\t                \t\t\t<span *ngIf=\"AdmissionTab4Form.controls.profile_pic.errors.required\">Please Upload Profile Pic</span>\n\t\t\t              \t\t\t</div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t          \t<div class=\"col-sm-6\">\n\t\t\t\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\"> Family Photo<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t\t\t\t<div *ngIf=\"family_photo_path\">\n\t\t\t\t\t\t\t\t\t\t<img height=\"200px\" src=\"{{family_photo_path}}\" alt=\"img\">\n\t\t\t\t\t                </div>\n\t\t\t\t\t\t\t\t\t<div class=\"inputfile-box\">\n\t\t\t\t                     \t<input class=\"inputfile\" id=\"file3\" type=\"file\" (change)=\"onfamilyPhotoUpload($event)\" name=\"family_photo\" formControlName=\"family_photo\">\n\t\t\t\t                     \t<label for=\"file3\"><span class=\"file-box\" id=\"familyphoto\"></span><span class=\"file-button\">Upload</span></label>\n\t\t\t\t                    </div>\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"AdmissionForm4Submitted && \n\t\t\t            \t\t\t\tAdmissionTab4Form.controls.family_photo.errors && family_photo_path == ''\">\n\t\t\t                \t\t\t<span *ngIf=\"AdmissionTab4Form.controls.family_photo.errors.required\">Please Upload Family Photo</span>\n\t\t\t              \t\t\t</div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t        </div>\n\n\t\t\t\t        <div class=\"row\">\n\t\t\t\t          \t<div class=\"col-sm-6\">\n\t\t\t\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\"> Address proof<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t\t\t\t<div *ngIf=\"address_proof_path\">\n\t\t\t\t\t\t\t\t\t\t<a href=\"{{address_proof_path}}\" target = \"_blank\">View Address Proof Detail</a>\n\t\t\t\t\t                </div>\n\t\t\t\t\t\t\t\t\t<div class=\"inputfile-box\">\n\t\t\t\t                     \t<input class=\"inputfile\" id=\"file4\" type=\"file\" (change)=\"onaddressProofUpload($event)\" name=\"address_proof\" formControlName=\"address_proof\">\n\t\t\t\t                     \t<label for=\"file4\"><span class=\"file-box\" id=\"addressProof\"></span><span class=\"file-button\">Upload</span></label>\n\t\t\t\t                    </div>\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"AdmissionForm4Submitted && \n\t\t\t            \t\t\t\tAdmissionTab4Form.controls.address_proof.errors\">\n\t\t\t                \t\t\t<span *ngIf=\"AdmissionTab4Form.controls.address_proof.errors.required && address_proof_path == ''\">Please Upload Address Proof</span>\n\t\t\t              \t\t\t</div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t          \t<div class=\"col-sm-6\">\n\t\t\t\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\"> Profile Form<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t\t\t\t<div *ngIf=\"profile_form_path\">\n\t\t\t\t\t\t\t\t\t\t<a href=\"{{profile_form_path}}\" target = \"_blank\">View Child Profile Form Detail</a>\n\t\t\t\t\t                </div>\n\t\t\t\t\t\t\t\t\t<div class=\"inputfile-box\">\n\t\t\t\t                     \t<input class=\"inputfile\" id=\"file5\" type=\"file\" (change)=\"onprofileFormUpload($event)\" name=\"duly_filled_child_profile_form\" formControlName=\"duly_filled_child_profile_form\">\n\t\t\t\t                     \t<label for=\"file5\"><span class=\"file-box\" id=\"profileForm\"></span><span class=\"file-button\">Upload</span></label>\n\t\t\t\t                    </div>\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"AdmissionForm4Submitted && \n\t\t\t            \t\t\t\tAdmissionTab4Form.controls.duly_filled_child_profile_form.errors && profile_form_path == ''\">\n\t\t\t                \t\t\t<span *ngIf=\"AdmissionTab4Form.controls.duly_filled_child_profile_form.errors.required\">Please Upload Profile Form</span>\n\t\t\t              \t\t\t</div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t        </div>\n\n\t\t\t            <div class=\"form-group\">\n\t\t\t\t\t\t\t<button type=\"submit\" class=\"btn btn-primary \" style=\"margin-right: 10px\">Save</button>\n\t\t\t\t\t\t\t<button type=\"button\" routerLink=\"/admissionlist\" class=\"btn btn-primary \">Cancel</button>\n\t\t\t            </div>\n\t\t\t\t      </form>\n\t\t\t\t</mat-tab>\n\n\t\t\t\t<mat-tab disabled=\"{{admissionForm5disable}}\" *ngIf=\"displaypayFees\">\n\t\t\t\t\t<ng-template mat-tab-label>\n\t\t\t\t\t\t<span class=\"label-text\">Pay Fees</span>\n\t\t\t\t\t</ng-template>\n\t\t\t\t    <form [formGroup]=\"AdmissionTab5Form\" (ngSubmit)=\"Form5OnSubmit(AdmissionTab5Form.value)\" class=\" org-form all-com-form\"\n\t\t\t\t        enctype=\"multipart/form-data\">\n\t\t\t\t        <div class=\"qtraq-subtitle-wrapper\">\n\t\t\t\t          <h1>Admission Process</h1>\n\t\t\t\t        </div>\n\n\t\t\t\t        <div class=\"row\">\n\t\t\t\t          \t<div class=\"col-sm-6\">\n\t\t\t\t\t            <div class=\"form-group mt10\">\n\t\t\t\t\t            \t<label>Fees Selection Type</label>\n\t\t\t\t         \t\t\t<br>\n\t\t\t\t\t\t\t\t\t<!-- <input type=\"radio\" [(ngModel)]=\"feestype\" formControlName=\"feestype\" name=\"feestype\" [value]=\"true\" [checked]=\"feestype\" disabled=\"disabled\">Group -->\n\t\t\t\t\t\t\t\t\t<input type=\"radio\" [(ngModel)]=\"feestype\" formControlName=\"feestype\" name=\"feestype\" [value]=\"false\"[checked]=\"!feestype\">Class\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t\t        <div class=\"col-sm-6\">\n\t\t\t\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\"> Fees Level<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t\t\t\t<select class=\"form-control\" name=\"fees_level\" formControlName=\"fees_level\" [(ngModel)]=\"feesLevel\" (change)=\"getCenterFees(feesLevel,'level')\" >\n\t\t\t\t\t\t                <option value=\"\" selected=\"selected\">Select Fees Level</option>\n\t\t\t\t\t\t                <option [selected]=\" feesLevel == feeslevel.fees_level_id\" [value]=\"feeslevel.fees_level_id\"\n\t\t\t\t\t\t                  *ngFor=\"let feeslevel of feesLevels\">{{feeslevel.fees_level_name}}</option>\n\t\t\t\t\t              \t</select>\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"AdmissionForm5Submitted && \n\t\t\t            \t\t\t\tAdmissionTab5Form.controls.fees_level.errors\">\n\t\t\t                \t\t\t<span *ngIf=\"AdmissionTab5Form.controls.fees_level.errors.required\">Select Fees Level</span>\n\t\t\t              \t\t\t</div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t        </div>\n\n\t\t\t\t        <div class=\"row\">\n\t\t\t\t        \t<div class=\"col-sm-6\">\n\t\t\t\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\">Fees<span class=\"text-danger\">*</span></label>\n\t\t\t\t\t\t\t\t\t<select class=\"form-control\" name=\"fees\" formControlName=\"fees\" [(ngModel)]=\"fees\" (change)=\"getCenterFees(feesLevel,'fees')\" >\n\t\t\t\t\t\t                <option value=\"\" selected=\"selected\">Select Fees</option>\n\t\t\t\t\t\t                <option [value]=\"fee.fees_id\"\n\t\t\t\t\t\t                  *ngFor=\"let fee of Fees\">{{fee.fees_name}}</option>\n\t\t\t\t\t              \t</select>\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"AdmissionForm5Submitted && \n\t\t\t            \t\t\t\tAdmissionTab5Form.controls.fees.errors\">\n\t\t\t                \t\t\t<span *ngIf=\"AdmissionTab5Form.controls.fees.errors.required\">Select Fees</span>\n\t\t\t              \t\t\t</div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t\t        <div class=\"col-sm-6\">\n\t\t\t\t\t            <div class=\"form-group mt10\">\n\t\t\t\t\t            \t<label>Fees Type</label>\n\t\t\t\t         \t\t\t<br>\n\t\t\t\t\t\t\t\t\t<input type=\"radio\" [(ngModel)]=\"feepaymenttype\" formControlName=\"feepaymenttype\" name=\"feepaymenttype\" [value]=\"true\" [checked]=\"feepaymenttype\" (change) = \"feesPaymentTypeChange(feepaymenttype)\">Insatllment\n\t\t\t\t\t\t\t\t\t<input type=\"radio\" [(ngModel)]=\"feepaymenttype\" formControlName=\"feepaymenttype\" name=\"feepaymenttype\" [value]=\"false\"[checked]=\"!feepaymenttype\" (change) = \"feesPaymentTypeChange(feepaymenttype)\">Lumsum\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t        </div>\n\n\t\t\t\t        <div class=\"row\" *ngIf=\"!feepaymenttype\">\n\t\t\t\t          \t<div class=\"col-sm-6\">\n\t\t\t\t\t            <div class=\"form-group mt10\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\"> Amount Collected </label>\n\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"fees_amount_collected\" [(ngModel)]=\"fees_amount_collected\" formControlName=\"fees_amount_collected\" disabled=\"disabled\">\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t\t        <div class=\"col-sm-6\">\n\t\t\t\t\t            <div class=\"form-group mt10\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\"> Amount Remaining </label>\n\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"fees_remaining_amount\" [(ngModel)]=\"fees_remaining_amount\" formControlName=\"fees_remaining_amount\" disabled=\"disabled\">\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t        </div>\n\n\t\t\t\t        <div class=\"row\" *ngIf=\"feepaymenttype && fees_total_amount != ''\">\n\t\t\t\t          \t<div class=\"col-sm-6\">\n\t\t\t\t\t            <div class=\"form-group mt10\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\"> Insatllment No<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t\t\t\t<input type=\"number\" class=\"form-control\" name=\"noofinstallment\" [(ngModel)]=\"noofinstallment\" formControlName=\"noofinstallment\" (keyup)=\"installmentnoChange(noofinstallment)\" min=\"0\">\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t\t\t\t<span  id=\"installmenterror\"></span>\n\t\t\t\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"AdmissionForm5Submitted && \n\t\t\t            \t\t\t\tAdmissionTab5Form.controls.noofinstallment.errors\">\n\t\t\t                \t\t\t<span *ngIf=\"AdmissionTab5Form.controls.noofinstallment.errors.required\">Enter Installment No</span>\n\t\t\t              \t\t\t</div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t        \t<div class=\"col-sm-6\">\n\t\t\t\t\t            <div class=\"form-group mt10\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\"> MaxInsatllment Allowed </label>\n\t\t\t\t\t\t\t\t\t<input type=\"number\" class=\"form-control\" name=\"maxinstallmentallowed\" [(ngModel)]=\"maxinstallmentallowed\" formControlName=\"maxinstallmentallowed\" disabled=\"disabled\">\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t        </div>\n\n\t\t\t\t        <div class=\"row\">\n\t\t\t\t          \t<div class=\"col-sm-6\">\n\t\t\t\t\t            <div class=\"form-group mt10\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\"> Total Amount </label>\n\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"fees_total_amount\" [(ngModel)]=\"fees_total_amount\" formControlName=\"fees_total_amount\" disabled=\"disabled\">\n\t\t\t\t\t\t\t\t\t<input type=\"hidden\" class=\"form-control\" name=\"fees_id\" [(ngModel)]=\"fees_id\" formControlName=\"fees_id\">\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t        \t<div class=\"col-sm-6\">\n\t\t\t\t\t            <div class=\"form-group mt10\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\"> Discount Amount </label>\n\t\t\t\t\t\t\t\t\t<input type=\"number\" class=\"form-control\" name=\"discount_amount\" [(ngModel)]=\"discount_amount\" formControlName=\"discount_amount\" (keyup)=\"discountChange(discount_amount)\" min=\"0\">\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t        </div>\n\n\t\t\t\t        <div class=\"row\">\n\t\t\t\t        \t<div class=\"col-sm-6\">\n\t\t\t\t\t            <div class=\"form-group mt10\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\">Amount After Discount </label>\n\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"total_amount\" [(ngModel)]=\"total_amount\" formControlName=\"total_amount\" disabled=\"disabled\">\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t          \t<div class=\"col-sm-6\" *ngIf=\"!feepaymenttype\">\n\t\t\t\t\t            <div class=\"form-group mt10\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\">Fees Remark<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t\t\t\t<textarea placeholder=\"Fees Remark\" class=\"form-control textarea-100\" name=\"fees_remark\" [(ngModel)]=\"fees_remark\" formControlName=\"fees_remark\" required></textarea>\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"AdmissionForm5Submitted && \n\t\t\t            \t\t\t\tAdmissionTab5Form.controls.fees_remark.errors\">\n\t\t\t                \t\t\t<span *ngIf=\"AdmissionTab5Form.controls.fees_remark.errors.required\">Please Enter Fees Remark</span>\n\t\t\t              \t\t\t</div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t        </div>\n\n\t\t\t\t        <div class=\"table-responsive\" *ngIf=\"feepaymenttype && fees_total_amount != ''\">\n\t\t\t\t        \t<table class=\"table table-striped\">\n\t\t\t\t        \t\t<thead>\n\t\t\t\t\t        \t\t<tr>\n\t\t\t\t\t        \t\t\t<td>Payment Due Date<span class=\"text-danger\">*</span></td>\n\t\t\t\t\t        \t\t\t<td>Installment Amount</td>\n\t\t\t\t\t        \t\t\t<td>Paying Amount<span class=\"text-danger\">*</span></td>\n\t\t\t\t\t        \t\t\t<td>Remaining Amount</td>\n\t\t\t\t\t        \t\t</tr>\n\t\t\t\t\t        \t</thead>\n\t\t\t\t\t        \t<tbody>\n\t\t\t\t\t\t\t\t\t<tr *ngFor=\"let i of installment; let i = index\">\n\t\t\t\t\t\t\t\t\t\t<td class=\"hidden\">1</td>\n\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t<select class=\"form-control\" name=\"installment_due_date[]\" formControlName=\"installment_due_date\" [(ngModel)]=\"monthId[i]\" id=\"month_id_{{i}}\" (change)=\"monthChange(i)\">\n\t\t\t\t\t\t\t                <option value=\"\" selected=\"selected\">Select Month</option>\n\t\t\t\t\t\t\t                <option [selected]=\" monthId[i] == month.id\" [value]=\"month.id\"\n\t\t\t\t\t\t\t                  *ngFor=\"let month of months\">{{month.value}}</option>\n\t\t\t\t\t\t              \t</select>\n<!-- \t\t\t\t\t\t              \t  <mat-form-field>\n  <input matInput [matDatepicker]=\"dp\" placeholder=\"Month and Year\" formControlName=\"date_{{i}}\">\n  <mat-datepicker-toggle matSuffix [for]=\"dp\"></mat-datepicker-toggle>\n  <mat-datepicker #dp\n                  startView=\"multi-year\"\n                  (yearSelected)=\"chosenYearHandler($event,i)\"\n                  (monthSelected)=\"chosenMonthHandler($event, dp,i)\"\n                  panelClass=\"example-month-picker\">\n  </mat-datepicker>\n</mat-form-field> -->\n\t\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t\t\t\t\t<div class=\"text-danger\" id=\"montherror_{{i}}\">\n\t\t\t\t              \t\t\t</div>\n\t\t\t\t              \t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t<td>{{installment_amount}}</td>\n\t\t\t\t\t\t\t\t\t\t<td style=\"width: 30%\"><input type=\"number\" class=\"form-control\" id=\"id_{{i}}\" name=\"installment_collected_amount[]\" [(ngModel)]=\"installment_collected_amount[i]\" formControlName=\"installment_collected_amount\" (keyup)=\"collectedinstallmentChange(i)\">\n\t\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t\t\t\t\t<div class=\"text-danger \" id=\"amounterror_{{i}}\">\n\t\t\t\t              \t\t\t</div>\n\t\t\t\t              \t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t<td><input type=\"text\" class=\"form-control\" id=\"remaining_id_{{i}}\" name=\"installment_remaining_amount[]\" [(ngModel)]=\"installment_remaining_amount[i]\" formControlName=\"installment_remaining_amount\" disabled=\"disabled\"></td>\n\t\t\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t\t</tbody>\n\t\t\t\t        \t</table>\n\t\t\t\t        </div>\n\n\t\t\t\t        <div class=\"row\">\n\t\t\t\t          \t<div class=\"col-sm-6\" *ngIf=\"feepaymenttype\">\n\t\t\t\t\t            <div class=\"form-group mt10\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\">Fees Remark<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t\t\t\t<textarea placeholder=\"Fees Remark\" class=\"form-control textarea-100\" name=\"fees_remark\" [(ngModel)]=\"fees_remark\" formControlName=\"fees_remark\" required></textarea>\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"AdmissionForm5Submitted && \n\t\t\t            \t\t\t\tAdmissionTab5Form.controls.fees_remark.errors\">\n\t\t\t                \t\t\t<span *ngIf=\"AdmissionTab5Form.controls.fees_remark.errors.required\">Please Enter Fees Remark</span>\n\t\t\t              \t\t\t</div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t\t        <div class=\"col-sm-6\">\n\t\t\t\t\t            <div class=\"form-group mt10\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\">Fees Accepted By<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t\t\t\t<input type=\"text\" placeholder=\"Enter Fees Accepted By\" class=\"form-control\" name=\"fees_approved_accepted_by_name\" [(ngModel)]=\"fees_approved_accepted_by_name\" formControlName=\"fees_approved_accepted_by_name\" disabled=\"disabled\" value=\"{{fees_approved_accepted_by_name}}\">\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"AdmissionForm5Submitted && \n\t\t\t            \t\t\t\tAdmissionTab5Form.controls.fees_approved_accepted_by_name.errors\">\n\t\t\t                \t\t\t<span *ngIf=\"AdmissionTab5Form.controls.fees_approved_accepted_by_name.errors.required\">Please Enter Fees Accepted By</span>\n\t\t\t              \t\t\t</div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t        </div>\n\t\t\t\t        <h3 style=\"background-color: red;color: white;\">Fill Payment Details</h3>\n\n\t\t\t\t         <div class=\"row\">\n\t\t\t\t        \t<div class=\"col-sm-6\">\n\t\t\t\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t            \t<label class=\"text_green\">Payment Mode</label>\n\t\t\t\t\t            \t<br>\n\t\t\t\t\t\t\t\t\t<label *ngFor=\"let option of payment_mode\" style=\"margin-right: 10px\">\n\t\t\t\t\t\t                <label>\n\t\t\t\t\t\t                    <input type=\"radio\"\n\t\t\t\t\t\t                   \tname=\"payment_mode\" value=\"{{option}}\" [(ngModel)]=\"ngpayment_mode\" [checked] = \"selectedCheckbox == option\" formControlName=\"payment_mode\" (change)=\"updatePaymentOptions(option, $event)\"/>\n\t\t\t\t\t\t                    {{option}}\n\t\t\t\t\t\t                </label>\n\t\t\t\t\t\t            </label>\n\t\t\t\t\t\t\t\t\t<br>\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"AdmissionForm5Submitted && \n\t\t\t            \t\t\t\tAdmissionTab5Form.controls.payment_mode.errors\">\n\t\t\t                \t\t\t<span *ngIf=\"AdmissionTab5Form.controls.payment_mode.errors.required\">Select atleast one</span>\n\t\t\t              \t\t\t</div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t            <div class=\"col-sm-6 bank_name\">\n\t\t\t\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t            \t<label class=\"text_green\">Bank Name<span class=\"text-danger\">*</span></label>\n\t\t\t\t\t            \t<br>\n\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"bank_name\" [(ngModel)]=\"bank_name\" formControlName=\"bank_name\">\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"AdmissionForm5Submitted && \n\t\t\t            \t\t\t\tAdmissionTab5Form.controls.bank_name.errors\">\n\t\t\t                \t\t\t<span *ngIf=\"AdmissionTab5Form.controls.bank_name.errors.required\">Please Enter Bank Name</span>\n\t\t\t              \t\t\t</div>\n\t\t\t\t\t\t\t\t\t<br>\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"AdmissionForm5Submitted && \n\t\t\t            \t\t\t\tAdmissionTab5Form.controls.payment_mode.errors\">\n\t\t\t                \t\t\t<span *ngIf=\"AdmissionTab5Form.controls.payment_mode.errors.required\">Select atleast one</span>\n\t\t\t              \t\t\t</div>\n\t\t\t\t\t            </div>\n\t\t\t\t        \t</div>\n\t\t\t\t        </div>\n\n\t\t\t\t        <div class=\"row\">\n\n\t\t\t\t\t        <div class=\"col-sm-6 cheque_no\">\n\t\t\t\t\t            <div class=\"form-group mt10\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\"> Cheque No<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"cheque_no\" [(ngModel)]=\"cheque_no\" formControlName=\"cheque_no\">\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"AdmissionForm5Submitted && \n\t\t\t            \t\t\t\tAdmissionTab5Form.controls.cheque_no.errors\">\n\t\t\t                \t\t\t<span *ngIf=\"AdmissionTab5Form.controls.cheque_no.errors.required\">Please Enter Cheque No</span>\n\t\t\t              \t\t\t</div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t\t        <div class=\"col-sm-6 transaction_no\">\n\t\t\t\t\t            <div class=\"form-group mt10\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\"> Transaction No<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"transaction_id\" [(ngModel)]=\"transaction_id\" formControlName=\"transaction_id\">\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"AdmissionForm5Submitted && \n\t\t\t            \t\t\t\tAdmissionTab5Form.controls.transaction_id.errors\">\n\t\t\t                \t\t\t<span *ngIf=\"AdmissionTab5Form.controls.transaction_id.errors.required\">Please Enter Transaction No</span>\n\t\t\t              \t\t\t</div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t\t        <div class=\"col-sm-6 transaction_no\">\n\t\t\t\t\t            <div class=\"form-group mt10\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\"> Transaction Date<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t\t\t\t<input ngx-mydatepicker name=\"transaction_date\" (dateChanged)=\"ontransactionDateChanged($event)\" [options]=\"myOptions\" [(ngModel)]=\"ngxtransactiondate\"\n\t\t\t\t\t\t\t        #dptransaction=\"ngx-mydatepicker\" formControlName=\"transaction_date\" (click)=\"dptransaction.toggleCalendar()\" class=\"form-control w100\" placeholder=\"Select Transaction Date\">\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"AdmissionForm5Submitted && \n\t\t\t            \t\t\t\tAdmissionTab5Form.controls.transaction_date.errors\">\n\t\t\t                \t\t\t<span *ngIf=\"AdmissionTab5Form.controls.transaction_date.errors.required\">Select Transaction Dat</span>\n\t\t\t              \t\t\t</div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t\t        <div class=\"col-sm-6 cheque_no\">\n\t\t\t\t\t            <div class=\"form-group mt10\">\n\t\t\t\t\t\t\t\t\t<label class=\"text_green\"> Cheque Date<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t\t\t\t<input ngx-mydatepicker name=\"transaction_date\" (dateChanged)=\"ontransactionDateChanged($event)\" [options]=\"myOptions\" [(ngModel)]=\"ngxtransactiondate\"\n\t\t\t\t\t\t\t        #dptransaction=\"ngx-mydatepicker\" formControlName=\"transaction_date\" (click)=\"dptransaction.toggleCalendar()\" class=\"form-control w100\" placeholder=\"Select Transaction Date\">\n\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"AdmissionForm5Submitted && \n\t\t\t            \t\t\t\tAdmissionTab5Form.controls.transaction_date.errors\">\n\t\t\t                \t\t\t<span *ngIf=\"AdmissionTab5Form.controls.transaction_date.errors.required\">Select Transaction Dat</span>\n\t\t\t              \t\t\t</div>\n\t\t\t\t\t            </div>\n\t\t\t\t\t        </div>\n\t\t\t\t        </div>\n\n\t\t\t            <div class=\"form-group\">\n\t\t\t\t\t\t\t<button type=\"submit\" class=\"btn btn-primary \" style=\"margin-right: 10px\">Save</button>\n\t\t\t\t\t\t\t<button type=\"button\" routerLink=\"/admissionlist\" class=\"btn btn-primary \">Cancel</button>\n\t\t\t            </div>\n\t\t\t\t      </form>\n\t\t\t\t</mat-tab>\n\t\t\t</mat-tab-group>\n\t    </div>\n\t  </section>\n\t</div>\n</div>"

/***/ }),

/***/ "./src/app/components/admission/admissiontab1/admissiontab1.component.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/components/admission/admissiontab1/admissiontab1.component.ts ***!
  \*******************************************************************************/
/*! exports provided: MY_FORMATS, Admissiontab1Component */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MY_FORMATS", function() { return MY_FORMATS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Admissiontab1Component", function() { return Admissiontab1Component; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _services_common_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/common.service */ "./src/app/services/common.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");
/* harmony import */ var _angular_material_moment_adapter__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material-moment-adapter */ "./node_modules/@angular/material-moment-adapter/esm2015/material-moment-adapter.js");
/* harmony import */ var _angular_material_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/core */ "./node_modules/@angular/material/esm2015/core.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_8__);










const moment = moment__WEBPACK_IMPORTED_MODULE_8___default.a || moment__WEBPACK_IMPORTED_MODULE_8__;
const MY_FORMATS = {
    parse: {
        dateInput: 'MM/YYYY',
    },
    display: {
        dateInput: 'MM/YYYY',
        monthYearLabel: 'MMM YYYY',
        dateA11yLabel: 'LL',
        monthYearA11yLabel: 'MMMM YYYY',
    },
};
let Admissiontab1Component = class Admissiontab1Component {
    constructor(route, commonService, formBuilder, snackbar, router, zone) {
        this.route = route;
        this.commonService = commonService;
        this.formBuilder = formBuilder;
        this.snackbar = snackbar;
        this.router = router;
        this.zone = zone;
        this.myOptions = {
            dateFormat: 'dd-mm-yyyy',
        };
        this.date = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](moment());
        this.installment_due_date = '';
        this.months = [{ 'id': '01', 'value': 'January' }, { 'id': '02', 'value': 'February' }, { 'id': '03', 'value': 'March' }, { 'id': '04', 'value': 'April' }, { 'id': '05', 'value': 'May' }, { 'id': '06', 'value': 'June' }, { 'id': '07', 'value': 'July' }, { 'id': '08', 'value': 'August' }, { 'id': '09', 'value': 'Sepetember' }, { 'id': '10', 'value': 'October' }, { 'id': '11', 'value': 'November' }, { 'id': '12', 'value': 'December' }];
        this.monthId = '';
        this.payment_mode = ['Cheque', 'Cash', 'Netbanking'];
        this.selectedOptions = [];
        this.optionsMap = {
            Cheque: false,
            Cash: false,
            Netbanking: false,
        };
        this.optionsChecked = [];
        this.admissionFilter = {
            enrollmentno: "",
        };
        this.size = 10;
        this.page = 0;
        this.admissionFormSelFile = "";
        this.birthCerySelFile = "";
        this.profilePicSelFile = "";
        this.familyPhotoSelFile = "";
        this.addressProofSelFile = "";
        this.profileFormSelFile = "";
        this.AdmissionFormSubmitted = false;
        this.AdmissionForm1Submitted = false;
        this.AdmissionForm2Submitted = false;
        this.AdmissionForm3Submitted = false;
        this.AdmissionForm4Submitted = false;
        this.AdmissionForm5Submitted = false;
        this.admissiondate = '';
        this.ngxadmissiondate = {
            jsdate: new Date()
        };
        this.transaction_date = '';
        this.ngxtransactiondate = {
            jsdate: new Date()
        };
        this.academicyearid = "";
        this.enrollmentno = "";
        this.categoryId = "";
        this.courseId = "";
        this.batchid = "";
        this.batchId = "";
        this.programmestartdate = "";
        this.programmeenddate = '';
        this.studentfirstname = "";
        this.studentlastname = "";
        this.nationality = "";
        this.religion = "";
        this.mothertongue = "";
        this.otherlanguages = "";
        this.preschoolname = "";
        this.selectedIndex = 0;
        this.fathername = "";
        this.mothername = "";
        this.fatherprof = '';
        this.motherprof = '';
        this.fathernationality = '';
        this.mothernationality = "";
        this.sibling1name = "";
        this.sibling2name = "";
        this.sibling1id = "";
        this.sibling2id = "";
        // sibling1dob : any
        // sibling1dob1 : any
        // ngxsibling1dob : any
        // sibling2dob : any
        // sibling2dob1 : any
        // ngxsibling2dob : any
        // sibling1school = "";
        // sibling2school = "";
        this.presentaddress = "";
        this.countryId = "";
        this.stateId = "";
        this.cityId = "";
        this.pincode = '';
        this.fathermobilecontactno = "";
        this.mothermobilecontactno = "";
        this.fatherhomecontactno = '';
        this.motherhomecontactno = "";
        this.fatherofficecontactno = "";
        this.motherofficecontactno = "";
        this.emergencycontactname = "";
        this.emergencycontactrelationship = '';
        this.emergencycontactmobileno = '';
        this.emergencycontacttelno = "";
        this.studentId = "";
        this.fatheremailid = "";
        this.motheremailid = "";
        this.authperson1 = "";
        this.authperson1relation = '';
        this.authperson2 = "";
        this.authperson2relation = '';
        this.authperson3 = "";
        this.authperson3relation = '';
        this.admissionForm1disable = true;
        this.admissionForm2disable = true;
        this.admissionForm3disable = true;
        this.admissionForm4disable = true;
        this.admissionForm5disable = true;
        this.selectedindex = 0;
        this.duly_filled_admission_form = "";
        this.birth_certificate = "";
        this.profile_pic = "";
        this.family_photo = "";
        this.address_proof = "";
        this.duly_filled_child_profile_form = "";
        this.profile_pic_path = '';
        this.family_photo_path = "";
        this.admission_form_path = "";
        this.birth_certificate_path = "";
        this.address_proof_path = '';
        this.profile_form_path = "";
        this.feestype = false;
        this.fees_id = "";
        this.feepaymenttype = false;
        this.fees_amount_collected = '';
        this.fees_remaining_amount = '';
        this.fees_remark = "";
        this.fees_approved_accepted_by_name = "";
        this.cheque_no = "";
        this.bank_name = "";
        this.transaction_id = "";
        this.maxinstallmentallowed = '';
        this.installment = [];
        this.installment_collected_amount = '';
        this.installment_remaining_amount = 0;
        this.isLoading = false;
        this.displaypayFees = true;
        this.Enquirydisable = true;
        this.admissiondateDisable = false;
        this.EnquirydisableCreate = false;
        this.inquiries1 = '';
        this.inquiry_no_edit = '';
        this.studentidpass = '';
        this.sValue = new Date(2017, 7);
        this.minMode = 'month';
    }
    chosenYearHandler(normalizedYear, index) {
        const ctrlValue = this.date.value;
        ctrlValue.year(normalizedYear.year());
        this.AdmissionTab5Form.controls['date_' + index].setValue(ctrlValue);
        // this.date.setValue(ctrlValue);
    }
    chosenMonthHandler(normalizedMonth, datepicker, index) {
        const ctrlValue = this.date.value;
        ctrlValue.month(normalizedMonth.month());
        this.AdmissionTab5Form.controls['date_' + index].setValue(ctrlValue);
        // this.date.setValue(ctrlValue);
        datepicker.close();
    }
    ngOnInit() {
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.fees_approved_accepted_by_name = currentUser.first_name + ' ' + currentUser.last_name;
        let student = localStorage.getItem('studentid');
        console.log(student);
        if (student == null) {
            console.log(1);
        }
        else {
            this.isLoading = true;
            this.Tab1(student);
            this.Tab2(student);
            this.Tab3(student);
            this.Tab4(student);
            this.Tab5(student);
            console.log('aaa');
            console.log(this.nationality);
            this.Enquirydisable = false;
            this.EnquirydisableCreate = true;
        }
        // this.index = localStorage.getItem('tabindex')
        // if(this.index == 1){
        // 	this.isLoading = true
        // 	this.admissionForm1disable = false
        // 	this.Tab1(student)
        // 	this.Tab2(student)
        // 	this.Tab3(student)
        // 	this.Tab4(student)
        // 	this.Tab5(student)
        // }
        // if(this.index == 2){
        // 	this.isLoading = true
        // 	this.admissionForm1disable = false
        // 	this.admissionForm2disable = false
        // 	this.Tab1(student)
        // 	this.Tab2(student)
        // 	this.Tab3(student)
        // 	this.Tab4(student)
        // 	this.Tab5(student)
        // }
        // if(this.index == 3){
        // 	this.isLoading = true
        // 	this.admissionForm1disable = false
        // 	this.admissionForm2disable = false
        // 	this.admissionForm3disable = false
        // 	this.Tab1(student)
        // 	this.Tab2(student)
        // 	this.Tab3(student)
        // 	this.Tab4(student)
        // 	this.Tab5(student)
        // }
        // if(this.index == 4){
        // 	this.isLoading = true
        // 	this.admissionForm1disable = false
        // 	this.admissionForm2disable = false
        // 	this.admissionForm3disable = false
        // 	this.admissionForm4disable = false
        // 	this.Tab1(student)
        // 	this.Tab2(student)
        // 	this.Tab3(student)
        // 	this.Tab4(student)
        // 	this.Tab5(student)
        // }
        // if(this.index == 5){
        // 	this.isLoading = true
        // 	this.admissionForm1disable = false
        // 	this.admissionForm2disable = false
        // 	this.admissionForm3disable = false
        // 	this.admissionForm4disable = false
        // 	this.admissionForm5disable = false
        // 	this.Tab1(student)
        // 	this.Tab2(student)
        // 	this.Tab3(student)
        // 	this.Tab4(student)
        // 	this.Tab5(student)
        // }
        if (localStorage.getItem('enquiryNO') == null) {
        }
        else {
            this.getInquiryDetails(localStorage.getItem('enquiryNO'));
        }
        this.optionsSelect = {
            placeholder: "Select Enquiry",
            allowClear: true,
            width: "100%",
        };
        this.academicyearid = '2';
        this.ngpayment_mode = 'Cash';
        this.AdmissionTabForm = this.formBuilder.group({
            inquiryid: [''],
            admissiondate: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]],
            academicyearid: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]],
            enrollmentno: [''],
            categoryid: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]],
            courseid: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]],
            batchid: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]],
            programmestartdate: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]],
            programmeenddate: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]],
            studentfirstname: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]],
            studentlastname: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]],
            dob: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]],
            ageon: [''],
            nationality: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]],
            religion: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]],
            mothertongue: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]],
            otherlanguages: [''],
            attendedpreschoolbefore: [''],
            preschoolname: [''],
            dobyear: [''],
            dobmonth: ['']
        });
        this.AdmissionTab1Form = this.formBuilder.group({
            fathername: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]],
            mothername: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]],
            fatherprof: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]],
            motherprof: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]],
            fatherlanguages: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]],
            motherlanguages: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]],
            fathernationality: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]],
            mothernationality: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]],
            sibling1name: [''],
            sibling2name: [''],
            sibling1id: [''],
            sibling2id: [''],
            // sibling1dob : [''],
            // sibling2dob : [''],
            // sibling1school : [''],
            // sibling2school : [''],
            sibling1enrollment: [''],
            sibling2enrollment: [''],
        });
        this.AdmissionTab2Form = this.formBuilder.group({
            presentaddress: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]],
            countryid: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]],
            stateid: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]],
            cityid: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]],
            pincode: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]],
            fatheremailid: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]],
            motheremailid: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]],
            fathermobilecontactno: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]],
            mothermobilecontactno: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]],
            fatherhomecontactno: [''],
            motherhomecontactno: [''],
            fatherofficecontactno: [''],
            motherofficecontactno: [''],
            emergencycontactname: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]],
            emergencycontactrelationship: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]],
            emergencycontactmobileno: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]],
            emergencycontacttelno: ['']
        });
        this.AdmissionTab3Form = this.formBuilder.group({
            authperson1: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]],
            authperson1relation: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]],
            authperson1gender: [''],
            authperson2: [''],
            authperson2relation: [''],
            authperson2gender: [''],
            authperson3: [''],
            authperson3relation: [''],
            authperson3gender: [''],
        });
        this.AdmissionTab4Form = this.formBuilder.group({
            duly_filled_admission_form: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]],
            birth_certificate: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]],
            profile_pic: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]],
            family_photo: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]],
            address_proof: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]],
            duly_filled_child_profile_form: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]],
        });
        this.AdmissionTab5Form = this.formBuilder.group({
            feestype: [''],
            fees_level: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]],
            discount_amount: [''],
            total_amount: [''],
            fees_total_amount: [''],
            fees_id: [''],
            feepaymenttype: [''],
            fees_amount_collected: [''],
            fees_remaining_amount: [''],
            fees_remark: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]],
            fees_approved_accepted_by_name: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]],
            payment_mode: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]],
            cheque_no: [''],
            bank_name: [''],
            transaction_id: [''],
            noofinstallment: [''],
            maxinstallmentallowed: [''],
            installment_collected_amount: [''],
            installment_remaining_amount: [''],
            installment_due_date: [''],
            fees: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]],
            transaction_date: [''],
        });
        // for(var i = 0; i < this.noofinstallment; i++) {
        //   <FormArray>this.AdmissionTab5Form.get('installment').push(new FormControl("date_"+i));
        // }
        this.getInquiries();
        this.getAcademicYears();
        this.getEnrollmentNo();
        this.getCategories();
        this.getCountries();
        this.getFeesLevel();
        this.route.params.subscribe(params => {
            if (this.route.snapshot.params.studentid) {
                this.Enquirydisable = false;
                this.isLoading = true;
                this.admissiondateDisable = true;
                this.EnquirydisableCreate = true;
                this.Tab1(this.route.snapshot.params.studentid);
                this.Tab2(this.route.snapshot.params.studentid);
                this.Tab3(this.route.snapshot.params.studentid);
                this.Tab4(this.route.snapshot.params.studentid);
                this.Tab5(this.route.snapshot.params.studentid);
                this.Tab6(this.route.snapshot.params.studentid);
            }
        });
    }
    Tab1(studentid) {
        this.commonService.getAdmissionTab1(studentid)
            .subscribe(data => {
            this.isLoading = false;
            if (data['status_code'] == 200) {
                this.inquiryId = data['body'][0].inquiry_master_id;
                this.commonService.getInquiryDetails(this.inquiryId).subscribe((response) => {
                    if (response['status_code'] == 200) {
                        this.inquiries1 = response['body'][0]['lead_no'][0]['inquiry_no'] + ' (' + data['body'][0].student_first_name + ' ' + data['body'][0].student_last_name + ')';
                    }
                    else {
                    }
                });
                this.admissiondate = data['body'][0].Student_details[0].admission_date;
                this.admissiondate1 = data['body'][0].Student_details[0].admission_date.split('-');
                this.ngxadmissiondate = {
                    date: {
                        year: parseInt(this.admissiondate1[2]),
                        month: parseInt(this.admissiondate1[1]),
                        day: parseInt(this.admissiondate1[0])
                    },
                };
                this.academicyearid = data['body'][0].Student_details[0].academic_year_id;
                this.enrollmentno = data['body'][0].enrollment_no;
                this.categoryId = data['body'][0].Student_details[0].category_id;
                this.getCourses(this.categoryId);
                this.courseId = data['body'][0].Student_details[0].course_id;
                localStorage.setItem('category', this.categoryId);
                localStorage.setItem('course', this.courseId);
                this.getCenterUserBatches(this.categoryId, this.courseId);
                this.batchId = data['body'][0].Student_details[0].batch_id;
                this.programmestartdate = data['body'][0].Student_details[0].programme_start_date;
                this.programmestartdate1 = data['body'][0].Student_details[0].programme_start_date.split('-');
                this.ngxprogramstartdate = {
                    date: {
                        year: parseInt(this.programmestartdate1[2]),
                        month: parseInt(this.programmestartdate1[1]),
                        day: parseInt(this.programmestartdate1[0])
                    },
                };
                this.programmeenddate = data['body'][0].Student_details[0].programme_end_date;
                this.programmeenddate1 = data['body'][0].Student_details[0].programme_end_date.split('-');
                this.ngxprogramenddate = {
                    date: {
                        year: parseInt(this.programmeenddate1[2]),
                        month: parseInt(this.programmeenddate1[1]),
                        day: parseInt(this.programmeenddate1[0])
                    },
                };
                this.studentfirstname = data['body'][0].student_first_name;
                this.studentlastname = data['body'][0].student_last_name;
                this.dob = data['body'][0].dob;
                this.dob1 = data['body'][0].dob.split('-');
                this.ngxstudentdob = {
                    date: {
                        year: parseInt(this.dob1[0]),
                        month: parseInt(this.dob1[1]),
                        day: parseInt(this.dob1[2])
                    },
                };
                this.ageon = data['body'][0].age_on;
                this.ageon1 = data['body'][0].age_on.split('-');
                this.ngxageon = {
                    date: {
                        year: parseInt(this.ageon1[2]),
                        month: parseInt(this.ageon1[1]),
                        day: parseInt(this.ageon1[0])
                    },
                };
                this.dobyear = data['body'][0].dob_year;
                this.dobmonth = data['body'][0].dob_month;
                this.nationality = data['body'][0].nationality;
                if (this.nationality != '') {
                    console.log(1);
                    this.admissionForm1disable = false;
                }
                this.religion = data['body'][0].religion;
                this.mothertongue = data['body'][0].mother_tongue;
                this.otherlanguages = data['body'][0].other_languages;
                if (data['body'][0].has_attended_preschool_before == 'Yes') {
                    this.attendedpreschoolbefore = true;
                }
                else {
                    this.attendedpreschoolbefore = false;
                }
                this.preschoolname = data['body'][0].preschool_name;
                this.studentId = studentid;
                if (this.route.snapshot.params.studentid) {
                    this.admissionForm1disable = false;
                    this.admissionForm2disable = false;
                    this.admissionForm3disable = false;
                    this.admissionForm5disable = false;
                }
            }
        });
    }
    Tab2(studentid) {
        this.commonService.getAdmissionTab2(studentid)
            .subscribe(data => {
            this.isLoading = false;
            if (data['status_code'] == 200) {
                this.fathername = data['body'][0].father_name;
                if (this.fathername == '') {
                    this.getInquiryDetails(localStorage.getItem('enquiryNO'));
                }
                this.mothername = data['body'][0].mother_name;
                this.fatherprof = data['body'][0].father_prof;
                this.motherprof = data['body'][0].mother_prof;
                this.fatherlanguages = data['body'][0].father_languages;
                if (this.fatherlanguages != '') {
                    this.admissionForm2disable = false;
                }
                this.motherlanguages = data['body'][0].mother_languages;
                this.fathernationality = data['body'][0].father_nationality;
                this.mothernationality = data['body'][0].mother_nationality;
                this.sibling1name = data['body'][0].sibling1_name;
                this.sibling2name = data['body'][0].sibling2_name;
                this.sibling1id = data['body'][0].sibling1_id;
                this.sibling2id = data['body'][0].sibling2_id;
                // 	this.sibling1name = data['body'][0].sibling1_name;
                // 	this.sibling2name = data['body'][0].sibling2_name;
                // 	this.sibling1dob = data['body'][0].sibling1_dob; 
                // 	if(data['body'][0].sibling1_dob != undefined){
                // this.sibling1dob1=data['body'][0].sibling1_dob.split('-');
                // 	this.ngxsibling1dob = {  
                //                  date: {  
                //                      year: parseInt(this.sibling1dob1[2]),  
                //                      month: parseInt(this.sibling1dob1[1]),  
                //                      day: parseInt(this.sibling1dob1[0])
                //                  },
                //              }
                //         	} 
                // 	if(data['body'][0].sibling2_dob != undefined){
                //              this.sibling2dob = data['body'][0].sibling2_dob; 
                // 	this.sibling2dob1=data['body'][0].sibling2_dob.split('-');
                // 	this.ngxsibling2dob = {  
                //                  date: {  
                //                      year: parseInt(this.sibling2dob1[2]),  
                //                      month: parseInt(this.sibling2dob1[1]),  
                //                      day: parseInt(this.sibling2dob1[0])
                //                  },
                //              }
                //          }
                // 	this.sibling1school = data['body'][0].sibling1_school;
                // 	this.sibling2school = data['body'][0].sibling2_school;
                this.studentId = studentid;
            }
        });
    }
    Tab3(studentid) {
        this.commonService.getAdmissionTab3(studentid)
            .subscribe(data => {
            this.isLoading = false;
            if (data['status_code'] == 200) {
                this.presentaddress = data['body'][0].present_address;
                if (this.presentaddress == '') {
                    this.getInquiryDetails(localStorage.getItem('enquiryNO'));
                }
                this.countryId = data['body'][0].country_id;
                this.getState(this.countryId);
                this.stateId = data['body'][0].state_id;
                this.getCity(this.stateId);
                this.cityId = data['body'][0].city_id;
                this.pincode = data['body'][0].pincode;
                this.fatheremailid = data['body'][0].father_email_id;
                this.motheremailid = data['body'][0].mother_email_id;
                this.fathermobilecontactno = data['body'][0].father_mobile_contact_no;
                this.mothermobilecontactno = data['body'][0].mother_mobile_contact_no;
                this.fatherhomecontactno = data['body'][0].father_home_contact_no;
                this.motherhomecontactno = data['body'][0].mother_home_contact_no;
                this.fatherofficecontactno = data['body'][0].father_office_contact_no;
                this.motherofficecontactno = data['body'][0].mother_office_contact_no;
                this.emergencycontactname = data['body'][0].emergency_contact_name;
                if (this.emergencycontactname != '') {
                    this.admissionForm3disable = false;
                }
                this.emergencycontactrelationship = data['body'][0].emergency_contact_relationship;
                this.emergencycontacttelno = data['body'][0].emergency_contact_tel_no;
                this.emergencycontactmobileno = data['body'][0].emergency_contact_mobile_no;
                this.studentId = studentid;
            }
        });
    }
    Tab4(studentid) {
        this.commonService.getAdmissionTab4(studentid)
            .subscribe(data => {
            this.isLoading = false;
            if (data['status_code'] == 200) {
                this.authperson1 = data['body'][0].auth_person1_to_collect;
                if (this.authperson1 != '') {
                    this.admissionForm4disable = false;
                }
                this.authperson1relation = data['body'][0].auth_person1_to_collect_relation;
                if (data['body'][0].auth_person1_to_collect_gender == 'Male') {
                    this.authperson1gender = true;
                }
                else {
                    this.authperson1gender = false;
                }
                this.authperson2 = data['body'][0].auth_person2_to_collect;
                this.authperson2relation = data['body'][0].auth_person2_to_collect_relation;
                if (data['body'][0].auth_person2_to_collect_gender == 'Male') {
                    this.authperson2gender = true;
                }
                else {
                    this.authperson2gender = false;
                }
                this.authperson3 = data['body'][0].auth_person3_to_collect;
                this.authperson3relation = data['body'][0].auth_person3_to_collect_relation;
                if (data['body'][0].auth_person3_to_collect_gender == 'Male') {
                    this.authperson3gender = true;
                }
                else {
                    this.authperson3gender = false;
                }
                this.studentId = studentid;
            }
        });
    }
    Tab5(studentid) {
        this.commonService.getAdmissionDocs(studentid)
            .subscribe(data => {
            this.isLoading = false;
            if (data['status_code'] == 200) {
                this.admission_form_path = data['body'][0].duly_filled_admission_form_path;
                if (this.admission_form_path != '') {
                    this.admissionForm5disable = false;
                }
                this.birth_certificate_path = data['body'][0].birth_certificate_path;
                this.profile_pic_path = data['body'][0].profile_pic_path;
                this.family_photo_path = data['body'][0].family_photo_path;
                this.address_proof_path = data['body'][0].address_proof_path;
                this.profile_form_path = data['body'][0].duly_filled_child_profile_form_path;
                this.duly_filled_admission_form = data['body'][0].duly_filled_admission_form;
                this.birth_certificate = data['body'][0].birth_certificate;
                this.profile_pic = data['body'][0].profile_pic;
                this.family_photo = data['body'][0].family_photo;
                this.address_proof = data['body'][0].address_proof;
                this.duly_filled_child_profile_form = data['body'][0].duly_filled_child_profile_form;
                if (this.authperson1 != '') {
                    this.admissionForm4disable = false;
                }
                this.studentId = studentid;
            }
        });
    }
    Tab6(studentid) {
        let feestype = 'Class';
        let category = localStorage.getItem('category');
        let course = localStorage.getItem('course');
        this.commonService.getStudentPaymentDetails(studentid, feestype, category, course)
            .subscribe(data => {
            this.isLoading = false;
            if (data['status_code'] == 200) {
                console.log(1);
                this.displaypayFees = true;
            }
            else {
                console.log(2);
                this.displaypayFees = false;
            }
        });
    }
    getInquiries() {
        this.commonService.getInquiries().subscribe((response) => {
            if (response['status_code'] == 200) {
                var arr = [];
                var arr_id = [];
                for (var i = 0; i < response['body'].length; i++) {
                    var no = response['body'][i].inquiry_no;
                    var first_name = response['body'][i].student_first_name;
                    var last_name = response['body'][i].student_last_name;
                    arr.push(no + ' (' + first_name + ' ' + last_name + ' )');
                }
                this.inquiries = response['body'];
            }
            else {
                this.snackbar.open(response['msg'], '', {
                    duration: 500,
                    verticalPosition: "top",
                    panelClass: ['bg-red']
                });
            }
        });
    }
    onadmissionDateChanged(event) {
        if (event.formatted !== '') {
            this.admissiondate = event.formatted;
        }
    }
    ontransactionDateChanged(event) {
        if (event.formatted !== '') {
            this.transaction_date = event.formatted;
        }
    }
    onprogramStartDateChanged(event) {
        if (event.formatted !== '') {
            this.programmestartdate = event.formatted;
        }
        if (this.programmeenddate != '') {
            let start = this.programmestartdate.split('-');
            let start_date = start[1] + '-' + start[0] + '-' + start[2];
            let end = this.programmeenddate.split('-');
            let end_date = end[1] + '-' + end[0] + '-' + end[2];
            if (new Date(start_date) > new Date(end_date)) {
                this.snackbar.open('Program Start Date less than Program End Date', '', {
                    duration: 500,
                    verticalPosition: "top",
                    panelClass: ['bg-red']
                });
                return;
            }
            else {
                this.calcDate(this.programmestartdate, this.dob);
            }
        }
        else {
            this.calcDate(this.programmestartdate, this.dob);
        }
    }
    onprogramEndDateChanged(event) {
        if (this.programmestartdate == '') {
            this.snackbar.open('Select Program Start date', '', {
                duration: 500,
                verticalPosition: "top",
                panelClass: ['bg-red']
            });
            return;
        }
        else {
            if (event.formatted !== '') {
                this.programmeenddate = event.formatted;
            }
            let start = this.programmestartdate.split('-');
            let start_date = start[1] + '-' + start[0] + '-' + start[2];
            let end = this.programmeenddate.split('-');
            let end_date = end[1] + '-' + end[0] + '-' + end[2];
            if (new Date(start_date) > new Date(end_date)) {
                this.snackbar.open('Program Start Date less than Program End Date', '', {
                    duration: 500,
                    verticalPosition: "top",
                    panelClass: ['bg-red']
                });
                return;
            }
            // else{
            //     this.calcDate(this.programmeenddate,this.dob);	
            // }
        }
    }
    // onsiblingdob1Changed(event: IMyDateModel): void 
    // {
    //     if(event.formatted !== '') {
    //         this.sibling1dob = event.formatted
    //     }
    // }
    // onsiblingdob2Changed(event: IMyDateModel): void 
    // {
    //     if(event.formatted !== '') {
    //         this.sibling2dob = event.formatted
    //     }
    // }
    // onageOnChanged(event: IMyDateModel): void 
    // {
    //     if(event.formatted !== '') {
    //         this.ageon = event.formatted
    //     }
    //     this.calcDate(this.ageon,this.dob);
    // }
    calcDate(ageOn, dob) {
        if (ageOn != undefined && dob != undefined) {
            var ageOnsplit = ageOn.split('-');
            var ageOndate = ageOnsplit[1] + '-' + ageOnsplit[0] + '-' + ageOnsplit[2];
            var dobsplit = dob.split('-');
            var firstsplitlength = dobsplit[0].length;
            if (firstsplitlength == 4) {
                var dobdate = dobsplit[1] + '-' + dobsplit[2] + '-' + dobsplit[0];
            }
            else {
                var dobdate = dobsplit[1] + '-' + dobsplit[0] + '-' + dobsplit[2];
            }
            var diff = Math.abs(new Date(ageOndate).getTime() - new Date(dobdate).getTime());
            var day = 1000 * 60 * 60 * 24;
            var days = Math.ceil(diff / day);
            this.dobmonth = Math.floor(days / 30);
            this.dobyear = Math.floor(this.dobmonth / 12);
            if (this.dobyear > 0) {
                var yeardays = days - (365 * this.dobyear);
                let dobmonthfloor = Math.floor(yeardays / 30);
                this.dobmonth = Math.abs(dobmonthfloor);
                // if(this.dobmonth < 0){
                // }
                // else{
                // 	this.dobmonth = Math.floor(yeardays/30);
                // }
            }
        }
    }
    getAcademicYears() {
        this.commonService.getAcademicYears().subscribe((response) => {
            if (response['status_code'] == 200) {
                this.academicyears = response['body'];
            }
            else {
                this.snackbar.open(response['msg'], '', {
                    duration: 500,
                    verticalPosition: "top",
                    panelClass: ['bg-red']
                });
            }
        });
    }
    getEnrollmentNo() {
        this.commonService.getEnrollmentNo().subscribe((response) => {
            if (response['status_code'] == 200) {
                this.enrollmentno = response['body'];
            }
            else {
                this.snackbar.open(response['msg'], '', {
                    duration: 500,
                    verticalPosition: "top",
                    panelClass: ['bg-red']
                });
            }
        });
    }
    getCategories() {
        this.commonService.getCategories().subscribe((response) => {
            if (response['status_code'] == 200) {
                this.category = response['body'];
            }
            else {
                this.snackbar.open(response['msg'], '', {
                    duration: 500,
                    verticalPosition: "top",
                    panelClass: ['bg-red']
                });
            }
        });
    }
    getCourses(categoryid) {
        this.commonService.getCourses(categoryid).subscribe((response) => {
            if (response['status_code'] == 200) {
                this.courses = response['body'];
            }
            else {
                this.snackbar.open(response['msg'], '', {
                    duration: 500,
                    verticalPosition: "top",
                    panelClass: ['bg-red']
                });
            }
        });
    }
    getCenterUserBatches(categoryid, courseid) {
        this.commonService.getCenterUserBatches(categoryid, courseid).subscribe((response) => {
            if (response['status_code'] == 200) {
                this.batches = response['body'];
            }
            else {
                this.snackbar.open(response['msg'], '', {
                    duration: 500,
                    verticalPosition: "top",
                    panelClass: ['bg-red']
                });
            }
        });
    }
    getFeesLevel() {
        this.commonService.getFeesLevel().subscribe((response) => {
            if (response['status_code'] == 200) {
                this.feesLevels = response['body'];
            }
            else {
                this.snackbar.open(response['msg'], '', {
                    duration: 500,
                    verticalPosition: "top",
                    panelClass: ['bg-red']
                });
            }
        });
    }
    getCenterFees(fees_level_id, name) {
        let feesselectiontype = 'Class';
        this.commonService.getCenterFees(fees_level_id, this.categoryId, this.courseId, feesselectiontype).subscribe((response) => {
            if (response['status_code'] == 200) {
                if (name == 'level') {
                    this.Fees = response['body'];
                }
                if (name == 'fees') {
                    this.fees_total_amount = response['body'][0]['fees_total_amount'];
                    this.fees_id = response['body'][0]['fees_id'];
                    this.fees_amount_collected = response['body'][0]['fees_total_amount'];
                    this.fees_remaining_amount = '0';
                    this.maxinstallmentallowed = response['body'][0]['max_installment_allowed'];
                }
            }
            else {
                this.snackbar.open(response['msg'], '', {
                    duration: 500,
                    verticalPosition: "top",
                    panelClass: ['bg-red']
                });
                this.fees_total_amount = '';
            }
        });
    }
    onDobChanged(event) {
        if (event.formatted !== '') {
            this.dob = event.formatted;
        }
        this.calcDate(this.programmestartdate, this.dob);
    }
    getCountries() {
        this.commonService.getCountries().subscribe((response) => {
            if (response['status_code'] == 200) {
                this.countries = response['body'];
            }
            else {
                this.snackbar.open(response['msg'], '', {
                    duration: 500,
                    verticalPosition: "top",
                    panelClass: ['bg-red']
                });
            }
        });
    }
    getState(countryid) {
        this.commonService.getState(countryid).subscribe((response) => {
            if (response['status_code'] == 200) {
                this.states = response['body'];
            }
            else {
                this.snackbar.open(response['msg'], '', {
                    duration: 500,
                    verticalPosition: "top",
                    panelClass: ['bg-red']
                });
            }
        });
    }
    getCity(stateid) {
        this.commonService.getCity(stateid).subscribe((response) => {
            if (response['status_code'] == 200) {
                this.cities = response['body'];
            }
            else {
                this.snackbar.open(response['msg'], '', {
                    duration: 500,
                    verticalPosition: "top",
                    panelClass: ['bg-red']
                });
            }
        });
    }
    getInquiryDetails(inquiryid) {
        if (inquiryid != null) {
            if (localStorage.getItem('studentid') == null) {
                this.getEnquiry = inquiryid.value;
                localStorage.setItem('enquiryNO', this.getEnquiry);
            }
            else {
                this.getEnquiry = inquiryid;
            }
            this.commonService.getInquiryDetails(this.getEnquiry).subscribe((response) => {
                this.isLoading = false;
                if (response['status_code'] == 200) {
                    this.inquiryId = response['body'][0].inquiry_master_id;
                    this.academicyearid = response['body'][0].academic_year_master_id;
                    this.categoryId = response['body'][0].category_id;
                    this.getCourses(this.categoryId);
                    this.courseId = response['body'][0].course_id;
                    this.getCenterUserBatches(this.categoryId, this.courseId);
                    this.studentfirstname = response['body'][0].student_first_name;
                    this.studentlastname = response['body'][0].student_last_name;
                    this.dob = response['body'][0].student_dob;
                    this.dob1 = response['body'][0].student_dob.split('-');
                    this.ngxstudentdob = {
                        date: {
                            year: parseInt(this.dob1[2]),
                            month: parseInt(this.dob1[1]),
                            day: parseInt(this.dob1[0])
                        },
                    };
                    if (response['body'][0].has_attended_preschool_before == 'Yes') {
                        this.attendedpreschoolbefore = true;
                    }
                    else {
                        this.attendedpreschoolbefore = false;
                    }
                    this.preschoolname = response['body'][0].preschool_name;
                    this.fatheremailid = response['body'][0].father_emailid;
                    this.motheremailid = response['body'][0].mother_emailid;
                    this.fathername = response['body'][0].father_name;
                    this.mothername = response['body'][0].mother_name;
                    this.fatherprof = response['body'][0].father_profession;
                    this.motherprof = response['body'][0].mother_profession;
                    this.presentaddress = response['body'][0].present_address;
                    this.countryId = response['body'][0].country_id;
                    this.getState(this.countryId);
                    this.stateId = response['body'][0].state_id;
                    this.getCity(this.stateId);
                    this.cityId = response['body'][0].city_id;
                    this.pincode = response['body'][0].pincode;
                    this.fathermobilecontactno = response['body'][0].father_contact_no;
                    this.mothermobilecontactno = response['body'][0].mother_contact_no;
                    this.calcDate(this.programmestartdate, this.dob);
                }
                else {
                }
            });
            // this.commonService.getInquiryId(this.getEnquiry).subscribe((response)=>{
            // 	let getInquiryId = response['body']['query_result'][0]['inquiry_master_id']
            // 	console.log(getInquiryId)
            // })
        }
    }
    feesPaymentTypeChange(feepaymenttype) {
        if (this.feepaymenttype == true) {
        }
        else {
            this.fees_amount_collected = this.fees_total_amount;
            this.fees_remaining_amount = '0';
        }
    }
    installmentnoChange(no) {
        if (no > this.maxinstallmentallowed) {
            this.snackbar.open('Installment No less then MaxInsatllment Allowed', '', {
                duration: 500,
                verticalPosition: "top",
                panelClass: ['bg-red']
            });
            this.isLoading = false;
            this.AdmissionTab5Form.get('noofinstallment').setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]);
            this.AdmissionTab5Form.get('noofinstallment').updateValueAndValidity();
        }
        else {
            this.AdmissionTab5Form.get('noofinstallment').clearValidators();
            this.AdmissionTab5Form.get('noofinstallment').updateValueAndValidity();
            if (no == null) {
                this.installment = [];
            }
            else {
                let num = no;
                for (var i = 0; i < no; i++) {
                    this.installment.push(i);
                    if (this.total_amount == '' || this.total_amount == undefined) {
                        this.installment_amount = (this.fees_total_amount / num);
                    }
                    else {
                        this.installment_amount = (this.total_amount / num);
                    }
                }
            }
        }
    }
    discountChange(discount_amount) {
        if (this.fees_total_amount == undefined) {
            this.snackbar.open('Select Fees Level', '', {
                duration: 500,
                verticalPosition: "top",
                panelClass: ['bg-red']
            });
            this.isLoading = false;
            this.AdmissionTab5Form.get('discount_amount').setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]);
            this.AdmissionTab5Form.get('discount_amount').updateValueAndValidity();
        }
        else {
            this.AdmissionTab5Form.get('discount_amount').clearValidators();
            this.AdmissionTab5Form.get('discount_amount').updateValueAndValidity();
            this.total_amount = this.fees_total_amount - discount_amount;
            this.installment_amount = (this.total_amount / this.noofinstallment).toFixed(2);
        }
    }
    collectedinstallmentChange(index) {
        let collected_amount = Number($('#id_' + index).val());
        let amount = Number(this.installment_amount);
        if (Number(collected_amount) > Number(amount)) {
            this.snackbar.open('Paying Amount grater than installment amount', '', {
                duration: 500,
                verticalPosition: "top",
                panelClass: ['bg-red']
            });
            this.isLoading = false;
            $('#remaining_id_' + index).val(amount);
        }
        else {
            this.installment_remaining_amount = (amount - collected_amount);
            $('#remaining_id_' + index).val(this.installment_remaining_amount);
            this.AdmissionTab5Form.controls['installment_collected_amount'].setErrors(null);
            $('#amounterror_' + index).text('');
        }
    }
    monthChange(index) {
        this.AdmissionTab5Form.controls['installment_due_date'].setErrors(null);
        $('#montherror_' + index).text('');
    }
    initOptionsMap() {
        for (var x = 0; x < this.payment_mode.length; x++) {
            this.optionsMap[this.payment_mode[x]] = false;
        }
    }
    updatePaymentOptions(option, event) {
        this.optionsMap[option] = event.target.checked;
        this.selectedOptions.push(option);
        if (option == 'Cheque' && event.target.checked == true) {
            $('.cheque_no').show();
        }
        else {
            $('.cheque_no').hide();
        }
        if (option == 'Netbanking' && event.target.checked == true) {
            $('.transaction_no').show();
        }
        else {
            $('.transaction_no').hide();
        }
        if ((option == 'Cheque' && event.target.checked == true) || (option == 'Netbanking' && event.target.checked == true)) {
            $('.bank_name').show();
        }
        else {
            $('.bank_name').hide();
        }
    }
    updateOptions() {
        for (var x in this.selectedOptions) {
            if (this.selectedOptions[x]) {
                this.optionsChecked.push(x);
            }
        }
        this.payment_mode1 = this.optionsChecked;
        this.optionsChecked = [];
    }
    sibling1enrollChnage(siblingenrollmentno) {
        if (siblingenrollmentno.length >= 6) {
            this.commonService.getStudentsWithEnrollmentNo(siblingenrollmentno).subscribe((response) => {
                if (response['status_code'] == 200) {
                    this.sibling1name = response['body'][0]['student_first_name'] + ' ' + response['body'][0]['student_last_name'];
                    this.sibling1id = response['body'][0]['student_id'];
                }
                else {
                    this.snackbar.open(response['msg'], '', {
                        duration: 500,
                        verticalPosition: "top",
                        panelClass: ['bg-red']
                    });
                }
            });
        }
    }
    sibling2enrollChnage(siblingenrollmentno) {
        if (siblingenrollmentno.length >= 6) {
            this.commonService.getStudentsWithEnrollmentNo(siblingenrollmentno).subscribe((response) => {
                if (response['status_code'] == 200) {
                    this.sibling2name = response['body'][0]['student_first_name'] + ' ' + response['body'][0]['student_last_name'];
                    this.sibling2id = response['body'][0]['student_id'];
                }
                else {
                    this.snackbar.open(response['msg'], '', {
                        duration: 500,
                        verticalPosition: "top",
                        panelClass: ['bg-red']
                    });
                }
            });
        }
    }
    admissionform1(admission) {
        console.log(admission.index);
        if (admission.index == 0) {
            this.Tab1(this.studentId);
            if (this.studentId != '' || this.studentId != null || this.studentId != undefined) {
                this.Enquirydisable = false;
                this.EnquirydisableCreate = true;
            }
        }
        if (admission.index == 1) {
            this.Tab2(this.studentId);
        }
        if (admission.index == 2) {
            this.Tab3(this.studentId);
        }
        if (admission.index == 3) {
            this.Tab4(this.studentId);
        }
        if (admission.index == 4) {
            $('#admissionForm').text(this.duly_filled_admission_form);
            $('#birthcerti').text(this.birth_certificate);
            $('#profilePic').text(this.profile_pic);
            $('#familyphoto').text(this.family_photo);
            $('#addressProof').text(this.address_proof);
            $('#profileForm').text(this.duly_filled_child_profile_form);
            this.Tab5(this.studentId);
        }
        if (admission.index == 5) {
            $('.cheque_no').hide();
            $('.transaction_no').hide();
            $('.bank_name').hide();
            // this.Tab5(this.studentId)
        }
        localStorage.setItem('tabindex', admission.index);
    }
    FormOnSubmit(formValue) {
        this.isLoading = true;
        this.AdmissionFormSubmitted = true;
        this.AdmissionTabForm.value.programmestartdate = this.programmestartdate;
        this.AdmissionTabForm.value.programmeenddate = this.programmeenddate;
        this.AdmissionTabForm.value.dob = this.dob;
        let start = this.programmestartdate.split('-');
        let start_date = start[1] + '-' + start[0] + '-' + start[2];
        let end = this.programmeenddate.split('-');
        let end_date = end[1] + '-' + end[0] + '-' + end[2];
        if (new Date(start_date) > new Date(end_date)) {
            this.isLoading = false;
            this.snackbar.open('Program Start Date less than Program End Date', '', {
                duration: 500,
                verticalPosition: "top",
                panelClass: ['bg-red']
            });
            return;
        }
        if (this.AdmissionTabForm.value.attendedpreschoolbefore === true) {
            this.isLoading = false;
            this.AdmissionTabForm.get('preschoolname').setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]);
            this.AdmissionTabForm.get('preschoolname').updateValueAndValidity();
        }
        else {
            this.AdmissionTabForm.get('preschoolname').clearValidators();
            this.AdmissionTabForm.get('preschoolname').updateValueAndValidity();
        }
        this.AdmissionTabForm.value.inquiryid = this.inquiryId;
        if (this.AdmissionTabForm.invalid) {
            this.isLoading = false;
            return;
        }
        this.commonService.addEditAdmissionTab1(this.AdmissionTabForm.value, this.studentId).subscribe((response) => {
            this.isLoading = false;
            if (response['status_code'] == 200) {
                this.admissionForm1disable = false;
                this.snackbar.open(response['Metadata']['Message'], '', {
                    duration: 500,
                    verticalPosition: "top",
                    panelClass: ['bg-green']
                });
                this.admissionForm1disable = false;
                this.selectedindex = 1;
                this.admissionFilter.enrollmentno = response.Data;
                this.commonService.listAdmissions((this.page), this.size, this.admissionFilter).subscribe((response) => {
                    if (response['status_code'] == 200) {
                        this.studentId = response['body']['query_result'][0].student_id;
                    }
                    if (this.route.snapshot.params.studentid) {
                    }
                    else {
                        localStorage.setItem('studentid', this.studentId);
                    }
                }, (err) => console.log("error", err), () => console.log());
            }
            else {
                this.snackbar.open(response['Metadata']['Message'], '', {
                    duration: 500,
                    verticalPosition: "top",
                    panelClass: ['bg-red']
                });
            }
        });
    }
    Form1OnSubmit(formValue) {
        this.isLoading = true;
        this.AdmissionForm1Submitted = true;
        // if(this.sibling1dob != undefined){
        // 	this.AdmissionTab1Form.value.sibling1dob = this.sibling1dob
        // }
        // else{
        // 	this.AdmissionTab1Form.value.sibling1dob = '00-00-0000'	
        // }
        // if(this.sibling2dob != undefined){
        // 	this.AdmissionTab1Form.value.sibling2dob = this.sibling2dob
        // }
        // else{
        // 	this.AdmissionTab1Form.value.sibling2dob = '00-00-0000'
        // }
        // if (this.AdmissionTab1Form.value.sibling1name != '') {
        //           console.log(1)
        //           this.AdmissionTab1Form.get('sibling1dob').setValidators([Validators.required]);
        //       	this.AdmissionTab1Form.get('sibling1dob').updateValueAndValidity();
        //           this.AdmissionTab1Form.get('sibling1school').setValidators([Validators.required]);
        //       	this.AdmissionTab1Form.get('sibling1school').updateValueAndValidity();
        //       }
        //       else{
        //       	console.log(2)
        // 	this.AdmissionTab1Form.get('sibling1dob').clearValidators();
        //       	this.AdmissionTab1Form.get('sibling1dob').updateValueAndValidity();
        // 	this.AdmissionTab1Form.get('sibling1school').clearValidators();
        //       	this.AdmissionTab1Form.get('sibling1school').updateValueAndValidity();
        //       }
        // if (this.AdmissionTab1Form.value.sibling2name != '') {
        //           console.log(3)
        //           this.AdmissionTab1Form.get('sibling2dob').setValidators([Validators.required]);
        //       	this.AdmissionTab1Form.get('sibling2dob').updateValueAndValidity();
        //           this.AdmissionTab1Form.get('sibling2school').setValidators([Validators.required]);
        //       	this.AdmissionTab1Form.get('sibling2school').updateValueAndValidity();
        //       }
        //       else{
        //       	console.log(4);
        // 	this.AdmissionTab1Form.get('sibling2dob').clearValidators();
        //       	this.AdmissionTab1Form.get('sibling2dob').updateValueAndValidity();
        // 	this.AdmissionTab1Form.get('sibling2school').clearValidators();
        //       	this.AdmissionTab1Form.get('sibling2school').updateValueAndValidity();
        //       }
        if (this.AdmissionTab1Form.invalid) {
            this.isLoading = false;
            return;
        }
        this.commonService.admissionTab2(this.AdmissionTab1Form.value, this.studentId).subscribe((response) => {
            this.isLoading = false;
            if (response['status_code'] == 200) {
                this.admissionForm2disable = false;
                this.snackbar.open(response['Metadata']['Message'], '', {
                    duration: 500,
                    verticalPosition: "top",
                    panelClass: ['bg-green']
                });
                this.admissionForm2disable = false;
                this.selectedindex = 2;
            }
            else {
                this.snackbar.open(response['Metadata']['Message'], '', {
                    duration: 500,
                    verticalPosition: "top",
                    panelClass: ['bg-red']
                });
            }
        });
    }
    Form2OnSubmit(formValue) {
        this.isLoading = true;
        this.AdmissionForm2Submitted = true;
        if (this.AdmissionTab2Form.invalid) {
            this.isLoading = false;
            return;
        }
        this.commonService.admissionTab3(this.AdmissionTab2Form.value, this.studentId).subscribe((response) => {
            this.isLoading = false;
            if (response['status_code'] == 200) {
                this.admissionForm3disable = false;
                this.snackbar.open(response['Metadata']['Message'], '', {
                    duration: 500,
                    verticalPosition: "top",
                    panelClass: ['bg-green']
                });
                this.admissionForm3disable = false;
                this.selectedindex = 3;
            }
            else {
                this.snackbar.open(response['Metadata']['Message'], '', {
                    duration: 500,
                    verticalPosition: "top",
                    panelClass: ['bg-red']
                });
            }
        });
    }
    Form3OnSubmit(formValue) {
        this.isLoading = true;
        this.AdmissionForm3Submitted = true;
        if (this.AdmissionTab3Form.invalid) {
            this.isLoading = false;
            return;
        }
        this.commonService.admissionTab4(this.AdmissionTab3Form.value, this.studentId).subscribe((response) => {
            this.isLoading = false;
            if (response['status_code'] == 200) {
                this.admissionForm4disable = false;
                this.snackbar.open(response['Metadata']['Message'], '', {
                    duration: 500,
                    verticalPosition: "top",
                    panelClass: ['bg-green']
                });
                this.admissionForm4disable = false;
                this.selectedindex = 4;
            }
            else {
                this.snackbar.open(response['Metadata']['Message'], '', {
                    duration: 500,
                    verticalPosition: "top",
                    panelClass: ['bg-red']
                });
            }
        });
    }
    Form4OnSubmit(formValue) {
        this.isLoading = true;
        this.AdmissionForm4Submitted = true;
        if (this.admissionFormSelFile == '') {
            this.AdmissionTab4Form.value.duly_filled_admission_form = this.duly_filled_admission_form;
        }
        else {
            this.AdmissionTab4Form.value.duly_filled_admission_form = this.admissionFormSelFile;
        }
        if (this.birthCerySelFile == '') {
            this.AdmissionTab4Form.value.birth_certificate = this.birth_certificate;
        }
        else {
            this.AdmissionTab4Form.value.birth_certificate = this.birthCerySelFile;
        }
        if (this.profilePicSelFile == '') {
            this.AdmissionTab4Form.value.profile_pic = this.profile_pic;
        }
        else {
            this.AdmissionTab4Form.value.profile_pic = this.profilePicSelFile;
        }
        if (this.familyPhotoSelFile == '') {
            this.AdmissionTab4Form.value.family_photo = this.family_photo;
        }
        else {
            this.AdmissionTab4Form.value.family_photo = this.familyPhotoSelFile;
        }
        if (this.addressProofSelFile == '') {
            this.AdmissionTab4Form.value.address_proof = this.address_proof;
        }
        else {
            this.AdmissionTab4Form.value.address_proof = this.addressProofSelFile;
        }
        if (this.profileFormSelFile == '') {
            this.AdmissionTab4Form.value.duly_filled_child_profile_form = this.duly_filled_child_profile_form;
        }
        else {
            this.AdmissionTab4Form.value.duly_filled_child_profile_form = this.profileFormSelFile;
        }
        if (this.AdmissionTab4Form.invalid && this.AdmissionTab4Form.value.duly_filled_admission_form == '' && this.AdmissionTab4Form.value.birth_certificate == '' && this.AdmissionTab4Form.value.profile_pic == '' && this.AdmissionTab4Form.value.family_photo == '' && this.AdmissionTab4Form.value.address_proof == '' && this.AdmissionTab4Form.value.duly_filled_child_profile_form == '') {
            this.isLoading = false;
            return;
        }
        this.commonService.addEditAdmissionDocs(this.AdmissionTab4Form.value, this.studentId).subscribe((response) => {
            this.isLoading = false;
            if (response['status_code'] == 200) {
                this.admissionForm5disable = false;
                localStorage.setItem('docsave', 'dacSave');
                this.snackbar.open(response['msg'], '', {
                    duration: 500,
                    verticalPosition: "top",
                    panelClass: ['bg-green']
                });
                if (this.route.snapshot.params.studentid) {
                    this.router.navigate(['/admissionlist']);
                }
                else {
                    this.admissionForm5disable = false;
                    this.selectedindex = 5;
                }
            }
            else {
                this.snackbar.open(response['Metadata']['Message'], '', {
                    duration: 500,
                    verticalPosition: "top",
                    panelClass: ['bg-red']
                });
            }
        });
    }
    Form5OnSubmit(formValue) {
        this.isLoading = true;
        this.AdmissionForm5Submitted = true;
        let collected_amount = [];
        let remainig_amount = [];
        let monthArray = [];
        let itemsArray = [];
        let installmentNo = [];
        if (formValue.noofinstallment == '') {
        }
        else {
            for (var i = 0; i < this.noofinstallment; i++) {
                this.isLoading = false;
                let amount = $('#id_' + i).val();
                if (amount == '') {
                    this.AdmissionTab5Form.controls['installment_collected_amount'].setErrors({ 'incorrect': true });
                    $('#amounterror_' + i).text('Enter amount');
                }
                else {
                    this.AdmissionTab5Form.controls['installment_collected_amount'].setErrors(null);
                    $('#amounterror_' + i).text('');
                }
                console.log(this.installment_amount);
                if (Number(amount) > Number(this.installment_amount)) {
                    this.snackbar.open('Paying Amount grater than installment amount', '', {
                        duration: 500,
                        verticalPosition: "top",
                        panelClass: ['bg-red']
                    });
                    return;
                }
                let remainig_amnt = $('#remaining_id_' + i).val();
                let month_no = $('#month_id_' + i).val();
                if (month_no == null) {
                    this.AdmissionTab5Form.controls['installment_due_date'].setErrors({ 'incorrect': true });
                    $('#montherror_' + i).text('Select Month');
                }
                else {
                    this.AdmissionTab5Form.controls['installment_due_date'].setErrors(null);
                    $('#montherror_' + i).text('');
                }
                collected_amount.push(amount);
                remainig_amount.push(remainig_amnt);
                monthArray.push(month_no);
            }
            installmentNo = [this.maxinstallmentallowed, this.noofinstallment];
            if (this.AdmissionTab5Form.value.noofinstallment > this.AdmissionTab5Form.value.maxinstallmentallowed) {
                this.snackbar.open('Installment No less then MaxInsatllment Allowed', '', {
                    duration: 500,
                    verticalPosition: "top",
                    panelClass: ['bg-red']
                });
                return;
            }
        }
        if (this.AdmissionTab5Form.value.feepaymenttype === true) {
            this.isLoading = false;
            this.AdmissionTab5Form.get('noofinstallment').setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]);
            this.AdmissionTab5Form.get('noofinstallment').updateValueAndValidity();
        }
        else {
            this.AdmissionTab5Form.get('noofinstallment').clearValidators();
            this.AdmissionTab5Form.get('noofinstallment').updateValueAndValidity();
        }
        if (this.AdmissionTab5Form.value.payment_mode === 'Cheque') {
            this.isLoading = false;
            this.AdmissionTab5Form.get('cheque_no').setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]);
            this.AdmissionTab5Form.get('cheque_no').updateValueAndValidity();
        }
        else {
            this.AdmissionTab5Form.get('cheque_no').clearValidators();
            this.AdmissionTab5Form.get('cheque_no').updateValueAndValidity();
        }
        if (this.AdmissionTab5Form.value.payment_mode === 'Netbanking') {
            this.isLoading = false;
            this.AdmissionTab5Form.get('transaction_id').setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]);
            this.AdmissionTab5Form.get('transaction_id').updateValueAndValidity();
            this.AdmissionTab5Form.get('transaction_date').setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]);
            this.AdmissionTab5Form.get('transaction_date').updateValueAndValidity();
        }
        else {
            this.AdmissionTab5Form.get('transaction_id').clearValidators();
            this.AdmissionTab5Form.get('transaction_id').updateValueAndValidity();
            this.AdmissionTab5Form.get('transaction_date').clearValidators();
            this.AdmissionTab5Form.get('transaction_date').updateValueAndValidity();
        }
        if (this.AdmissionTab5Form.value.payment_mode === 'Cheque' || this.AdmissionTab5Form.value.payment_mode === 'Netbanking') {
            this.AdmissionTab5Form.get('bank_name').setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]);
            this.AdmissionTab5Form.get('bank_name').updateValueAndValidity();
        }
        else {
            this.AdmissionTab5Form.get('bank_name').clearValidators();
            this.AdmissionTab5Form.get('bank_name').updateValueAndValidity();
        }
        this.AdmissionTab5Form.value.categoryid = this.categoryId;
        this.AdmissionTab5Form.value.courseid = this.courseId;
        this.AdmissionTab5Form.value.batchid = this.batchId;
        if (this.AdmissionTab5Form.value.payment_mode === '') {
            this.isLoading = false;
            this.AdmissionTab5Form.value.payment_mode = 'Cash';
        }
        if (this.AdmissionTab5Form.invalid) {
            this.isLoading = false;
            return;
        }
        this.commonService.payAdmissionFees(this.AdmissionTab5Form.value, this.studentId, collected_amount, remainig_amount, monthArray, installmentNo).subscribe((response) => {
            this.isLoading = false;
            if (response['status_code'] == 200) {
                localStorage.removeItem('studentid');
                localStorage.removeItem('tabindex');
                localStorage.removeItem('enquiryNO');
                localStorage.removeItem('docsave');
                localStorage.removeItem('category');
                localStorage.removeItem('course');
                this.snackbar.open(response['msg'], '', {
                    duration: 500,
                    verticalPosition: "top",
                    panelClass: ['bg-green']
                });
                window.open(response['body'], '_blank');
                this.router.navigate(['/admissionlist']);
            }
            else {
                this.snackbar.open(response['msg'], '', {
                    duration: 500,
                    verticalPosition: "top",
                    panelClass: ['bg-red']
                });
            }
        });
    }
    onadmissionFormUpload(event) {
        if ((event.target.files[0].type == "image/jpeg") || (event.target.files[0].type == "image/jpg") || (event.target.files[0].type == "image/png") || (event.target.files[0].type == "application/pdf")) {
            document.getElementById("admissionForm").innerHTML = event.target.files[0].name;
            this.admissionFormSelFile = event.target.files[0];
            this.isLoading = true;
            if (localStorage.getItem('docsave') != null || localStorage.getItem('docsave') != '' || localStorage.getItem('docsave') != undefined) {
                if (this.admissionFormSelFile == '') {
                    this.AdmissionTab4Form.value.duly_filled_admission_form = this.duly_filled_admission_form;
                }
                else {
                    this.AdmissionTab4Form.value.duly_filled_admission_form = this.admissionFormSelFile;
                }
                if (this.birthCerySelFile == '') {
                    this.AdmissionTab4Form.value.birth_certificate = this.birth_certificate;
                }
                else {
                    this.AdmissionTab4Form.value.birth_certificate = this.birthCerySelFile;
                }
                if (this.profilePicSelFile == '') {
                    this.AdmissionTab4Form.value.profile_pic = this.profile_pic;
                }
                else {
                    this.AdmissionTab4Form.value.profile_pic = this.profilePicSelFile;
                }
                if (this.familyPhotoSelFile == '') {
                    this.AdmissionTab4Form.value.family_photo = this.family_photo;
                }
                else {
                    this.AdmissionTab4Form.value.family_photo = this.familyPhotoSelFile;
                }
                if (this.addressProofSelFile == '') {
                    this.AdmissionTab4Form.value.address_proof = this.address_proof;
                }
                else {
                    this.AdmissionTab4Form.value.address_proof = this.addressProofSelFile;
                }
                if (this.profileFormSelFile == '') {
                    this.AdmissionTab4Form.value.duly_filled_child_profile_form = this.duly_filled_child_profile_form;
                }
                else {
                    this.AdmissionTab4Form.value.duly_filled_child_profile_form = this.profileFormSelFile;
                }
                this.commonService.addEditAdmissionDocs(this.AdmissionTab4Form.value, this.studentId).subscribe((response) => {
                    if (response['status_code'] == 200) {
                        if (this.route.snapshot.params.studentid) {
                            this.studentidpass = this.route.snapshot.params.studentid;
                        }
                        else {
                            this.studentidpass = localStorage.getItem('studentid');
                        }
                        this.commonService.getAdmissionDocs(this.studentidpass)
                            .subscribe(data => {
                            this.isLoading = false;
                            if (data['status_code'] == 200) {
                                this.admission_form_path = data['body'][0].duly_filled_admission_form_path;
                            }
                        });
                    }
                });
            }
        }
        else {
            this.snackbar.open("Invalid file type selected. Please select .png, *.jpg,.jpeg files", '', {
                duration: 2000,
                verticalPosition: "top",
                panelClass: ['bg-red']
            });
        }
    }
    onbirthCertyUpload(event) {
        if ((event.target.files[0].type == "image/jpeg") || (event.target.files[0].type == "image/jpg") || (event.target.files[0].type == "image/png") || (event.target.files[0].type == "application/pdf")) {
            document.getElementById("birthcerti").innerHTML = event.target.files[0].name;
            this.birthCerySelFile = event.target.files[0];
            this.isLoading = true;
            if (localStorage.getItem('docsave') != null || localStorage.getItem('docsave') != '' || localStorage.getItem('docsave') != undefined) {
                if (this.admissionFormSelFile == '') {
                    this.AdmissionTab4Form.value.duly_filled_admission_form = this.duly_filled_admission_form;
                }
                else {
                    this.AdmissionTab4Form.value.duly_filled_admission_form = this.admissionFormSelFile;
                }
                if (this.birthCerySelFile == '') {
                    this.AdmissionTab4Form.value.birth_certificate = this.birth_certificate;
                }
                else {
                    this.AdmissionTab4Form.value.birth_certificate = this.birthCerySelFile;
                }
                if (this.profilePicSelFile == '') {
                    this.AdmissionTab4Form.value.profile_pic = this.profile_pic;
                }
                else {
                    this.AdmissionTab4Form.value.profile_pic = this.profilePicSelFile;
                }
                if (this.familyPhotoSelFile == '') {
                    this.AdmissionTab4Form.value.family_photo = this.family_photo;
                }
                else {
                    this.AdmissionTab4Form.value.family_photo = this.familyPhotoSelFile;
                }
                if (this.addressProofSelFile == '') {
                    this.AdmissionTab4Form.value.address_proof = this.address_proof;
                }
                else {
                    this.AdmissionTab4Form.value.address_proof = this.addressProofSelFile;
                }
                if (this.profileFormSelFile == '') {
                    this.AdmissionTab4Form.value.duly_filled_child_profile_form = this.duly_filled_child_profile_form;
                }
                else {
                    this.AdmissionTab4Form.value.duly_filled_child_profile_form = this.profileFormSelFile;
                }
                this.commonService.addEditAdmissionDocs(this.AdmissionTab4Form.value, this.studentId).subscribe((response) => {
                    if (response['status_code'] == 200) {
                        if (this.route.snapshot.params.studentid) {
                            this.studentidpass = this.route.snapshot.params.studentid;
                        }
                        else {
                            this.studentidpass = localStorage.getItem('studentid');
                        }
                        this.commonService.getAdmissionDocs(this.studentidpass)
                            .subscribe(data => {
                            this.isLoading = false;
                            if (data['status_code'] == 200) {
                                this.birth_certificate_path = data['body'][0].birth_certificate_path;
                            }
                        });
                    }
                });
            }
        }
        else {
            this.snackbar.open("Invalid file type selected. Please select .png, *.jpg,.jpeg files", '', {
                duration: 2000,
                verticalPosition: "top",
                panelClass: ['bg-red']
            });
        }
    }
    onprofilePicUpload(event) {
        if ((event.target.files[0].type == "image/jpeg") || (event.target.files[0].type == "image/jpg") || (event.target.files[0].type == "image/png")) {
            document.getElementById("profilePic").innerHTML = event.target.files[0].name;
            this.profilePicSelFile = event.target.files[0];
            this.isLoading = true;
            if (localStorage.getItem('docsave') != null || localStorage.getItem('docsave') != '' || localStorage.getItem('docsave') != undefined) {
                if (this.admissionFormSelFile == '') {
                    this.AdmissionTab4Form.value.duly_filled_admission_form = this.duly_filled_admission_form;
                }
                else {
                    this.AdmissionTab4Form.value.duly_filled_admission_form = this.admissionFormSelFile;
                }
                if (this.birthCerySelFile == '') {
                    this.AdmissionTab4Form.value.birth_certificate = this.birth_certificate;
                }
                else {
                    this.AdmissionTab4Form.value.birth_certificate = this.birthCerySelFile;
                }
                if (this.profilePicSelFile == '') {
                    this.AdmissionTab4Form.value.profile_pic = this.profile_pic;
                }
                else {
                    this.AdmissionTab4Form.value.profile_pic = this.profilePicSelFile;
                }
                if (this.familyPhotoSelFile == '') {
                    this.AdmissionTab4Form.value.family_photo = this.family_photo;
                }
                else {
                    this.AdmissionTab4Form.value.family_photo = this.familyPhotoSelFile;
                }
                if (this.addressProofSelFile == '') {
                    this.AdmissionTab4Form.value.address_proof = this.address_proof;
                }
                else {
                    this.AdmissionTab4Form.value.address_proof = this.addressProofSelFile;
                }
                if (this.profileFormSelFile == '') {
                    this.AdmissionTab4Form.value.duly_filled_child_profile_form = this.duly_filled_child_profile_form;
                }
                else {
                    this.AdmissionTab4Form.value.duly_filled_child_profile_form = this.profileFormSelFile;
                }
                this.commonService.addEditAdmissionDocs(this.AdmissionTab4Form.value, this.studentId).subscribe((response) => {
                    if (response['status_code'] == 200) {
                        if (this.route.snapshot.params.studentid) {
                            this.studentidpass = this.route.snapshot.params.studentid;
                        }
                        else {
                            this.studentidpass = localStorage.getItem('studentid');
                        }
                        this.commonService.getAdmissionDocs(this.studentidpass)
                            .subscribe(data => {
                            this.isLoading = false;
                            if (data['status_code'] == 200) {
                                this.profile_pic_path = data['body'][0].profile_pic_path;
                            }
                        });
                    }
                });
            }
        }
        else {
            this.snackbar.open("Invalid file type selected. Please select .png, *.jpg,.jpeg files", '', {
                duration: 2000,
                verticalPosition: "top",
                panelClass: ['bg-red']
            });
        }
    }
    onfamilyPhotoUpload(event) {
        if ((event.target.files[0].type == "image/jpeg") || (event.target.files[0].type == "image/jpg") || (event.target.files[0].type == "image/png")) {
            document.getElementById("familyphoto").innerHTML = event.target.files[0].name;
            this.familyPhotoSelFile = event.target.files[0];
            this.isLoading = true;
            if (localStorage.getItem('docsave') != null || localStorage.getItem('docsave') != '' || localStorage.getItem('docsave') != undefined) {
                if (this.admissionFormSelFile == '') {
                    this.AdmissionTab4Form.value.duly_filled_admission_form = this.duly_filled_admission_form;
                }
                else {
                    this.AdmissionTab4Form.value.duly_filled_admission_form = this.admissionFormSelFile;
                }
                if (this.birthCerySelFile == '') {
                    this.AdmissionTab4Form.value.birth_certificate = this.birth_certificate;
                }
                else {
                    this.AdmissionTab4Form.value.birth_certificate = this.birthCerySelFile;
                }
                if (this.profilePicSelFile == '') {
                    this.AdmissionTab4Form.value.profile_pic = this.profile_pic;
                }
                else {
                    this.AdmissionTab4Form.value.profile_pic = this.profilePicSelFile;
                }
                if (this.addressProofSelFile == '') {
                    this.AdmissionTab4Form.value.address_proof = this.address_proof;
                }
                else {
                    this.AdmissionTab4Form.value.address_proof = this.addressProofSelFile;
                }
                if (this.profileFormSelFile == '') {
                    this.AdmissionTab4Form.value.duly_filled_child_profile_form = this.duly_filled_child_profile_form;
                }
                else {
                    this.AdmissionTab4Form.value.duly_filled_child_profile_form = this.profileFormSelFile;
                }
                if (this.familyPhotoSelFile == '') {
                    this.AdmissionTab4Form.value.family_photo = this.family_photo;
                }
                else {
                    this.AdmissionTab4Form.value.family_photo = this.familyPhotoSelFile;
                }
                this.commonService.addEditAdmissionDocs(this.AdmissionTab4Form.value, this.studentId).subscribe((response) => {
                    if (response['status_code'] == 200) {
                        if (this.route.snapshot.params.studentid) {
                            this.studentidpass = this.route.snapshot.params.studentid;
                        }
                        else {
                            this.studentidpass = localStorage.getItem('studentid');
                        }
                        this.commonService.getAdmissionDocs(this.studentidpass)
                            .subscribe(data => {
                            this.isLoading = false;
                            if (data['status_code'] == 200) {
                                this.family_photo_path = data['body'][0].family_photo_path;
                            }
                        });
                    }
                });
            }
        }
        else {
            this.snackbar.open("Invalid file type selected. Please select .png, *.jpg,.jpeg files", '', {
                duration: 2000,
                verticalPosition: "top",
                panelClass: ['bg-red']
            });
        }
    }
    onaddressProofUpload(event) {
        if ((event.target.files[0].type == "image/jpeg") || (event.target.files[0].type == "image/jpg") || (event.target.files[0].type == "image/png") || (event.target.files[0].type == "application/pdf")) {
            document.getElementById("addressProof").innerHTML = event.target.files[0].name;
            this.addressProofSelFile = event.target.files[0];
            this.isLoading = true;
            if (localStorage.getItem('docsave') != null || localStorage.getItem('docsave') != '' || localStorage.getItem('docsave') != undefined) {
                if (this.admissionFormSelFile == '') {
                    this.AdmissionTab4Form.value.duly_filled_admission_form = this.duly_filled_admission_form;
                }
                else {
                    this.AdmissionTab4Form.value.duly_filled_admission_form = this.admissionFormSelFile;
                }
                if (this.birthCerySelFile == '') {
                    this.AdmissionTab4Form.value.birth_certificate = this.birth_certificate;
                }
                else {
                    this.AdmissionTab4Form.value.birth_certificate = this.birthCerySelFile;
                }
                if (this.profilePicSelFile == '') {
                    this.AdmissionTab4Form.value.profile_pic = this.profile_pic;
                }
                else {
                    this.AdmissionTab4Form.value.profile_pic = this.profilePicSelFile;
                }
                if (this.familyPhotoSelFile == '') {
                    this.AdmissionTab4Form.value.family_photo = this.family_photo;
                }
                else {
                    this.AdmissionTab4Form.value.family_photo = this.familyPhotoSelFile;
                }
                if (this.addressProofSelFile == '') {
                    this.AdmissionTab4Form.value.address_proof = this.address_proof;
                }
                else {
                    this.AdmissionTab4Form.value.address_proof = this.addressProofSelFile;
                }
                if (this.profileFormSelFile == '') {
                    this.AdmissionTab4Form.value.duly_filled_child_profile_form = this.duly_filled_child_profile_form;
                }
                else {
                    this.AdmissionTab4Form.value.duly_filled_child_profile_form = this.profileFormSelFile;
                }
                this.commonService.addEditAdmissionDocs(this.AdmissionTab4Form.value, this.studentId).subscribe((response) => {
                    if (response['status_code'] == 200) {
                        if (this.route.snapshot.params.studentid) {
                            this.studentidpass = this.route.snapshot.params.studentid;
                        }
                        else {
                            this.studentidpass = localStorage.getItem('studentid');
                        }
                        this.commonService.getAdmissionDocs(this.studentidpass)
                            .subscribe(data => {
                            this.isLoading = false;
                            if (data['status_code'] == 200) {
                                this.address_proof_path = data['body'][0].address_proof_path;
                            }
                        });
                    }
                });
            }
        }
        else {
            this.snackbar.open("Invalid file type selected. Please select .png, *.jpg,.jpeg files", '', {
                duration: 2000,
                verticalPosition: "top",
                panelClass: ['bg-red']
            });
        }
    }
    onprofileFormUpload(event) {
        if ((event.target.files[0].type == "image/jpeg") || (event.target.files[0].type == "image/jpg") || (event.target.files[0].type == "image/png") || (event.target.files[0].type == "application/pdf")) {
            document.getElementById("profileForm").innerHTML = event.target.files[0].name;
            this.profileFormSelFile = event.target.files[0];
            this.isLoading = true;
            if (localStorage.getItem('docsave') != null || localStorage.getItem('docsave') != '' || localStorage.getItem('docsave') != undefined) {
                if (this.admissionFormSelFile == '') {
                    this.AdmissionTab4Form.value.duly_filled_admission_form = this.duly_filled_admission_form;
                }
                else {
                    this.AdmissionTab4Form.value.duly_filled_admission_form = this.admissionFormSelFile;
                }
                if (this.birthCerySelFile == '') {
                    this.AdmissionTab4Form.value.birth_certificate = this.birth_certificate;
                }
                else {
                    this.AdmissionTab4Form.value.birth_certificate = this.birthCerySelFile;
                }
                if (this.profilePicSelFile == '') {
                    this.AdmissionTab4Form.value.profile_pic = this.profile_pic;
                }
                else {
                    this.AdmissionTab4Form.value.profile_pic = this.profilePicSelFile;
                }
                if (this.familyPhotoSelFile == '') {
                    this.AdmissionTab4Form.value.family_photo = this.family_photo;
                }
                else {
                    this.AdmissionTab4Form.value.family_photo = this.familyPhotoSelFile;
                }
                if (this.addressProofSelFile == '') {
                    this.AdmissionTab4Form.value.address_proof = this.address_proof;
                }
                else {
                    this.AdmissionTab4Form.value.address_proof = this.addressProofSelFile;
                }
                if (this.profileFormSelFile == '') {
                    this.AdmissionTab4Form.value.duly_filled_child_profile_form = this.duly_filled_child_profile_form;
                }
                else {
                    this.AdmissionTab4Form.value.duly_filled_child_profile_form = this.profileFormSelFile;
                }
                this.commonService.addEditAdmissionDocs(this.AdmissionTab4Form.value, this.studentId).subscribe((response) => {
                    this.isLoading = false;
                    if (response['status_code'] == 200) {
                        if (this.route.snapshot.params.studentid) {
                            this.studentidpass = this.route.snapshot.params.studentid;
                        }
                        else {
                            this.studentidpass = localStorage.getItem('studentid');
                        }
                        this.commonService.getAdmissionDocs(this.studentidpass)
                            .subscribe(data => {
                            if (data['status_code'] == 200) {
                                this.profile_form_path = data['body'][0].duly_filled_child_profile_form_path;
                            }
                        });
                    }
                });
            }
        }
        else {
            this.snackbar.open("Invalid file type selected. Please select .png, *.jpg,.jpeg files", '', {
                duration: 2000,
                verticalPosition: "top",
                panelClass: ['bg-red']
            });
        }
    }
};
Admissiontab1Component = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
        selector: 'app-admissiontab1',
        template: __webpack_require__(/*! ./admissiontab1.component.html */ "./src/app/components/admission/admissiontab1/admissiontab1.component.html"),
        providers: [
            { provide: _angular_material_core__WEBPACK_IMPORTED_MODULE_7__["DateAdapter"], useClass: _angular_material_moment_adapter__WEBPACK_IMPORTED_MODULE_6__["MomentDateAdapter"], deps: [_angular_material_core__WEBPACK_IMPORTED_MODULE_7__["MAT_DATE_LOCALE"]] },
            { provide: _angular_material_core__WEBPACK_IMPORTED_MODULE_7__["MAT_DATE_FORMATS"], useValue: MY_FORMATS },
        ],
        styles: [__webpack_require__(/*! ./admissiontab1.component.css */ "./src/app/components/admission/admissiontab1/admissiontab1.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
        _services_common_service__WEBPACK_IMPORTED_MODULE_4__["CommonService"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"],
        _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSnackBar"],
        _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["NgZone"]])
], Admissiontab1Component);



/***/ }),

/***/ "./src/app/components/admission/remainingfees/remainingfees.component.css":
/*!********************************************************************************!*\
  !*** ./src/app/components/admission/remainingfees/remainingfees.component.css ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvYWRtaXNzaW9uL3JlbWFpbmluZ2ZlZXMvcmVtYWluaW5nZmVlcy5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/components/admission/remainingfees/remainingfees.component.html":
/*!*********************************************************************************!*\
  !*** ./src/app/components/admission/remainingfees/remainingfees.component.html ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"contents\">\n\t<div class=\"loader\" *ngIf=\"isLoading\">\n\t\t<img src=\"assets/images/loader.gif\">\n\t</div>\n\t<div class=\"container-fluid\" class=\"load_table\">\n\t  <section class=\"content\">\n\t    <div id=\"main-form-content\">\n\t      <form [formGroup]=\"AdmissionRemainigPayFees\" (ngSubmit)=\"OnSubmit()\" class=\" org-form all-com-form\"\n\t        enctype=\"multipart/form-data\">\n\t        <div class=\"qtraq-subtitle-wrapper\">\n\t          <h1>Admission Reamining Pay Fees Process</h1>\n\t        </div>\n\t        <div class=\"row\">\n\t\t\t\t<div class=\"col-sm-6\">\n\t\t            <div class=\"form-group mt10\">\n\t\t            \t<label>Fees Selection Type</label>\n\t         \t\t\t<br>\n\t\t\t\t\t\t<input type=\"radio\" [(ngModel)]=\"feestype\" formControlName=\"feestype\" name=\"feestype\" [value]=\"true\" [checked]=\"feestype\" disabled=\"disabled\" style=\"margin-right: 10px\">Group\n\t\t\t\t\t\t<input type=\"radio\" [(ngModel)]=\"feestype\" formControlName=\"feestype\" name=\"feestype\" [value]=\"false\"[checked]=\"!feestype\" style=\"margin-right: 10px\">Class\n\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t            </div>\n\t\t        </div>\n\t          \t<div class=\"col-sm-6\">\n\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t<label class=\"text_green\"> Category<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t<select class=\"form-control\" name=\"categoryid\" formControlName=\"categoryid\" [(ngModel)]=\"categoryId\" (change)=\"getCourses(categoryId);getStudent(categoryId,courseId,batchId)\">\n\t\t\t                <option value=\"\" selected=\"selected\">Select Category</option>\n\t\t\t                <option [selected]=\" categoryId == cat.category_id\" [value]=\"cat.category_id\"\n\t\t\t                  *ngFor=\"let cat of category\">{{cat.categoy_name}}</option>\n\t\t              \t</select>\n\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"AdmissionFormSubmitted && \n            \t\t\t\tAdmissionRemainigPayFees.controls.categoryid.errors\">\n                \t\t\t<span *ngIf=\"AdmissionRemainigPayFees.controls.categoryid.errors.required\">Select Category</span>\n              \t\t\t</div>\n\t\t            </div>\n\t\t        </div>\n\t        </div>\n\n\t        <div class=\"row\">\n\t        \t<div class=\"col-sm-6\">\n\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t<label class=\"text_green\"> Course<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t<select class=\"form-control\" name=\"courseid\" formControlName=\"courseid\" [(ngModel)]=\"courseId\" (change)=\"getCenterUserBatches(categoryId,courseId);getStudent(categoryId,courseId,batchId)\">\n\t\t\t                <option value=\"\">Select Course</option>\n\t\t\t                <option [selected]=\" courseId == course.course_id\" [value]=\"course.course_id\"\n\t\t\t                  *ngFor=\"let course of courses\">{{course.course_name}}</option>\n\t\t              \t</select>\n\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"AdmissionFormSubmitted && \n            \t\t\t\tAdmissionRemainigPayFees.controls.courseid.errors\">\n                \t\t\t<span *ngIf=\"AdmissionRemainigPayFees.controls.courseid.errors.required\">Select Course</span>\n              \t\t\t</div>\n\t\t            </div>\n\t\t        </div>\n\t        \t<div class=\"col-sm-6\">\n\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t<label class=\"text_green\"> Batch<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t<select class=\"form-control\" name=\"batchid\" formControlName=\"batchid\" [(ngModel)]=\"batchId\" (change)=\"getStudent(categoryId,courseId,batchId)\">\n\t\t\t                <option value=\"\" selected=\"selected\">Select Batch</option>\n\t\t\t                <option [selected]=\" batchId == batch.batch_id\" [value]=\"batch.batch_id\"\n\t\t\t                  *ngFor=\"let batch of batches\">{{batch.batch_name}}</option>\n\t\t              \t</select>\n\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"AdmissionFormSubmitted && \n            \t\t\t\tAdmissionRemainigPayFees.controls.batchid.errors\">\n                \t\t\t<span *ngIf=\"AdmissionRemainigPayFees.controls.batchid.errors.required\">Select Batch</span>\n              \t\t\t</div>\n\t\t            </div>\n\t\t        </div>\n\t        </div>\n\n\t        <div class=\"row\">\n\t        \t<div class=\"col-sm-6\">\n\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t<label class=\"text_green\"> Select Student<span class=\"text-danger\">*</span> </label><br>\n\t\t\t\t\t\t<select2 [cssImport]=true  [options]=\"optionsSelect\"  (valueChanged)=\"getStudentDetails($event)\">\n\t\t\t\t\t\t\t<option>Select Student</option>\n\t\t\t\t\t\t\t<option *ngFor=\"let student of students\"[value]=\"student.student_id\"> {{ student.enrollment_no +' ('+student.student_first_name +' '+ student.student_last_name +')' }}</option>\n\t\t\t\t\t\t</select2>\n\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t            </div>\n\t\t        </div>\n\t        </div>\n\n\t        <div *ngIf = \"payFees\">\n\t\t        <h3 style=\"background-color: red;color: white;\">Fill Payment Details</h3>\n\n\t\t         <div class=\"row\">\n\t\t         \t<div class=\"col-sm-6\">\n\t\t         \t\t<div class=\"form-group mt10\">\n\t\t\t            \t<label>Fees Type</label>\n\t\t         \t\t\t<br>\n\t\t\t\t\t\t\t<input type=\"radio\" [(ngModel)]=\"feepaymenttype\" formControlName=\"feepaymenttype\" name=\"feepaymenttype\" [value]=\"true\" [checked]=\"feepaymenttype\" disabled=\"disabled\">Insatllment\n\t\t\t\t\t\t\t<input type=\"radio\" [(ngModel)]=\"feepaymenttype\" formControlName=\"feepaymenttype\" name=\"feepaymenttype\" [value]=\"false\"[checked]=\"!feepaymenttype\" disabled=\"disabled\">Lumsum\n\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t            </div>\n\t\t\t        </div>\n\t\t          \t<div class=\"col-sm-6\">\n\t\t\t            <div class=\"form-group mt10\">\n\t\t\t\t\t\t\t<label class=\"text_green\"> Total Amount </label>\n\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"fees_total_amount\" [(ngModel)]=\"fees_total_amount\" formControlName=\"fees_total_amount\" disabled=\"disabled\">\n\t\t\t            </div>\n\t\t\t        </div>\n\t\t        </div>\n\n\t\t        <div class=\"row\">\n\t\t          \t<div class=\"col-sm-6\">\n\t\t\t            <div class=\"form-group mt10\">\n\t\t\t\t\t\t\t<label class=\"text_green\"> Amount Collected </label>\n\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"fees_amount_collected\" [(ngModel)]=\"fees_amount_collected\" formControlName=\"fees_amount_collected\" disabled=\"disabled\">\n\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t            </div>\n\t\t\t        </div>\n\t\t\t        <div class=\"col-sm-6\">\n\t\t\t            <div class=\"form-group mt10\">\n\t\t\t\t\t\t\t<label class=\"text_green\"> Amount Remaining </label>\n\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"fees_remaining_amount\" [(ngModel)]=\"fees_remaining_amount\" formControlName=\"fees_remaining_amount\" disabled=\"disabled\">\n\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t            </div>\n\t\t\t        </div>\n\t\t        </div>\n\n\t\t        <div class=\"table-responsive\" *ngIf=\"feepaymenttype\">\n\t\t        \t<table class=\"table table-striped\">\n\t\t        \t\t<thead>\n\t\t\t        \t\t<tr>\n\t\t\t        \t\t\t<td>Payment Due Date</td>\n\t\t\t        \t\t\t<td>Installment Amount</td>\n\t\t\t        \t\t\t<td>Collected Amount</td>\n\t\t\t        \t\t\t<td>Paying Amount<span class=\"text-danger\">*</span></td>\n\t\t\t        \t\t\t<td>Remaining Amount</td>\n\t\t\t        \t\t</tr>\n\t\t\t        \t</thead>\n\t\t\t        \t<tbody>\n\t\t\t\t\t\t\t<tr *ngFor=\"let insatll of installment; let i = index\">\n\t\t\t\t\t\t\t\t<td *ngIf=\"insatll.instalment_amount != insatll.instalment_collected_amount\"><input type=\"text\" class=\"form-control\" value=\"{{insatll.instalment_due_date}}\" disabled=\"disabled\">\n\t\t              \t\t\t</td>\n\t\t\t\t\t\t\t\t<td *ngIf=\"insatll.instalment_amount != insatll.instalment_collected_amount\"><input type=\"text\" class=\"form-control\" value=\"{{insatll.instalment_amount}}\" disabled=\"disabled\"></td>\n\t\t\t\t\t\t\t\t<td *ngIf=\"insatll.instalment_amount != insatll.instalment_collected_amount\"><input type=\"text\" class=\"form-control\" value=\"{{insatll.instalment_collected_amount}}\" disabled=\"disabled\" id=\"collected_amount_{{i}}\"></td>\n\t\t\t\t\t\t\t\t<td *ngIf=\"insatll.instalment_amount != insatll.instalment_collected_amount\"><input type=\"number\" class=\"form-control\" id=\"id_{{i}}\" name=\"installment_collected_amount[]\" formControlName=\"installment_collected_amount\" (keyup)=\"collectedinstallmentChange(i)\">\n\t\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t\t\t\t\t<div class=\"text-danger \" id=\"amounterror_{{i}}\">\n\t\t\t\t\t\t\t\t\t\t</div>\n\t\t              \t\t\t</td>\n\t\t\t\t\t\t\t\t<td *ngIf=\"insatll.instalment_amount != insatll.instalment_collected_amount\"><input type=\"text\" class=\"form-control\" id=\"remaining_id_{{i}}\" name=\"installment_remaining_amount[]\" formControlName=\"installment_remaining_amount\" disabled=\"disabled\" value=\"{{insatll.instalment_remaining_amount}}\"\n\t\t\t\t\t\t\t\t>\n\t\t\t\t\t\t\t\t<input type=\"hidden\" name=\"admission_fees_instalment_id\" formControlName=\"admission_fees_instalment_id\" value=\"{{insatll.admission_fees_instalment_id}}\" id=\"admission_fees_instalment_id_{{i}}\"></td>\n\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t</tbody>\n\t\t        \t</table>\n\t\t        </div>\n\t\t    </div>\n\n\t\t    <div class=\"row\">\n\t          \t<div class=\"col-sm-6\">\n\t\t            <div class=\"form-group mt10\">\n\t\t\t\t\t\t<label class=\"text_green\">Fees Remark<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t<textarea placeholder=\"Fees Remark\" class=\"form-control textarea-100\" name=\"fees_remark\" [(ngModel)]=\"fees_remark\" formControlName=\"fees_remark\" required></textarea>\n\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"AdmissionFormSubmitted && \n            \t\t\t\tAdmissionRemainigPayFees.controls.fees_remark.errors\">\n                \t\t\t<span *ngIf=\"AdmissionRemainigPayFees.controls.fees_remark.errors.required\">Please Enter Fees Remark</span>\n              \t\t\t</div>\n\t\t            </div>\n\t\t        </div>\n\t\t        <div class=\"col-sm-6\">\n\t\t            <div class=\"form-group mt10\">\n\t\t\t\t\t\t<label class=\"text_green\">Fees Accepted By<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t<input type=\"text\" placeholder=\"Enter Fees Accepted By\" class=\"form-control\" name=\"fees_approved_accepted_by_name\" [(ngModel)]=\"fees_approved_accepted_by_name\" formControlName=\"fees_approved_accepted_by_name\" disabled=\"disabled\" value=\"{{fees_approved_accepted_by_name}}\">\n\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"AdmissionFormSubmitted && \n            \t\t\t\tAdmissionRemainigPayFees.controls.fees_approved_accepted_by_name.errors\">\n                \t\t\t<span *ngIf=\"AdmissionRemainigPayFees.controls.fees_approved_accepted_by_name.errors.required\">Please Enter Fees Accepted By</span>\n              \t\t\t</div>\n\t\t            </div>\n\t\t        </div>\n\t        </div>\n\t        <h3 style=\"background-color: red;color: white;\">Fill Payment Details</h3>\n\n\t         <div class=\"row\">\n\t        \t<div class=\"col-sm-6\">\n\t\t            <div class=\"form-group mt10 required\">\n\t\t            \t<label class=\"text_green\">Payment Mode</label>\n\t\t            \t<br>\n\t\t\t\t\t\t<label *ngFor=\"let option of payment_mode\" style=\"margin-right: 10px\">\n\t\t\t                <label>\n\t\t\t                    <input type=\"radio\"\n\t\t\t                   \tname=\"payment_mode\" value=\"{{option}}\" [(ngModel)]=\"ngpayment_mode\" [checked] = \"selectedCheckbox == option\" formControlName=\"payment_mode\" (change)=\"updatePaymentOptions(option, $event)\"/>\n\t\t\t                    {{option}}\n\t\t\t                </label>\n\t\t\t            </label>\n\t\t\t\t\t\t<br>\n\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"AdmissionForm5Submitted && \n            \t\t\t\tAdmissionTab5Form.controls.payment_mode.errors\">\n                \t\t\t<span *ngIf=\"AdmissionTab5Form.controls.payment_mode.errors.required\">Select atleast one</span>\n              \t\t\t</div>\n\t\t            </div>\n\t\t        </div>\n\t            <div class=\"col-sm-6 bank_name\">\n\t\t            <div class=\"form-group mt10 required\">\n\t\t            \t<label class=\"text_green\">Bank Name<span class=\"text-danger\">*</span></label>\n\t\t            \t<br>\n\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"bank_name\" [(ngModel)]=\"bank_name\" formControlName=\"bank_name\">\n\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"AdmissionForm5Submitted && \n            \t\t\t\tAdmissionTab5Form.controls.bank_name.errors\">\n                \t\t\t<span *ngIf=\"AdmissionTab5Form.controls.bank_name.errors.required\">Please Enter Bank Name</span>\n              \t\t\t</div>\n\t\t\t\t\t\t<br>\n\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"AdmissionForm5Submitted && \n            \t\t\t\tAdmissionTab5Form.controls.payment_mode.errors\">\n                \t\t\t<span *ngIf=\"AdmissionTab5Form.controls.payment_mode.errors.required\">Select atleast one</span>\n              \t\t\t</div>\n\t\t            </div>\n\t        \t</div>\n\t        </div>\n\n\t        <div class=\"row\">\n\n\t\t        <div class=\"col-sm-6 cheque_no\">\n\t\t            <div class=\"form-group mt10\">\n\t\t\t\t\t\t<label class=\"text_green\"> Cheque No<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"cheque_no\" [(ngModel)]=\"cheque_no\" formControlName=\"cheque_no\">\n\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"AdmissionForm5Submitted && \n            \t\t\t\tAdmissionTab5Form.controls.cheque_no.errors\">\n                \t\t\t<span *ngIf=\"AdmissionTab5Form.controls.cheque_no.errors.required\">Please Enter Cheque No</span>\n              \t\t\t</div>\n\t\t            </div>\n\t\t        </div>\n\t\t        <div class=\"col-sm-6 transaction_no\">\n\t\t            <div class=\"form-group mt10\">\n\t\t\t\t\t\t<label class=\"text_green\"> Transaction No<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"transaction_id\" [(ngModel)]=\"transaction_id\" formControlName=\"transaction_id\">\n\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"AdmissionForm5Submitted && \n            \t\t\t\tAdmissionTab5Form.controls.transaction_id.errors\">\n                \t\t\t<span *ngIf=\"AdmissionTab5Form.controls.transaction_id.errors.required\">Please Enter Transaction No</span>\n              \t\t\t</div>\n\t\t            </div>\n\t\t        </div>\n\t\t        <div class=\"col-sm-6 transaction_no\">\n\t\t            <div class=\"form-group mt10\">\n\t\t\t\t\t\t<label class=\"text_green\"> Transaction Date<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t<input ngx-mydatepicker name=\"transaction_date\" (dateChanged)=\"ontransactionDateChanged($event)\" [options]=\"myOptions\" [(ngModel)]=\"ngxtransactiondate\"\n\t\t\t\t        #dptransaction=\"ngx-mydatepicker\" formControlName=\"transaction_date\" (click)=\"dptransaction.toggleCalendar()\" class=\"form-control w100\" placeholder=\"Select Transaction Date\">\n\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"AdmissionForm5Submitted && \n            \t\t\t\tAdmissionTab5Form.controls.transaction_date.errors\">\n                \t\t\t<span *ngIf=\"AdmissionTab5Form.controls.transaction_date.errors.required\">Select Transaction Dat</span>\n              \t\t\t</div>\n\t\t            </div>\n\t\t        </div>\n\t\t        \n\t\t        <div class=\"col-sm-6 cheque_no\">\n\t\t            <div class=\"form-group mt10\">\n\t\t\t\t\t\t<label class=\"text_green\"> Cheque Date<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t<input ngx-mydatepicker name=\"transaction_date\" (dateChanged)=\"ontransactionDateChanged($event)\" [options]=\"myOptions\" [(ngModel)]=\"ngxtransactiondate\"\n\t\t\t\t        #dptransaction=\"ngx-mydatepicker\" formControlName=\"transaction_date\" (click)=\"dptransaction.toggleCalendar()\" class=\"form-control w100\" placeholder=\"Select Transaction Date\">\n\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"AdmissionForm5Submitted && \n            \t\t\t\tAdmissionTab5Form.controls.transaction_date.errors\">\n                \t\t\t<span *ngIf=\"AdmissionTab5Form.controls.transaction_date.errors.required\">Select Transaction Dat</span>\n              \t\t\t</div>\n\t\t            </div>\n\t\t        </div>\n\t        </div>\n\n            <div class=\"form-group\">\n              <button type=\"submit\" class=\"btn btn-primary\" style=\"margin-right: 10px\">Save</button>\n              <button type=\"button\" routerLink=\"/enquirylist\" class=\"btn btn-primary \">Cancel</button>\n            </div>\n\t      </form>\n\t    </div>\n\t  </section>\n\t</div>\n</div>"

/***/ }),

/***/ "./src/app/components/admission/remainingfees/remainingfees.component.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/components/admission/remainingfees/remainingfees.component.ts ***!
  \*******************************************************************************/
/*! exports provided: RemainingfeesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RemainingfeesComponent", function() { return RemainingfeesComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _services_common_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/common.service */ "./src/app/services/common.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");






let RemainingfeesComponent = class RemainingfeesComponent {
    constructor(route, commonService, formBuilder, snackbar, router, zone) {
        this.route = route;
        this.commonService = commonService;
        this.formBuilder = formBuilder;
        this.snackbar = snackbar;
        this.router = router;
        this.zone = zone;
        this.feestype = false;
        this.AdmissionFormSubmitted = false;
        this.categoryId = "";
        this.courseId = "";
        this.batchId = "";
        this.batchid = "";
        this.isLoading = false;
        this.studentId = "";
        this.admissionfeesid = '';
        this.fees_amount_collected = "";
        this.fees_remaining_amount = "";
        this.payFees = false;
        this.feepaymenttype = false;
        this.installment = [];
        this.installment_remaining_amount = 0;
        this.key = 0;
        this.installmentArray = [];
        this.fees_remark = "";
        this.fees_approved_accepted_by_name = "";
        this.payment_mode = ['Cheque', 'Cash', 'Netbanking'];
        this.selectedOptions = [];
        this.optionsMap = {
            Cheque: false,
            Cash: false,
            Netbanking: false,
        };
        this.optionsChecked = [];
        this.bank_name = "";
        this.transaction_date = '';
        this.ngxtransactiondate = {
            jsdate: new Date()
        };
    }
    ngOnInit() {
        localStorage.removeItem('studentid');
        localStorage.removeItem('tabindex');
        localStorage.removeItem('enquiryNO');
        localStorage.removeItem('docsave');
        localStorage.removeItem('category');
        localStorage.removeItem('course');
        this.optionsSelect = {
            placeholder: "Select Student",
            allowClear: true,
            width: "100%",
        };
        $('.cheque_no').hide();
        $('.transaction_no').hide();
        $('.bank_name').hide();
        this.ngpayment_mode = 'Cash';
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.fees_approved_accepted_by_name = currentUser.first_name + ' ' + currentUser.last_name;
        this.AdmissionRemainigPayFees = this.formBuilder.group({
            feestype: [''],
            categoryid: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]],
            courseid: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]],
            batchid: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]],
            fees_total_amount: [''],
            fees_amount_collected: [''],
            fees_remaining_amount: [''],
            feepaymenttype: [''],
            installment_collected_amount: [''],
            installment_remaining_amount: [''],
            fees_remark: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]],
            fees_approved_accepted_by_name: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]],
            payment_mode: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]],
            cheque_no: [''],
            transaction_id: [''],
            noofinstallment: [''],
            admission_fees_instalment_id: [''],
            bank_name: [''],
            transaction_date: [''],
        });
        this.getCategories();
    }
    getCategories() {
        this.commonService.getCategories().subscribe((response) => {
            if (response['status_code'] == 200) {
                this.category = response['body'];
            }
            else {
                this.snackbar.open(response['msg'], '', {
                    duration: 500,
                    verticalPosition: "top",
                    panelClass: ['bg-red']
                });
            }
        });
    }
    getCourses(categoryid) {
        this.commonService.getCourses(categoryid).subscribe((response) => {
            if (response['status_code'] == 200) {
                this.courses = response['body'];
            }
            else {
                this.snackbar.open(response['msg'], '', {
                    duration: 500,
                    verticalPosition: "top",
                    panelClass: ['bg-red']
                });
            }
        });
    }
    getCenterUserBatches(categoryid, courseid) {
        this.commonService.getCenterUserBatches(categoryid, courseid).subscribe((response) => {
            if (response['status_code'] == 200) {
                this.batches = response['body'];
            }
            else {
                this.snackbar.open(response['msg'], '', {
                    duration: 500,
                    verticalPosition: "top",
                    panelClass: ['bg-red']
                });
            }
        });
    }
    getStudent(categoryid, courseid, batchid) {
        if (categoryid != '' && courseid != '' && batchid != '') {
            this.commonService.getStudentList(categoryid, courseid, batchid, this.feestype).subscribe((response) => {
                if (response['status_code'] == 200) {
                    // var arr = [];
                    // for(var i=0; i<response['body'].length; i++) {
                    //           var no = response['body'][i].enrollment_no ;
                    //           var first_name = response['body'][i].student_first_name ;
                    //           var last_name = response['body'][i].student_last_name ;
                    //           arr.push(no + ' (' + first_name + ' ' + last_name +' )');
                    //       }
                    this.students = response['body'];
                }
                else {
                    this.snackbar.open(response['msg'], '', {
                        duration: 500,
                        verticalPosition: "top",
                        panelClass: ['bg-red']
                    });
                }
            });
        }
    }
    collectedinstallmentChange(index) {
        let collected_amount = $('#collected_amount_' + index).val();
        let paying_amount = Number($('#id_' + index).val());
        let amount = Number(this.installment_amount);
        $('#id_' + index).val(paying_amount);
        let remaining_amount = $('#remaining_id_' + index).val();
        console.log(paying_amount);
        // if(Number(paying_amount) > Number(remaining_amount)){
        // 			}
        // // if(paying_amount == 0 || paying_amount == 0.00){
        // // 	this.snackbar.open('Paying Amount could not be 0', '', {
        // // 		duration: 500,
        // // 		verticalPosition:"top",
        // // 		panelClass: ['bg-red']
        // // 	});
        // // 	this.isLoading = false
        // // 	$('#remaining_id_'+index).val(remaining_amount)
        // // }
        // else{
        // if(collected_amount != 0.00){
        let total_minus_amount = Number(collected_amount) + Number(paying_amount);
        if (Number(total_minus_amount) > Number(collected_amount)) {
            this.snackbar.open('Paying Amount grater than remianing amount', '', {
                duration: 500,
                verticalPosition: "top",
                panelClass: ['bg-red']
            });
            this.isLoading = false;
            console.log(collected_amount);
            // $('#remaining_id_'+index).val(collected_amount)
        }
        else {
            console.log(total_minus_amount);
            this.installment_remaining_amount = (amount - total_minus_amount);
            // }
            // else{
            // 	this.installment_remaining_amount = (amount - paying_amount)
            // }
            $('#remaining_id_' + index).val(this.installment_remaining_amount.toFixed(2));
            this.AdmissionRemainigPayFees.controls['installment_collected_amount'].setErrors(null);
            $('#amounterror_' + index).text('');
        }
        // }
    }
    getStudentDetails(enrollmentno) {
        let getStudentId = enrollmentno.value;
        this.commonService.getStudentPaymentDetails(getStudentId, this.feestype, this.categoryId, this.courseId).subscribe((response) => {
            if (response['status_code'] == 200) {
                console.log(response);
                this.payFees = true;
                this.admissionfeesid = response['body'][0]['admission_fees_id'];
                this.studentId = response['body'][0]['student_id'];
                this.fees_total_amount = response['body'][0]['fees_total_amount'];
                this.fees_amount_collected = response['body'][0]['fees_amount_collected'];
                this.fees_remaining_amount = response['body'][0]['fees_remaining_amount'];
                this.installmentNo = response['body'][0]['no_of_installments'];
                if (response['body'][0]['is_instalment'] == 'Yes') {
                    this.feepaymenttype = true;
                    this.installment_amount = response['body'][0]['instalment_details'][0]['instalment_amount'];
                }
                else {
                    this.feepaymenttype = false;
                }
                this.installment = response['body'][0]['instalment_details'];
            }
            else {
                this.payFees = false;
                this.snackbar.open(response['msg'], '', {
                    duration: 500,
                    verticalPosition: "top",
                    panelClass: ['bg-red']
                });
            }
        });
        // this.commonService.getStudentId(enrollmentsplit[0]).subscribe((response)=>{
        // 	let getStudentId = response['body']['query_result'][0]['student_id']
        // 	console.log(getStudentId)
        // })
    }
    initOptionsMap() {
        for (var x = 0; x < this.payment_mode.length; x++) {
            this.optionsMap[this.payment_mode[x]] = false;
        }
    }
    updatePaymentOptions(option, event) {
        this.optionsMap[option] = event.target.checked;
        this.selectedOptions.push(option);
        if (option == 'Cheque' && event.target.checked == true) {
            $('.cheque_no').show();
        }
        else {
            $('.cheque_no').hide();
        }
        if (option == 'Netbanking' && event.target.checked == true) {
            $('.transaction_no').show();
        }
        else {
            $('.transaction_no').hide();
        }
        if ((option == 'Cheque' && event.target.checked == true) || (option == 'Netbanking' && event.target.checked == true)) {
            $('.bank_name').show();
        }
        else {
            $('.bank_name').hide();
        }
    }
    updateOptions() {
        for (var x in this.selectedOptions) {
            if (this.selectedOptions[x]) {
                this.optionsChecked.push(x);
            }
        }
        this.payment_mode1 = this.optionsChecked;
        this.optionsChecked = [];
    }
    ontransactionDateChanged(event) {
        if (event.formatted !== '') {
            this.transaction_date = event.formatted;
        }
    }
    OnSubmit(formValue) {
        this.isLoading = true;
        this.AdmissionFormSubmitted = true;
        this.AdmissionRemainigPayFees.value.categoryid = this.categoryId;
        this.AdmissionRemainigPayFees.value.courseid = this.courseId;
        this.AdmissionRemainigPayFees.value.batchid = this.batchId;
        let collected_amount = [];
        let remainig_amount = [];
        let installment_amount = [];
        console.log(this.AdmissionRemainigPayFees.value);
        if (this.feepaymenttype == false) {
        }
        else {
            this.AdmissionRemainigPayFees.value.feepaymenttype = 'Yes';
            for (var i = 0; i < this.installmentNo; i++) {
                this.isLoading = false;
                let amount = $('#id_' + i).val();
                let installmentamnt = $('#admission_fees_instalment_id_' + i).val();
                console.log(amount);
                if (amount == '') {
                    this.AdmissionRemainigPayFees.controls['installment_collected_amount'].setErrors({ 'incorrect': true });
                    $('#amounterror_' + i).text('Enter amount');
                }
                else {
                    this.AdmissionRemainigPayFees.controls['installment_collected_amount'].setErrors(null);
                    $('#amounterror_' + i).text('');
                }
                let remainig_amnt = $('#remaining_id_' + i).val();
                if (Number(amount) > Number(remainig_amnt)) {
                    this.snackbar.open('Paying Amount grater than remianing amount', '', {
                        duration: 500,
                        verticalPosition: "top",
                        panelClass: ['bg-red']
                    });
                    this.isLoading = false;
                    return;
                }
                // if(amount == 0 || amount == 0.00){
                // 	this.snackbar.open('Paying Amount could not be 0', '', {
                // 		duration: 500,
                // 		verticalPosition:"top",
                // 		panelClass: ['bg-red']
                // 	});
                // 	this.isLoading = false
                // }
                if (amount != undefined) {
                    collected_amount.push(amount);
                    remainig_amount.push(remainig_amnt);
                    installment_amount.push(installmentamnt);
                }
            }
        }
        if (this.AdmissionRemainigPayFees.value.payment_mode === 'Cheque') {
            this.isLoading = false;
            this.AdmissionRemainigPayFees.get('cheque_no').setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]);
            this.AdmissionRemainigPayFees.get('cheque_no').updateValueAndValidity();
        }
        else {
            this.AdmissionRemainigPayFees.get('cheque_no').clearValidators();
            this.AdmissionRemainigPayFees.get('cheque_no').updateValueAndValidity();
        }
        if (this.AdmissionRemainigPayFees.value.payment_mode === 'Netbanking') {
            this.isLoading = false;
            this.AdmissionRemainigPayFees.get('transaction_id').setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]);
            this.AdmissionRemainigPayFees.get('transaction_id').updateValueAndValidity();
            this.AdmissionRemainigPayFees.get('transaction_date').setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]);
            this.AdmissionRemainigPayFees.get('transaction_date').updateValueAndValidity();
        }
        else {
            this.AdmissionRemainigPayFees.get('transaction_id').clearValidators();
            this.AdmissionRemainigPayFees.get('transaction_id').updateValueAndValidity();
        }
        if (this.AdmissionRemainigPayFees.value.payment_mode === 'Cheque' || this.AdmissionRemainigPayFees.value.payment_mode === 'Netbanking') {
            this.AdmissionRemainigPayFees.get('bank_name').setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]);
            this.AdmissionRemainigPayFees.get('bank_name').updateValueAndValidity();
        }
        else {
            this.AdmissionRemainigPayFees.get('bank_name').clearValidators();
            this.AdmissionRemainigPayFees.get('bank_name').updateValueAndValidity();
            this.AdmissionRemainigPayFees.get('transaction_date').clearValidators();
            this.AdmissionRemainigPayFees.get('transaction_date').updateValueAndValidity();
        }
        if (this.AdmissionRemainigPayFees.invalid) {
            this.isLoading = false;
            return;
        }
        this.commonService.payAdmissionRemainingFees(this.AdmissionRemainigPayFees.value, this.studentId, this.admissionfeesid, this.installmentNo, collected_amount, remainig_amount, installment_amount).subscribe((response) => {
            this.isLoading = false;
            if (response['status_code'] == 200) {
                this.snackbar.open(response['msg'], '', {
                    duration: 500,
                    verticalPosition: "top",
                    panelClass: ['bg-green']
                });
                window.open(response['body'], '_blank');
                this.router.navigate(['/']);
            }
            else {
                this.snackbar.open(response['msg'], '', {
                    duration: 500,
                    verticalPosition: "top",
                    panelClass: ['bg-red']
                });
            }
        });
    }
};
RemainingfeesComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
        selector: 'app-remainingfees',
        template: __webpack_require__(/*! ./remainingfees.component.html */ "./src/app/components/admission/remainingfees/remainingfees.component.html"),
        styles: [__webpack_require__(/*! ./remainingfees.component.css */ "./src/app/components/admission/remainingfees/remainingfees.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
        _services_common_service__WEBPACK_IMPORTED_MODULE_4__["CommonService"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"],
        _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSnackBar"],
        _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["NgZone"]])
], RemainingfeesComponent);



/***/ }),

/***/ "./src/app/components/album/albumimages/albumimages.component.css":
/*!************************************************************************!*\
  !*** ./src/app/components/album/albumimages/albumimages.component.css ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvYWxidW0vYWxidW1pbWFnZXMvYWxidW1pbWFnZXMuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/components/album/albumimages/albumimages.component.html":
/*!*************************************************************************!*\
  !*** ./src/app/components/album/albumimages/albumimages.component.html ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"contents\">\n\t<div class=\"loader\" *ngIf=\"isLoading\">\n\t\t<img src=\"assets/images/loader.gif\">\n\t</div>\n\t<div class=\"container-fluid\">\n\t  <section class=\"content\">\n\t    <div id=\"main-form-content\">\n\t    \t<div class=\"row\">\n\t    \t\t<div class=\"col-md-12\">\n\t\t\t\t\t<div  class=\"mb20\" style=\"float: right\">\n\t\t\t\t\t\t<button class=\"btn btn-danger\" data-toggle=\"modal\" data-target=\"#myModal\"> Upload Image</button>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"col-md-12\">\n\t\t\t    \t<div *ngFor=\"let image of images\" class=\"row\">\n\t\t\t    \t\t<div class=\"col-md-12\"><h4 style=\"color: black;\">{{image.uploaded_on}}</h4></div>\n\t\t\t    \t\t<div *ngFor=\"let img of image.images\"  class=\"col-md-4\">\n\t\t\t\t\t\t    <div>\n\t\t\t\t\t\t      \t<div class=\"thumbnail\" style=\"float: left;width: 100%\">\n\t\t\t\t\t\t       \t\t<img src=\"{{img.album_pic_path}}\" alt=\"Lights\" style=\"width:100%;height: 200px\">\n\t\t\t\t\t\t\t\t\t<div class=\"caption pull-right\">\n\t\t\t\t\t\t\t\t\t\t<a><i style=\"font-size: 24px\" class=\"fa fa-trash\" (click)=\"deleteImage(img.album_image_id)\" aria-hidden=\"true\"></i></a>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t     \t </div>\n\t\t\t\t\t\t    </div>\n\t\t\t    \t\t</div>\n\t\t\t    \t</div> \n\t\t\t    </div>\n\t    \t</div>\n\t    </div>\n\t  </section>\n\t</div>\n</div>\n\n<div class=\"modal\" id=\"myModal\">\n    <div class=\"modal-dialog\" style=\"margin-top: 130px\">\n\t\t<div class=\"modal-content\">\n\n\t\t<!-- Modal Header -->\n\t\t<div class=\"modal-header\">\n\t\t  <h4 class=\"modal-title\">Upload Image</h4>\n\t\t  <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>\n\t\t</div>\n\n\t\t<!-- Modal body -->\n\t\t<div class=\"modal-body\">\n\t\t\t<form [formGroup]=\"ImageForm\" (ngSubmit)=\"OnSubmit()\" class=\" org-form all-com-form\" enctype=\"multipart/form-data\">\n\t\t\t\t<div class=\"\">\n                \t<!-- <input type=\"file\" class=\"form-control custom-img \" name=\"albumpic\" formControlName=\"albumpic\"\n                  [required]=\"albumpic == ''\" (change)=\"onFileChange($event)\"> -->\n                  \t<div class=\"inputfile-box\">\n                      <input class=\"inputfile\" id=\"file\" type=\"file\" (change)=\"onFileChange($event)\" name=\"albumpic\" formControlName=\"albumpic\">\n                      <label for=\"file\"><span class=\"file-box\" id=\"file-name\"></span><span class=\"file-button\">Upload</span></label>\n                    </div>\n              \t</div>\n              \t<br>\n              \t<div class=\"form-group\">\n\t\t\t\t\t<button type=\"submit\" class=\"btn btn-primary \">Save</button>\n\t\t\t\t\t<!-- <button type=\"button\" routerLink=\"/admissionlist\" class=\"btn btn-primary \">Cancel</button> -->\n\t            </div>\n              \t<div class=\"clearfix\"></div>\n\t\t\t</form>\n\t\t</div>\n\n\t\t</div>\n\t</div>\n</div>"

/***/ }),

/***/ "./src/app/components/album/albumimages/albumimages.component.ts":
/*!***********************************************************************!*\
  !*** ./src/app/components/album/albumimages/albumimages.component.ts ***!
  \***********************************************************************/
/*! exports provided: AlbumimagesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AlbumimagesComponent", function() { return AlbumimagesComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _services_common_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/common.service */ "./src/app/services/common.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");






let AlbumimagesComponent = class AlbumimagesComponent {
    constructor(route, commonService, snackbar, formBuilder, router, zone) {
        this.route = route;
        this.commonService = commonService;
        this.snackbar = snackbar;
        this.formBuilder = formBuilder;
        this.router = router;
        this.zone = zone;
        this.albumId = '';
        this.isLoading = true;
    }
    ngOnInit() {
        this.ImageForm = this.formBuilder.group({
            albumpic: ['']
        });
        this.route.params.subscribe(params => {
            if (this.route.snapshot.params.albumid) {
                this.commonService.getAlbumsImages(this.route.snapshot.params.albumid)
                    .subscribe(data => {
                    this.isLoading = false;
                    if (data['status_code'] == 200) {
                        console.log(data['msg']);
                        this.snackbar.open(data['msg'], '', {
                            duration: 500,
                            verticalPosition: "top",
                            panelClass: ['bg-green']
                        });
                        this.images = data['body'];
                        this.albumId = this.route.snapshot.params.albumid;
                    }
                    else {
                        this.snackbar.open(data['msg'], '', {
                            duration: 500,
                            verticalPosition: "top",
                            panelClass: ['bg-red']
                        });
                    }
                });
            }
        });
    }
    deleteImage(albumimageid) {
        this.isLoading = true;
        this.commonService.deleteAlbumsImages(albumimageid, this.albumId)
            .subscribe(data => {
            this.isLoading = false;
            this.snackbar.open(data['msg'], '', {
                duration: 1000,
                verticalPosition: "top",
                panelClass: ['bg-green']
            });
            this.zone.runOutsideAngular(() => {
                $('body').find('#main-form-content').load('index.html');
            });
        });
    }
    onFileChange(event) {
        if ((event.target.files[0].type == "image/jpeg") || (event.target.files[0].type == "image/jpg") || (event.target.files[0].type == "image/png")) {
            document.getElementById("file-name").innerHTML = event.target.files[0].name;
            this.selectedFile = event.target.files[0];
        }
        else {
            this.snackbar.open("Invalid file type selected. Please select .png, *.jpg,.jpeg files", '', {
                duration: 2000,
                verticalPosition: "top",
                panelClass: ['bg-red']
            });
        }
    }
    OnSubmit() {
        this.albumpic = this.selectedFile;
        this.commonService.uploadAlbumImage(this.selectedFile, this.albumId)
            .subscribe(data => {
            this.snackbar.open(data['msg'], '', {
                duration: 1000,
                verticalPosition: "top",
                panelClass: ['bg-green']
            });
            this.zone.runOutsideAngular(() => {
                $('body').find('.modal').remove();
                $('body').find('.modal-backdrop').remove();
                $('body').removeClass("modal-open");
                $('body').find('#main-form-content').load('index.html');
            });
        });
    }
};
AlbumimagesComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
        selector: 'app-albumimages',
        template: __webpack_require__(/*! ./albumimages.component.html */ "./src/app/components/album/albumimages/albumimages.component.html"),
        styles: [__webpack_require__(/*! ./albumimages.component.css */ "./src/app/components/album/albumimages/albumimages.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
        _services_common_service__WEBPACK_IMPORTED_MODULE_4__["CommonService"],
        _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSnackBar"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"],
        _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["NgZone"]])
], AlbumimagesComponent);



/***/ }),

/***/ "./src/app/components/album/albumlist/albumlist.component.css":
/*!********************************************************************!*\
  !*** ./src/app/components/album/albumlist/albumlist.component.css ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvYWxidW0vYWxidW1saXN0L2FsYnVtbGlzdC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/components/album/albumlist/albumlist.component.html":
/*!*********************************************************************!*\
  !*** ./src/app/components/album/albumlist/albumlist.component.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"contents\">\n\t<div class=\"loader\" *ngIf=\"isLoading\">\n\t\t<img src=\"assets/images/loader.gif\">\n\t</div>\n\t<div class=\"container-fluid\">\n\t\t<section class=\"content\">\n\t\t\t<div id=\"main-form-content\">\n\t\t\t<!-- <form name=\"form\" (ngSubmit)=\"onSubmit()\" #f=\"ngForm\" class=\"form-inline\"> \n\t\t\t<div class=\"col-sm-6\">\n\t\t\t\t<div class=\"form-group\">\n\t        \t\t<label for=\"status\" class=\"text_green\">Status</label>\n\t        \t\t<select name=\"status\" [(ngModel)]=\"status\" class=\"form-control\">\n\t            \t\t<option value=\"Active\"> Active </option>\n\t            \t\t<option value=\"In-active\">In-active </option>\n\t          \t\t</select>\n\t        \t</div>\n\t      \t</div>\n\t\t\t<div class=\"col-sm-12 col-lg-2\">\n\t\t\t\t<button class=\"btn btn-info\">Search</button>\n\t\t\t\t<button (click)=\"clearfilter()\" class=\"btn btn-info\">Clear</button>\n\t\t\t</div>\n\t\t\t</form> -->\n\t\t\t</div>\n\t\t\t<div class=\"clearfix\"></div>\n\t\t\t<div class=\"col-md-12\">\n\t\t\t\t<div  class=\"qtraq-subtitle-wrapper mt20\">\n\t\t\t\t\t<h1> Albums</h1>\n\t\t\t\t</div>\n\t\t\t\t<table class=\"table table-striped\">\n\t\t\t\t\t<thead>\n\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t<th class=\"hidden\">Id</th>\n\t\t\t\t\t\t\t<th>Album Name</th>\n\t\t\t\t\t\t\t<th>Status</th>\n\t\t\t\t\t\t\t<th>Action</th>\n\t\t\t\t\t\t</tr>\n\t\t\t\t\t</thead>\n\t\t\t\t\t<tbody>\n\t\t\t\t\t\t<tr *ngFor=\"let album of albumList| paginate: { itemsPerPage: this.size, \n\t\t  currentPage: this.page, totalItems: this.totalRecords }\">\n\t\t\t\t\t\t\t<td class=\"hidden\">1</td>\n\t\t\t\t\t\t\t<td>{{album.album_name}}</td>\n\t\t\t\t\t\t\t<td>{{album.status}}</td>\n\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t<button class=\"btn btn-danger\" routerLink=\"albumimages/{{album.album_id}}\" \n\t\t\t\t\t\t\t\t\tstyle=\"margin-left: 20px;\"> View</button>\n\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t</tr>\n\t\t\t\t\t</tbody>\n\t\t\t\t</table>\n\t\t\t\t\n\t\t\t</div>\n\t\t</section>\n\t</div>\n\t<div class=\"clearfix\"></div>\n</div>\n"

/***/ }),

/***/ "./src/app/components/album/albumlist/albumlist.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/components/album/albumlist/albumlist.component.ts ***!
  \*******************************************************************/
/*! exports provided: AlbumlistComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AlbumlistComponent", function() { return AlbumlistComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");
/* harmony import */ var _services_common_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/common.service */ "./src/app/services/common.service.ts");





let AlbumlistComponent = class AlbumlistComponent {
    constructor(commonService, snackbar, router) {
        this.commonService = commonService;
        this.snackbar = snackbar;
        this.router = router;
        this.array = [];
        this.sum = 10;
        this.page = 0;
        this.albumList = [];
        this.totalRecords = 0;
        this.size = 10;
        this.albumFilter = {
            status: "",
        };
        this.isLoading = true;
        if (this.router.url == 'albumlist') {
            window.onscroll = () => {
                let status = "not reached";
                let windowHeight = "innerHeight" in window ? window.innerHeight
                    : document.documentElement.offsetHeight;
                let body = document.body, html = document.documentElement;
                let docHeight = Math.max(body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight);
                let windowBottom = windowHeight + window.pageYOffset;
                if (windowBottom >= docHeight) {
                    //this.page = this.page
                    this.commonService.getAlbumsScroll(this.page).subscribe((response) => {
                        this.isLoading = false;
                        if (response['status_code'] == 200) {
                        }
                        else {
                            this.snackbar.open(response['msg'], '', {
                                duration: 500,
                                verticalPosition: "top",
                                panelClass: ['bg-red']
                            });
                        }
                    }, (err) => console.log("error", err), () => console.log());
                }
            };
        }
    }
    ngOnInit() {
        localStorage.removeItem('studentid');
        localStorage.removeItem('tabindex');
        localStorage.removeItem('enquiryNO');
        localStorage.removeItem('docsave');
        localStorage.removeItem('category');
        localStorage.removeItem('course');
        this.getAlbums(1, this.albumFilter);
    }
    getAlbums(page, albumFilter) {
        this.commonService.getAlbums((page - 1), this.size, albumFilter).subscribe((response) => {
            this.isLoading = false;
            if (response['status_code'] == 200) {
                this.snackbar.open(response['msg'], '', {
                    duration: 500,
                    verticalPosition: "top",
                    panelClass: ['bg-green']
                });
                this.albumList = response['body'];
                this.page = page;
            }
            else {
                this.snackbar.open(response['msg'], '', {
                    verticalPosition: "top",
                    panelClass: ['bg-red']
                });
                this.albumList = [];
                this.totalRecords = 0;
            }
        }, (err) => console.log("error", err), () => console.log("getCustomersPage() retrieved customers for page:", +page));
    }
    clearfilter() {
        this.albumFilter.status = "";
        this.getAlbums(1, this.albumFilter);
    }
};
AlbumlistComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
        selector: 'app-albumlist',
        template: __webpack_require__(/*! ./albumlist.component.html */ "./src/app/components/album/albumlist/albumlist.component.html"),
        styles: [__webpack_require__(/*! ./albumlist.component.css */ "./src/app/components/album/albumlist/albumlist.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_common_service__WEBPACK_IMPORTED_MODULE_4__["CommonService"],
        _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSnackBar"],
        _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
], AlbumlistComponent);



/***/ }),

/***/ "./src/app/components/attendence/markattendence/markattendence.component.css":
/*!***********************************************************************************!*\
  !*** ./src/app/components/attendence/markattendence/markattendence.component.css ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvYXR0ZW5kZW5jZS9tYXJrYXR0ZW5kZW5jZS9tYXJrYXR0ZW5kZW5jZS5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/components/attendence/markattendence/markattendence.component.html":
/*!************************************************************************************!*\
  !*** ./src/app/components/attendence/markattendence/markattendence.component.html ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"contents\">\n\t<div class=\"loader\" *ngIf=\"isLoading\">\n\t\t<img src=\"assets/images/loader.gif\">\n\t</div>\n\t<div class=\"container-fluid\" class=\"load_table\">\n\t  <section class=\"content\">\n\t    <div id=\"main-form-content\">\n\t      <form [formGroup]=\"markAttendence\" (ngSubmit)=\"OnSubmit()\" class=\" org-form all-com-form\"\n\t        enctype=\"multipart/form-data\">\n\t        <div class=\"qtraq-subtitle-wrapper\">\n\t          <h1>Mark Attendence</h1>\n\t        </div>\n\t        <div class=\"row\">\n\t\t\t\t<div class=\"col-sm-6\">\n\t\t            <div class=\"form-group mt10\">\n\t\t            \t<label>Fees Selection Type</label>\n\t         \t\t\t<br>\n\t\t\t\t\t\t<input type=\"radio\" [(ngModel)]=\"feesselectiontype\" formControlName=\"feesselectiontype\" name=\"feesselectiontype\" [value]=\"true\" [checked]=\"feesselectiontype\"  style=\"margin-right: 10px\" (change)=\"feestypeChange(feesselectiontype)\">Group\n\t\t\t\t\t\t<input type=\"radio\" [(ngModel)]=\"feesselectiontype\" formControlName=\"feesselectiontype\" name=\"feesselectiontype\" [value]=\"false\"[checked]=\"!feesselectiontype\" style=\"margin-right: 10px\" (change)=\"feestypeChange(feesselectiontype)\">Class\n\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t            </div>\n\t\t        </div>\n\t\t        <div class=\"col-sm-6\">\n\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t<label class=\"text_green\"> Attendence Date<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t<input ngx-mydatepicker name=\"attendancedate\" (dateChanged)=\"onattendanceDateChanged($event)\" [options]=\"myOptions\" [(ngModel)]=\"ngxattendancendate\"\n\t\t\t\t        #dp1=\"ngx-mydatepicker\" formControlName=\"attendancedate\" (click)=\"dp1.toggleCalendar()\" class=\"form-control w100\" placeholder=\"Select Attendence Date\">\n\t\t\t\t\t\t<div class=\"clearfix\"></div>\n                \t\t<span id=\"attendancedate_error\" class=\"text-danger\"></span>\n\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"AttendenceFormSubmitted && \n            \t\t\t\tmarkAttendence.controls.attendancedate.errors\">\n                \t\t\t<span *ngIf=\"markAttendence.controls.attendancedate.errors.required\">Select Attendence Date</span>\n              \t\t\t</div>\n\t\t            </div>\n\t\t        </div>\n\t        </div>\n\n\t        <div class=\"row\" *ngIf = \"!feesselectiontype\">\n\t          \t<div class=\"col-sm-6\">\n\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t<label class=\"text_green\"> Category<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t<select class=\"form-control\" name=\"categoryid\" formControlName=\"categoryid\" [(ngModel)]=\"categoryId\" (change)=\"getCourses(categoryId);getStudent(categoryId,courseId,batchId,groupId)\">\n\t\t\t                <option value=\"\" selected=\"selected\">Select Category</option>\n\t\t\t                <option [selected]=\" categoryId == cat.category_id\" [value]=\"cat.category_id\"\n\t\t\t                  *ngFor=\"let cat of category\">{{cat.categoy_name}}</option>\n\t\t              \t</select>\n\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"AttendenceFormSubmitted && \n            \t\t\t\tmarkAttendence.controls.categoryid.errors\">\n                \t\t\t<span *ngIf=\"markAttendence.controls.categoryid.errors.required\">Select Group</span>\n              \t\t\t</div>\n\t\t            </div>\n\t\t        </div>\n\t        \t<div class=\"col-sm-6\">\n\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t<label class=\"text_green\"> Course<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t<select class=\"form-control\" name=\"courseid\" formControlName=\"courseid\" [(ngModel)]=\"courseId\" (change)=\"getCenterUserBatches(categoryId,courseId);getStudent(categoryId,courseId,batchId,groupId)\">\n\t\t\t                <option value=\"\">Select Course</option>\n\t\t\t                <option [selected]=\" courseId == course.course_id\" [value]=\"course.course_id\"\n\t\t\t                  *ngFor=\"let course of courses\">{{course.course_name}}</option>\n\t\t              \t</select>\n\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"AttendenceFormSubmitted && \n            \t\t\t\tmarkAttendence.controls.courseid.errors\">\n                \t\t\t<span *ngIf=\"markAttendence.controls.courseid.errors.required\">Select Course</span>\n              \t\t\t</div>\n\t\t            </div>\n\t\t        </div>\n\t        </div>\n\n\t        <div class=\"row\">\n\t        \t<div class=\"col-sm-6\" *ngIf = \"!feesselectiontype\">\n\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t<label class=\"text_green\"> Batch<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t<select class=\"form-control\" name=\"batchid\" formControlName=\"batchid\" [(ngModel)]=\"batchId\" (change)=\"getStudent(categoryId,courseId,batchId,groupId)\">\n\t\t\t                <option value=\"\" selected=\"selected\">Select Batch</option>\n\t\t\t                <option [selected]=\" batchId == batch.batch_id\" [value]=\"batch.batch_id\"\n\t\t\t                  *ngFor=\"let batch of batches\">{{batch.batch_name}}</option>\n\t\t              \t</select>\n\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"AttendenceFormSubmitted && \n            \t\t\t\tmarkAttendence.controls.batchid.errors\">\n                \t\t\t<span *ngIf=\"markAttendence.controls.batchid.errors.required\">Select Batch</span>\n              \t\t\t</div>\n\t\t            </div>\n\t\t        </div>\n\t\t        <div class=\"col-sm-6\" *ngIf = \"feesselectiontype\">\n\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t<label class=\"text_green\"> Group<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t<select class=\"form-control\" name=\"groupid\" formControlName=\"groupid\" [(ngModel)]=\"groupId\" (change)=\"getStudent(categoryId,courseId,batchId,groupId)\">\n\t\t\t                <option value=\"\" selected=\"selected\">Select Group</option>\n\t\t\t                <option [selected]=\" groupId == grp.group_master_id\" [value]=\"grp.group_master_id\"\n\t\t\t                  *ngFor=\"let grp of groups\">{{grp.group_master_name}}</option>\n\t\t              \t</select>\n\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"AttendenceFormSubmitted && \n            \t\t\t\tmarkAttendence.controls.groupid.errors\">\n                \t\t\t<span *ngIf=\"markAttendence.controls.groupid.errors.required\">Select Category</span>\n              \t\t\t</div>\n\t\t            </div>\n\t\t        </div>\n\t        </div>\n\n\t        <div class=\"table-responsive\" *ngIf=\"getstudent\">\n\t        \t<table class=\"table table-striped\">\n\t        \t\t<thead>\n\t\t        \t\t<tr>\n\t\t        \t\t\t<td><input type=\"checkbox\" id=\"checkedAll\"name=\"all\" formControlName=\"all\"  [(ngModel)]=\"all\" (change)=\"checkboxChange(all)\"></td>\n\t\t        \t\t\t<td>Enrollment No</td>\n\t\t        \t\t\t<td>Student Name</td>\n\t\t        \t\t</tr>\n\t\t        \t</thead>\n\t\t        \t<tbody>\n\t\t\t\t\t\t<tr *ngFor=\"let student of students; let i = index\">\n\t\t\t\t\t\t\t<td><input type=\"checkbox\" id=\"singlecheckbox_{{i}}\" name=\"single\"  class=\"singleChecked\" (change)=\"checkboxSingleChange(i)\"><input type=\"hidden\" name=\"student_id\" value=\"{{student.student_id}}\" id=\"student_{{i}}\">\n\t              \t\t\t</td>\n\t\t\t\t\t\t\t<td>{{student.enrollment_no}}</td>\n\t\t\t\t\t\t\t<td>{{student.student_first_name + ' ' + student.student_last_name}}</td>\n\t\t\t\t\t\t</tr>\n\t\t\t\t\t</tbody>\n\t        \t</table>\n\t        </div>\n\n\t        <div class=\"row\" style=\"float: left;width: 100%\">\n\t        \t<div class=\"col-sm-6\">\n\t\t\t        <input type=\"hidden\" formControlName=\"student_array\"  [(ngModel)]=\"student_array\">\n\t\t\t        <div class=\"text-danger \" *ngIf=\"AttendenceFormSubmitted && \n\t\t\t\t\t\tmarkAttendence.controls.student_array.errors\">\n\t\t    \t\t\t<span *ngIf=\"markAttendence.controls.student_array.errors.required\">Select Student</span>\n\t\t  \t\t\t</div>\n\t\t  \t\t</div>\n\t  \t\t</div><br>\n\n            <div class=\"form-group\">\n              <button type=\"submit\" class=\"btn btn-primary\" style=\"margin-right: 10px\">Save</button>\n              <button type=\"button\" routerLink=\"/enquirylist\" class=\"btn btn-primary \">Cancel</button>\n            </div>\n\t      </form>\n\t    </div>\n\t  </section>\n\t</div>\n</div>"

/***/ }),

/***/ "./src/app/components/attendence/markattendence/markattendence.component.ts":
/*!**********************************************************************************!*\
  !*** ./src/app/components/attendence/markattendence/markattendence.component.ts ***!
  \**********************************************************************************/
/*! exports provided: MarkattendenceComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MarkattendenceComponent", function() { return MarkattendenceComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _services_common_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/common.service */ "./src/app/services/common.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");






let MarkattendenceComponent = class MarkattendenceComponent {
    constructor(route, commonService, formBuilder, snackbar, router, zone) {
        this.route = route;
        this.commonService = commonService;
        this.formBuilder = formBuilder;
        this.snackbar = snackbar;
        this.router = router;
        this.zone = zone;
        this.feesselectiontype = false;
        this.categoryId = "";
        this.courseId = "";
        this.batchId = "";
        this.batchid = "";
        this.groupId = '';
        this.isLoading = false;
        this.AttendenceFormSubmitted = false;
        this.getstudent = false;
        this.student_single_array = [];
        this.student_array = [];
        this.myOptions = {
            dateFormat: 'dd-mm-yyyy',
        };
        this.attendancedate = '';
        this.ngxattendancedate = {
            jsdate: new Date()
        };
    }
    ngOnInit() {
        localStorage.removeItem('studentid');
        localStorage.removeItem('tabindex');
        localStorage.removeItem('enquiryNO');
        localStorage.removeItem('docsave');
        localStorage.removeItem('category');
        localStorage.removeItem('course');
        this.markAttendence = this.formBuilder.group({
            feesselectiontype: [''],
            categoryid: [''],
            courseid: [''],
            batchid: [''],
            groupid: [''],
            all: [''],
            single: [''],
            attendancedate: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]],
            student_array: [''],
        });
        this.getCategories();
    }
    onattendanceDateChanged(event) {
        $('#attendancedate_error').text('');
        if (event.formatted !== '') {
            this.attendancedate = event.formatted;
        }
        this.getStudent(this.categoryId, this.courseId, this.batchId, this.groupId);
    }
    getCategories() {
        this.commonService.getCategories().subscribe((response) => {
            if (response['status_code'] == 200) {
                this.category = response['body'];
            }
            else {
                this.snackbar.open(response['msg'], '', {
                    duration: 500,
                    verticalPosition: "top",
                    panelClass: ['bg-red']
                });
            }
        });
    }
    getCourses(categoryid) {
        this.commonService.getCourses(categoryid).subscribe((response) => {
            if (response['status_code'] == 200) {
                this.courses = response['body'];
            }
            else {
                this.snackbar.open(response['msg'], '', {
                    duration: 500,
                    verticalPosition: "top",
                    panelClass: ['bg-red']
                });
            }
        });
    }
    getCenterUserBatches(categoryid, courseid) {
        this.commonService.getCenterUserBatches(categoryid, courseid).subscribe((response) => {
            if (response['status_code'] == 200) {
                this.batches = response['body'];
            }
            else {
                this.snackbar.open(response['msg'], '', {
                    duration: 500,
                    verticalPosition: "top",
                    panelClass: ['bg-red']
                });
            }
        });
    }
    getStudent(categoryid, courseid, batchid, groupid) {
        if (this.feesselectiontype == true) {
            categoryid = null;
            courseid = null;
            batchid = null;
        }
        if (categoryid != '' && courseid != '' && batchid != '') {
            if (this.attendancedate == '') {
                $('#attendancedate_error').text('Select Attendance Date');
            }
            else {
                this.commonService.getStudentListAttendence(categoryid, courseid, batchid, groupid, this.feesselectiontype, this.attendancedate).subscribe((response) => {
                    if (response['status_code'] == 200) {
                        this.getstudent = true;
                        this.students = response['body'];
                    }
                    else {
                        this.snackbar.open(response['msg'], '', {
                            duration: 500,
                            verticalPosition: "top",
                            panelClass: ['bg-red']
                        });
                    }
                });
            }
        }
    }
    feestypeChange(type) {
        if (type == true) {
            this.commonService.getGroupsDD().subscribe((response) => {
                if (response['status_code'] == 200) {
                    this.groups = response['body'];
                }
                else {
                    this.snackbar.open(response['msg'], '', {
                        duration: 500,
                        verticalPosition: "top",
                        panelClass: ['bg-red']
                    });
                }
            });
        }
    }
    checkboxChange(type) {
        for (let i = 0; i < this.students.length; i++) {
            if (type == true) {
                let getStudentId = $('#student_' + i).val();
                this.student_array.push(getStudentId);
                $('#singlecheckbox_' + i).prop('checked', true);
            }
            else {
                this.student_array.pop();
                $('.singleChecked').prop('checked', false);
            }
        }
        this.student_array = $.unique(this.student_array);
        console.log(this.student_array);
    }
    checkboxSingleChange(index) {
        if ($('#singlecheckbox_' + index).is(":checked") == true) {
            this.student_array.push($('#student_' + index).val());
            if (this.student_array.length == this.students.length) {
                $('#checkedAll').prop('checked', true);
            }
            else {
                $('#checkedAll').prop('checked', false);
            }
        }
        else {
            console.log(this.student_array);
            var array = this.student_array;
            var removeItem = $('#student_' + index).val();
            array.splice($.inArray(removeItem, array), 1);
            this.student_array = $.unique(array);
            $('#checkedAll').prop('checked', false);
        }
        console.log(this.student_array);
    }
    OnSubmit() {
        this.isLoading = true;
        this.AttendenceFormSubmitted = true;
        if (this.feesselectiontype == false) {
            this.markAttendence.value.categoryid = this.categoryId;
            this.markAttendence.value.courseid = this.courseId;
            this.markAttendence.value.batchid = this.batchId;
            this.isLoading = false;
            this.markAttendence.get('categoryid').setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]);
            this.markAttendence.get('categoryid').updateValueAndValidity();
            this.markAttendence.get('courseid').setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]);
            this.markAttendence.get('courseid').updateValueAndValidity();
            this.markAttendence.get('batchid').setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]);
            this.markAttendence.get('batchid').updateValueAndValidity();
            this.markAttendence.get('groupid').clearValidators();
            this.markAttendence.get('groupid').updateValueAndValidity();
        }
        else {
            this.markAttendence.value.groupid = this.groupId;
            this.markAttendence.get('categoryid').clearValidators();
            this.markAttendence.get('categoryid').updateValueAndValidity();
            this.markAttendence.get('courseid').clearValidators();
            this.markAttendence.get('courseid').updateValueAndValidity();
            this.markAttendence.get('batchid').clearValidators();
            this.markAttendence.get('batchid').updateValueAndValidity();
            this.markAttendence.get('groupid').setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]);
            this.markAttendence.get('groupid').updateValueAndValidity();
        }
        if (this.student_array.length >= 1) {
            this.markAttendence.get('student_array').clearValidators();
            this.markAttendence.get('student_array').updateValueAndValidity();
        }
        else {
            this.markAttendence.get('student_array').setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]);
            this.markAttendence.get('student_array').updateValueAndValidity();
        }
        this.markAttendence.value.attendancedate = this.attendancedate;
        if (this.markAttendence.invalid) {
            this.isLoading = false;
            return;
        }
        this.commonService.markattendence(this.markAttendence.value, this.student_array).subscribe((response) => {
            this.isLoading = false;
            if (response['status_code'] == 200) {
                this.snackbar.open(response['msg'], '', {
                    duration: 500,
                    verticalPosition: "top",
                    panelClass: ['bg-green']
                });
                this.router.navigate(['/']);
            }
            else {
                this.snackbar.open(response['msg'], '', {
                    duration: 500,
                    verticalPosition: "top",
                    panelClass: ['bg-red']
                });
            }
        });
    }
};
MarkattendenceComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
        selector: 'app-markattendence',
        template: __webpack_require__(/*! ./markattendence.component.html */ "./src/app/components/attendence/markattendence/markattendence.component.html"),
        styles: [__webpack_require__(/*! ./markattendence.component.css */ "./src/app/components/attendence/markattendence/markattendence.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
        _services_common_service__WEBPACK_IMPORTED_MODULE_4__["CommonService"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"],
        _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSnackBar"],
        _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["NgZone"]])
], MarkattendenceComponent);



/***/ }),

/***/ "./src/app/components/common/footer/footer.component.css":
/*!***************************************************************!*\
  !*** ./src/app/components/common/footer/footer.component.css ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvY29tbW9uL2Zvb3Rlci9mb290ZXIuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/components/common/footer/footer.component.html":
/*!****************************************************************!*\
  !*** ./src/app/components/common/footer/footer.component.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/common/footer/footer.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/components/common/footer/footer.component.ts ***!
  \**************************************************************/
/*! exports provided: FooterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FooterComponent", function() { return FooterComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let FooterComponent = class FooterComponent {
    constructor() { }
    ngOnInit() {
    }
};
FooterComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-footer',
        template: __webpack_require__(/*! ./footer.component.html */ "./src/app/components/common/footer/footer.component.html"),
        styles: [__webpack_require__(/*! ./footer.component.css */ "./src/app/components/common/footer/footer.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], FooterComponent);



/***/ }),

/***/ "./src/app/components/common/header/header.component.css":
/*!***************************************************************!*\
  !*** ./src/app/components/common/header/header.component.css ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvY29tbW9uL2hlYWRlci9oZWFkZXIuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/components/common/header/header.component.html":
/*!****************************************************************!*\
  !*** ./src/app/components/common/header/header.component.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-side-nav></app-side-nav>\n<section id=\"contents\">\n\t<app-top-nav></app-top-nav>\n</section>\n\t\t"

/***/ }),

/***/ "./src/app/components/common/header/header.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/components/common/header/header.component.ts ***!
  \**************************************************************/
/*! exports provided: HeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderComponent", function() { return HeaderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let HeaderComponent = class HeaderComponent {
    constructor() { }
    ngOnInit() {
    }
};
HeaderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-header',
        template: __webpack_require__(/*! ./header.component.html */ "./src/app/components/common/header/header.component.html"),
        styles: [__webpack_require__(/*! ./header.component.css */ "./src/app/components/common/header/header.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], HeaderComponent);



/***/ }),

/***/ "./src/app/components/common/layout/layout.component.css":
/*!***************************************************************!*\
  !*** ./src/app/components/common/layout/layout.component.css ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvY29tbW9uL2xheW91dC9sYXlvdXQuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/components/common/layout/layout.component.html":
/*!****************************************************************!*\
  !*** ./src/app/components/common/layout/layout.component.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-header *ngIf=\"showHead\"></app-header>\n<router-outlet></router-outlet>\n<app-footer></app-footer>\n\n"

/***/ }),

/***/ "./src/app/components/common/layout/layout.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/components/common/layout/layout.component.ts ***!
  \**************************************************************/
/*! exports provided: LayoutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LayoutComponent", function() { return LayoutComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let LayoutComponent = class LayoutComponent {
    constructor() { }
    ngOnInit() {
    }
};
LayoutComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-layout',
        template: __webpack_require__(/*! ./layout.component.html */ "./src/app/components/common/layout/layout.component.html"),
        styles: [__webpack_require__(/*! ./layout.component.css */ "./src/app/components/common/layout/layout.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], LayoutComponent);



/***/ }),

/***/ "./src/app/components/common/side-nav/side-nav.component.css":
/*!*******************************************************************!*\
  !*** ./src/app/components/common/side-nav/side-nav.component.css ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvY29tbW9uL3NpZGUtbmF2L3NpZGUtbmF2LmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/components/common/side-nav/side-nav.component.html":
/*!********************************************************************!*\
  !*** ./src/app/components/common/side-nav/side-nav.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<aside class=\"side-nav\" id=\"show-side-navigation1\">\n  <i class=\"fa fa-bars close-aside hidden-sm hidden-md hidden-lg\" data-close=\"show-side-navigation1\"></i>\n  <div class=\"heading\">\n    <img src=\"https://cardboardchallenge.com/wp-content/uploads/2018/07/aptech-montana-logo.jpg\" alt=\"\">\n  </div>\n  <ul class=\"categories\">\n    <li><i class=\"fa fa-home fa-fw\" aria-hidden=\"true\"></i><a [routerLink]=\"['/']\"> dashboard</a></li>\n    <li routerLinkActive=\"active\"><i class=\"fa fa-home fa-fw\" aria-hidden=\"true\"></i><a [routerLink]=\"['/enquirylist']\"> Enquiry</a></li>\n    <li routerLinkActive=\"active\"><i class=\"fa fa-home fa-fw\" aria-hidden=\"true\"></i><a [routerLink]=\"['/admissionlist']\"> Admission</a></li>\n    <li routerLinkActive=\"active\"><i class=\"fa fa-home fa-fw\" aria-hidden=\"true\"></i><a [routerLink]=\"['/remainingfees']\"> Remainig Fees</a></li>\n    <li routerLinkActive=\"active\"><i class=\"fa fa-home fa-fw\" aria-hidden=\"true\"></i><a [routerLink]=\"['/albumlist']\"> Album</a></li>\n    <li routerLinkActive=\"active\"><i class=\"fa fa-home fa-fw\" aria-hidden=\"true\"></i><a [routerLink]=\"['/timetables']\"> Time Tables</a></li>\n    <li routerLinkActive=\"active\"><i class=\"fa fa-home fa-fw\" aria-hidden=\"true\"></i><a [routerLink]=\"['/documentfolder']\"> Document Folders</a></li>\n    <li routerLinkActive=\"active\"><i class=\"fa fa-home fa-fw\" aria-hidden=\"true\"></i><a [routerLink]=\"['/markattendence']\"> Mark Attendence</a></li>\n  </ul>\n</aside>"

/***/ }),

/***/ "./src/app/components/common/side-nav/side-nav.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/components/common/side-nav/side-nav.component.ts ***!
  \******************************************************************/
/*! exports provided: SideNavComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SideNavComponent", function() { return SideNavComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let SideNavComponent = class SideNavComponent {
    constructor() { }
    ngOnInit() {
    }
};
SideNavComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-side-nav',
        template: __webpack_require__(/*! ./side-nav.component.html */ "./src/app/components/common/side-nav/side-nav.component.html"),
        styles: [__webpack_require__(/*! ./side-nav.component.css */ "./src/app/components/common/side-nav/side-nav.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], SideNavComponent);



/***/ }),

/***/ "./src/app/components/common/top-nav/top-nav.component.css":
/*!*****************************************************************!*\
  !*** ./src/app/components/common/top-nav/top-nav.component.css ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvY29tbW9uL3RvcC1uYXYvdG9wLW5hdi5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/components/common/top-nav/top-nav.component.html":
/*!******************************************************************!*\
  !*** ./src/app/components/common/top-nav/top-nav.component.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nav class=\"navbar navbar-default\">\n    <div class=\"container-fluid\">\n    <div class=\"navbar-header\">\n      <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-1\" aria-expanded=\"false\">\n      <i class=\"fa fa-align-right\"></i>\n      </button>\n    </div>\n    <div class=\"collapse navbar-collapse navbar-left\" id=\"bs-example-navbar-collapse-1\">\n      <ul class=\"nav navbar-nav\">\n        <li><a><i data-show=\"show-side-navigation1\" class=\"fa fa-bars show-side-btn\"></i></a></li>\n      </ul>\n    </div>\n    <div class=\"row\">\n        <div class=\"col-md-3\">\n        </div>\n        <div class=\"col-md-2\" style=\"padding: 15px\">\n            <label>Select Academic Year</label>\n        </div>\n        <div class=\"col-sm-4 col-lg-4\" style=\"padding: 18px\">\n            <div class=\"form-group\">\n              <select class=\"form-control\" name=\"academicyear\" \n                [(ngModel)]=\"year\"(change)=\"changeYear(year)\">\n                <option value=\"\">Select Academic Year</option>\n                <option [selected]=\" year == academy.academic_year_master_id\" [value]=\"academy.academic_year_master_id\"\n                  *ngFor=\"let academy of academicyear\">{{academy.academic_year_master_name}}</option>\n              </select>\n            </div>\n        </div>\n        <div class=\"collapse navbar-collapse navbar-right\" id=\"bs-example-navbar-collapse-1\">\n            <ul class=\"nav navbar-nav\">\n            <li class=\"dropdown\">\n            <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\">My profile <span class=\"caret\"></span></a>\n            <ul class=\"dropdown-menu\">\n            <li><a href=\"#\"><i class=\"fa fa-user-o fw\"></i> My account</a></li>\n            <li><a href=\"#\"><i class=\"fa fa-envelope-o fw\"></i> My inbox</a></li>\n            <li><a href=\"#\"><i class=\"fa fa-question-circle-o fw\"></i> Help</a></li>\n            <li role=\"separator\" class=\"divider\"></li>\n            <li><a (click)=\"logout()\"><i class=\"fa fa-sign-out\"></i> Log out</a></li>\n            </ul>\n            </li>\n            </ul>\n        </div>\n        </div>\n    </div>\n  </nav>"

/***/ }),

/***/ "./src/app/components/common/top-nav/top-nav.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/components/common/top-nav/top-nav.component.ts ***!
  \****************************************************************/
/*! exports provided: TopNavComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TopNavComponent", function() { return TopNavComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");
/* harmony import */ var _services_common_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../services/common.service */ "./src/app/services/common.service.ts");






let TopNavComponent = class TopNavComponent {
    constructor(router, authService, snackbar, commonService, zone) {
        this.router = router;
        this.authService = authService;
        this.snackbar = snackbar;
        this.commonService = commonService;
        this.zone = zone;
    }
    ngOnInit() {
        this.getAcademicYears();
    }
    logout() {
        this.authService.userLogOut().subscribe((response) => {
            console.log(response);
            if (response['status_code'] == 200) {
                localStorage.clear();
                this.router.navigate(['/login']);
            }
            else {
                this.snackbar.open(response['Metadata']['Message'], '', {
                    duration: 3000,
                    verticalPosition: "top",
                    panelClass: ['bg-red']
                });
            }
        });
    }
    getAcademicYears() {
        this.commonService.getAcademicYears().subscribe((response) => {
            this.academicyear = response.body;
            this.year = atob(localStorage.getItem('academicyearid'));
            console.log(this.academicyear);
        });
    }
    changeYear(yearid) {
        localStorage.setItem('academicyearid', btoa(yearid));
        this.zone.runOutsideAngular(() => { $('body').find('.load_table').load('index.html'); });
    }
};
TopNavComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-top-nav',
        template: __webpack_require__(/*! ./top-nav.component.html */ "./src/app/components/common/top-nav/top-nav.component.html"),
        styles: [__webpack_require__(/*! ./top-nav.component.css */ "./src/app/components/common/top-nav/top-nav.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
        _services_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"],
        _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSnackBar"],
        _services_common_service__WEBPACK_IMPORTED_MODULE_5__["CommonService"],
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"]])
], TopNavComponent);



/***/ }),

/***/ "./src/app/components/constant/constant.component.css":
/*!************************************************************!*\
  !*** ./src/app/components/constant/constant.component.css ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvY29uc3RhbnQvY29uc3RhbnQuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/components/constant/constant.component.html":
/*!*************************************************************!*\
  !*** ./src/app/components/constant/constant.component.html ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  constant works!\n</p>\n"

/***/ }),

/***/ "./src/app/components/constant/constant.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/components/constant/constant.component.ts ***!
  \***********************************************************/
/*! exports provided: ConstantComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConstantComponent", function() { return ConstantComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let ConstantComponent = class ConstantComponent {
};
ConstantComponent.defaultLogo = 'assets/images/aptech-montana-logo.jpg';
ConstantComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-constant',
        template: __webpack_require__(/*! ./constant.component.html */ "./src/app/components/constant/constant.component.html"),
        styles: [__webpack_require__(/*! ./constant.component.css */ "./src/app/components/constant/constant.component.css")]
    })
], ConstantComponent);



/***/ }),

/***/ "./src/app/components/dashboard/dashboard.component.css":
/*!**************************************************************!*\
  !*** ./src/app/components/dashboard/dashboard.component.css ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvZGFzaGJvYXJkL2Rhc2hib2FyZC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/components/dashboard/dashboard.component.html":
/*!***************************************************************!*\
  !*** ./src/app/components/dashboard/dashboard.component.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section id=\"contents\">\n  <div class=\"welcome\">\n    <div class=\"container-fluid\">\n      <div class=\"row\">\n        <div class=\"col-md-12\">\n          <div class=\"content\">\n            <h2>Welcome to Dashboard</h2>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</section>"

/***/ }),

/***/ "./src/app/components/dashboard/dashboard.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/components/dashboard/dashboard.component.ts ***!
  \*************************************************************/
/*! exports provided: DashboardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardComponent", function() { return DashboardComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let DashboardComponent = class DashboardComponent {
    constructor() { }
    ngOnInit() {
        localStorage.removeItem('studentid');
        localStorage.removeItem('tabindex');
        localStorage.removeItem('enquiryNO');
        localStorage.removeItem('docsave');
        localStorage.removeItem('category');
        localStorage.removeItem('course');
    }
};
DashboardComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-dashboard',
        template: __webpack_require__(/*! ./dashboard.component.html */ "./src/app/components/dashboard/dashboard.component.html"),
        styles: [__webpack_require__(/*! ./dashboard.component.css */ "./src/app/components/dashboard/dashboard.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], DashboardComponent);



/***/ }),

/***/ "./src/app/components/enquiry/addeditenquiry/addeditenquiry.component.css":
/*!********************************************************************************!*\
  !*** ./src/app/components/enquiry/addeditenquiry/addeditenquiry.component.css ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".before-title, .title-text {\n  font-family: monospace;}\n\n.before-title {\n  position: absolute;\n  top: 7px;\n  left: 7px;\n  color: #E9237F;\n    z-index: 1 ; \n}\n\n#holder {\n    position: relative; \n    overflow: hidden;\n}\n\n.title-text {\n  line-height: 40px;\n  letter-spacing: 1px;\n  width: 100%;\n  display: block;\n  overflow: auto;\n  z-index : 99999 ;   \n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9lbnF1aXJ5L2FkZGVkaXRlbnF1aXJ5L2FkZGVkaXRlbnF1aXJ5LmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxzQkFBc0IsQ0FBQzs7QUFFekI7RUFDRSxrQkFBa0I7RUFDbEIsUUFBUTtFQUNSLFNBQVM7RUFDVCxjQUFjO0lBQ1osV0FBVztBQUNmOztBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLGdCQUFnQjtBQUNwQjs7QUFDQTtFQUNFLGlCQUFpQjtFQUNqQixtQkFBbUI7RUFDbkIsV0FBVztFQUNYLGNBQWM7RUFDZCxjQUFjO0VBQ2QsZ0JBQWdCO0FBQ2xCIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9lbnF1aXJ5L2FkZGVkaXRlbnF1aXJ5L2FkZGVkaXRlbnF1aXJ5LmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuYmVmb3JlLXRpdGxlLCAudGl0bGUtdGV4dCB7XG4gIGZvbnQtZmFtaWx5OiBtb25vc3BhY2U7fVxuXG4uYmVmb3JlLXRpdGxlIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDdweDtcbiAgbGVmdDogN3B4O1xuICBjb2xvcjogI0U5MjM3RjtcbiAgICB6LWluZGV4OiAxIDsgXG59XG4jaG9sZGVyIHtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7IFxuICAgIG92ZXJmbG93OiBoaWRkZW47XG59XG4udGl0bGUtdGV4dCB7XG4gIGxpbmUtaGVpZ2h0OiA0MHB4O1xuICBsZXR0ZXItc3BhY2luZzogMXB4O1xuICB3aWR0aDogMTAwJTtcbiAgZGlzcGxheTogYmxvY2s7XG4gIG92ZXJmbG93OiBhdXRvO1xuICB6LWluZGV4IDogOTk5OTkgOyAgIFxufSJdfQ== */"

/***/ }),

/***/ "./src/app/components/enquiry/addeditenquiry/addeditenquiry.component.html":
/*!*********************************************************************************!*\
  !*** ./src/app/components/enquiry/addeditenquiry/addeditenquiry.component.html ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"contents\">\n\t<div class=\"loader\" *ngIf=\"isLoading\">\n\t\t<img src=\"assets/images/loader.gif\">\n\t</div>\n\t<div class=\"container-fluid\">\n\t  <section class=\"content\">\n\t    <div id=\"main-form-content\" class=\"load_table\">\n\t      <form [formGroup]=\"EnquiryForm\" (ngSubmit)=\"OnSubmit()\" class=\" org-form all-com-form\"\n\t        enctype=\"multipart/form-data\">\n\t        <div class=\"qtraq-subtitle-wrapper\">\n\t          <h1>Add Lead/Enquiry</h1>\n\t        </div>\n\t        <div class=\"row\">\n\t        \t<div class=\"col-sm-6\">\n\t\t            <div class=\"form-group mt10\">\n\t\t            \t<label>Choose Lead/Enquiry</label>\n\t         \t\t\t<br>\n\t\t\t\t\t\t<input type=\"radio\" [(ngModel)]=\"rectype\" formControlName=\"rectype\" name=\"rectype\" [value]=\"true\" [checked]=\"rectype\" style=\"margin-right: 10px\" (change) = \"rectypeChange(rectype)\" [attr.disabled]=\"leaddisable ? '' : null\">Lead\n\t\t\t\t\t\t<input type=\"radio\" [(ngModel)]=\"rectype\" formControlName=\"rectype\" name=\"rectype\" [value]=\"false\" [checked]=\"!rectype\" style=\"margin-right: 10px\" (change) = \"rectypeChange(rectype)\" [attr.disabled]=\"enquirydisable ? '' : null\">Enquiry\n\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t            </div>\n\t\t        </div>\n\t          \t<div class=\"col-sm-6\" *ngIf = \"rectype\">\n\t\t            <div class=\"form-group mt10\">\n\t\t\t\t\t\t<label class=\"text_green\"> Lead No </label>\n\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"inquiryno\" [(ngModel)]=\"inquiryno\" formControlName=\"inquiryno\" readonly=\"readonly\">\n\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t            </div>\n\t\t        </div>\n\n\t\t\t\t<div class=\"col-sm-6\" *ngIf = \"!rectype\">\n\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t<label class=\"text_green\"> Academic Year<span class=\"text-danger\">*</span></label>\n\t\t\t\t\t\t<select class=\"form-control\" name=\"academicyear\" formControlName=\"academicyear\" [(ngModel)]=\"academicyear\" [attr.disabled]=\"disableYear ? '' : null\">\n\t\t\t                <option value=\"\" disabled=\"disabled\">Select Academic Year</option>\n\t\t\t                <option [selected]=\" academicyear == academy.academic_year_master_id\" [value]=\"academy.academic_year_master_id\"\n\t\t\t                  *ngFor=\"let academy of academicyears\">{{academy.academic_year_master_name}}</option>\n\t\t              \t</select>\n\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"enquiryFormSubmitted && \n            \t\t\t\tEnquiryForm.controls.academicyear.errors\">\n                \t\t\t<span *ngIf=\"EnquiryForm.controls.academicyear.errors.required\">Select Academic Year</span>\n              \t\t\t</div>\n\t\t            </div>\n\t\t        </div>\n\t        </div>\n\n\t        <div class=\"row\" *ngIf = \"!rectype\">\n\t        \t<div class=\"col-sm-6\">\n\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t<label class=\"text_green\"> Enquiry Date<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t<input ngx-mydatepicker formControlName=\"inquirydate\" [(ngModel)]=\"ngxinquirydate\" name=\"inquirydate\" (dateChanged)=\"onEnquiryDateChanged($event)\" [options]=\"myOptionsInquirydate\"\n\t\t\t\t        #dp1=\"ngx-mydatepicker\" (click)=\"dp1.toggleCalendar()\" class=\"form-control w100\" placeholder=\"Select Enquiry Date\" [attr.disabled]=\"disableEnquirydate ? '' : null\">\n\t\t\t\t        <div class=\"clearfix\"></div>\n\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"enquiryFormSubmitted && \n            \t\t\t\tEnquiryForm.controls.inquirydate.errors\">\n                \t\t\t<span *ngIf=\"EnquiryForm.controls.inquirydate.errors.required\">Select Enquiry Date</span>\n              \t\t\t</div>\n\t\t            </div>\n\t\t        </div>\n\n\t\t\t\t<div class=\"col-sm-6\" *ngIf= \"leadenableCreate\">\n\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t<label class=\"text_green\"> Select Lead </label><br>\n\t\t\t\t\t\t<select2 [cssImport]=true  [options]=\"optionsSelect\"  (valueChanged)=\"getInquiryDetails($event)\">\n\t\t\t\t\t\t\t<option>Select Lead</option>\n\t\t\t\t\t\t\t<option *ngFor=\"let lead of leads\"> {{ lead }}</option>\n\t\t\t\t\t\t</select2>\n\t\t            </div>\n\t\t        </div>\n\t\t        <div class=\"col-sm-6\" *ngIf= \"leaddisableCreate\">\n\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t<label class=\"text_green\"> Lead </label><br>\n\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"leadid\" [(ngModel)]=\"leadId\" [value]=\"lead1\" formControlName=\"leadid\" readonly=\"readonly\">\n\t\t            </div>\n\t\t        </div>\n\t        </div>\n\t\t\t<h3 style=\"background-color: red;color: white;\">CHILD INFORMATION</h3> \n\t        <div class=\"row\">\n\t          \t<div class=\"col-sm-6\">\n\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t<label class=\"text_green\"> First Name<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"student_first_name\" [(ngModel)]=\"student_first_name\" formControlName=\"student_first_name\"\n\t\t\t\t\t\tplaceholder=\"Enter First Name\">\n\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"enquiryFormSubmitted && \n            \t\t\t\tEnquiryForm.controls.student_first_name.errors\">\n                \t\t\t<span *ngIf=\"EnquiryForm.controls.student_first_name.errors.required\">Please Enter Student First Name</span>\n              \t\t\t</div>\n\t\t            </div>\n\t\t        </div>\n\t\t        <div class=\"col-sm-6\">\n\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t<label class=\"text_green\"> Last Name<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"student_last_name\"[(ngModel)]=\"student_last_name\" formControlName=\"student_last_name\"\n\t\t\t\t\t\tplaceholder=\"Enter Last Name\">\n\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"enquiryFormSubmitted && \n            \t\t\t\tEnquiryForm.controls.student_last_name.errors\">\n                \t\t\t<span *ngIf=\"EnquiryForm.controls.student_last_name.errors.required\">Please Enter Student Last Name</span>\n              \t\t\t</div>\n\t\t            </div>\n\t\t        </div>\n\t        </div>\n\n\t        <div class=\"row\" *ngIf = \"!rectype\">\n\t          \t<div class=\"col-sm-6\">\n\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t<label class=\"text_green\"> Date Of Birth<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t<input ngx-mydatepicker name=\"student_dob\" (dateChanged)=\"onDobChanged($event)\" [options]=\"myOptions\" [(ngModel)]=\"ngxstudentdob\"\n\t\t\t\t        #dp2=\"ngx-mydatepicker\" formControlName=\"student_dob\" (click)=\"dp2.toggleCalendar()\" class=\"form-control w100\" placeholder=\"Select Birth Date\">\n\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"enquiryFormSubmitted && \n            \t\t\t\tEnquiryForm.controls.student_dob.errors\">\n                \t\t\t<span *ngIf=\"EnquiryForm.controls.student_dob.errors.required\">Select Date Of Birth</span>\n              \t\t\t</div>\n\t\t            </div>\n\t\t        </div>\n\t\t        <div class=\"col-sm-3\">\n\t\t            <div class=\"form-group mt10\">\n\t\t\t\t\t\t<label class=\"text_green\"> Years </label>\n\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"age_year\" [(ngModel)]=\"age_year\" formControlName=\"age_year\" placeholder=\"Enter Year\" disabled=\"disabled\">\t\n\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t            </div>\n\t\t        </div>\n\t\t        <div class=\"col-sm-3\">\n\t\t            <div class=\"form-group mt10\">\n\t\t\t\t\t\t<label class=\"text_green\"> Months </label>\n\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"age_month\" [(ngModel)]=\"age_month\" formControlName=\"age_month\" placeholder=\"Enter Months\" disabled=\"disabled\">\n\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t            </div>\n\t\t        </div>\n\t        </div>\n\n\t        <div class=\"row\" *ngIf = \"!rectype\">\n\t          \t<div class=\"col-sm-6\">\n\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t<label class=\"text_green\"> Category<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t<select class=\"form-control\" name=\"category_id\" formControlName=\"category_id\" [(ngModel)]=\"categoryId\" (change)=\"getCourses(categoryId)\">\n\t\t\t                <option value=\"\" selected=\"selected\">Select Category</option>\n\t\t\t                <option [selected]=\" categoryId == cat.category_id\" [value]=\"cat.category_id\"\n\t\t\t                  *ngFor=\"let cat of category\">{{cat.categoy_name}}</option>\n\t\t              \t</select>\n\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"enquiryFormSubmitted && \n            \t\t\t\tEnquiryForm.controls.category_id.errors\">\n                \t\t\t<span *ngIf=\"EnquiryForm.controls.category_id.errors.required\">Select Category</span>\n              \t\t\t</div>\n\t\t            </div>\n\t\t        </div>\n\t\t        <div class=\"col-sm-6\">\n\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t<label class=\"text_green\"> Course<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t<select class=\"form-control\" name=\"course_id\" formControlName=\"course_id\" [(ngModel)]=\"courseId\">\n\t\t\t                <option value=\"\">Select Course</option>\n\t\t\t                <option [selected]=\" courseId == course.course_id\" [value]=\"course.course_id\"\n\t\t\t                  *ngFor=\"let course of courses\">{{course.course_name}}</option>\n\t\t              \t</select>\n\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"enquiryFormSubmitted && \n            \t\t\t\tEnquiryForm.controls.course_id.errors\">\n                \t\t\t<span *ngIf=\"EnquiryForm.controls.course_id.errors.required\">Select Course</span>\n              \t\t\t</div>\n\t\t            </div>\n\t\t        </div>\n\t        </div>\n\n\t        <div class=\"row\" *ngIf = \"!rectype\">\n\t          \t<div class=\"col-sm-6\">\n\t\t            <div class=\"form-group mt10\">\n\t\t            \t<label>Gender</label>\n\t         \t\t\t<br>\n\t\t\t\t\t\t<input type=\"radio\" [(ngModel)]=\"gender\" formControlName=\"gender\" name=\"gender\" [value]=\"true\" [checked]=\"gender\">Male\n\t\t\t\t\t\t<input type=\"radio\" [(ngModel)]=\"gender\" formControlName=\"gender\" name=\"gender\" [value]=\"false\"[checked]=\"!gender\">Female\n\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t            </div>\n\t\t        </div>\n\t\t        <div class=\"col-sm-6\">\n\t\t            <div class=\"form-group mt10\">\n\t         \t\t\t<label>Has Child attended preschool before?</label>\n\t         \t\t\t<br>\n\t\t\t\t\t\t<input type=\"radio\" name=\"has_attended_preschool_before\" [value]=\"true\" [(ngModel)]=\"has_attended_preschool_before\" formControlName=\"has_attended_preschool_before\"[checked]=\"has_attended_preschool_before\">Yes\n\t\t\t\t\t\t<input type=\"radio\" name=\"has_attended_preschool_before\" [value]=\"false\" [(ngModel)]=\"has_attended_preschool_before\" formControlName=\"has_attended_preschool_before\" [checked]=\"!has_attended_preschool_before\">No\n\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t            </div>\n\t\t        </div>\n\t        </div>\n\n\t\t\t<div class=\"row\" *ngIf=\"has_attended_preschool_before && !rectype\">\n\t         \t<div class=\"col-sm-6\">\n\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t<label class=\"text_green\"> Preschool Name<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"preschool_name\" [(ngModel)]=\"preschool_name\" formControlName=\"preschool_name\"\n\t\t\t\t\t\tplaceholder=\"Enter Preschool Name\">\n\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"enquiryFormSubmitted && \n            \t\t\t\tEnquiryForm.controls.preschool_name.errors\">\n                \t\t\t<span *ngIf=\"EnquiryForm.controls.preschool_name.errors.required\">Enter Preschool Name</span>\n              \t\t\t</div>\n\t\t            </div>\n\t\t        </div>\n\t        </div>\n\t        <h3 style=\"background-color: red;color: white;\">FAMILY INFORMATION</h3>\n\t        <div class=\"row\">\n\t          \t<div class=\"col-sm-6\">\n\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t<label class=\"text_green\"> Father's Name<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"father_name\" [(ngModel)]=\"father_name\" formControlName=\"father_name\"\n\t\t\t\t\t\tplaceholder=\"Enter Father Name\">\n\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"enquiryFormSubmitted && \n            \t\t\t\tEnquiryForm.controls.father_name.errors\">\n                \t\t\t<span *ngIf=\"EnquiryForm.controls.father_name.errors.required\">Please Enter Father Name</span>\n              \t\t\t</div>\n\t\t            </div>\n\t\t        </div>\n\t\t        <div class=\"col-sm-6\" *ngIf = \"!rectype\">\n\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t<label class=\"text_green\"> Mother's Name<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"mother_name\" [(ngModel)]=\"mother_name\" formControlName=\"mother_name\"\n\t\t\t\t\t\tplaceholder=\"Enter Mother Name\">\n\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"enquiryFormSubmitted && \n            \t\t\t\tEnquiryForm.controls.mother_name.errors\">\n                \t\t\t<span *ngIf=\"EnquiryForm.controls.mother_name.errors.required\">Please Enter Student Mother Name</span>\n              \t\t\t</div>\n\t\t            </div>\n\t\t        </div>\n\t\t        <div class=\"col-sm-6\" *ngIf = \"rectype\">\n\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t<label class=\"text_green\"> Country<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t<select class=\"form-control\" name=\"country_id\" formControlName=\"country_id\" [(ngModel)]=\"countryId\" (change)=\"getState(countryId)\">\n\t\t\t                <option value=\"\" selected=\"selected\">Select Country</option>\n\t\t\t                <option [selected]=\" countryId == country.country_id\" [value]=\"country.country_id\"\n\t\t\t                  *ngFor=\"let country of countries\">{{country.country_name}}</option>\n\t\t              \t</select>\n\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"enquiryFormSubmitted && \n            \t\t\t\tEnquiryForm.controls.country_id.errors\">\n                \t\t\t<span *ngIf=\"EnquiryForm.controls.country_id.errors.required\">Select Country</span>\n              \t\t\t</div>\n\t\t            </div>\n\t\t        </div>\n\t        </div>\n\n\t        <div class=\"row\" *ngIf = \"rectype\">\n\t        \t<div class=\"col-sm-6\">\n\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t<label class=\"text_green\"> State<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t<select class=\"form-control\" name=\"state_id\" formControlName=\"state_id\" [(ngModel)]=\"stateId\" (change)=\"getCity(stateId)\">\n\t\t\t                <option value=\"\">Select State</option>\n\t\t\t                <option [selected]=\" stateId == state.state_id\" [value]=\"state.state_id\"\n\t\t\t                  *ngFor=\"let state of states\">{{state.state_name}}</option>\n\t\t              \t</select>\n\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"enquiryFormSubmitted && \n            \t\t\t\tEnquiryForm.controls.state_id.errors\">\n                \t\t\t<span *ngIf=\"EnquiryForm.controls.state_id.errors.required\">Select State</span>\n              \t\t\t</div>\n\t\t            </div>\n\t\t        </div>\n\t\t        <div class=\"col-sm-6\">\n\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t<label class=\"text_green\"> City<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t<select class=\"form-control\" name=\"city_id\" formControlName=\"city_id\" [(ngModel)]=\"cityId\">\n\t\t\t                <option value=\"\" selected=\"selected\">Select City</option>\n\t\t\t                <option [selected]=\" cityId == city.city_id\" [value]=\"city.city_id\"\n\t\t\t                  *ngFor=\"let city of cities\">{{city.city_name}}</option>\n\t\t              \t</select>\n\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"enquiryFormSubmitted && \n            \t\t\t\tEnquiryForm.controls.city_id.errors\">\n                \t\t\t<span *ngIf=\"EnquiryForm.controls.city_id.errors.required\">Select City</span>\n              \t\t\t</div>\n\t\t            </div>\n\t\t        </div>\n\t        </div>\n\n\t        <div class=\"row\" *ngIf = \"rectype\">\n\t        \t<div class=\"col-sm-6\">\n\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t<label class=\"text_green\"> Pincode<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t<input type=\"number\" class=\"form-control\" name=\"pincode\" [(ngModel)]=\"pincode\" formControlName=\"pincode\"\n\t\t\t\t\t\tplaceholder=\"Enter Pincode\">\n\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"enquiryFormSubmitted && \n            \t\t\t\tEnquiryForm.controls.pincode.errors\">\n                \t\t\t<span *ngIf=\"EnquiryForm.controls.pincode.errors.required\">Please Enter Pincode</span>\n              \t\t\t</div>\n\t\t            </div>\n\t\t        </div>\n\t\t        <div class=\"col-sm-6\">\n\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t<label class=\"text_green\">Father Contact Number<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t<input type=\"number\" class=\"form-control\" name=\"father_contact_no\" [(ngModel)]=\"father_contact_no\" formControlName=\"father_contact_no\"\n\t\t\t\t\t\tplaceholder=\"Enter Father Contact No\">\n\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"enquiryFormSubmitted && \n            \t\t\t\tEnquiryForm.controls.father_contact_no.errors\">\n                \t\t\t<span *ngIf=\"EnquiryForm.controls.father_contact_no.errors.required\">Please Enter Father Contact No</span>\n              \t\t\t</div>\n\t\t            </div>\n\t\t        </div>\n\t        </div>\n\n\t        <div class=\"row\" *ngIf = \"rectype\">\n\t\t        <div class=\"col-sm-6\">\n\t\t            <div class=\"form-group mt10\">\n\t\t\t\t\t\t<label class=\"text_green\"> Father's Email Id<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"father_emailid\" [(ngModel)]=\"father_emailid\" formControlName=\"father_emailid\"\n\t\t\t\t\t\tplaceholder=\"Enter Father Email Id\">\n\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"enquiryFormSubmitted && \n            \t\t\t\tEnquiryForm.controls.father_emailid.errors\">\n                \t\t\t<span *ngIf=\"EnquiryForm.controls.father_emailid.errors.email\">Enter Proper Email</span>\n              \t\t\t</div>\n\t\t            </div>\n\t\t        </div>\n\t          \t<div class=\"col-sm-6\">\n\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t<label class=\"text_green\"> Enquiry Status<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t<select class=\"form-control\" name=\"inquirystatus\" formControlName=\"inquirystatus\" [(ngModel)]=\"statusId\">\n\t\t\t                <option value=\"\" selected=\"selected\">Select  Enquiry Status</option>\n\t\t\t\t\t\t    <option [selected]=\" statusId == st\" [value]=\"st\"\n\t\t\t\t\t\t    *ngFor=\"let st of stauts\">{{st}}</option>\n\t\t              \t</select>\n\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"enquiryFormSubmitted && \n            \t\t\t\tEnquiryForm.controls.inquirystatus.errors\">\n                \t\t\t<span *ngIf=\"EnquiryForm.controls.inquirystatus.errors.required\">Select Enquiry Status</span>\n              \t\t\t</div>\n\t\t            </div>\n\t\t        </div>\n\t        </div>\n\n\t        <div class=\"row\" *ngIf = \"rectype\">\n\t\t        <div class=\"col-sm-6\">\n\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t<label class=\"text_green\"> Enquiry Progress<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t<select class=\"form-control\" name=\"inquiryprogress\" formControlName=\"inquiryprogress\" [(ngModel)]=\"progressId\">\n\t\t\t                <option value=\"\" selected=\"selected\">Select Enquiry Progress</option>\n\t\t\t\t\t\t    <option [selected]=\" progressId == pr\" [value]=\"pr\"\n\t\t\t\t\t\t    *ngFor=\"let pr of progress\">{{pr}}</option>\n\t\t              \t</select>\n\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"enquiryFormSubmitted && \n            \t\t\t\tEnquiryForm.controls.inquiryprogress.errors\">\n                \t\t\t<span *ngIf=\"EnquiryForm.controls.inquiryprogress.errors.required\">Select Enquiry Progress</span>\n              \t\t\t</div>\n\t\t            </div>\n\t\t        </div>\n\t        </div>\n\n\t        <div class=\"row\" *ngIf = \"!rectype\">\n\t          \t<div class=\"col-sm-12\">\n\t\t            <div class=\"form-group mt10\">\n\t\t\t\t\t\t<label class=\"text_green\">Present Address<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t<textarea type=\"text\" placeholder=\"Present Address\" class=\"form-control textarea-150\" name=\"present_address\" [(ngModel)]=\"present_address\" formControlName=\"present_address\" required></textarea>\n\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"enquiryFormSubmitted && \n            \t\t\t\tEnquiryForm.controls.present_address.errors\">\n                \t\t\t<span *ngIf=\"EnquiryForm.controls.present_address.errors.required\">Please Enter Present Address</span>\n              \t\t\t</div>\n\t\t            </div>\n\t\t        </div>\n\t        </div>\n\n\t        <div class=\"row\"  *ngIf = \"!rectype\">\n\t          \t<div class=\"col-sm-6\">\n\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t<label class=\"text_green\"> Country<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t<select class=\"form-control\" name=\"country_id\" formControlName=\"country_id\" [(ngModel)]=\"countryId\" (change)=\"getState(countryId)\">\n\t\t\t                <option value=\"\" selected=\"selected\">Select Country</option>\n\t\t\t                <option [selected]=\" countryId == country.country_id\" [value]=\"country.country_id\"\n\t\t\t                  *ngFor=\"let country of countries\">{{country.country_name}}</option>\n\t\t              \t</select>\n\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"enquiryFormSubmitted && \n            \t\t\t\tEnquiryForm.controls.country_id.errors\">\n                \t\t\t<span *ngIf=\"EnquiryForm.controls.country_id.errors.required\">Select Country</span>\n              \t\t\t</div>\n\t\t            </div>\n\t\t        </div>\n\t\t        <div class=\"col-sm-6\">\n\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t<label class=\"text_green\"> State<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t<select class=\"form-control\" name=\"state_id\" formControlName=\"state_id\" [(ngModel)]=\"stateId\" (change)=\"getCity(stateId)\">\n\t\t\t                <option value=\"\">Select State</option>\n\t\t\t                <option [selected]=\" stateId == state.state_id\" [value]=\"state.state_id\"\n\t\t\t                  *ngFor=\"let state of states\">{{state.state_name}}</option>\n\t\t              \t</select>\n\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"enquiryFormSubmitted && \n            \t\t\t\tEnquiryForm.controls.state_id.errors\">\n                \t\t\t<span *ngIf=\"EnquiryForm.controls.state_id.errors.required\">Select State</span>\n              \t\t\t</div>\n\t\t            </div>\n\t\t        </div>\n\t        </div>\n\n\t        <div class=\"row\"  *ngIf = \"!rectype\">\n\t          \t<div class=\"col-sm-6\">\n\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t<label class=\"text_green\"> City<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t<select class=\"form-control\" name=\"city_id\" formControlName=\"city_id\" [(ngModel)]=\"cityId\">\n\t\t\t                <option value=\"\" selected=\"selected\">Select City</option>\n\t\t\t                <option [selected]=\" cityId == city.city_id\" [value]=\"city.city_id\"\n\t\t\t                  *ngFor=\"let city of cities\">{{city.city_name}}</option>\n\t\t              \t</select>\n\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"enquiryFormSubmitted && \n            \t\t\t\tEnquiryForm.controls.city_id.errors\">\n                \t\t\t<span *ngIf=\"EnquiryForm.controls.city_id.errors.required\">Select City</span>\n              \t\t\t</div>\n\t\t            </div>\n\t\t        </div>\n\t\t        <div class=\"col-sm-6\">\n\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t<label class=\"text_green\"> Pincode<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t<input type=\"number\" class=\"form-control\" name=\"pincode\" [(ngModel)]=\"pincode\" formControlName=\"pincode\"\n\t\t\t\t\t\tplaceholder=\"Enter Pincode\">\n\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"enquiryFormSubmitted && \n            \t\t\t\tEnquiryForm.controls.pincode.errors\">\n                \t\t\t<span *ngIf=\"EnquiryForm.controls.pincode.errors.required\">Please Enter Pincode</span>\n              \t\t\t</div>\n\t\t            </div>\n\t\t        </div>\n\t        </div>\n\n\t        <div class=\"row\" *ngIf = \"!rectype\">\n\t          \t<div class=\"col-sm-6\">\n\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t<label class=\"text_green\">Father Contact Number<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t<input type=\"number\" class=\"form-control\" name=\"father_contact_no\" [(ngModel)]=\"father_contact_no\" formControlName=\"father_contact_no\"\n\t\t\t\t\t\tplaceholder=\"Enter Father Contact No\">\n\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"enquiryFormSubmitted && \n            \t\t\t\tEnquiryForm.controls.father_contact_no.errors\">\n                \t\t\t<span *ngIf=\"EnquiryForm.controls.father_contact_no.errors.required\">Please Enter Father Contact No</span>\n              \t\t\t</div>\n\t\t            </div>\n\t\t        </div>\n\t\t        <div class=\"col-sm-6\">\n\t\t            <div class=\"form-group mt10\">\n\t\t\t\t\t\t<label class=\"text_green\">Mother Contact Number<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t<input type=\"number\" class=\"form-control\" name=\"mother_contact_no\" [(ngModel)]=\"mother_contact_no\" formControlName=\"mother_contact_no\"\n\t\t\t\t\t\tplaceholder=\"Enter Mother Contact No\">\n\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"enquiryFormSubmitted && \n            \t\t\t\tEnquiryForm.controls.mother_contact_no.errors\">\n                \t\t\t<span *ngIf=\"EnquiryForm.controls.mother_contact_no.errors.required\">Please Enter Mothetr Contact No</span>\n              \t\t\t</div>\n\t\t            </div>\n\t\t        </div>\n\t        </div>\n\n\n\t        <div class=\"row\" *ngIf = \"!rectype\">\n\t\t        <div class=\"col-sm-6\">\n\t\t            <div class=\"form-group mt10\">\n\t\t\t\t\t\t<label class=\"text_green\"> Father's Email Id<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"father_emailid\" [(ngModel)]=\"father_emailid\" formControlName=\"father_emailid\"\n\t\t\t\t\t\tplaceholder=\"Enter Father Email Id\">\n\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"enquiryFormSubmitted && \n            \t\t\t\tEnquiryForm.controls.father_emailid.errors\">\n                \t\t\t<span *ngIf=\"EnquiryForm.controls.father_emailid.errors.email\">Enter Proper Email</span>\n              \t\t\t</div>\n\t\t            </div>\n\t\t        </div>\n\t\t        <div class=\"col-sm-6\">\n\t\t            <div class=\"form-group mt10\">\n\t\t\t\t\t\t<label class=\"text_green\"> Mother Email Id<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"mother_emailid\" [(ngModel)]=\"mother_emailid\" formControlName=\"mother_emailid\"\n\t\t\t\t\t\tplaceholder=\"Enter Mother Email Id\">\n\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"enquiryFormSubmitted && \n            \t\t\t\tEnquiryForm.controls.mother_emailid.errors\">\n                \t\t\t<span *ngIf=\"EnquiryForm.controls.mother_emailid.errors.email\">Enter Proper Email</span>\n              \t\t\t</div>\n\t\t            </div>\n\t\t        </div>\n\t        </div>\n\n\t        <div class=\"row\" *ngIf = \"!rectype\">\n\t\t        <div class=\"col-sm-6\">\n\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t<label class=\"text_green\"> Father's Education<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"father_education\" [(ngModel)]=\"father_education\" formControlName=\"father_education\" placeholder=\"Enter Father Education\">\n\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"enquiryFormSubmitted && \n            \t\t\t\tEnquiryForm.controls.father_education.errors\">\n                \t\t\t<span *ngIf=\"EnquiryForm.controls.father_education.errors.required\">Please Enter  Father Education</span>\n              \t\t\t</div>\n\t\t            </div>\n\t\t        </div>\n\t\t        <div class=\"col-sm-6\">\n\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t<label class=\"text_green\"> Mother Education<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"mother_education\" [(ngModel)]=\"mother_education\" formControlName=\"mother_education\" placeholder=\"Enter Mother Education\">\n\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"enquiryFormSubmitted && \n            \t\t\t\tEnquiryForm.controls.mother_education.errors\">\n                \t\t\t<span *ngIf=\"EnquiryForm.controls.mother_education.errors.required\">Please Enter  Mother Education</span>\n              \t\t\t</div>\n\t\t            </div>\n\t\t        </div>\n\t        </div>\n\n\t        <div class=\"row\" *ngIf = \"!rectype\">\n\t\t        <div class=\"col-sm-6\">\n\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t<label class=\"text_green\"> Father's Profession<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"father_profession\" [(ngModel)]=\"father_profession\" formControlName=\"father_profession\" placeholder=\"Enter Father Profession\">\n\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"enquiryFormSubmitted && \n            \t\t\t\tEnquiryForm.controls.father_profession.errors\">\n                \t\t\t<span *ngIf=\"EnquiryForm.controls.father_profession.errors.required\">Please Enter  Father Profession</span>\n              \t\t\t</div>\n\t\t            </div>\n\t\t        </div>\n\t\t        <div class=\"col-sm-6\">\n\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t<label class=\"text_green\"> Mother Profession<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"mother_profession\" [(ngModel)]=\"mother_profession\" formControlName=\"mother_profession\" placeholder=\"Enter Mother Profession\">\n\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"enquiryFormSubmitted && \n            \t\t\t\tEnquiryForm.controls.mother_profession.errors\">\n                \t\t\t<span *ngIf=\"EnquiryForm.controls.mother_profession.errors.required\">Please Enter  Mother Profession</span>\n              \t\t\t</div>\n\t\t            </div>\n\t\t        </div>\n\t        </div>\n\n\t        <div class=\"row\" *ngIf = \"!rectype\">\n\t\t        <div class=\"col-sm-12\">\n\t\t            <div class=\"form-group mt10 required\">\n\t\t            \t<label class=\"text_green\">How did you come to know about the school?</label>\n\t\t            \t<br>\n\t\t\t\t\t\t<label *ngFor=\"let option of how_know_about_school\">\n\t\t\t                <label style=\"margin-right: 10px\">\n\t\t\t                    <input type=\"radio\"\n\t\t\t                           name=\"how_know_about_school\"\n\t\t\t                           value=\"{{option}}\" [(ngModel)]=\"nghow_know_about_school\" [checked] = \"selectedCheckbox == option\" formControlName=\"how_know_about_school\"\n\t\t\t                           (change)=\"updateCheckedOptions(option, $event)\"/>\n\t\t\t                    {{option}}\n\t\t\t                </label>\n\t\t\t            </label>\n\t\t\t\t\t\t<br>\n\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"enquiryFormSubmitted && \n            \t\t\t\tEnquiryForm.controls.how_know_about_school.errors\">\n                \t\t\t<span *ngIf=\"EnquiryForm.controls.how_know_about_school.errors.required\">Select atleast one</span>\n              \t\t\t</div>\n\t\t            </div>\n\t\t        </div>\n\t        </div>\n\n\t        <div class=\"row\">\n\t        \t<div class=\"col-sm-12 other\">\n\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t<label class=\"text_green\"> Others<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"how_know_about_other\" [(ngModel)]=\"how_know_about_other\" formControlName=\"how_know_about_other\"\n\t\t\t\t\t\tplaceholder=\"Enter Other\">\n\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"enquiryFormSubmitted && \n            \t\t\t\tEnquiryForm.controls.how_know_about_other.errors\">\n                \t\t\t<span *ngIf=\"EnquiryForm.controls.how_know_about_other.errors.required\">Enter how to know about school</span>\n              \t\t\t</div>\n\t\t            </div>\n\t\t        </div>\n\t        </div>\n\n\t         <div class=\"row\" *ngIf = \"!rectype\">\n\t          \t<div class=\"col-sm-6\">\n\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t<label class=\"text_green\"> Enquiry Status<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t<select class=\"form-control\" name=\"inquirystatus\" formControlName=\"inquirystatus\" [(ngModel)]=\"statusId\">\n\t\t\t                <option value=\"\" selected=\"selected\">Select  Enquiry Status</option>\n\t\t\t\t\t\t    <option [selected]=\" statusId == st\" [value]=\"st\"\n\t\t\t\t\t\t    *ngFor=\"let st of stauts\">{{st}}</option>\n\t\t              \t</select>\n\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"enquiryFormSubmitted && \n            \t\t\t\tEnquiryForm.controls.inquirystatus.errors\">\n                \t\t\t<span *ngIf=\"EnquiryForm.controls.inquirystatus.errors.required\">Select Enquiry Status</span>\n              \t\t\t</div>\n\t\t            </div>\n\t\t        </div>\n\t\t        <div class=\"col-sm-6\">\n\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t<label class=\"text_green\"> Enquiry Progress<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t<select class=\"form-control\" name=\"inquiryprogress\" formControlName=\"inquiryprogress\" [(ngModel)]=\"progressId\">\n\t\t\t                <option value=\"\" selected=\"selected\">Select Enquiry Progress</option>\n\t\t\t\t\t\t    <option [selected]=\" progressId == pr\" [value]=\"pr\"\n\t\t\t\t\t\t    *ngFor=\"let pr of progress\">{{pr}}</option>\n\t\t              \t</select>\n\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"enquiryFormSubmitted && \n            \t\t\t\tEnquiryForm.controls.inquiryprogress.errors\">\n                \t\t\t<span *ngIf=\"EnquiryForm.controls.inquiryprogress.errors.required\">Select Enquiry Progress</span>\n              \t\t\t</div>\n\t\t            </div>\n\t\t        </div>\n\t        </div>\n\n\t        <div class=\"row\">\n\t          \t<div class=\"col-sm-12\">\n\t\t            <div class=\"form-group mt10\">\n\t\t\t\t\t\t<label class=\"text_green\">Remark (For Office Use)<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t<div id=\"holder\"><textarea class=\"form-control textarea-150 remark title-text\" placeholder=\"\" name=\"inquiry_remark\" id=\"myTextarea\" formControlName=\"inquiry_remark\" [(ngModel)]=\"inquiry_remark\" [innerHTML]=\"inquiry_remarks\"></textarea>\n\t\t\t\t\t\t<span class=\"before-title\">{{remark_date}}<br/></span>\n\n    \t\t\t\t\t<span id=\"testscrol\"></span></div>\n\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t\t\t\t\t<div class=\"text-danger \" *ngIf=\"enquiryFormSubmitted && \n            \t\t\t\tEnquiryForm.controls.inquiry_remark.errors\">\n                \t\t\t<span *ngIf=\"EnquiryForm.controls.inquiry_remark.errors.required\">Enter Remark</span>\n              \t\t\t</div>\n\t\t            </div>\n\t\t        </div>\n\t\t        <input type=\"hidden\" name=\"hidden_remark\" id=\"hidden_remark\">\n\t        </div>\n\t        \n\t        <div class=\"row\">\n\t          \t<div class=\"col-sm-6\">\n\t\t            <div class=\"form-group mt10 required\">\n\t\t\t\t\t\t<label class=\"text_green\"> Handled By<span class=\"text-danger\">*</span> </label>\n\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"inquiry_handled_by\" [(ngModel)]=\"inquiry_handled_by\" formControlName=\"inquiry_handled_by\" disabled=\"disabled\" [value]=\"inquiry_handled_by\">\n\t\t\t\t\t\t<div class=\"clearfix\"></div>\n\t\t            </div>\n\t\t        </div>\n\t        </div>\n\n            <div class=\"form-group\">\n              <button type=\"submit\" class=\"btn btn-primary\" style=\"margin-right: 10px\">Save</button>\n              <button type=\"button\" routerLink=\"/enquirylist\" class=\"btn btn-primary \">Cancel</button>\n            </div>\n\t      </form>\n\t    </div>\n\t  </section>\n\t</div>\n</div>"

/***/ }),

/***/ "./src/app/components/enquiry/addeditenquiry/addeditenquiry.component.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/components/enquiry/addeditenquiry/addeditenquiry.component.ts ***!
  \*******************************************************************************/
/*! exports provided: AddeditenquiryComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddeditenquiryComponent", function() { return AddeditenquiryComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _services_common_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/common.service */ "./src/app/services/common.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");






let AddeditenquiryComponent = class AddeditenquiryComponent {
    constructor(route, commonService, formBuilder, snackbar, router) {
        this.route = route;
        this.commonService = commonService;
        this.formBuilder = formBuilder;
        this.snackbar = snackbar;
        this.router = router;
        this.myOptions = {
            dateFormat: 'dd-mm-yyyy',
        };
        this.myOptionsInquirydate = {
            dateFormat: 'dd-mm-yyyy',
        };
        this.disableYear = false;
        this.disableEnquirydate = false;
        this.how_know_about_school = ['Website', 'Newspaper', 'FacebookPage', 'JustDial', 'SchoolSignBoard', 'Reference', 'PosterBanner', 'Leaflet', 'Other'];
        this.selectedOptions = [];
        this.optionsMap = {
            Website: true,
            Newspaper: false,
            FacebookPage: false,
            JustDial: false,
            SchoolSignBoard: false,
            Reference: false,
            PosterBanner: false,
            Leaflet: false,
            Other: false,
        };
        this.inquiryDate = false;
        this.optionsChecked = [];
        this.ngxinquirydate = {
            jsdate: new Date()
        };
        this.inquiryno = "";
        this.student_first_name = "";
        this.student_last_name = "";
        this.father_name = "";
        this.mother_name = "";
        this.present_address = "";
        this.pincode = "";
        this.father_contact_no = '';
        this.mother_contact_no = "";
        this.father_emailid = '';
        this.mother_emailid = '';
        this.father_education = '';
        this.mother_education = '';
        this.father_profession = '';
        this.mother_profession = '';
        this.inquiry_remark = '';
        this.inquiry_handled_by = '';
        this.enquiryFormSubmitted = false;
        this.enquiryId = "";
        this.rectype = false;
        this.how_know_about_other = "";
        this.isLoading = false;
        this.statusId = "";
        this.progressId = "";
        this.stauts = ['Hot', 'Cold', 'Warm'];
        this.progress = ['Meeting', 'Call', 'Walk inn'];
        this.enquirydisable = false;
        this.leaddisable = false;
        this.leadenableCreate = true;
        this.leaddisableCreate = false;
        this.remark_date = '';
    }
    ngOnInit() {
        this.optionsSelect = {
            placeholder: "Select Enquiry",
            allowClear: true,
            width: "100%",
        };
        this.academicyear = '2';
        this.nghow_know_about_school = 'Website';
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.inquiry_handled_by = currentUser.first_name + ' ' + currentUser.last_name;
        $('.other').hide();
        this.EnquiryForm = this.formBuilder.group({
            inquiryno: [''],
            inquirydate: [''],
            student_first_name: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]],
            student_last_name: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]],
            student_dob: [''],
            age_year: [''],
            age_month: [''],
            category_id: [''],
            course_id: [''],
            has_attended_preschool_before: [''],
            preschool_name: [''],
            gender: [''],
            rectype: [''],
            academicyear: [''],
            father_name: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]],
            mother_name: [''],
            present_address: [''],
            country_id: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]],
            state_id: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]],
            city_id: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]],
            pincode: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]],
            father_contact_no: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]],
            mother_contact_no: [''],
            father_emailid: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].email]],
            mother_emailid: [''],
            father_education: [''],
            mother_education: [''],
            father_profession: [''],
            mother_profession: [''],
            how_know_about_school: [''],
            how_know_about_other: [''],
            inquiry_remark: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]],
            inquiry_handled_by: [''],
            inquirystatus: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]],
            inquiryprogress: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]],
            leadid: ['']
        });
        this.getCategories();
        this.getCountries();
        this.getInquiryNo();
        this.getAcademicYears();
        this.initOptionsMap();
        this.getLeads();
        this.route.params.subscribe(params => {
            if (this.route.snapshot.params.enquiryid) {
                this.isLoading = true;
                this.leadenableCreate = false;
                this.leaddisableCreate = true;
                this.commonService.getInquiryDetails(this.route.snapshot.params.enquiryid)
                    .subscribe(data => {
                    this.isLoading = false;
                    if (data['body'][0].rec_type == 'Lead') {
                        this.rectype = true;
                        this.enquirydisable = true;
                    }
                    else {
                        this.rectype = false;
                        this.leaddisable = true;
                        this.lead1 = data['body'][0]['lead_no'][0]['inquiry_no'] + ' (' + data['body'][0].student_first_name + ' ' + data['body'][0].student_last_name + ')';
                        this.leadId = data['body'][0].inquiry_master_id;
                    }
                    this.inquiryno = data['body'][0].inquiry_no;
                    this.inquirydate = data['body'][0].inquiry_date;
                    this.inquirydate1 = data['body'][0].inquiry_date.split('-');
                    this.ngxinquirydate = {
                        date: {
                            year: parseInt(this.inquirydate1[0]),
                            month: parseInt(this.inquirydate1[1]),
                            day: parseInt(this.inquirydate1[2])
                        },
                    };
                    this.student_first_name = data['body'][0].student_first_name;
                    this.student_last_name = data['body'][0].student_last_name;
                    this.student_dob = data['body'][0].student_dob;
                    this.student_dob1 = data['body'][0].student_dob.split('-');
                    this.ngxstudentdob = {
                        date: {
                            year: parseInt(this.student_dob1[2]),
                            month: parseInt(this.student_dob1[1]),
                            day: parseInt(this.student_dob1[0])
                        },
                    };
                    this.age_year = data['body'][0].age_year;
                    this.age_month = data['body'][0].age_month;
                    this.categoryId = data['body'][0].category_id;
                    this.getCourses(this.categoryId);
                    this.courseId = data['body'][0].course_id;
                    if (data['body'][0].gender == 'Male') {
                        this.gender = true;
                    }
                    else {
                        this.gender = false;
                    }
                    if (data['body'][0].has_attended_preschool_before == 'Yes') {
                        this.has_attended_preschool_before = true;
                    }
                    else {
                        this.has_attended_preschool_before = false;
                    }
                    this.preschool_name = data['body'][0].preschool_name;
                    this.academicyear = data['body'][0].academic_year_master_id;
                    this.father_name = data['body'][0].father_name;
                    this.mother_name = data['body'][0].mother_name;
                    this.present_address = data['body'][0].present_address;
                    this.countryId = data['body'][0].country_id;
                    this.getState(this.countryId);
                    this.stateId = data['body'][0].state_id;
                    this.getCity(this.stateId);
                    this.cityId = data['body'][0].city_id;
                    this.pincode = data['body'][0].pincode;
                    this.father_contact_no = data['body'][0].father_contact_no;
                    if (this.mother_contact_no != undefined) {
                        this.mother_contact_no = data['body'][0].mother_contact_no;
                    }
                    if (this.father_emailid != undefined) {
                        this.father_emailid = data['body'][0].father_emailid;
                    }
                    if (this.mother_emailid != undefined) {
                        this.mother_emailid = data['body'][0].mother_emailid;
                    }
                    this.father_education = data['body'][0].father_education;
                    this.mother_education = data['body'][0].mother_education;
                    this.father_profession = data['body'][0].father_profession;
                    this.mother_profession = data['body'][0].mother_profession;
                    this.selectedCheckbox = data['body'][0].how_know_about_school;
                    this.selectedOptions.push(data['body'][0].how_know_about_school);
                    this.nghow_know_about_school = data['body'][0].how_know_about_school;
                    if (this.selectedCheckbox == 'Other') {
                        $('.other').show();
                    }
                    else {
                        $('.other').hide();
                    }
                    if (this.how_know_about_other != undefined) {
                        this.how_know_about_other = data['body'][0].how_know_about_other;
                    }
                    this.inquiry_remark = data['body'][0].inquiry_remark;
                    this.updated_on = data['body'][0].updated_on;
                    this.inquiry_handled_by = data['body'][0].inquiry_handled_by;
                    this.inquiryDate = true;
                    this.statusId = data['body'][0].inquiry_status;
                    this.progressId = data['body'][0].inquiry_progress;
                    this.enquiryId = params.enquiryid;
                    console.log(21);
                    var myTextArea = $('#myTextarea');
                    var today = new Date();
                    var dd1 = today.getDate();
                    var mm1 = today.getMonth() + 1; //January is 0!
                    var yy = today.getFullYear();
                    var yyyy = today.getFullYear();
                    if (dd1 < 10) {
                        var dd = '0' + dd1;
                    }
                    else {
                        var dd = '' + dd1;
                    }
                    if (mm1 < 10) {
                        var mm = '0' + mm1;
                    }
                    else {
                        var mm = '' + mm1;
                    }
                    var prev_date = this.updated_on.split(' ');
                    if (yy + '-' + mm + '-' + dd == prev_date[0]) {
                        console.log(1);
                        let split_date = prev_date[0].split('-');
                        this.remark_date = split_date[2] + '-' + split_date[1] + '-' + split_date[0] + ' ::';
                    }
                    //       else{
                    //       	console.log(myTextArea)
                    //        this.remark_date = prev_date[0]
                    //        // myTextArea.setValue('<h1>hi</h1>')
                    // }
                });
                this.disableYear = true;
                this.disableEnquirydate = true;
            }
        });
        if (this.route.snapshot.params.enquiryid == undefined) {
            var myTextArea = $('#myTextarea');
            var today = new Date();
            var dd1 = today.getDate();
            var mm1 = today.getMonth() + 1; //January is 0!
            var yy = today.getFullYear();
            var yyyy = today.getFullYear();
            if (dd1 < 10) {
                var dd = '0' + dd1;
            }
            else {
                var dd = '' + dd1;
            }
            if (mm1 < 10) {
                var mm = '0' + mm1;
            }
            else {
                var mm = '' + mm1;
            }
            this.remark_date = dd + '-' + mm + '-' + yy + ' :: ';
            // myTextArea.val(myTextArea.val() + '</br>')
        }
        else {
        }
    }
    getLeads() {
        this.commonService.getILeads().subscribe((response) => {
            if (response['status_code'] == 200) {
                var arr = [];
                var arr_id = [];
                for (var i = 0; i < response['body'].length; i++) {
                    var no = response['body'][i].inquiry_no;
                    var first_name = response['body'][i].student_first_name;
                    var last_name = response['body'][i].student_last_name;
                    arr.push(no + ' (' + first_name + ' ' + last_name + ' )');
                }
                this.leads = arr;
            }
            else {
                this.snackbar.open(response['msg'], '', {
                    duration: 500,
                    verticalPosition: "top",
                    panelClass: ['bg-red']
                });
            }
        });
    }
    getInquiryDetails(inquiryid) {
        if (this.route.snapshot.params.enquiryid) {
        }
        else {
            if (this.rectype == false && inquiryid.value != null) {
                let inquirysplit = inquiryid.value.split(' ');
                this.getEnquiry = inquirysplit[0];
                this.commonService.getInquiryId(this.getEnquiry).subscribe((response) => {
                    let getInquiryId = response['body']['query_result'][0]['inquiry_master_id'];
                    console.log(getInquiryId);
                    this.commonService.getInquiryDetails(getInquiryId).subscribe((response) => {
                        if (response['status_code'] == 200) {
                            this.leadId = response['body'][0].inquiry_master_id; //instaed of inquiryid 
                            //get leadid
                            // this.inquiryno = response['body'][0].inquiry_no;
                            //       this.academicyear = response['body'][0].academic_year_master_id;
                            //       this.inquirydate = response['body'][0].inquiry_date;	 
                            // this.inquirydate1=response['body'][0].inquiry_date.split('-');
                            // this.ngxinquirydate = {  
                            //                 date: {  
                            //                     year: parseInt(this.inquirydate1[0]),  
                            //                     month: parseInt(this.inquirydate1[1]),  
                            //                     day: parseInt(this.inquirydate1[2])
                            //                 },
                            //             }
                            this.student_first_name = response['body'][0].student_first_name;
                            this.student_last_name = response['body'][0].student_last_name;
                            // this.student_dob=response['body'][0].student_dob;
                            // this.student_dob1=response['body'][0].student_dob.split('-');
                            // this.ngxstudentdob = {  
                            //              date: {  
                            //                  year: parseInt(this.student_dob1[2]),  
                            //                  month: parseInt(this.student_dob1[1]),  
                            //                  day: parseInt(this.student_dob1[0])
                            //              },
                            //          }
                            //          this.age_year = response['body'][0].age_year;
                            //          this.age_month = response['body'][0].age_month;
                            //       this.categoryId = response['body'][0].category_id;
                            // this.getCourses(this.categoryId);
                            // this.courseId = response['body'][0].course_id;
                            // if(response['body'][0].gender == 'Male'){
                            // 	this.gender = true;
                            // }
                            // else{
                            // 	this.gender = false;
                            // }
                            //          if(response['body'][0].has_attended_preschool_before == 'Yes'){
                            // 	this.has_attended_preschool_before = true;
                            // }
                            // else{
                            // 	this.has_attended_preschool_before = false;
                            // }
                            // this.preschool_name = response['body'][0].preschool_name;
                            this.father_name = response['body'][0].father_name;
                            // this.mother_name = response['body'][0].mother_name;
                            // this.present_address = response['body'][0].present_address;
                            this.countryId = response['body'][0].country_id;
                            this.getState(this.countryId);
                            this.stateId = response['body'][0].state_id;
                            this.getCity(this.stateId);
                            this.cityId = response['body'][0].city_id;
                            this.pincode = response['body'][0].pincode;
                            this.father_contact_no = response['body'][0].father_contact_no;
                            // this.mother_contact_no = response['body'][0].mother_contact_no;
                            this.father_emailid = response['body'][0].father_emailid;
                            // this.mother_emailid = response['body'][0].mother_emailid;
                            // this.father_profession = response['body'][0].father_profession;
                            // this.mother_profession = response['body'][0].mother_profession;
                            // this.father_education = response['body'][0].father_education;
                            // this.mother_education = response['body'][0].mother_education;
                            // this.calcDate(this.student_dob);
                        }
                        else {
                            console.log(2);
                        }
                    });
                });
            }
        }
    }
    rectypeChange(value) {
        if (value == true) {
            this.leadId = ''; //instaed of inquiryid 
            //get leadid
            this.getInquiryNo();
            this.inquiryno = this.inquiryno;
            this.academicyear = '2';
            this.student_first_name = '';
            this.student_last_name = '';
            this.father_name = '';
            this.mother_name = '';
            this.countryId = '';
            this.stateId = '';
            this.cityId = '';
            this.pincode = '';
            this.father_contact_no = '';
            this.father_emailid = '';
        }
    }
    initOptionsMap() {
        for (var x = 0; x < this.how_know_about_school.length; x++) {
            this.optionsMap[this.how_know_about_school[x]] = false;
        }
    }
    updateCheckedOptions(option, event) {
        this.selectedOptions = [];
        this.optionsMap[option] = event.target.checked;
        this.selectedOptions.push(option);
        if (option == 'Other' && event.target.checked == true) {
            $('.other').show();
        }
        else {
            $('.other').hide();
        }
    }
    onEnquiryDateChanged(event) {
        if (event.formatted !== '') {
            this.inquirydate = event.formatted;
        }
    }
    onDobChanged(event) {
        if (event.formatted !== '') {
            this.student_dob = event.formatted;
        }
        this.calcDate(this.student_dob);
    }
    calcDate(dob) {
        if (dob != undefined) {
            var dobsplit = dob.split('-');
            var firstsplitlength = dobsplit[0].length;
            if (firstsplitlength == 4) {
                var dobdate = dobsplit[1] + '-' + dobsplit[2] + '-' + dobsplit[0];
            }
            else {
                var dobdate = dobsplit[1] + '-' + dobsplit[0] + '-' + dobsplit[2];
            }
            var diff = Math.abs(new Date().getTime() - new Date(dobdate).getTime());
            var day = 1000 * 60 * 60 * 24;
            var days = Math.ceil(diff / day);
            this.age_month = Math.floor(days / 30);
            console.log(this.age_month);
            this.age_year = Math.floor(this.age_month / 12);
            if (this.age_year > 0) {
                var yeardays = days - (365 * this.age_year);
                let dobmonthfloor = Math.floor(yeardays / 30);
                this.age_month = Math.abs(dobmonthfloor);
            }
        }
    }
    updateOptions() {
        for (var x in this.selectedOptions) {
            if (this.selectedOptions[x]) {
                this.optionsChecked.push(x);
            }
        }
        this.how_know_about_school1 = this.optionsChecked;
        this.optionsChecked = [];
    }
    OnSubmit() {
        this.isLoading = true;
        this.updateOptions();
        this.enquiryFormSubmitted = true;
        if (this.EnquiryForm.value.has_attended_preschool_before === true) {
            this.isLoading = false;
            this.EnquiryForm.get('preschool_name').setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]);
            this.EnquiryForm.get('preschool_name').updateValueAndValidity();
        }
        else {
            this.EnquiryForm.get('preschool_name').clearValidators();
            this.EnquiryForm.get('preschool_name').updateValueAndValidity();
        }
        if (this.EnquiryForm.value.rectype === false) {
            this.EnquiryForm.get('inquirydate').setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]);
            this.EnquiryForm.get('inquirydate').updateValueAndValidity();
            this.EnquiryForm.get('student_dob').setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]);
            this.EnquiryForm.get('student_dob').updateValueAndValidity();
            this.EnquiryForm.get('age_year').setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]);
            this.EnquiryForm.get('age_year').updateValueAndValidity();
            this.EnquiryForm.get('category_id').setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]);
            this.EnquiryForm.get('category_id').updateValueAndValidity();
            this.EnquiryForm.get('course_id').setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]);
            this.EnquiryForm.get('course_id').updateValueAndValidity();
            this.EnquiryForm.get('academicyear').setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]);
            this.EnquiryForm.get('academicyear').updateValueAndValidity();
            this.EnquiryForm.get('present_address').setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]);
            this.EnquiryForm.get('present_address').updateValueAndValidity();
            this.EnquiryForm.get('mother_name').setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]);
            this.EnquiryForm.get('mother_name').updateValueAndValidity();
            this.EnquiryForm.get('mother_contact_no').setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]);
            this.EnquiryForm.get('mother_contact_no').updateValueAndValidity();
            this.EnquiryForm.get('mother_emailid').setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]);
            this.EnquiryForm.get('mother_emailid').updateValueAndValidity();
            this.EnquiryForm.get('father_education').setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]);
            this.EnquiryForm.get('father_education').updateValueAndValidity();
            this.EnquiryForm.get('mother_education').setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]);
            this.EnquiryForm.get('mother_education').updateValueAndValidity();
            this.EnquiryForm.get('father_profession').setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]);
            this.EnquiryForm.get('father_profession').updateValueAndValidity();
            this.EnquiryForm.get('mother_profession').setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]);
            this.EnquiryForm.get('mother_profession').updateValueAndValidity();
            this.EnquiryForm.get('how_know_about_school').setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]);
            this.EnquiryForm.get('how_know_about_school').updateValueAndValidity();
            this.EnquiryForm.value.inquirydate = this.inquirydate;
            this.EnquiryForm.value.student_dob = this.student_dob;
            this.EnquiryForm.value.how_know_about_school = this.selectedOptions.toString();
            this.EnquiryForm.value.leadid = this.leadId;
        }
        else {
            this.EnquiryForm.get('inquirydate').clearValidators();
            this.EnquiryForm.get('inquirydate').updateValueAndValidity();
            this.EnquiryForm.get('student_dob').clearValidators();
            this.EnquiryForm.get('student_dob').updateValueAndValidity();
            this.EnquiryForm.get('age_year').clearValidators();
            this.EnquiryForm.get('age_year').updateValueAndValidity();
            this.EnquiryForm.get('category_id').clearValidators();
            this.EnquiryForm.get('category_id').updateValueAndValidity();
            this.EnquiryForm.get('course_id').clearValidators();
            this.EnquiryForm.get('course_id').updateValueAndValidity();
            this.EnquiryForm.get('academicyear').clearValidators();
            this.EnquiryForm.get('academicyear').updateValueAndValidity();
            this.EnquiryForm.get('present_address').clearValidators();
            this.EnquiryForm.get('present_address').updateValueAndValidity();
            this.EnquiryForm.get('mother_name').clearValidators();
            this.EnquiryForm.get('mother_name').updateValueAndValidity();
            this.EnquiryForm.get('mother_contact_no').clearValidators();
            this.EnquiryForm.get('mother_contact_no').updateValueAndValidity();
            this.EnquiryForm.get('mother_emailid').clearValidators();
            this.EnquiryForm.get('mother_emailid').updateValueAndValidity();
            this.EnquiryForm.get('father_education').clearValidators();
            this.EnquiryForm.get('father_education').updateValueAndValidity();
            this.EnquiryForm.get('mother_education').clearValidators();
            this.EnquiryForm.get('mother_education').updateValueAndValidity();
            this.EnquiryForm.get('father_profession').clearValidators();
            this.EnquiryForm.get('father_profession').updateValueAndValidity();
            this.EnquiryForm.get('mother_profession').clearValidators();
            this.EnquiryForm.get('mother_profession').updateValueAndValidity();
            this.EnquiryForm.get('how_know_about_school').clearValidators();
            this.EnquiryForm.get('how_know_about_school').updateValueAndValidity();
        }
        if (this.EnquiryForm.value.how_know_about_school === 'Other') {
            this.isLoading = false;
            this.EnquiryForm.get('how_know_about_other').setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]);
            this.EnquiryForm.get('how_know_about_other').updateValueAndValidity();
        }
        else {
            this.EnquiryForm.get('how_know_about_other').clearValidators();
            this.EnquiryForm.get('how_know_about_other').updateValueAndValidity();
        }
        if (this.EnquiryForm.value.how_know_about_school === '') {
            this.isLoading = false;
            this.EnquiryForm.value.how_know_about_school = 'Website';
        }
        console.log(this.EnquiryForm.value);
        if (this.EnquiryForm.invalid) {
            this.isLoading = false;
            return;
        }
        this.commonService.addEditEnquiryForm(this.EnquiryForm.value, this.leadId).subscribe((response) => {
            this.isLoading = false;
            if (response['status_code'] == 200) {
                this.snackbar.open(response['Metadata']['Message'], '', {
                    duration: 500,
                    verticalPosition: "top",
                    panelClass: ['bg-green']
                });
                this.router.navigate(['/enquirylist']);
            }
            else {
                this.snackbar.open(response['Metadata']['Message'], '', {
                    duration: 500,
                    verticalPosition: "top",
                    panelClass: ['bg-red']
                });
            }
        });
    }
    getCategories() {
        this.commonService.getCategories().subscribe((response) => {
            this.category = response['body'];
        });
    }
    getCourses(categoryid) {
        this.commonService.getCourses(categoryid).subscribe((response) => {
            this.courses = response['body'];
        });
    }
    getCountries() {
        this.commonService.getCountries().subscribe((response) => {
            this.countries = response['body'];
        });
    }
    getState(countryid) {
        this.commonService.getState(countryid).subscribe((response) => {
            this.states = response['body'];
        });
    }
    getCity(stateid) {
        this.commonService.getCity(stateid).subscribe((response) => {
            this.cities = response['body'];
        });
    }
    getInquiryNo() {
        this.commonService.getInquiryNo().subscribe((response) => {
            if (this.route.snapshot.params.enquiryid == undefined) {
                this.inquiryno = response['body'];
            }
        });
    }
    getAcademicYears() {
        this.commonService.getAcademicYears().subscribe((response) => {
            this.academicyears = response.body;
        });
    }
};
AddeditenquiryComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
        selector: 'app-addeditenquiry',
        template: __webpack_require__(/*! ./addeditenquiry.component.html */ "./src/app/components/enquiry/addeditenquiry/addeditenquiry.component.html"),
        styles: [__webpack_require__(/*! ./addeditenquiry.component.css */ "./src/app/components/enquiry/addeditenquiry/addeditenquiry.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
        _services_common_service__WEBPACK_IMPORTED_MODULE_4__["CommonService"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"],
        _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSnackBar"],
        _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
], AddeditenquiryComponent);



/***/ }),

/***/ "./src/app/components/enquiry/enquirylist/enquirylist.component.css":
/*!**************************************************************************!*\
  !*** ./src/app/components/enquiry/enquirylist/enquirylist.component.css ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvZW5xdWlyeS9lbnF1aXJ5bGlzdC9lbnF1aXJ5bGlzdC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/components/enquiry/enquirylist/enquirylist.component.html":
/*!***************************************************************************!*\
  !*** ./src/app/components/enquiry/enquirylist/enquirylist.component.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"contents\">\n\t<div class=\"loader\" *ngIf=\"isLoading\">\n\t\t<img src=\"assets/images/loader.gif\">\n\t</div>\n\t<div class=\"container-fluid\">\n\t\t<section class=\"content\">\n\t\t\t<div id=\"main-form-content\">\n\t\t\t\t<form name=\"form\" (ngSubmit)=\"onSubmit()\" #f=\"ngForm\" class=\"form-inline\"> \n\t\t\t\t<div class=\"col-sm-6 col-lg-3\">\n\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t<input [(ngModel)]=\"enquiryFilter.inquiryno\" placeholder=\"Lead/Enquiry no\" name=\"inquiryno\"   \n\t\t\t\t\t\t#inquiryno=\"ngModel\" class=\"form-control w100\">\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"col-sm-6 col-lg-3\">\n\t\t\t      <div class=\"form-group\">\n\t\t\t        <input [(ngModel)]=\"enquiryFilter.inquirydate\"  \n\t\t\t        ngx-mydatepicker\n\t\t\t        name=\"inquirydate\"\n\t\t\t        (dateChanged)=\"onEnquiryDateChanged($event)\" \n\t\t\t        placeholder=\"Lead/Enquiry Date\"\n\t\t\t        [options]=\"myOptions\"\n\t\t\t          #dp1=\"ngx-mydatepicker\" (click)=\"dp1.toggleCalendar()\"\n\t\t\t        class=\"form-control w100\">\n\t\t\t      </div>\n\t\t\t    </div> \n\t\t\t\t<!-- <div class=\"col-sm-4 col-lg-4\">\n\t\t\t\t\t<div class=\"form-group\">\n\t\t              <select class=\"form-control\" name=\"academicyear\" \n\t\t                [(ngModel)]=\"enquiryFilter.year\">\n\t\t                <option value=\"\">Select Academic Year</option>\n\t\t                <option [value]=\"year.academic_year_master_id\"\n\t\t                  *ngFor=\"let year of academicyear\">{{year.academic_year_master_name}}</option>\n\t\t              </select>\n\t\t            </div>\n\t\t\t\t</div> -->\n\t\t\t\t<div class=\"col-sm-12 col-lg-2\">\n\t\t\t\t\t<button class=\"btn btn-info\">Search</button>\n\t\t\t\t\t<button (click)=\"clearfilter()\" class=\"btn btn-info\">Clear</button>\n\t\t\t\t</div>\n\t\t\t\t</form>\n\t\t\t</div>\n\t\t\t<div class=\"clearfix\"></div>\n\t\t\t<div class=\"col-md-12 load_table\">\n\t\t\t\t<div  class=\"qtraq-subtitle-wrapper mt20\">\n\t\t\t\t\t<h1> Lead/Enquiry Details</h1>\n\t\t\t\t\t<button class=\"btn btn-danger\" routerLink=\"addeditenquiry\"> Add Lead/Enquiry</button>\n\t\t\t\t</div>\n\t\t\t\t<table class=\"table table-striped\">\n\t\t\t\t\t<thead>\n\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t<th class=\"hidden\">Id</th>\n\t\t\t\t\t\t\t<th>Student Name</th>\n\t\t\t\t\t\t\t<th>Type</th>\n\t\t\t\t\t\t\t<th>Lead/Enquiry No</th>\n\t\t\t\t\t\t\t<th>Lead/Enquiry Date</th>\n\t\t\t\t\t\t\t<th>Academic Year</th>\n\t\t\t\t\t\t\t<th>Action</th>\n\t\t\t\t\t\t</tr>\n\t\t\t\t\t</thead>\n\t\t\t\t\t<tbody>\n\t\t\t\t\t\t<tr *ngFor=\"let enquiry of enquiryList| paginate: { itemsPerPage: this.size, \n\t\t  currentPage: this.page, totalItems: this.totalRecords }\">\n\t\t\t\t\t\t\t<td class=\"hidden\">1</td>\n\t\t\t\t\t\t\t<td>{{enquiry.student_first_name + \" \" +enquiry.student_last_name}}</td>\n\t\t\t\t\t\t\t<td>{{enquiry.rec_type}}</td>\n\t\t\t\t\t\t\t<td *ngIf=\"enquiry.rec_type == 'Lead'\">LD - {{enquiry.inquiry_no}}</td>\n\t\t\t\t\t\t\t<td *ngIf=\"enquiry.rec_type == 'Inquiry'\"></td>\n\t\t\t\t\t\t\t<td *ngIf=\"enquiry.rec_type == 'Inquiry'\">{{enquiry.inquiry_date}}</td>\n\t\t\t\t\t\t\t<td *ngIf=\"enquiry.rec_type == 'Lead'\"></td>\n\t\t\t\t\t\t\t<td>{{enquiry.academic_year_name}}</td>\n\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t<button class=\"btn btn-danger\" routerLink=\"addeditenquiry/{{enquiry.inquiry_master_id}}\" > Edit</button>\n\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t</tr>\n\t\t\t\t\t</tbody>\n\t\t\t\t</table>\n\t\t\t\t<pagination-controls  (pageChange)=\"pageChanged($event)\"></pagination-controls>\n\t\t\t</div>\n\t\t</section>\n\t</div>\n\t<div class=\"clearfix\"></div>\n</div>\n"

/***/ }),

/***/ "./src/app/components/enquiry/enquirylist/enquirylist.component.ts":
/*!*************************************************************************!*\
  !*** ./src/app/components/enquiry/enquirylist/enquirylist.component.ts ***!
  \*************************************************************************/
/*! exports provided: EnquirylistComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EnquirylistComponent", function() { return EnquirylistComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");
/* harmony import */ var _services_common_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/common.service */ "./src/app/services/common.service.ts");




let EnquirylistComponent = class EnquirylistComponent {
    constructor(commonService, snackbar) {
        this.commonService = commonService;
        this.snackbar = snackbar;
        this.page = 0;
        this.enquiryList = [];
        this.totalRecords = 0;
        this.size = 10;
        this.inquiryDate = "";
        this.enquiryFilter = {
            inquiryno: "",
            inquirydate: "",
            year: "",
        };
        this.isLoading = true;
        this.myOptions = {
            dateFormat: 'dd-mm-yyyy',
        };
    }
    ngOnInit() {
        localStorage.removeItem('studentid');
        localStorage.removeItem('tabindex');
        localStorage.removeItem('enquiryNO');
        localStorage.removeItem('docsave');
        localStorage.removeItem('category');
        localStorage.removeItem('course');
        this.getenquiryList(1, this.enquiryFilter);
        this.getAcademicYears();
    }
    getenquiryList(page, enquiryFilter) {
        this.commonService.listInquiries((page - 1), this.size, enquiryFilter).subscribe((response) => {
            console.log(response);
            this.isLoading = false;
            if (response['status_code'] == 200) {
                this.enquiryList = response['body']['query_result'];
                this.totalRecords = response['body']['totalRecords'];
                this.page = page;
            }
            else {
                // this.snackbar.open(response['msg'], '', {
                //  duration : 500,
                //   verticalPosition:"top",
                //   panelClass: ['bg-red']
                // });
                this.enquiryList = [];
                this.totalRecords = 0;
            }
        }, (err) => console.log("error", err), () => console.log("getCustomersPage() retrieved customers for page:", +page));
    }
    onEnquiryDateChanged(event) {
        if (event.formatted !== '') {
            this.inquiryDate = event.formatted;
        }
    }
    clearfilter() {
        this.enquiryFilter.inquiryno = "";
        this.enquiryFilter.inquirydate = "";
        this.enquiryFilter.year = "";
        this.getenquiryList(1, this.enquiryFilter);
    }
    onSubmit() {
        this.enquiryFilter.inquirydate = this.inquiryDate;
        this.getenquiryList(1, this.enquiryFilter);
    }
    getAcademicYears() {
        this.commonService.getAcademicYears().subscribe((response) => {
            this.academicyear = response.body;
        });
    }
    pageChanged(event) {
        console.log(event);
        this.getenquiryList(event, this.enquiryFilter);
    }
};
EnquirylistComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-enquirylist',
        template: __webpack_require__(/*! ./enquirylist.component.html */ "./src/app/components/enquiry/enquirylist/enquirylist.component.html"),
        styles: [__webpack_require__(/*! ./enquirylist.component.css */ "./src/app/components/enquiry/enquirylist/enquirylist.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_common_service__WEBPACK_IMPORTED_MODULE_3__["CommonService"],
        _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSnackBar"]])
], EnquirylistComponent);



/***/ }),

/***/ "./src/app/components/folderdocuments/documentdetails/documentdetails.component.css":
/*!******************************************************************************************!*\
  !*** ./src/app/components/folderdocuments/documentdetails/documentdetails.component.css ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvZm9sZGVyZG9jdW1lbnRzL2RvY3VtZW50ZGV0YWlscy9kb2N1bWVudGRldGFpbHMuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/components/folderdocuments/documentdetails/documentdetails.component.html":
/*!*******************************************************************************************!*\
  !*** ./src/app/components/folderdocuments/documentdetails/documentdetails.component.html ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"contents\">\n\t<div class=\"loader\" *ngIf=\"isLoading\">\n\t\t<img src=\"assets/images/loader.gif\">\n\t</div>\n\t<div class=\"container-fluid\">\n\t\t<section class=\"content\">\n\t\t\t<div id=\"main-form-content\">\n\t\t\t\t<div class=\"col-md-12\">\n\t\t\t\t\t<div  class=\"qtraq-subtitle-wrapper mt20\">\n\t\t\t\t\t\t<h1> Folder Documents</h1>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div>\n\t\t\t\t\t\t<div class=\"table-responsive scroll-table\" style=\"overflow-x:auto;width:100%;\">\n\t\t\t\t\t\t\t<table class=\"table table-bordered\">\n\t\t\t\t\t\t\t\t<thead>\n\t\t\t\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t\t\t\t<th>Name</th>\n\t\t\t\t\t\t\t\t\t\t<th>Document/Video Title</th>\n\t\t\t\t\t\t\t\t\t\t<th>Document/Video</th>\n\t\t\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t\t</thead>\n\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t\t<tbody>\t\n\t\t\t\t\t\t\t\t\t<tr *ngFor=\"let folder of folder_details | paginate: { itemsPerPage: this.size, \n\t\t  \t\t\t\t\t\t\tcurrentPage: this.page, totalItems: this.totalRecords }\">\n\t\t\t\t\t\t\t\t\t\t<td>{{folder.folder_name}}</td>\n\t\t\t\t\t\t\t\t\t\t<td>{{folder.document_title}}\n\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t<td *ngIf=\"folder.doc_type == 'doc_excel' && folder.is_downloadable == 'Yes'\"><a href=\"{{folder.doc_path}}\"><i class=\"fa fa-file\" style=\"font-size:60px;margin-bottom:2px;\"></i><br>Document</a>\n\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t<td *ngIf=\"folder.doc_type == 'doc_excel' && folder.is_downloadable == 'No'\"><i class=\"fa fa-file\" style=\"font-size:60px;margin-bottom:2px;\"></i><br>Document\n\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t<td *ngIf=\"folder.doc_type == 'doc'\"><a routerLink=\"{{folder.folder_document_id}}\"><i class=\"fa fa-file\" style=\"font-size:60px;margin-bottom:2px;\"></i><br>Pdf Document</a>\n\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t<td *ngIf=\"folder.doc_type == 'video'\"><iframe [src]=\"iframeUrl(folder.video_path)\" style=\"border:0;height:200;width:350px;max-width: 100%\" allowFullScreen=\"true\" allow=\"encrypted-media\"></iframe>\n\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t\t</tbody>\n\t\t\t\t\t\t\t</table>\t\n\t\t\t\t\t\t\t<pagination-controls  (pageChange)=\"pageChanged($event)\"></pagination-controls>\t\t\t\t\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</section>\n\t</div>\n\t<div class=\"clearfix\"></div>\n</div>\n"

/***/ }),

/***/ "./src/app/components/folderdocuments/documentdetails/documentdetails.component.ts":
/*!*****************************************************************************************!*\
  !*** ./src/app/components/folderdocuments/documentdetails/documentdetails.component.ts ***!
  \*****************************************************************************************/
/*! exports provided: DocumentdetailsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DocumentdetailsComponent", function() { return DocumentdetailsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _services_common_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/common.service */ "./src/app/services/common.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");







let DocumentdetailsComponent = class DocumentdetailsComponent {
    constructor(route, commonService, formBuilder, snackbar, router, sanitizer) {
        this.route = route;
        this.commonService = commonService;
        this.formBuilder = formBuilder;
        this.snackbar = snackbar;
        this.router = router;
        this.sanitizer = sanitizer;
        this.page = 0;
        this.totalRecords = 0;
        this.size = 10;
        this.documentdetailfilter = {};
        this.isLoading = true;
    }
    ngOnInit() {
        this.viewDocuments(1, this.documentdetailfilter);
    }
    viewDocuments(page, documentdetailfilter) {
        this.route.params.subscribe(params => {
            if (this.route.snapshot.params.folderid) {
                this.commonService.viewFolderDocuments((page - 1), this.size, this.route.snapshot.params.folderid)
                    .subscribe(data => {
                    this.isLoading = false;
                    if (data['status_code'] == 200) {
                        this.folder_details = data['body']['query_result'];
                        this.totalRecords = data['body']['totalRecords'];
                        this.page = page;
                    }
                    else {
                        this.snackbar.open(data['msg'], '', {
                            duration: 500,
                            verticalPosition: "top",
                            panelClass: ['bg-red']
                        });
                    }
                }, (err) => console.log("error", err), () => console.log("getCustomersPage() retrieved customers for page:", +page));
            }
        });
    }
    iframeUrl(video_url) {
        return this.sanitizer.bypassSecurityTrustResourceUrl(video_url);
    }
    pageChanged(event) {
        console.log(event);
        this.viewDocuments(event, this.documentdetailfilter);
    }
};
DocumentdetailsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
        selector: 'app-documentdetails',
        template: __webpack_require__(/*! ./documentdetails.component.html */ "./src/app/components/folderdocuments/documentdetails/documentdetails.component.html"),
        styles: [__webpack_require__(/*! ./documentdetails.component.css */ "./src/app/components/folderdocuments/documentdetails/documentdetails.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
        _services_common_service__WEBPACK_IMPORTED_MODULE_4__["CommonService"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"],
        _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSnackBar"],
        _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
        _angular_platform_browser__WEBPACK_IMPORTED_MODULE_6__["DomSanitizer"]])
], DocumentdetailsComponent);



/***/ }),

/***/ "./src/app/components/folderdocuments/folderdocument/folderdocument.component.css":
/*!****************************************************************************************!*\
  !*** ./src/app/components/folderdocuments/folderdocument/folderdocument.component.css ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvZm9sZGVyZG9jdW1lbnRzL2ZvbGRlcmRvY3VtZW50L2ZvbGRlcmRvY3VtZW50LmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/components/folderdocuments/folderdocument/folderdocument.component.html":
/*!*****************************************************************************************!*\
  !*** ./src/app/components/folderdocuments/folderdocument/folderdocument.component.html ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"contents\">\n\t<div class=\"loader\" *ngIf=\"isLoading\">\n\t\t<img src=\"assets/images/loader.gif\">\n\t</div>\n\t<div class=\"container-fluid\">\n\t\t<section class=\"content\">\n\t\t\t<div class=\"clearfix\"></div>\n\t\t\t<div class=\"col-md-12\">\n\t\t\t\t<div  class=\"qtraq-subtitle-wrapper mt20\">\n\t\t\t\t\t<h1> Dcument Folders</h1>\n\t\t\t\t</div>\n\t\t\t\t<table class=\"table table-striped\">\n\t\t\t\t\t<thead>\n\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t<th class=\"hidden\">Id</th>\n\t\t\t\t\t\t\t<th style=\"width: 70%\">Name</th>\n\t\t\t\t\t\t\t<th>Action</th>\n\t\t\t\t\t\t</tr>\n\t\t\t\t\t</thead>\n\t\t\t\t\t<tbody>\n\t\t\t\t\t\t<tr *ngFor=\"let doc of documentList| paginate: { itemsPerPage: this.size, \n\t\t  currentPage: this.page, totalItems: this.totalRecords }\">\n\t\t\t\t\t\t\t<td class=\"hidden\">1</td>\n\t\t\t\t\t\t\t<td>{{doc.folder_name}}</td>\n\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t<button class=\"btn btn-danger\" routerLink=\"viewDocument/{{doc.document_folder_id}}\"> View Document</button>\n\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t</tr>\n\t\t\t\t\t</tbody>\n\t\t\t\t</table>\n\t\t\t\t<pagination-controls  (pageChange)=\"pageChanged($event)\"></pagination-controls>\n\t\t\t</div>\n\t\t</section>\n\t</div>\n\t<div class=\"clearfix\"></div>\n</div>\n"

/***/ }),

/***/ "./src/app/components/folderdocuments/folderdocument/folderdocument.component.ts":
/*!***************************************************************************************!*\
  !*** ./src/app/components/folderdocuments/folderdocument/folderdocument.component.ts ***!
  \***************************************************************************************/
/*! exports provided: FolderdocumentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FolderdocumentComponent", function() { return FolderdocumentComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");
/* harmony import */ var _services_common_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/common.service */ "./src/app/services/common.service.ts");




let FolderdocumentComponent = class FolderdocumentComponent {
    constructor(commonService, snackbar) {
        this.commonService = commonService;
        this.snackbar = snackbar;
        this.page = 0;
        this.documentList = [];
        this.totalRecords = 0;
        this.size = 10;
        this.documentFilter = {};
        this.isLoading = true;
    }
    ngOnInit() {
        localStorage.removeItem('studentid');
        localStorage.removeItem('tabindex');
        localStorage.removeItem('enquiryNO');
        localStorage.removeItem('docsave');
        localStorage.removeItem('category');
        localStorage.removeItem('course');
        this.viewDocumentFolders(1, this.documentFilter);
    }
    viewDocumentFolders(page, documentFilter) {
        this.commonService.viewDocumentFolders((page - 1), this.size, documentFilter).subscribe((response) => {
            this.isLoading = false;
            if (response['status_code'] == 200) {
                this.snackbar.open(response['msg'], '', {
                    duration: 500,
                    verticalPosition: "top",
                    panelClass: ['bg-green']
                });
                this.documentList = response['body']['query_result'];
                this.totalRecords = response['body']['totalRecords'];
                this.page = page;
            }
            else {
                this.snackbar.open(response['msg'], '', {
                    duration: 500,
                    verticalPosition: "top",
                    panelClass: ['bg-red']
                });
                this.documentList = [];
                this.totalRecords = 0;
            }
        }, (err) => console.log("error", err), () => console.log("getCustomersPage() retrieved customers for page:", +page));
    }
    // clearfilter()
    // {
    //     this.documentFilter.inquiryno=""
    //     this.documentFilter.inquirydate=""
    //     this.documentFilter.year=""
    //     this.viewTimetables(1,this.documentFilter)
    // }
    onSubmit() {
        this.viewDocumentFolders(1, this.documentFilter);
    }
    pageChanged(event) {
        this.viewDocumentFolders(event, this.documentFilter);
    }
};
FolderdocumentComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-folderdocument',
        template: __webpack_require__(/*! ./folderdocument.component.html */ "./src/app/components/folderdocuments/folderdocument/folderdocument.component.html"),
        styles: [__webpack_require__(/*! ./folderdocument.component.css */ "./src/app/components/folderdocuments/folderdocument/folderdocument.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_common_service__WEBPACK_IMPORTED_MODULE_3__["CommonService"],
        _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSnackBar"]])
], FolderdocumentComponent);



/***/ }),

/***/ "./src/app/components/folderdocuments/pdfdocview/pdfdocview.component.css":
/*!********************************************************************************!*\
  !*** ./src/app/components/folderdocuments/pdfdocview/pdfdocview.component.css ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvZm9sZGVyZG9jdW1lbnRzL3BkZmRvY3ZpZXcvcGRmZG9jdmlldy5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/components/folderdocuments/pdfdocview/pdfdocview.component.html":
/*!*********************************************************************************!*\
  !*** ./src/app/components/folderdocuments/pdfdocview/pdfdocview.component.html ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"contents\">\n\t<div class=\"loader\" *ngIf=\"isLoading\">\n\t\t<img src=\"assets/images/loader.gif\">\n\t</div>\n\t<div class=\"container-fluid\">\n\t\t<section class=\"content\">\n\t\t\t<div id=\"main-form-content\">\n\t\t\t\t<div class=\"col-md-12\">\n\t\t\t\t\t<div  class=\"qtraq-subtitle-wrapper mt20\">\n\t\t\t\t\t\t<h1> Folder Document</h1>\n\t\t\t\t\t</div>\n\t\t\t\t\t<br>\n\t\t\t\t\t<div *ngIf=\"isLoaded\" style=\"text-align: center;\">\n\t\t\t\t\t\t<a href=\"{{fileURL}}\" *ngIf=\"is_downloadable\"><button class=\"btn btn-sm pdf-btn\" type=\"button\">\n\t\t\t\t\t\t\t\t<i class=\"fa fa-download\"></i>\n\t\t\t\t\t\t\t</button></a>\n\t\t\t\t\t\t<button class=\"btn btn-sm pdf-btn\" (click)=\"zoom_out()\" type=\"button\">\n\t\t\t\t\t\t\t\t<i class=\"fa fa-search-plus\"></i>\n\t\t\t\t\t\t\t</button>\n\t\t\t\t\t\t<button class=\"btn btn-sm pdf-btn\"(click)=\"zoom_in()\" type=\"button\">\n\t\t\t\t\t\t\t<i class=\"fa fa-search-minus\"></i>\n\t\t\t\t\t\t</button>\n\n\t\t\t\t\t\t<button class=\"btn btn-sm pdf-btn\"  (click)=\"prevPage()\" [disabled]=\"page === 1\"><i class=\"fa fa-angle-left\" style=\"font-size:18px;\"></i></button>\n\t\t\t\t\t\t<button class=\"btn btn-sm pdf-btn\" (click)=\"nextPage()\" [disabled]=\"page === totalPages\"><i class=\"fa fa-angle-right\" style=\"font-size:18px;\"></i></button>\n\t\t\t\t\t\t&nbsp; &nbsp;\n\t\t\t\t\t\t<span class=\"page-text\">Page: <span>{{page}}</span> / <span>{{totalPages}}</span></span>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"\">\n\t\t\t\t\t\t<pdf-viewer src=\"\" [render-text]=\"true\" style=\"display: block;\" [show-all]=\"false\" [page]=\"page\" (after-load-complete)=\"afterLoadComplete($event)\" [zoom]=\"pdfZoom\" ></pdf-viewer>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</section>\n\t</div>\n\t<div class=\"clearfix\"></div>\n</div>\n"

/***/ }),

/***/ "./src/app/components/folderdocuments/pdfdocview/pdfdocview.component.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/components/folderdocuments/pdfdocview/pdfdocview.component.ts ***!
  \*******************************************************************************/
/*! exports provided: PdfdocviewComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PdfdocviewComponent", function() { return PdfdocviewComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _services_common_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/common.service */ "./src/app/services/common.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");






const ZOOM_STEP = 0.25;
const DEFAULT_ZOOM = 1;
let PdfdocviewComponent = class PdfdocviewComponent {
    constructor(route, commonService, formBuilder, snackbar, router) {
        this.route = route;
        this.commonService = commonService;
        this.formBuilder = formBuilder;
        this.snackbar = snackbar;
        this.router = router;
        this.fileURL = '';
        this.page = 1;
        this.isLoaded = false;
        this.pdfZoom = DEFAULT_ZOOM;
        this.isLoading = true;
        this.is_downloadable = true;
    }
    ngOnInit() {
        this.route.params.subscribe(params => {
            if (this.route.snapshot.params.docfolderid) {
                this.commonService.viewFolderDoc(this.route.snapshot.params.docfolderid)
                    .subscribe(data => {
                    this.isLoading = false;
                    if (data['status_code'] == 200) {
                        this.fileURL = data['body'][0]['doc_path'];
                        this.is_downloadable = data['body'][0]['is_downloadable'];
                    }
                    else {
                        this.snackbar.open(data['msg'], '', {
                            duration: 500,
                            verticalPosition: "top",
                            panelClass: ['bg-red']
                        });
                    }
                });
            }
        });
    }
    afterLoadComplete(pdfData) {
        this.totalPages = pdfData.numPages;
        this.isLoaded = true;
    }
    nextPage() {
        this.page++;
    }
    prevPage() {
        this.page--;
    }
    zoom_out() {
        this.pdfZoom += ZOOM_STEP;
    }
    zoom_in() {
        console.log('hi');
        if (this.pdfZoom > DEFAULT_ZOOM) {
            this.pdfZoom -= ZOOM_STEP;
        }
    }
};
PdfdocviewComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
        selector: 'app-pdfdocview',
        template: __webpack_require__(/*! ./pdfdocview.component.html */ "./src/app/components/folderdocuments/pdfdocview/pdfdocview.component.html"),
        styles: [__webpack_require__(/*! ./pdfdocview.component.css */ "./src/app/components/folderdocuments/pdfdocview/pdfdocview.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
        _services_common_service__WEBPACK_IMPORTED_MODULE_4__["CommonService"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"],
        _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSnackBar"],
        _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
], PdfdocviewComponent);



/***/ }),

/***/ "./src/app/components/login/login.component.css":
/*!******************************************************!*\
  !*** ./src/app/components/login/login.component.css ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvbG9naW4vbG9naW4uY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/components/login/login.component.html":
/*!*******************************************************!*\
  !*** ./src/app/components/login/login.component.html ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"hold-transition login-page\">\n\t<div class=\"login-box\">\n\t\t<div class=\"login-logo\">\n\t\t\t<span class=\"logo-mini\">\n\t\t\t<a  routerLink=\"/\" href=\"javascript:void(0)\" class=\"main-logo\" >\n\t\t\t<img [src]=\"defaultLogo\" class=\"site-logo\" alt=\"Logo Image\">\n\t\t\t</a>\n\t\t\t</span>\n\t\t</div>\n\t\t<div class=\"login-box-body\">\n\t\t\t<p class=\"login-box-msg\">Sign in to start your session</p>\n\t\t\t<form [formGroup]=\"loginForm\" (ngSubmit)=\"onSubmit()\">\n\t\t\t<div class=\"form-group has-feedback\">\n\t\t\t\t<input type=\"text\" formControlName=\"username\" placeholder=\"Username\" class=\"form-control\" [ngClass]=\"{ 'is-invalid': submitted && f.username.errors }\" />\n\t\t\t\t<div *ngIf=\"submitted && f.username.errors\" class=\"invalid-feedback\">\n\t\t\t\t\t<div *ngIf=\"f.username.errors.required\">Username is required</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"form-group has-feedback\">\n\t\t\t\t<input type=\"password\" formControlName=\"password\" placeholder=\"******\" class=\"form-control\" [ngClass]=\"{ 'is-invalid': submitted && f.password.errors }\" />\n\t\t\t\t<div *ngIf=\"submitted && f.password.errors\" class=\"invalid-feedback\">\n\t\t\t\t\t<div *ngIf=\"f.password.errors.required\">Password is required</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"row\">\n\t\t\t\t<div class=\"col-xs-8\">\n\t\t\t\t\t<div class=\"checkbox icheck\">\n\t\t\t\t\t\t<label>\n\t\t\t\t\t\t</label>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"col-xs-4\">\n\t\t\t\t\t<button type=\"submit\" class=\"btn btn-primary btn-block btn-flat\">Sign In</button>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t</form>\n\t\t\t<p *ngIf=\"error\" class=\"login-box-msg\">{{error}}</p>\n\t\t\t<p *ngIf=\"massage\" class=\"login-box-msg\">{{massage}}</p>\n\t\t</div>\n\t</div>\n</div>"

/***/ }),

/***/ "./src/app/components/login/login.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/components/login/login.component.ts ***!
  \*****************************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _constant_constant_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../constant/constant.component */ "./src/app/components/constant/constant.component.ts");






let LoginComponent = class LoginComponent {
    constructor(formBuilder, authService, router) {
        this.formBuilder = formBuilder;
        this.authService = authService;
        this.router = router;
        this.classes = ['hold-transition', 'login-page'];
        this.loading = false;
        this.submitted = false;
        this.defaultLogo = _constant_constant_component__WEBPACK_IMPORTED_MODULE_5__["ConstantComponent"].defaultLogo;
        this.massage = "";
    }
    initForm() {
        this.loginForm = this.formBuilder.group({
            username: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]
        });
    }
    get f() { return this.loginForm.controls; }
    onSubmit() {
        this.submitted = true;
        if (this.loginForm.invalid) {
            return;
        }
        else {
            this.authService.sendToken(this.f.username.value);
        }
        //this.loading = true;
        this.authService.login(this.f.username.value, this.f.password.value).subscribe((response) => {
            if (response.status_code = 200) {
                if (response.body) {
                    if (response) {
                        var user_details = {
                            "utoken": btoa(response.body.utoken),
                            "zone_id": btoa(response.body.zone_id),
                            "center_id": btoa(response.body.center_id),
                            "user_id": btoa(response.body.user_id),
                            "first_name": response.body.first_name,
                            "last_name": response.body.last_name,
                        };
                        localStorage.setItem('currentUser', JSON.stringify(user_details));
                        localStorage.setItem('academicyearid', btoa(response.body.academic_year));
                        this.massage = response.msg;
                        this.router.navigate(['']);
                    }
                }
                else {
                    this.massage = response.msg;
                }
            }
            else {
                this.massage = response.msg;
            }
        });
    }
    ngOnInit() {
        if (this.authService.isLoggedIn()) {
            this.router.navigate([""]);
        }
        else {
            this.router.navigate(["login"]);
        }
        this.initForm();
        const body = document.getElementsByTagName('body')[0];
        for (const cl of this.classes) {
            body.classList.add(cl);
        }
    }
};
LoginComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
        selector: 'app-login',
        template: __webpack_require__(/*! ./login.component.html */ "./src/app/components/login/login.component.html"),
        styles: [__webpack_require__(/*! ./login.component.css */ "./src/app/components/login/login.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"],
        _services_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"],
        _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
], LoginComponent);



/***/ }),

/***/ "./src/app/components/remainingfees/feesdetails/feesdetails.component.css":
/*!********************************************************************************!*\
  !*** ./src/app/components/remainingfees/feesdetails/feesdetails.component.css ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvcmVtYWluaW5nZmVlcy9mZWVzZGV0YWlscy9mZWVzZGV0YWlscy5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/components/remainingfees/feesdetails/feesdetails.component.html":
/*!*********************************************************************************!*\
  !*** ./src/app/components/remainingfees/feesdetails/feesdetails.component.html ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  feesdetails works!\n</p>\n"

/***/ }),

/***/ "./src/app/components/remainingfees/feesdetails/feesdetails.component.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/components/remainingfees/feesdetails/feesdetails.component.ts ***!
  \*******************************************************************************/
/*! exports provided: FeesdetailsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FeesdetailsComponent", function() { return FeesdetailsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let FeesdetailsComponent = class FeesdetailsComponent {
    constructor() { }
    ngOnInit() {
    }
};
FeesdetailsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-feesdetails',
        template: __webpack_require__(/*! ./feesdetails.component.html */ "./src/app/components/remainingfees/feesdetails/feesdetails.component.html"),
        styles: [__webpack_require__(/*! ./feesdetails.component.css */ "./src/app/components/remainingfees/feesdetails/feesdetails.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], FeesdetailsComponent);



/***/ }),

/***/ "./src/app/components/timetable/pdfview/pdfview.component.css":
/*!********************************************************************!*\
  !*** ./src/app/components/timetable/pdfview/pdfview.component.css ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvdGltZXRhYmxlL3BkZnZpZXcvcGRmdmlldy5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/components/timetable/pdfview/pdfview.component.html":
/*!*********************************************************************!*\
  !*** ./src/app/components/timetable/pdfview/pdfview.component.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"contents\">\n\t<div class=\"loader\" *ngIf=\"isLoading\">\n\t\t<img src=\"assets/images/loader.gif\">\n\t</div>\n\t<div class=\"container-fluid\">\n\t\t<section class=\"content\">\n\t\t\t<div id=\"main-form-content\">\n\t\t\t\t<div class=\"col-md-12\">\n\t\t\t\t\t<div  class=\"qtraq-subtitle-wrapper mt20\">\n\t\t\t\t\t\t<h1> Timetable Details</h1>\n\t\t\t\t\t</div>\n\t\t\t\t\t<br>\n\t\t\t\t\t<div *ngIf=\"isLoaded\" style=\"text-align: center;\">\n\t\t\t\t\t\t<button class=\"btn btn-sm pdf-btn\" (click)=\"zoom_out()\" type=\"button\">\n\t\t\t\t\t\t\t\t<i class=\"fa fa-search-plus\"></i>\n\t\t\t\t\t\t\t</button>\n\t\t\t\t\t\t<button class=\"btn btn-sm pdf-btn\"(click)=\"zoom_in()\" type=\"button\">\n\t\t\t\t\t\t\t<i class=\"fa fa-search-minus\"></i>\n\t\t\t\t\t\t</button>\n\n\t\t\t\t\t\t<button class=\"btn btn-sm pdf-btn\"  (click)=\"prevPage()\" [disabled]=\"page === 1\"><i class=\"fa fa-angle-left\" style=\"font-size:18px;\"></i></button>\n\t\t\t\t\t\t<button class=\"btn btn-sm pdf-btn\" (click)=\"nextPage()\" [disabled]=\"page === totalPages\"><i class=\"fa fa-angle-right\" style=\"font-size:18px;\"></i></button>\n\t\t\t\t\t\t&nbsp; &nbsp;\n\t\t\t\t\t\t<span class=\"page-text\">Page: <span>{{page}}</span> / <span>{{totalPages}}</span></span>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"\">\n\t\t\t\t\t\t<pdf-viewer src=\"{{fileURL}}\" [render-text]=\"true\" style=\"display: block;\" [show-all]=\"false\" [page]=\"page\" (after-load-complete)=\"afterLoadComplete($event)\" [zoom]=\"pdfZoom\" ></pdf-viewer>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</section>\n\t</div>\n\t<div class=\"clearfix\"></div>\n</div>\n"

/***/ }),

/***/ "./src/app/components/timetable/pdfview/pdfview.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/components/timetable/pdfview/pdfview.component.ts ***!
  \*******************************************************************/
/*! exports provided: PdfviewComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PdfviewComponent", function() { return PdfviewComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _services_common_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/common.service */ "./src/app/services/common.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");






const ZOOM_STEP = 0.25;
const DEFAULT_ZOOM = 1;
let PdfviewComponent = class PdfviewComponent {
    constructor(route, commonService, formBuilder, snackbar, router) {
        this.route = route;
        this.commonService = commonService;
        this.formBuilder = formBuilder;
        this.snackbar = snackbar;
        this.router = router;
        this.fileURL = '';
        this.page = 1;
        this.isLoaded = false;
        this.pdfZoom = DEFAULT_ZOOM;
        this.isLoading = true;
    }
    ngOnInit() {
        this.route.params.subscribe(params => {
            if (this.route.snapshot.params.timetabledocid) {
                this.commonService.viewTimeTableDoc(this.route.snapshot.params.timetabledocid)
                    .subscribe(data => {
                    this.isLoading = false;
                    if (data['status_code'] == 200) {
                        this.fileURL = data['body'][0]['doc_path'];
                    }
                    else {
                        this.snackbar.open(data['msg'], '', {
                            duration: 500,
                            verticalPosition: "top",
                            panelClass: ['bg-red']
                        });
                    }
                });
            }
        });
    }
    afterLoadComplete(pdfData) {
        this.totalPages = pdfData.numPages;
        this.isLoaded = true;
    }
    nextPage() {
        this.page++;
    }
    prevPage() {
        this.page--;
    }
    zoom_out() {
        this.pdfZoom += ZOOM_STEP;
    }
    zoom_in() {
        console.log('hi');
        if (this.pdfZoom > DEFAULT_ZOOM) {
            this.pdfZoom -= ZOOM_STEP;
        }
    }
};
PdfviewComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
        selector: 'app-pdfview',
        template: __webpack_require__(/*! ./pdfview.component.html */ "./src/app/components/timetable/pdfview/pdfview.component.html"),
        styles: [__webpack_require__(/*! ./pdfview.component.css */ "./src/app/components/timetable/pdfview/pdfview.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
        _services_common_service__WEBPACK_IMPORTED_MODULE_4__["CommonService"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"],
        _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSnackBar"],
        _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
], PdfviewComponent);



/***/ }),

/***/ "./src/app/components/timetable/timetabledetails/timetabledetails.component.css":
/*!**************************************************************************************!*\
  !*** ./src/app/components/timetable/timetabledetails/timetabledetails.component.css ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvdGltZXRhYmxlL3RpbWV0YWJsZWRldGFpbHMvdGltZXRhYmxlZGV0YWlscy5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/components/timetable/timetabledetails/timetabledetails.component.html":
/*!***************************************************************************************!*\
  !*** ./src/app/components/timetable/timetabledetails/timetabledetails.component.html ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"contents\">\n\t<div class=\"loader\" *ngIf=\"isLoading\">\n\t\t<img src=\"assets/images/loader.gif\">\n\t</div>\n\t<div class=\"container-fluid\">\n\t\t<section class=\"content\">\n\t\t\t<div id=\"main-form-content\">\n\t\t\t\t<div class=\"col-md-12\">\n\t\t\t\t\t<div  class=\"qtraq-subtitle-wrapper mt20\">\n\t\t\t\t\t\t<h1> Timetable Details</h1>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"\">\n\t\t\t\t\t\t<h5>Timetable Title</h5>\n\t\t\t\t\t\t<p>{{timetable_title}}</p>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"\">\n\t\t\t\t\t\t<h5>Cover Image</h5>\n\t\t\t\t\t\t<p><img style=\"width: 50%;height:50%;padding-top:5px;\" src=\"{{cover_image}}\"></p>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div *ngIf = \"timetable_videos\">\n\t\t\t\t\t\t<h2><strong>Timetable Videos</strong></h2>\n\t\t\t\t\t\t<div class=\"table-responsive scroll-table\" style=\"overflow-x:auto;width:100%;\">\n\t\t\t\t\t\t\t<table class=\"table table-bordered\">\n\t\t\t\t\t\t\t\t<thead>\n\t\t\t\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t\t\t\t<th>Video Title</th>\n\t\t\t\t\t\t\t\t\t\t<th>Video URL</th>\n\t\t\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t\t</thead>\n\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t\t<tbody>\t\n\t\t\t\t\t\t\t\t\t<tr *ngFor=\"let video of timetable_videos\">\n\t\t\t\t\t\t\t\t\t\t<td>{{video.video_title}}</td>\n\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t<iframe [src]=\"iframeUrl(video.video_url)\" style=\"border:0;height:200;width:350px;max-width: 100%\" allowFullScreen=\"true\" allow=\"encrypted-media\"></iframe>\n\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t\t</tbody>\n\t\t\t\t\t\t\t</table>\t\t\t\t\t\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div *ngIf = \"timetable_documents\" style=\"margin-bottom: 20px\">\n\t\t\t\t\t\t<h2><strong>Timetable Documents</strong></h2>\n\t\t\t\t\t\t<div class=\"table-responsive scroll-table\" style=\"overflow-x:auto;width:100%;\">\n\t\t\t\t\t\t\t<table class=\"table table-bordered\">\n\t\t\t\t\t\t\t\t<thead>\n\t\t\t\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t\t\t\t<th>Document Title</th>\n\t\t\t\t\t\t\t\t\t\t<th>Description</th>\n\t\t\t\t\t\t\t\t\t\t<th>Document</th>\n\t\t\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t\t</thead>\n\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t\t<tbody>\t\n\t\t\t\t\t\t\t\t\t<tr *ngFor=\"let doc of timetable_documents\">\n\t\t\t\t\t\t\t\t\t\t<td>{{doc.doc_title}}</td>\n\t\t\t\t\t\t\t\t\t\t<td>{{doc.doc_description}}</td>\n\t\t\t\t\t\t\t\t\t\t<td *ngIf = \"doc.doc_type === 'doc'\"><a routerLink=\"{{doc.timetable_document_id}}\"><i class=\"fa fa-file\" style=\"font-size:60px;margin-bottom:2px;\"></i><br>Pdf Document</a></td>\n\t\t\t\t\t\t\t\t\t\t<td *ngIf = \"doc.doc_type === 'image'\"><img style=\"width: 50%;height:50%;padding-top:5px;\" src=\"{{doc.image_path}}\"></td>\n\t\t\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t\t</tbody>\n\t\t\t\t\t\t\t</table>\t\t\t\t\t\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</section>\n\t</div>\n\t<div class=\"clearfix\"></div>\n</div>\n"

/***/ }),

/***/ "./src/app/components/timetable/timetabledetails/timetabledetails.component.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/components/timetable/timetabledetails/timetabledetails.component.ts ***!
  \*************************************************************************************/
/*! exports provided: TimetabledetailsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TimetabledetailsComponent", function() { return TimetabledetailsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _services_common_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/common.service */ "./src/app/services/common.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");







let TimetabledetailsComponent = class TimetabledetailsComponent {
    constructor(route, commonService, formBuilder, snackbar, router, sanitizer) {
        this.route = route;
        this.commonService = commonService;
        this.formBuilder = formBuilder;
        this.snackbar = snackbar;
        this.router = router;
        this.sanitizer = sanitizer;
        this.timetable_title = '';
        this.cover_image = '';
        this.isLoading = true;
    }
    ngOnInit() {
        this.route.params.subscribe(params => {
            if (this.route.snapshot.params.timetableid) {
                this.commonService.viewTimetableDetails(this.route.snapshot.params.timetableid)
                    .subscribe(data => {
                    this.isLoading = false;
                    if (data['status_code'] == 200) {
                        this.timetable_title = data['body']['timetable_details'][0]['timetable_title'];
                        this.cover_image = data['body']['timetable_details'][0]['cover_image_path'];
                        this.timetable_videos = data['body']['timetable_video'];
                        this.timetable_documents = data['body']['timetable_documents'];
                    }
                    else {
                        this.snackbar.open(data['msg'], '', {
                            duration: 500,
                            verticalPosition: "top",
                            panelClass: ['bg-red']
                        });
                    }
                });
            }
        });
    }
    iframeUrl(video_url) {
        return this.sanitizer.bypassSecurityTrustResourceUrl(video_url);
    }
};
TimetabledetailsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
        selector: 'app-timetabledetails',
        template: __webpack_require__(/*! ./timetabledetails.component.html */ "./src/app/components/timetable/timetabledetails/timetabledetails.component.html"),
        styles: [__webpack_require__(/*! ./timetabledetails.component.css */ "./src/app/components/timetable/timetabledetails/timetabledetails.component.css")]
    }),
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Pipe"])({ name: 'safe' }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
        _services_common_service__WEBPACK_IMPORTED_MODULE_4__["CommonService"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"],
        _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSnackBar"],
        _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
        _angular_platform_browser__WEBPACK_IMPORTED_MODULE_6__["DomSanitizer"]])
], TimetabledetailsComponent);



/***/ }),

/***/ "./src/app/components/timetable/timetablemaster/timetablemaster.component.css":
/*!************************************************************************************!*\
  !*** ./src/app/components/timetable/timetablemaster/timetablemaster.component.css ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvdGltZXRhYmxlL3RpbWV0YWJsZW1hc3Rlci90aW1ldGFibGVtYXN0ZXIuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/components/timetable/timetablemaster/timetablemaster.component.html":
/*!*************************************************************************************!*\
  !*** ./src/app/components/timetable/timetablemaster/timetablemaster.component.html ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"contents\">\n\t<div class=\"loader\" *ngIf=\"isLoading\">\n\t\t<img src=\"assets/images/loader.gif\">\n\t</div>\n\t<div class=\"container-fluid\">\n\t\t<section class=\"content\">\n\t\t\t<div class=\"clearfix\"></div>\n\t\t\t<div class=\"col-md-12\">\n\t\t\t\t<div  class=\"qtraq-subtitle-wrapper mt20\">\n\t\t\t\t\t<h1> Timetables</h1>\n\t\t\t\t</div>\n\t\t\t\t<table class=\"table table-striped\">\n\t\t\t\t\t<thead>\n\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t<th class=\"hidden\">Id</th>\n\t\t\t\t\t\t\t<th>Timetable Title</th>\n\t\t\t\t\t\t\t<th>Action</th>\n\t\t\t\t\t\t</tr>\n\t\t\t\t\t</thead>\n\t\t\t\t\t<tbody>\n\t\t\t\t\t\t<tr *ngFor=\"let timetable of timetableList| paginate: { itemsPerPage: this.size, \n\t\t  currentPage: this.page, totalItems: this.totalRecords }\">\n\t\t\t\t\t\t\t<td class=\"hidden\">1</td>\n\t\t\t\t\t\t\t<td>{{timetable.timetable_title}}</td>\n\t\t\t\t\t\t\t<td *ngIf = \"timetable.is_timetable_visible\">\n\t\t\t\t\t\t\t\t<button class=\"btn btn-danger\" routerLink=\"viewTimetable/{{timetable.timetable_id}}\"> View Timetable</button>\n\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t<td *ngIf = \"!timetable.is_timetable_visible\"> View Timetable\n\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t</tr>\n\t\t\t\t\t</tbody>\n\t\t\t\t</table>\n\t\t\t\t<pagination-controls  (pageChange)=\"pageChanged($event)\"></pagination-controls>\n\t\t\t</div>\n\t\t</section>\n\t</div>\n\t<div class=\"clearfix\"></div>\n</div>\n"

/***/ }),

/***/ "./src/app/components/timetable/timetablemaster/timetablemaster.component.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/components/timetable/timetablemaster/timetablemaster.component.ts ***!
  \***********************************************************************************/
/*! exports provided: TimetablemasterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TimetablemasterComponent", function() { return TimetablemasterComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");
/* harmony import */ var _services_common_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/common.service */ "./src/app/services/common.service.ts");




let TimetablemasterComponent = class TimetablemasterComponent {
    constructor(commonService, snackbar) {
        this.commonService = commonService;
        this.snackbar = snackbar;
        this.page = 0;
        this.timetableList = [];
        this.totalRecords = 0;
        this.size = 10;
        this.timetableFilter = {};
        this.isLoading = true;
    }
    ngOnInit() {
        localStorage.removeItem('studentid');
        localStorage.removeItem('tabindex');
        localStorage.removeItem('enquiryNO');
        localStorage.removeItem('docsave');
        localStorage.removeItem('category');
        localStorage.removeItem('course');
        this.viewTimetables(1, this.timetableFilter);
    }
    viewTimetables(page, timetableFilter) {
        this.commonService.viewTimetables((page - 1), this.size, timetableFilter).subscribe((response) => {
            this.isLoading = false;
            if (response['status_code'] == 200) {
                this.snackbar.open(response['msg'], '', {
                    duration: 500,
                    verticalPosition: "top",
                    panelClass: ['bg-green']
                });
                this.timetableList = response['body']['query_result'];
                this.totalRecords = response['body']['totalRecords'];
                this.page = page;
            }
            else {
                this.snackbar.open(response['msg'], '', {
                    duration: 500,
                    verticalPosition: "top",
                    panelClass: ['bg-red']
                });
                this.timetableList = [];
                this.totalRecords = 0;
            }
        }, (err) => console.log("error", err), () => console.log("getCustomersPage() retrieved customers for page:", +page));
    }
    // clearfilter()
    // {
    //     this.timetableFilter.inquiryno=""
    //     this.timetableFilter.inquirydate=""
    //     this.timetableFilter.year=""
    //     this.viewTimetables(1,this.timetableFilter)
    // }
    onSubmit() {
        this.viewTimetables(1, this.timetableFilter);
    }
    pageChanged(event) {
        this.viewTimetables(event, this.timetableFilter);
    }
};
TimetablemasterComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-timetablemaster',
        template: __webpack_require__(/*! ./timetablemaster.component.html */ "./src/app/components/timetable/timetablemaster/timetablemaster.component.html"),
        styles: [__webpack_require__(/*! ./timetablemaster.component.css */ "./src/app/components/timetable/timetablemaster/timetablemaster.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_common_service__WEBPACK_IMPORTED_MODULE_3__["CommonService"],
        _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSnackBar"]])
], TimetablemasterComponent);



/***/ }),

/***/ "./src/app/directives/select2.directive.ts":
/*!*************************************************!*\
  !*** ./src/app/directives/select2.directive.ts ***!
  \*************************************************/
/*! exports provided: Select2Directive */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Select2Directive", function() { return Select2Directive; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");



let Select2Directive = class Select2Directive {
    constructor(el, control) {
        this.el = el;
        this.control = control;
        this.itemSelected = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
    }
    ngAfterViewInit() {
        if (this.dataUrl) {
            this.initializeSelect2();
        }
    }
    ngOnChanges(changes) {
        const dataUrl = changes.dataUrl;
        // There are instances that at first dataUrl are not available.
        if (dataUrl.currentValue) {
            this.initializeSelect2();
        }
    }
    // TODO: Use select2 transport option instead of ajax
    initializeSelect2() {
        $('.select2').on('select2:select', (event) => {
            // Code you want to execute when you hit enter in select2 input box   
        });
    }
    clear() {
        if (this.select2) {
            this.select2.val(null).trigger('change');
            this.control.control.setValue(null);
        }
    }
};
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
], Select2Directive.prototype, "dataUrl", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
], Select2Directive.prototype, "placeholder", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
], Select2Directive.prototype, "itemSelected", void 0);
Select2Directive = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Directive"])({
        selector: '[appSelect2]'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["NgControl"]])
], Select2Directive);



/***/ }),

/***/ "./src/app/routing/app-routing.module.ts":
/*!***********************************************!*\
  !*** ./src/app/routing/app-routing.module.ts ***!
  \***********************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _components_dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../components/dashboard/dashboard.component */ "./src/app/components/dashboard/dashboard.component.ts");
/* harmony import */ var _components_login_login_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../components/login/login.component */ "./src/app/components/login/login.component.ts");
/* harmony import */ var _auth_auth_guard__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../auth/auth.guard */ "./src/app/auth/auth.guard.ts");
/* harmony import */ var _components_enquiry_enquirylist_enquirylist_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../components/enquiry/enquirylist/enquirylist.component */ "./src/app/components/enquiry/enquirylist/enquirylist.component.ts");
/* harmony import */ var _components_enquiry_addeditenquiry_addeditenquiry_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../components/enquiry/addeditenquiry/addeditenquiry.component */ "./src/app/components/enquiry/addeditenquiry/addeditenquiry.component.ts");
/* harmony import */ var _components_admission_admissionmaster_admissionmaster_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../components/admission/admissionmaster/admissionmaster.component */ "./src/app/components/admission/admissionmaster/admissionmaster.component.ts");
/* harmony import */ var _components_admission_admissiontab1_admissiontab1_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../components/admission/admissiontab1/admissiontab1.component */ "./src/app/components/admission/admissiontab1/admissiontab1.component.ts");
/* harmony import */ var _components_admission_remainingfees_remainingfees_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../components/admission/remainingfees/remainingfees.component */ "./src/app/components/admission/remainingfees/remainingfees.component.ts");
/* harmony import */ var _components_album_albumlist_albumlist_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../components/album/albumlist/albumlist.component */ "./src/app/components/album/albumlist/albumlist.component.ts");
/* harmony import */ var _components_album_albumimages_albumimages_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../components/album/albumimages/albumimages.component */ "./src/app/components/album/albumimages/albumimages.component.ts");
/* harmony import */ var _components_timetable_timetablemaster_timetablemaster_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../components/timetable/timetablemaster/timetablemaster.component */ "./src/app/components/timetable/timetablemaster/timetablemaster.component.ts");
/* harmony import */ var _components_timetable_timetabledetails_timetabledetails_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../components/timetable/timetabledetails/timetabledetails.component */ "./src/app/components/timetable/timetabledetails/timetabledetails.component.ts");
/* harmony import */ var _components_timetable_pdfview_pdfview_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../components/timetable/pdfview/pdfview.component */ "./src/app/components/timetable/pdfview/pdfview.component.ts");
/* harmony import */ var _components_folderdocuments_folderdocument_folderdocument_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../components/folderdocuments/folderdocument/folderdocument.component */ "./src/app/components/folderdocuments/folderdocument/folderdocument.component.ts");
/* harmony import */ var _components_folderdocuments_documentdetails_documentdetails_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ../components/folderdocuments/documentdetails/documentdetails.component */ "./src/app/components/folderdocuments/documentdetails/documentdetails.component.ts");
/* harmony import */ var _components_folderdocuments_pdfdocview_pdfdocview_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ../components/folderdocuments/pdfdocview/pdfdocview.component */ "./src/app/components/folderdocuments/pdfdocview/pdfdocview.component.ts");
/* harmony import */ var _components_attendence_markattendence_markattendence_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ../components/attendence/markattendence/markattendence.component */ "./src/app/components/attendence/markattendence/markattendence.component.ts");




















const routes = [
    {
        path: "login",
        component: _components_login_login_component__WEBPACK_IMPORTED_MODULE_4__["LoginComponent"],
    },
    {
        path: "",
        component: _components_dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_3__["DashboardComponent"],
        canActivate: [_auth_auth_guard__WEBPACK_IMPORTED_MODULE_5__["AuthGuard"]],
    },
    {
        path: "enquirylist",
        component: _components_enquiry_enquirylist_enquirylist_component__WEBPACK_IMPORTED_MODULE_6__["EnquirylistComponent"],
        canActivate: [_auth_auth_guard__WEBPACK_IMPORTED_MODULE_5__["AuthGuard"]],
    },
    {
        path: "enquirylist/addeditenquiry",
        component: _components_enquiry_addeditenquiry_addeditenquiry_component__WEBPACK_IMPORTED_MODULE_7__["AddeditenquiryComponent"],
        canActivate: [_auth_auth_guard__WEBPACK_IMPORTED_MODULE_5__["AuthGuard"]],
    },
    {
        path: 'enquirylist/addeditenquiry/:enquiryid',
        component: _components_enquiry_addeditenquiry_addeditenquiry_component__WEBPACK_IMPORTED_MODULE_7__["AddeditenquiryComponent"],
        canActivate: [_auth_auth_guard__WEBPACK_IMPORTED_MODULE_5__["AuthGuard"]],
    },
    {
        path: "admissionlist",
        component: _components_admission_admissionmaster_admissionmaster_component__WEBPACK_IMPORTED_MODULE_8__["AdmissionmasterComponent"],
        canActivate: [_auth_auth_guard__WEBPACK_IMPORTED_MODULE_5__["AuthGuard"]],
    },
    {
        path: "admissionlist/addeditadmission",
        component: _components_admission_admissiontab1_admissiontab1_component__WEBPACK_IMPORTED_MODULE_9__["Admissiontab1Component"],
        canActivate: [_auth_auth_guard__WEBPACK_IMPORTED_MODULE_5__["AuthGuard"]],
    },
    {
        path: "remainingfees",
        component: _components_admission_remainingfees_remainingfees_component__WEBPACK_IMPORTED_MODULE_10__["RemainingfeesComponent"],
        canActivate: [_auth_auth_guard__WEBPACK_IMPORTED_MODULE_5__["AuthGuard"]],
    },
    {
        path: 'admissionlist/addeditadmission/:studentid',
        component: _components_admission_admissiontab1_admissiontab1_component__WEBPACK_IMPORTED_MODULE_9__["Admissiontab1Component"],
        canActivate: [_auth_auth_guard__WEBPACK_IMPORTED_MODULE_5__["AuthGuard"]],
    },
    {
        path: "albumlist",
        component: _components_album_albumlist_albumlist_component__WEBPACK_IMPORTED_MODULE_11__["AlbumlistComponent"],
        canActivate: [_auth_auth_guard__WEBPACK_IMPORTED_MODULE_5__["AuthGuard"]],
    },
    {
        path: 'albumlist/albumimages/:albumid',
        component: _components_album_albumimages_albumimages_component__WEBPACK_IMPORTED_MODULE_12__["AlbumimagesComponent"],
        canActivate: [_auth_auth_guard__WEBPACK_IMPORTED_MODULE_5__["AuthGuard"]],
    },
    {
        path: "timetables",
        component: _components_timetable_timetablemaster_timetablemaster_component__WEBPACK_IMPORTED_MODULE_13__["TimetablemasterComponent"],
        canActivate: [_auth_auth_guard__WEBPACK_IMPORTED_MODULE_5__["AuthGuard"]],
    },
    {
        path: "timetables/viewTimetable/:timetableid",
        component: _components_timetable_timetabledetails_timetabledetails_component__WEBPACK_IMPORTED_MODULE_14__["TimetabledetailsComponent"],
        canActivate: [_auth_auth_guard__WEBPACK_IMPORTED_MODULE_5__["AuthGuard"]],
    },
    {
        path: "timetables/viewTimetable/:timetableid/:timetabledocid",
        component: _components_timetable_pdfview_pdfview_component__WEBPACK_IMPORTED_MODULE_15__["PdfviewComponent"],
        canActivate: [_auth_auth_guard__WEBPACK_IMPORTED_MODULE_5__["AuthGuard"]],
    },
    {
        path: "documentfolder",
        component: _components_folderdocuments_folderdocument_folderdocument_component__WEBPACK_IMPORTED_MODULE_16__["FolderdocumentComponent"],
        canActivate: [_auth_auth_guard__WEBPACK_IMPORTED_MODULE_5__["AuthGuard"]],
    },
    {
        path: "documentfolder/viewDocument/:folderid",
        component: _components_folderdocuments_documentdetails_documentdetails_component__WEBPACK_IMPORTED_MODULE_17__["DocumentdetailsComponent"],
        canActivate: [_auth_auth_guard__WEBPACK_IMPORTED_MODULE_5__["AuthGuard"]],
    },
    {
        path: "documentfolder/viewDocument/:folderid/:docfolderid",
        component: _components_folderdocuments_pdfdocview_pdfdocview_component__WEBPACK_IMPORTED_MODULE_18__["PdfdocviewComponent"],
        canActivate: [_auth_auth_guard__WEBPACK_IMPORTED_MODULE_5__["AuthGuard"]],
    },
    {
        path: "markattendence",
        component: _components_attendence_markattendence_markattendence_component__WEBPACK_IMPORTED_MODULE_19__["MarkattendenceComponent"],
        canActivate: [_auth_auth_guard__WEBPACK_IMPORTED_MODULE_5__["AuthGuard"]],
    },
];
let AppRoutingModule = class AppRoutingModule {
};
AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], AppRoutingModule);



/***/ }),

/***/ "./src/app/services/auth.service.ts":
/*!******************************************!*\
  !*** ./src/app/services/auth.service.ts ***!
  \******************************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");







let AuthService = class AuthService {
    constructor(myRoute, http) {
        this.myRoute = myRoute;
        this.http = http;
    }
    sendToken(token) {
        localStorage.setItem("LoggedInUser", token);
    }
    getToken() {
        return localStorage.getItem("LoggedInUser");
    }
    isLoggedIn() {
        return localStorage.getItem("LoggedInUser") != null;
    }
    login(username, password) {
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpHeaders"]({
                'username': username,
                'password': password
            })
        };
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].url + `userLogin`, "", httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(Loginresponse => {
            return Loginresponse;
        }));
    }
    userLogOut() {
        const formData = new FormData();
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid = currentUser.user_id;
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpHeaders"]({
                'userid': atob(usermasterid),
            })
        };
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].url + `userLogOut`, formData, httpOptions);
    }
};
AuthService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
        _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]])
], AuthService);



/***/ }),

/***/ "./src/app/services/common.service.ts":
/*!********************************************!*\
  !*** ./src/app/services/common.service.ts ***!
  \********************************************/
/*! exports provided: CommonService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CommonService", function() { return CommonService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");




let CommonService = class CommonService {
    constructor(http) {
        this.http = http;
    }
    listInquiries(page, size, enquiryFilter) {
        const formData = new FormData();
        formData.append('page', page.toString());
        console.log(page);
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let academicyearid = localStorage.getItem('academicyearid');
        let usermasterid = currentUser.user_id;
        let zoneid = currentUser.zone_id;
        let centerid = currentUser.center_id;
        let utoken = currentUser.utoken;
        let academicyear = atob(academicyearid);
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                "Access-Control-Allow-Methods": "GET, POST",
                "Access-Control-Allow-Origin": "*",
                'utoken': atob(utoken),
                'userid': atob(usermasterid),
                'zoneid': atob(zoneid),
                'centerid': atob(centerid),
                'inquiryno': enquiryFilter.inquiryno,
                'inquirydate': enquiryFilter.inquirydate,
                "page": page.toString(),
                'academicyear': academicyear
            })
        };
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].url + `listInquiries`, formData, httpOptions);
    }
    getAcademicYears() {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].url + `getAcademicYears`);
    }
    getCategories() {
        const formData = new FormData();
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid = currentUser.user_id;
        let utoken = currentUser.utoken;
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                "Access-Control-Allow-Methods": "GET, POST",
                "Access-Control-Allow-Origin": "*",
                'utoken': atob(utoken),
                'userid': atob(usermasterid),
            })
        };
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].url + `getCategories`, formData, httpOptions);
    }
    getCourses(categoryid) {
        const formData = new FormData();
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid = currentUser.user_id;
        let utoken = currentUser.utoken;
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                "Access-Control-Allow-Methods": "GET, POST",
                "Access-Control-Allow-Origin": "*",
                'utoken': atob(utoken),
                'userid': atob(usermasterid),
                'categoryid': categoryid,
            })
        };
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].url + `getCourses`, formData, httpOptions);
    }
    getCountries() {
        const formData = new FormData();
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid = currentUser.user_id;
        let utoken = currentUser.utoken;
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                "Access-Control-Allow-Methods": "GET, POST",
                "Access-Control-Allow-Origin": "*",
                'utoken': atob(utoken),
                'userid': atob(usermasterid),
            })
        };
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].url + `getCountries`, formData, httpOptions);
    }
    getState(countryid) {
        const formData = new FormData();
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid = currentUser.user_id;
        let utoken = currentUser.utoken;
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                "Access-Control-Allow-Methods": "GET, POST",
                "Access-Control-Allow-Origin": "*",
                'utoken': atob(utoken),
                'userid': atob(usermasterid),
                'countryid': countryid,
            })
        };
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].url + `getState`, formData, httpOptions);
    }
    getCity(stateid) {
        const formData = new FormData();
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid = currentUser.user_id;
        let utoken = currentUser.utoken;
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                "Access-Control-Allow-Methods": "GET, POST",
                "Access-Control-Allow-Origin": "*",
                'utoken': atob(utoken),
                'userid': atob(usermasterid),
                'stateid': stateid,
            })
        };
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].url + `getCities`, formData, httpOptions);
    }
    getInquiryNo() {
        const formData = new FormData();
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid = currentUser.user_id;
        let utoken = currentUser.utoken;
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                "Access-Control-Allow-Methods": "GET, POST",
                "Access-Control-Allow-Origin": "*",
                'utoken': atob(utoken),
                'userid': atob(usermasterid),
            })
        };
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].url + `getInquiryNo`, formData, httpOptions);
    }
    addEditEnquiryForm(formValues, enquiry_id) {
        const formData = new FormData();
        // formData.append('inquiryid',enquiry_id);
        formData.append('student_first_name', formValues.student_first_name);
        formData.append('student_last_name', formValues.student_last_name);
        formData.append('country_id', formValues.country_id);
        formData.append('state_id', formValues.state_id);
        formData.append('city_id', formValues.city_id);
        formData.append('pincode', formValues.pincode);
        formData.append('father_name', formValues.father_name);
        formData.append('father_contact_no', formValues.father_contact_no);
        formData.append('father_emailid', formValues.father_emailid);
        formData.append('inquiry_remark', formValues.inquiry_remark);
        formData.append('inquiry_handled_by', formValues.inquiry_handled_by);
        if (formValues.rectype == false) {
            formValues.rectype = 'Inquiry';
            formValues.inquiryno = '';
            if (formValues.student_dob.formatted == undefined) {
                formData.append('student_dob', formValues.student_dob);
            }
            else {
                console.log(2);
                formData.append('student_dob', formValues.student_dob.formatted);
            }
            if (formValues.gender == false || formValues.gender == undefined) {
                formValues.gender = 'Female';
            }
            else {
                formValues.gender = "Male";
            }
            // if(formValues.inquirydate == undefined){
            //     var today = new Date();
            //     var dd1 = today.getDate();
            //     var mm1 = today.getMonth() + 1; //January is 0!
            //     var yyyy = today.getFullYear();
            //     if (dd1 < 10) {
            //         var dd = '0' + dd1;
            //     } 
            //     else{
            //         var dd = '' + dd1
            //     }
            //     if (mm1 < 10) {
            //         var mm = '0' + mm1;
            //     } 
            //     else{
            //         var mm = '' + mm1
            //     }
            //     formValues.inquirydate = dd + '-' + mm + '-' + yyyy;
            // }
            formData.append('gender', formValues.gender);
            formData.append('age_year', formValues.age_year);
            formData.append('age_month', formValues.age_month);
            formData.append('category_id', formValues.category_id);
            formData.append('course_id', formValues.course_id);
            if (formValues.has_attended_preschool_before == false || formValues.has_attended_preschool_before == undefined) {
                formValues.has_attended_preschool_before = 'No';
            }
            else {
                formValues.has_attended_preschool_before = 'Yes';
            }
            formData.append('has_attended_preschool_before', formValues.has_attended_preschool_before);
            formData.append('preschool_name', formValues.preschool_name);
            formData.append('present_address', formValues.present_address);
            formData.append('father_education', formValues.father_education);
            formData.append('father_profession', formValues.father_profession);
            formData.append('mother_name', formValues.mother_name);
            formData.append('mother_contact_no', formValues.mother_contact_no);
            formData.append('mother_emailid', formValues.mother_emailid);
            formData.append('mother_education', formValues.mother_education);
            formData.append('mother_profession', formValues.mother_profession);
            formData.append('how_know_about_school', formValues.how_know_about_school);
            formData.append('how_know_about_other', formValues.how_know_about_other);
            if (formValues.inquirydate.jsdate == undefined) {
                if (formValues.inquirydate.date == undefined) {
                    formData.append('inquirydate', formValues.inquirydate);
                }
                else {
                    formValues.inquirydate = formValues.inquirydate.day + '-' + formValues.inquirydate.month + '-' + formValues.inquirydate.date.year;
                }
            }
            else {
                var date = new Date(formValues.inquirydate.jsdate), month = ("0" + (date.getMonth() + 1)).slice(-2), day = ("0" + date.getDate()).slice(-2);
                formData.append('inquirydate', day + '-' + month + '-' + date.getFullYear());
                formValues.inquirydate = day + '-' + month + '-' + date.getFullYear();
            }
            formData.append('academicyear', formValues.academicyear);
        }
        else {
            formValues.rectype = 'Lead';
            enquiry_id = '';
        }
        console.log(enquiry_id);
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid = currentUser.user_id;
        let utoken = currentUser.utoken;
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'utoken': atob(utoken),
                'userid': atob(usermasterid),
                'zoneid': atob(currentUser.zone_id),
                'centerid': atob(currentUser.center_id),
                'inquirydate': formValues.inquirydate,
                'academicyear': formValues.academicyear,
                'rectype': formValues.rectype,
                'leadid': enquiry_id,
                'inquirystatus': formValues.inquirystatus,
                'inquiryprogress': formValues.inquiryprogress,
                'inquiryno': formValues.inquiryno
            })
        };
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].url + `addEditInquiry`, formData, httpOptions);
    }
    getInquiryDetails(enquiryid) {
        const formData = new FormData();
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid = currentUser.user_id;
        let utoken = currentUser.utoken;
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                "Access-Control-Allow-Methods": "GET, POST",
                "Access-Control-Allow-Origin": "*",
                'utoken': atob(utoken),
                'userid': atob(usermasterid),
                'inquiryid': enquiryid,
            })
        };
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].url + `getInquiryDetails`, formData, httpOptions);
    }
    getInquiries() {
        const formData = new FormData();
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid = currentUser.user_id;
        let utoken = currentUser.utoken;
        let academicyearid = localStorage.getItem('academicyearid');
        let academicyear = atob(academicyearid);
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                "Access-Control-Allow-Methods": "GET, POST",
                "Access-Control-Allow-Origin": "*",
                'utoken': atob(utoken),
                'userid': atob(usermasterid),
                'academicyear': academicyear
            })
        };
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].url + `getInquiries`, formData, httpOptions);
    }
    getEnrollmentNo() {
        const formData = new FormData();
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid = currentUser.user_id;
        let utoken = currentUser.utoken;
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                "Access-Control-Allow-Methods": "GET, POST",
                "Access-Control-Allow-Origin": "*",
                'utoken': atob(utoken),
                'userid': atob(usermasterid),
            })
        };
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].url + `getEnrollmentNo`, formData, httpOptions);
    }
    getCenterUserBatches(categoryid, courseid) {
        const formData = new FormData();
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid = currentUser.user_id;
        let utoken = currentUser.utoken;
        let zoneid = currentUser.zone_id;
        let centerid = currentUser.center_id;
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                "Access-Control-Allow-Methods": "GET, POST",
                "Access-Control-Allow-Origin": "*",
                'utoken': atob(utoken),
                'userid': atob(usermasterid),
                'zoneid': atob(zoneid),
                'centerid': atob(centerid),
                'categoryid': categoryid,
                'courseid': courseid
            })
        };
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].url + `getCenterUserBatches`, formData, httpOptions);
    }
    addEditAdmissionTab1(formValues, student_id) {
        const formData = new FormData();
        formValues.dobyear = formValues.dobyear.toString();
        formValues.dobmonth = formValues.dobmonth.toString();
        formData.append('studentid', student_id);
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid = currentUser.user_id;
        let utoken = currentUser.utoken;
        let zoneid = currentUser.zone_id;
        let centerid = currentUser.center_id;
        if (formValues.attendedpreschoolbefore == false || formValues.attendedpreschoolbefore == undefined) {
            formValues.attendedpreschoolbefore = 'No';
        }
        else {
            formValues.attendedpreschoolbefore = 'Yes';
        }
        if (formValues.admissiondate.formatted == undefined) {
            var today = new Date();
            var dd1 = today.getDate();
            var mm1 = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();
            if (dd1 < 10) {
                var dd = '0' + dd1;
            }
            else {
                var dd = '' + dd1;
            }
            if (mm1 < 10) {
                var mm = '0' + mm1;
            }
            else {
                var mm = '' + mm1;
            }
            formValues.admissiondate = dd + '-' + mm + '-' + yyyy;
        }
        else {
            formValues.admissiondate = formValues.admissiondate.formatted;
        }
        if (formValues.programmestartdate.formatted == undefined) {
            let programmestartdate = formValues.programmestartdate.date.day + '-' + formValues.programmestartdate.date.month + '-' + formValues.programmestartdate.date.year;
            formValues.programmestartdate = programmestartdate.toString();
        }
        else {
            formValues.programmestartdate = formValues.programmestartdate.formatted;
        }
        formValues.ageon = formValues.programmestartdate;
        if (formValues.programmeenddate.formatted == undefined) {
            let programmeenddate = formValues.programmeenddate.date.day + '-' + formValues.programmeenddate.date.month + '-' + formValues.programmeenddate.date.year;
            formValues.programmeenddate = programmeenddate.toString();
        }
        else {
            formValues.programmeenddate = formValues.programmeenddate.formatted;
        }
        console.log(formValues);
        console.log(formValues.programmeenddate);
        if (formValues.dob.date.day == undefined) {
            formValues.dob = formValues.dob;
        }
        else {
            let dob = formValues.dob.date.day + '-' + formValues.dob.date.month + '-' + formValues.dob.date.year;
            formValues.dob = dob.toString();
        }
        // if(formValues.ageon.formatted == undefined){
        //     let ageon = formValues.ageon.date.day+'-'+formValues.ageon.date.month+'-'+formValues.ageon.date.year
        //     formValues.ageon = ageon.toString()
        // }
        // else{
        //     formValues.ageon = formValues.ageon.formatted
        // }
        formData.append('zoneid', atob(zoneid));
        formData.append('centerid', atob(centerid));
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'utoken': atob(utoken),
                'userid': atob(usermasterid),
                'inquiryid': formValues.inquiryid,
                'enrollmentno': formValues.enrollmentno,
                'academicyearid': formValues.academicyearid,
                'zoneid': atob(zoneid),
                'centerid': atob(centerid),
                'studentid': student_id,
                'categoryid': formValues.categoryid,
                'courseid': formValues.courseid,
                'batchid': formValues.batchid,
                'admissiondate': formValues.admissiondate,
                'programmestartdate': formValues.programmestartdate,
                'programmeenddate': formValues.programmeenddate,
                'studentfirstname': formValues.studentfirstname,
                'studentlastname': formValues.studentlastname,
                'dob': formValues.dob,
                'ageon': formValues.ageon,
                'dobyear': formValues.dobyear,
                'dobmonth': formValues.dobmonth,
                'nationality': formValues.nationality,
                'religion': formValues.religion,
                'mothertongue': formValues.mothertongue,
                'otherlanguages': formValues.otherlanguages,
                'attendedpreschoolbefore': formValues.attendedpreschoolbefore,
                'preschoolname': formValues.preschoolname
            })
        };
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].url + `addEditAdmissionTab1`, formData, httpOptions);
    }
    admissionTab2(formValues, student_id) {
        const formData = new FormData();
        console.log(formValues);
        formData.append('studentid', student_id);
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid = currentUser.user_id;
        let utoken = currentUser.utoken;
        // if(formValues.sibling1dob == undefined){
        //     formValues.sibling1dob = '00-00-0000' 
        //     formValues.sibling1school = '' 
        // }
        // if(formValues.sibling2dob == undefined){
        //     formValues.sibling2dob = '00-00-0000'
        //     formValues.sibling2school = ''
        // }
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'utoken': atob(utoken),
                'userid': atob(usermasterid),
                'studentid': student_id,
                'fathername': formValues.fathername,
                'fatherprof': formValues.fatherprof,
                'fatherlanguages': formValues.fatherlanguages,
                'fathernationality': formValues.fathernationality,
                'mothername': formValues.mothername,
                'motherprof': formValues.motherprof,
                'motherlanguages': formValues.motherlanguages,
                'mothernationality': formValues.mothernationality,
                'sibling1id': formValues.sibling1id,
                'sibling2id': formValues.sibling2id,
            })
        };
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].url + `admissionTab2`, formData, httpOptions);
    }
    getStudentsWithEnrollmentNo(enrollmentno) {
        const formData = new FormData();
        console.log(enrollmentno);
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid = currentUser.user_id;
        let utoken = currentUser.utoken;
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'utoken': atob(utoken),
                'userid': atob(usermasterid),
                'enrollmentno': enrollmentno
            })
        };
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].url + `getStudentsWithEnrollmentNo`, formData, httpOptions);
    }
    admissionTab3(formValues, student_id) {
        const formData = new FormData();
        formData.append('studentid', student_id);
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid = currentUser.user_id;
        let utoken = currentUser.utoken;
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'utoken': atob(utoken),
                'userid': atob(usermasterid),
                'studentid': student_id,
                'presentaddress': formValues.presentaddress,
                'countryid': formValues.countryid,
                'stateid': formValues.stateid,
                'cityid': formValues.cityid,
                'pincode': formValues.pincode.toString(),
                'motherhomecontactno': formValues.motherhomecontactno.toString(),
                'motherofficecontactno': formValues.motherofficecontactno.toString(),
                'mothermobilecontactno': formValues.mothermobilecontactno.toString(),
                'motheremailid': formValues.motheremailid,
                'fatherhomecontactno': formValues.fatherhomecontactno.toString(),
                'fatherofficecontactno': formValues.fatherofficecontactno.toString(),
                'fathermobilecontactno': formValues.fathermobilecontactno.toString(),
                'fatheremailid': formValues.fatheremailid,
                'emergencycontactname': formValues.emergencycontactname,
                'emergencycontacttelno': formValues.emergencycontacttelno.toString(),
                'emergencycontactmobileno': formValues.emergencycontactmobileno.toString(),
                'emergencycontactrelationship': formValues.emergencycontactrelationship,
            })
        };
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].url + `admissionTab3`, formData, httpOptions);
    }
    admissionTab4(formValues, student_id) {
        const formData = new FormData();
        formData.append('studentid', student_id);
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid = currentUser.user_id;
        let utoken = currentUser.utoken;
        if (formValues.authperson1gender == false || formValues.authperson1gender == undefined) {
            formValues.authperson1gender = 'Female';
        }
        else {
            formValues.authperson1gender = "Male";
        }
        if (formValues.authperson2gender == false || formValues.authperson2gender == undefined) {
            formValues.authperson2gender = 'Female';
        }
        else {
            formValues.authperson2gender = "Male";
        }
        if (formValues.authperson3gender == false || formValues.authperson3gender == undefined) {
            formValues.authperson3gender = 'Female';
        }
        else {
            formValues.authperson3gender = "Male";
        }
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'utoken': atob(utoken),
                'userid': atob(usermasterid),
                'studentid': student_id,
                'authperson1': formValues.authperson1,
                'authperson1relation': formValues.authperson1relation,
                'authperson1gender': formValues.authperson1gender,
                'authperson2': formValues.authperson2,
                'authperson2relation': formValues.authperson2relation,
                'authperson2gender': formValues.authperson2gender,
                'authperson3': formValues.authperson3,
                'authperson3relation': formValues.authperson3relation,
                'authperson3gender': formValues.authperson3gender,
            })
        };
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].url + `admissionTab4`, formData, httpOptions);
    }
    listAdmissions(page, size, admissionFilter) {
        const formData = new FormData();
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid = currentUser.user_id;
        let zoneid = currentUser.zone_id;
        let centerid = currentUser.center_id;
        let utoken = currentUser.utoken;
        let academicyearid = localStorage.getItem('academicyearid');
        let academicyear = atob(academicyearid);
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                "Access-Control-Allow-Methods": "GET, POST",
                "Access-Control-Allow-Origin": "*",
                'utoken': atob(utoken),
                'userid': atob(usermasterid),
                'zoneid': atob(zoneid),
                'centerid': atob(centerid),
                'enrollmentno': admissionFilter.enrollmentno,
                "page": page.toString(),
                'academicyear': academicyear
            })
        };
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].url + `listAdmissions`, formData, httpOptions);
    }
    getAdmissionTab1(studentid) {
        const formData = new FormData();
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid = currentUser.user_id;
        let utoken = currentUser.utoken;
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                "Access-Control-Allow-Methods": "GET, POST",
                "Access-Control-Allow-Origin": "*",
                'utoken': atob(utoken),
                'userid': atob(usermasterid),
                'studentid': studentid,
            })
        };
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].url + `getAdmissionTab1`, formData, httpOptions);
    }
    getAdmissionTab2(studentid) {
        const formData = new FormData();
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid = currentUser.user_id;
        let utoken = currentUser.utoken;
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                "Access-Control-Allow-Methods": "GET, POST",
                "Access-Control-Allow-Origin": "*",
                'utoken': atob(utoken),
                'userid': atob(usermasterid),
                'studentid': studentid,
            })
        };
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].url + `getAdmissionTab2`, formData, httpOptions);
    }
    getAdmissionTab3(studentid) {
        const formData = new FormData();
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid = currentUser.user_id;
        let utoken = currentUser.utoken;
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                "Access-Control-Allow-Methods": "GET, POST",
                "Access-Control-Allow-Origin": "*",
                'utoken': atob(utoken),
                'userid': atob(usermasterid),
                'studentid': studentid,
            })
        };
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].url + `getAdmissionTab3`, formData, httpOptions);
    }
    getAdmissionTab4(studentid) {
        const formData = new FormData();
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid = currentUser.user_id;
        let utoken = currentUser.utoken;
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                "Access-Control-Allow-Methods": "GET, POST",
                "Access-Control-Allow-Origin": "*",
                'utoken': atob(utoken),
                'userid': atob(usermasterid),
                'studentid': studentid,
            })
        };
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].url + `getAdmissionTab4`, formData, httpOptions);
    }
    getAlbums(page, size, albumFilter) {
        let newpage = page.toString();
        const formData = new FormData();
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid = currentUser.user_id;
        let zoneid = currentUser.zone_id;
        let centerid = currentUser.center_id;
        let utoken = currentUser.utoken;
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                "Access-Control-Allow-Methods": "GET, POST",
                "Access-Control-Allow-Origin": "*",
                'utoken': atob(utoken),
                'userid': atob(usermasterid),
                'zoneid': atob(zoneid),
                'centerid': atob(centerid),
                'page': newpage,
            })
        };
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].url + `getAlbums`, formData, httpOptions);
    }
    getAlbumsScroll(page) {
        let pageScroll = page.toString();
        const formData = new FormData();
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid = currentUser.user_id;
        let zoneid = currentUser.zone_id;
        let centerid = currentUser.center_id;
        let utoken = currentUser.utoken;
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                "Access-Control-Allow-Methods": "GET, POST",
                "Access-Control-Allow-Origin": "*",
                'utoken': atob(utoken),
                'userid': atob(usermasterid),
                'zoneid': atob(zoneid),
                'centerid': atob(centerid),
                'page': pageScroll,
            })
        };
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].url + `getAlbums`, formData, httpOptions);
    }
    getAlbumsImages(albumId) {
        const formData = new FormData();
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid = currentUser.user_id;
        let utoken = currentUser.utoken;
        let zoneid = currentUser.zone_id;
        let centerid = currentUser.center_id;
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                "Access-Control-Allow-Methods": "GET, POST",
                "Access-Control-Allow-Origin": "*",
                'utoken': atob(utoken),
                'userid': atob(usermasterid),
                'zoneid': atob(zoneid),
                'centerid': atob(centerid),
                'albumid': albumId,
            })
        };
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].url + `getAlbumsImages`, formData, httpOptions);
    }
    deleteAlbumsImages(albumimageid, albumid) {
        const formData = new FormData();
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid = currentUser.user_id;
        let utoken = currentUser.utoken;
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                "Access-Control-Allow-Methods": "GET, POST",
                "Access-Control-Allow-Origin": "*",
                'userid': usermasterid,
                'albumid': albumid,
                'albumimageid': albumimageid,
            })
        };
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].url + `deleteAlbumsImages`, formData, httpOptions);
    }
    uploadAlbumImage(formValues, album_id) {
        const formData = new FormData();
        console.log(formValues);
        formData.append('albumpic', formValues);
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid = currentUser.user_id;
        let utoken = currentUser.utoken;
        let zoneid = currentUser.zone_id;
        let centerid = currentUser.center_id;
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'utoken': atob(utoken),
                'userid': atob(usermasterid),
                'zoneid': atob(zoneid),
                'centerid': atob(centerid),
                'albumid': album_id,
            })
        };
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].url + `uploadAlbumImage`, formData, httpOptions);
    }
    addEditAdmissionDocs(formValues, student_id) {
        const formData = new FormData();
        console.log(formValues);
        formData.append('duly_filled_admission_form', formValues.duly_filled_admission_form);
        formData.append('birth_certificate', formValues.birth_certificate);
        formData.append('profile_pic', formValues.profile_pic);
        formData.append('family_photo', formValues.family_photo);
        formData.append('address_proof', formValues.address_proof);
        formData.append('duly_filled_child_profile_form', formValues.duly_filled_child_profile_form);
        formData.append('duly_filled_admission_form_value', formValues.duly_filled_admission_form);
        formData.append('birth_certificate_value', formValues.birth_certificate);
        formData.append('profile_pic_value', formValues.profile_pic);
        formData.append('family_photo_value', formValues.family_photo);
        formData.append('address_proof_value', formValues.address_proof);
        formData.append('duly_filled_child_profile_form_value', formValues.duly_filled_child_profile_form);
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid = currentUser.user_id;
        let utoken = currentUser.utoken;
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'utoken': atob(utoken),
                'userid': atob(usermasterid),
                'studentid': student_id,
            })
        };
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].url + `addEditAdmissionDocs`, formData, httpOptions);
    }
    getAdmissionDocs(studentid) {
        const formData = new FormData();
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid = currentUser.user_id;
        let utoken = currentUser.utoken;
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                "Access-Control-Allow-Methods": "GET, POST",
                "Access-Control-Allow-Origin": "*",
                'utoken': atob(utoken),
                'userid': atob(usermasterid),
                'studentid': studentid,
            })
        };
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].url + `getAdmissionDocs`, formData, httpOptions);
    }
    getFeesLevel() {
        const formData = new FormData();
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid = currentUser.user_id;
        let utoken = currentUser.utoken;
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                "Access-Control-Allow-Methods": "GET, POST",
                "Access-Control-Allow-Origin": "*",
                'utoken': atob(utoken),
                'userid': atob(usermasterid),
            })
        };
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].url + `getFeesLevel`, formData, httpOptions);
    }
    getCenterFees(fees_level_id, category_id, course_id, feesselectiontype) {
        const formData = new FormData();
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid = currentUser.user_id;
        let utoken = currentUser.utoken;
        let academicyearid = localStorage.getItem('academicyearid');
        let zoneid = currentUser.zone_id;
        let centerid = currentUser.center_id;
        let academicyear = atob(academicyearid);
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                "Access-Control-Allow-Methods": "GET, POST",
                "Access-Control-Allow-Origin": "*",
                'utoken': atob(utoken),
                'userid': atob(usermasterid),
                'zoneid': atob(zoneid),
                'centerid': atob(centerid),
                'acadmicyear': academicyear,
                'feesselectiontype': feesselectiontype,
                'categoryid': category_id,
                'courseid': course_id,
                'feeslevelid': fees_level_id,
            })
        };
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].url + `getCenterFees`, formData, httpOptions);
    }
    payAdmissionFees(formValues, student_id, collected_amount, remainig_amount, monthArray, installmentNo) {
        const formData = new FormData();
        let fees_collected_amount = 0;
        let fees_remainig_amount = 0;
        var date = new Date();
        var year = date.getFullYear();
        let isinstallment = '';
        let noofinstallment = '';
        let maxinstallmentallowed = '';
        if (formValues.noofinstallment == '') {
            fees_collected_amount = formValues.fees_amount_collected;
            fees_remainig_amount = formValues.fees_remaining_amount;
            isinstallment = 'No';
            noofinstallment = '';
            maxinstallmentallowed = '';
        }
        else {
            for (let i = 0; i < installmentNo[1]; i++) {
                if (formValues.total_amount == undefined) {
                    let installment_amnt = (formValues.fees_total_amount / installmentNo[1]);
                    formData.append('installment_amount[' + i + ']', installment_amnt.toString());
                }
                else {
                    let installment_amnt = (formValues.total_amount / installmentNo[1]);
                    formData.append('installment_amount[' + i + ']', installment_amnt.toString());
                }
                fees_collected_amount = fees_collected_amount + Number(collected_amount[i]);
                fees_remainig_amount = fees_remainig_amount + Number(remainig_amount[i]);
                formData.append('installment_due_date[' + i + ']', '01-' + monthArray[i] + '-' + year);
                formData.append('installment_collected_amount[' + i + ']', collected_amount[i]);
                formData.append('installment_remaining_amount[' + i + ']', remainig_amount[i]);
            }
            isinstallment = 'Yes';
            noofinstallment = installmentNo[1].toString();
            maxinstallmentallowed = installmentNo[0];
        }
        if (formValues.feestype == false) {
            formValues.feestype = 'Class';
        }
        else {
            formValues.feestype = 'Group';
        }
        formData.append('fees_total_amount', formValues.fees_total_amount);
        formData.append('fees_amount_collected', fees_collected_amount.toString());
        formData.append('fees_remaining_amount', fees_remainig_amount.toString());
        formData.append('fees_remark', formValues.fees_remark);
        formData.append('fees_approved_accepted_by_name', formValues.fees_approved_accepted_by_name);
        formData.append('payment_mode', formValues.payment_mode);
        formData.append('discount_amount', formValues.discount_amount);
        formData.append('total_amount', formValues.total_amount);
        if (formValues.payment_mode == 'Cheque') {
            formData.append('cheque_no', formValues.cheque_no);
        }
        if (formValues.payment_mode == 'Netbanking') {
            formData.append('transaction_id', formValues.transaction_id);
        }
        if (formValues.payment_mode == 'Cheque' || formValues.payment_mode == 'Netbanking') {
            if (formValues.transaction_date.formatted == undefined) {
                var today = new Date();
                var dd1 = today.getDate();
                var mm1 = today.getMonth() + 1; //January is 0!
                var yyyy = today.getFullYear();
                if (dd1 < 10) {
                    var dd = '0' + dd1;
                }
                else {
                    var dd = '' + dd1;
                }
                if (mm1 < 10) {
                    var mm = '0' + mm1;
                }
                else {
                    var mm = '' + mm1;
                }
                formValues.transaction_date = dd + '-' + mm + '-' + yyyy;
            }
            else {
                formValues.transaction_date = formValues.transaction_date.formatted;
            }
            formData.append('transaction_date', formValues.transaction_date);
        }
        if (formValues.payment_mode == 'Cheque' || formValues.payment_mode == 'Netbanking') {
            formData.append('bank_name', formValues.bank_name);
        }
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid = currentUser.user_id;
        let utoken = currentUser.utoken;
        let academicyearid = localStorage.getItem('academicyearid');
        let academicyear = atob(academicyearid);
        let zoneid = currentUser.zone_id;
        let centerid = currentUser.center_id;
        // if(formValues.feepaymenttype == false || formValues.feepaymenttype == undefined){
        //     formValues.isinstallment = 'No';
        //     formValues.noofinstallment = '';
        //     formValues.maxinstallmentallowed = '';
        // }
        // else{
        //     formValues.isinstallment = "Yes"; 
        //     formValues.noofinstallment = '';
        //     formValues.maxinstallmentallowed = '';
        // }
        // console.log(student_id)
        console.log(formValues);
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'utoken': atob(utoken),
                'userid': atob(usermasterid),
                'studentid': student_id,
                'acadmicyear': academicyear,
                'feesid': formValues.fees_id,
                'feestype': formValues.feestype.toString(),
                'zoneid': atob(zoneid),
                'centerid': atob(centerid),
                'categoryid': formValues.categoryid,
                'courseid': formValues.courseid,
                'batchid': formValues.batchid,
                'isinstallment': isinstallment,
                'noofinstallment': noofinstallment,
                'maxinstallmentallowed': maxinstallmentallowed,
            })
        };
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].url + `payAdmissionFees`, formData, httpOptions);
    }
    viewTimetables(page, size, timetableFilter) {
        const formData = new FormData();
        formData.append('page', page.toString());
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid = currentUser.user_id;
        let zoneid = currentUser.zone_id;
        let centerid = currentUser.center_id;
        let utoken = currentUser.utoken;
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                "Access-Control-Allow-Methods": "GET, POST",
                "Access-Control-Allow-Origin": "*",
                'utoken': atob(utoken),
                'userid': atob(usermasterid),
                'zoneid': atob(zoneid),
                'centerid': atob(centerid),
                "page": page.toString(),
            })
        };
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].url + `viewTimetables`, formData, httpOptions);
    }
    viewTimetableDetails(timetable_id) {
        const formData = new FormData();
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid = currentUser.user_id;
        let utoken = currentUser.utoken;
        let zoneid = currentUser.zone_id;
        let centerid = currentUser.center_id;
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'utoken': atob(utoken),
                'userid': atob(usermasterid),
                'zoneid': atob(currentUser.zone_id),
                'centerid': atob(currentUser.center_id),
                'timetableid': timetable_id
            })
        };
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].url + `viewTimetableDetails`, formData, httpOptions);
    }
    viewDocumentFolders(page, size, docmentFilter) {
        const formData = new FormData();
        formData.append('page', page.toString());
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid = currentUser.user_id;
        let zoneid = currentUser.zone_id;
        let centerid = currentUser.center_id;
        let utoken = currentUser.utoken;
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                "Access-Control-Allow-Methods": "GET, POST",
                "Access-Control-Allow-Origin": "*",
                'utoken': atob(utoken),
                'userid': atob(usermasterid),
                'zoneid': atob(zoneid),
                'centerid': atob(centerid),
                "page": page.toString(),
            })
        };
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].url + `viewDocumentFolders`, formData, httpOptions);
    }
    viewFolderDocuments(page, size, folder_id) {
        const formData = new FormData();
        formData.append('page', page.toString());
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid = currentUser.user_id;
        let utoken = currentUser.utoken;
        let zoneid = currentUser.zone_id;
        let centerid = currentUser.center_id;
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'utoken': atob(utoken),
                'userid': atob(usermasterid),
                'zoneid': atob(currentUser.zone_id),
                'centerid': atob(currentUser.center_id),
                'folderid': folder_id,
                "page": page.toString(),
            })
        };
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].url + `viewFolderDocuments`, formData, httpOptions);
    }
    viewFolderDoc(docfolder_id) {
        const formData = new FormData();
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid = currentUser.user_id;
        let utoken = currentUser.utoken;
        let zoneid = currentUser.zone_id;
        let centerid = currentUser.center_id;
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'utoken': atob(utoken),
                'userid': atob(usermasterid),
                'zoneid': atob(currentUser.zone_id),
                'centerid': atob(currentUser.center_id),
                'folderdocumentid': docfolder_id
            })
        };
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].url + `viewFolderDoc`, formData, httpOptions);
    }
    viewTimeTableDoc(timetabledoc_id) {
        const formData = new FormData();
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid = currentUser.user_id;
        let utoken = currentUser.utoken;
        let zoneid = currentUser.zone_id;
        let centerid = currentUser.center_id;
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'utoken': atob(utoken),
                'userid': atob(usermasterid),
                'zoneid': atob(currentUser.zone_id),
                'centerid': atob(currentUser.center_id),
                'timetabledocumentid': timetabledoc_id
            })
        };
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].url + `viewTimeTableDoc`, formData, httpOptions);
    }
    getInquiryId(enquiryno) {
        const formData = new FormData();
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let academicyearid = localStorage.getItem('academicyearid');
        let usermasterid = currentUser.user_id;
        let zoneid = currentUser.zone_id;
        let centerid = currentUser.center_id;
        let utoken = currentUser.utoken;
        let academicyear = atob(academicyearid);
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                "Access-Control-Allow-Methods": "GET, POST",
                "Access-Control-Allow-Origin": "*",
                'utoken': atob(utoken),
                'userid': atob(usermasterid),
                'zoneid': atob(zoneid),
                'centerid': atob(centerid),
                'inquiryno': enquiryno,
                'academicyear': academicyear
            })
        };
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].url + `listInquiries`, formData, httpOptions);
    }
    getStudentList(categoryid, courseid, batchid, feestype) {
        const formData = new FormData();
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let academicyearid = localStorage.getItem('academicyearid');
        let usermasterid = currentUser.user_id;
        let zoneid = currentUser.zone_id;
        let centerid = currentUser.center_id;
        let utoken = currentUser.utoken;
        let academicyear = atob(academicyearid);
        if (feestype == false) {
            feestype = 'Class';
        }
        else {
            feestype = 'Group';
            categoryid = '';
            courseid = '';
            batchid = '';
        }
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                "Access-Control-Allow-Methods": "GET, POST",
                "Access-Control-Allow-Origin": "*",
                'utoken': atob(utoken),
                'userid': atob(usermasterid),
                'zoneid': atob(zoneid),
                'centerid': atob(centerid),
                'feesselectiontype': feestype,
                'acadmicyear': academicyear,
                'categoryid': categoryid,
                'courseid': courseid,
                'batchid': batchid,
            })
        };
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].url + `getStudentList`, formData, httpOptions);
    }
    getStudentListAttendence(categoryid, courseid, batchid, groupid, feestype, attendancedate) {
        const formData = new FormData();
        console.log(groupid);
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let academicyearid = localStorage.getItem('academicyearid');
        let usermasterid = currentUser.user_id;
        let zoneid = currentUser.zone_id;
        let centerid = currentUser.center_id;
        let utoken = currentUser.utoken;
        let academicyear = atob(academicyearid);
        if (feestype == false) {
            feestype = 'Class';
            groupid = '';
        }
        else {
            feestype = 'Group';
            categoryid = '';
            courseid = '';
            batchid = '';
        }
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                "Access-Control-Allow-Methods": "GET, POST",
                "Access-Control-Allow-Origin": "*",
                'utoken': atob(utoken),
                'userid': atob(usermasterid),
                'zoneid': atob(zoneid),
                'centerid': atob(centerid),
                'feesselectiontype': feestype,
                'acadmicyear': academicyear,
                'categoryid': categoryid,
                'courseid': courseid,
                'batchid': batchid,
                'groupid': groupid,
                'formarkattendance': 'Yes',
                'attendancedate': attendancedate
            })
        };
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].url + `getStudentList`, formData, httpOptions);
    }
    getStudentId(enrollmentno) {
        const formData = new FormData();
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid = currentUser.user_id;
        let zoneid = currentUser.zone_id;
        let centerid = currentUser.center_id;
        let utoken = currentUser.utoken;
        let academicyearid = localStorage.getItem('academicyearid');
        let academicyear = atob(academicyearid);
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                "Access-Control-Allow-Methods": "GET, POST",
                "Access-Control-Allow-Origin": "*",
                'utoken': atob(utoken),
                'userid': atob(usermasterid),
                'zoneid': atob(zoneid),
                'centerid': atob(centerid),
                'enrollmentno': enrollmentno,
                'academicyear': academicyear
            })
        };
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].url + `listAdmissions`, formData, httpOptions);
    }
    getStudentPaymentDetails(studentid, feestype, categoryid, courseid) {
        const formData = new FormData();
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid = currentUser.user_id;
        let zoneid = currentUser.zone_id;
        let centerid = currentUser.center_id;
        let utoken = currentUser.utoken;
        let academicyearid = localStorage.getItem('academicyearid');
        let academicyear = atob(academicyearid);
        if (feestype == false) {
            feestype = 'Class';
        }
        else {
            feestype = 'Group';
        }
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                "Access-Control-Allow-Methods": "GET, POST",
                "Access-Control-Allow-Origin": "*",
                'utoken': atob(utoken),
                'userid': atob(usermasterid),
                'studentid': studentid,
                'zoneid': atob(zoneid),
                'centerid': atob(centerid),
                'categoryid': categoryid,
                'courseid': courseid,
                'feesselectiontype': feestype,
                'acadmicyear': academicyear
            })
        };
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].url + `getStudentPaymentDetails`, formData, httpOptions);
    }
    payAdmissionRemainingFees(formValues, student_id, admissionfessid, installmentNo, collected_amount, remainig_amount, installment_amount) {
        const formData = new FormData();
        if (formValues.feepaymenttype == false) {
        }
        else {
            for (let i = 0; i < Number(installmentNo); i++) {
                if (collected_amount[i] != undefined) {
                    formData.append('admission_fees_instalment_id[' + i + ']', installment_amount[i]);
                    formData.append('installment_collected_amount[' + i + ']', collected_amount[i]);
                }
            }
        }
        formData.append('fees_approved_accepted_by_name', formValues.fees_approved_accepted_by_name);
        formData.append('fees_remark', formValues.fees_remark);
        formData.append('payment_mode', formValues.payment_mode);
        if (formValues.payment_mode == 'Cheque') {
            formData.append('cheque_no', formValues.cheque_no);
        }
        if (formValues.payment_mode == 'Netbanking') {
            formData.append('transaction_id', formValues.transaction_id);
        }
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid = currentUser.user_id;
        let utoken = currentUser.utoken;
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'utoken': atob(utoken),
                'userid': atob(usermasterid),
                'studentid': student_id,
                'admissionfeesid': admissionfessid,
            })
        };
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].url + `payAdmissionRemainingFees`, formData, httpOptions);
    }
    getILeads() {
        const formData = new FormData();
        let academicyearid = localStorage.getItem('academicyearid');
        let academicyear = atob(academicyearid);
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                "Access-Control-Allow-Methods": "GET, POST",
                "Access-Control-Allow-Origin": "*",
                'academicyear': academicyear
            })
        };
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].url + `getILeads`, formData, httpOptions);
    }
    getGroupsDD() {
        const formData = new FormData();
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid = currentUser.user_id;
        let utoken = currentUser.utoken;
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                "Access-Control-Allow-Methods": "GET, POST",
                "Access-Control-Allow-Origin": "*",
                'utoken': atob(utoken),
                'userid': atob(usermasterid),
            })
        };
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].url + `getGroupsDD`, formData, httpOptions);
    }
    markattendence(formValues, student_array) {
        const formData = new FormData();
        if (formValues.feesselectiontype == false) {
            formValues.feesselectiontype = 'Class';
            formValues.groupid = '';
        }
        else {
            formValues.feesselectiontype = 'Group';
            formValues.categoryid = '';
            formValues.courseid = '';
            formValues.batchid = '';
        }
        for (let i = 0; i < student_array.length; i++) {
            console.log(student_array);
            formData.append('student_id[' + i + ']', student_array[i]);
        }
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid = currentUser.user_id;
        let zoneid = currentUser.zone_id;
        let centerid = currentUser.center_id;
        let utoken = currentUser.utoken;
        let academicyearid = localStorage.getItem('academicyearid');
        let academicyear = atob(academicyearid);
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'utoken': atob(utoken),
                'userid': atob(usermasterid),
                'zoneid': atob(zoneid),
                'centerid': atob(centerid),
                'acadmicyear': academicyear,
                'feesselectiontype': formValues.feesselectiontype,
                'categoryid': formValues.categoryid,
                'courseid': formValues.courseid,
                'batchid': formValues.batchid,
                'groupid': formValues.groupid,
                'attendancedate': formValues.attendancedate
            })
        };
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].url + `markAttendance`, formData, httpOptions);
    }
};
CommonService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
], CommonService);



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const environment = {
    production: true,
    url: 'https://attoinfotech.in/demo/aptechmontanacrm_git/montanaapis/teachersapi/'
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm2015/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"]).catch(err => console.error(err));


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! d:\crm\crm2\arulg3-aptechmontanacrm-angular-fc7e237fa744\src\main.ts */"./src/main.ts");


/***/ }),

/***/ 1:
/*!**********************!*\
  !*** zlib (ignored) ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 2:
/*!********************!*\
  !*** fs (ignored) ***!
  \********************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 3:
/*!**********************!*\
  !*** http (ignored) ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 4:
/*!***********************!*\
  !*** https (ignored) ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map