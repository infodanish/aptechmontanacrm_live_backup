<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Batchmaster extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

		ini_set( 'memory_limit', '25M' );
		ini_set('upload_max_filesize', '25M');  
		ini_set('post_max_size', '25M');  
		ini_set('max_input_time', 3600);  
		ini_set('max_execution_time', 3600);

		$this->load->model('batchmastermodel', '', TRUE);
	}

	function index()
	{
		if (!empty($_SESSION["webadmin"])) {
			$this->load->view('template/header.php');
			$this->load->view('batchmaster/index');
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
	function addEdit($id = NULL)
	{
		if (!empty($_SESSION["webadmin"])) {
			$record_id = "";

			if (!empty($_GET['text']) && isset($_GET['text'])) {
				$varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
				parse_str($varr, $url_prams);
				$record_id = $url_prams['id'];
			}
			$result = [];
			$result['categories'] = $this->batchmastermodel->getDropdown("tbl_categories","category_id,categoy_name");
			$result['zones'] = $this->batchmastermodel->getDropdown("tbl_zones","zone_id,zone_name");
			$result['groups'] = $this->batchmastermodel->getDropdown("tbl_group_master","group_master_id,group_master_name");
			$result['details'] = $this->batchmastermodel->getFormdata($record_id);
			
			$this->load->view('template/header.php');
			$this->load->view('batchmaster/addEdit', $result);
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
	public function getCourses(){
		$result = $this->batchmastermodel->getOptions("tbl_courses",$_REQUEST['category_id'],"category_id");
		
		$option = '';
		$course_id = '';
		
		if(isset($_REQUEST['course_id']) && !empty($_REQUEST['course_id'])){
			$course_id = $_REQUEST['course_id'];
		}
		
		
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$sel = ($result[$i]->course_id == $course_id) ? 'selected="selected"' : '';
				$option .= '<option value="'.$result[$i]->course_id.'" '.$sel.' >'.$result[$i]->course_name.'</option>';
			}
		}
		
		
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}
	
	public function getCenters(){
		$result = $this->batchmastermodel->getOptions("tbl_centers",$_REQUEST['zone_id'],"zone_id");
		$option = '';
		$center_id = '';
		
		if(isset($_REQUEST['center_id']) && !empty($_REQUEST['center_id'])){
			$center_id = $_REQUEST['center_id'];
		}
		
		
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$sel = ($result[$i]->center_id == $center_id) ? 'selected="selected"' : '';
				$option .= '<option value="'.$result[$i]->center_id.'" '.$sel.' >'.$result[$i]->center_name.'</option>';
			}
		}
		
		
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}	


	function submitForm()
	{ 
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
		
			if($_POST['batch_selection_type'] == 'Class'){
				$condition = "category_id='".$_POST['category_id']."' && course_id='".$_POST['course_id']."' &&  zone_id='".$_POST['zone_id']."' &&  center_id='".$_POST['center_id']."' &&  batch_name='".$_POST['batch_name']."' ";
			}
			if($_POST['batch_selection_type'] == 'Group'){
				$condition = "group_id='".$_POST['group_id']."' &&  zone_id='".$_POST['zone_id']."' &&  center_id='".$_POST['center_id']."' &&  center_id='".$_POST['center_id']."' &&  batch_name='".$_POST['batch_name']."'  ";
			}

			if(isset($_POST['batch_id']) && $_POST['batch_id'] > 0)
			{
				$condition .= " &&  batch_id != ".$_POST['batch_id'];
			}
			
			$check_name = $this->batchmastermodel->getdata("tbl_batch_master",$condition);
			if(!empty($check_name)){
				echo json_encode(array("success"=>"0",'msg'=>'Record Already Present!'));
				exit;
			}
			if(strtotime($_POST['start_date']) > strtotime($_POST['end_date'])){
				echo json_encode(array("success"=>"0",'msg'=>'Start date not greater than end date!'));
				exit;
			}
			
			if (!empty($_POST['batch_id'])) {
				$data_array = array();			
				$batch_id = $_POST['batch_id'];
		 		
		 		$data_array['batch_selection_type'] = (!empty($_POST['batch_selection_type'])) ? $_POST['batch_selection_type'] : '';
				$data_array['category_id'] = (!empty($_POST['category_id'])) ? $_POST['category_id'] : 0;
				$data_array['course_id'] = (!empty($_POST['course_id'])) ? $_POST['course_id'] : 0;
				$data_array['zone_id'] = (!empty($_POST['zone_id'])) ? $_POST['zone_id'] : 0;
				$data_array['center_id'] = (!empty($_POST['center_id'])) ? $_POST['center_id'] : 0;
				$data_array['group_id'] = (!empty($_POST['group_id'])) ? $_POST['group_id'] : 0;
				$data_array['batch_name'] = (!empty($_POST['batch_name'])) ? $_POST['batch_name'] : '';
				$data_array['start_date'] = (!empty($_POST['start_date'])) ? date("Y-m-d", strtotime($_POST['start_date'])) : '';
				$data_array['end_date'] = (!empty($_POST['end_date'])) ? date("Y-m-d", strtotime($_POST['end_date'])) : '';
				
				$data_array['updated_on'] = date("Y-m-d H:i:s");
				$data_array['updated_by'] = $_SESSION["webadmin"][0]->user_id;
				
				$result = $this->batchmastermodel->updateRecord('tbl_batch_master', $data_array,'batch_id',$batch_id);
				
				
			}else {
				
				$data_array = array();
				$data_array['batch_selection_type'] = (!empty($_POST['batch_selection_type'])) ? $_POST['batch_selection_type'] : '';
				$data_array['category_id'] = (!empty($_POST['category_id'])) ? $_POST['category_id'] : 0;
				$data_array['course_id'] = (!empty($_POST['course_id'])) ? $_POST['course_id'] : 0;
				$data_array['zone_id'] = (!empty($_POST['zone_id'])) ? $_POST['zone_id'] : 0;
				$data_array['center_id'] = (!empty($_POST['center_id'])) ? $_POST['center_id'] : 0;
				$data_array['group_id'] = (!empty($_POST['group_id'])) ? $_POST['group_id'] : 0;
				$data_array['batch_name'] = (!empty($_POST['batch_name'])) ? $_POST['batch_name'] : '';
				$data_array['start_date'] = (!empty($_POST['start_date'])) ? date("Y-m-d", strtotime($_POST['start_date'])) : '';
				$data_array['end_date'] = (!empty($_POST['end_date'])) ? date("Y-m-d", strtotime($_POST['end_date'])) : '';
				
				$data_array['created_on'] = date("Y-m-d H:i:s");
				$data_array['created_by'] = $_SESSION["webadmin"][0]->user_id;
				$data_array['updated_on'] = date("Y-m-d H:i:s");
				$data_array['updated_by'] = $_SESSION["webadmin"][0]->user_id;
				
				$result = $this->batchmastermodel->insertData('tbl_batch_master', $data_array, '1');
				
			}
		
		
			if (!empty($result)) {
				echo json_encode(array(
					'success' => '1',
					'msg' => 'Record Added/Updated Successfully.'
				));
				exit;
			}
			else{
				echo json_encode(array(
					'success' => '0',
					'msg' => 'Problem in data update.'
				));
				exit;
			}
			
		}
		else {
			return false;
		}
	
	}
	
	/*new code end*/
	function fetch($id=null)
	{
		$get_result = $this->batchmastermodel->getRecords($_GET);

		$result = array();
		$result["sEcho"] = $_GET['sEcho'];
		
		$result["iTotalRecords"] = $get_result['totalRecords']; //	iTotalRecords get no of total recors
		$result["iTotalDisplayRecords"] = $get_result['totalRecords']; //  iTotalDisplayRecords for display the no of records in data table.
		$items = array();
		if(!empty($get_result['query_result'])){
			for ($i = 0; $i < sizeof($get_result['query_result']); $i++) {
				$temp = array();
				array_push($temp, $get_result['query_result'][$i]->batch_selection_type);
				array_push($temp, $get_result['query_result'][$i]->zone_name);
				array_push($temp, $get_result['query_result'][$i]->center_name);
				array_push($temp, $get_result['query_result'][$i]->categoy_name);
				array_push($temp, $get_result['query_result'][$i]->course_name);
				array_push($temp, $get_result['query_result'][$i]->group_master_name);
				array_push($temp, $get_result['query_result'][$i]->batch_name);
				
				$actionCol21="";
				if($this->privilegeduser->hasPrivilege("BatchAddEdit")){
					$actionCol21 .= '<a href="javascript:void(0);" onclick="deleteData1(\'' . $get_result['query_result'][$i]->batch_id. '\',\'' . $get_result['query_result'][$i]->status. '\');" title="">'.$get_result['query_result'][$i]->status.'</a>';
				}	
				
				$actionCol1 = "";
				if($this->privilegeduser->hasPrivilege("BatchAddEdit")){
					$actionCol1.= '<a href="batchmaster/addEdit?text=' . rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->batch_id) , '+/', '-_') , '=') . '" title="Edit"><i class="fa fa-edit"></i></a> ';
				}	
				
				array_push($temp, $actionCol21);
				array_push($temp, $actionCol1);
				
				
				array_push($items, $temp);
			}
		}	

		$result["aaData"] = $items;
		echo json_encode($result);
		exit;
	}
	
	function delrecord12()
	{
		$id=$_POST['id'];
		$status=$_POST['status'];
		
		$appdResult = $this->batchmastermodel->delrecord12("tbl_batch_master","batch_id",$id,$status);
		if($appdResult)
		{
			echo "1";
		}
		else
		{
			echo "2";	
		}	
	}
	
}

?>
