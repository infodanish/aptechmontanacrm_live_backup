<?php 
if (!empty($_GET['id']) && isset($_GET['id'])) {
	$varr = base64_decode(strtr($_GET['id'], '-_', '+/'));
	parse_str($varr, $url_prams);
	$record_id = $url_prams['id'];
}
?>
<style>
	.appended .appenddia{
		border-bottom: 1px solid black;
	}
</style>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/adapters/jquery.js"></script>
<div id="content" class="content-wrapper">
	<div class="page-title">
      <div>
        <h1>User - Groups</h1>            
      </div>
      <div>
        <ul class="breadcrumb">
          <li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="<?php echo base_url();?>centerusers">Center User</a></li>
        </ul>
      </div>
    </div>
    <div class="card">       
     <div class="card-body">             
        <div class="box-content">
            <div class="col-sm-8 col-md-12">
				<form class="form-horizontal" id="form-validate" method="post" enctype="multipart/form-data">
					<input type="hidden" id="user_id" name="user_id" value="<?php echo $record_id;?>" />

					<div class="control-group form-group">
						<label class="control-label"><span>Group*</span></label> 
						<input type="checkbox" id="checkbox" >Select All
						<div class="controls">
							<select id="group_id" name="group_id[]" class="form-control select2 group_id" multiple >
								<option value="" disabled="disabled">Select Group</option>
								<?php 
								if(isset($groups) && !empty($groups)){
									foreach($groups as $cdrow){
								?>
								<option value="<?php echo $cdrow->group_master_id;?>"><?php echo $cdrow->group_master_name;?></option>
								<?php }}?>
							</select>
						</div>
					</div>
					
					<div class="form-actions form-group">
						<button type="submit" class="btn btn-primary">Submit</button>
						<a href="javascript:history.go(-1)" class="btn btn-primary">Cancel</a>
					</div>
				</form>
            </div>
        <div class="clearfix"></div>
        </div>
     </div>
    </div>        
</div><!-- end: Content -->								
<script>
$( document ).ready(function() {
	$("#checkbox").click(function(){
		if($("#checkbox").is(':checked') ){
			$("#group_id > option").prop("selected","selected");
			$("#group_id").trigger("change");
		}else{
			$("#group_id > option").removeAttr("selected");
			$("#group_id").trigger("change");
		}
	});
	
});

$("#form-validate").validate({
	submitHandler: function(form) 
	{
		var act = "<?php echo base_url();?>usergroups/submitForm";
		$("#form-validate").ajaxSubmit({
			url: act, 
			type: 'post',
			cache: false,
			clearForm: false,
			success: function (response) {
				var res = eval('('+response+')');
				if(res['success'] == "1")
				{
					displayMsg("success",res['msg']);
					setTimeout(function(){
						window.location = "<?php echo base_url();?>usergroups/index?text=<?php echo $_GET['id'];?>";
					},2000);

				}
				else
				{	
					displayMsg("error",res['msg']);
					return false;
				}
			}
		});
	}
});


document.title = "AddEdit - Center User Groups";

 
</script>					
