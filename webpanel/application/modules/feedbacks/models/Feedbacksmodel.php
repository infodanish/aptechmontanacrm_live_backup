<?PHP
class Feedbacksmodel extends CI_Model
{
	function insertData($tbl_name,$data_array,$sendid = NULL)
	 {
	 	
	 	$this->db->insert($tbl_name,$data_array);
	 	$result_id = $this->db->insert_id();
	 	
	 	/*echo $result_id;
	 	exit;*/
	 	
	 	if($sendid == 1)
	 	{
	 		//return id
	 		return $result_id;
	 	}
	 }
	 
	function updateRecord($tableName, $data, $column, $value)
	{
		$this->db->where("$column", $value);
		$this->db->update($tableName, $data);
		//print_r($this->db->last_query());
		//exit;
		if ($this->db->affected_rows() > 0) {
			return true;
		}
		else {
			return true;
		}
	} 
	 
	function getRecords($get){
		//echo "here...<br/>";
		//print_r($get);
		
		$table = "tbl_feedbacks";
		$table_id = 'i.feedback_id';
		$default_sort_column = 'i.feedback_id';
		$default_sort_order = 'desc';
		$condition = "";
		$condition .= "  1=1 ";
		
		$colArray = array('z.zone_name','c.center_name','i.feedback_name', 'i.feedback_email', 'i.feedback_contact', 'i.feedback_msg','i.created_on');
		
		$page = $get['iDisplayStart'];											// iDisplayStart starting offset of limit funciton
		$rows = $get['iDisplayLength'];											// iDisplayLength no of records from the offset
		
		// sort order by column
		$sort = isset($get['iSortCol_0']) ? strval($colArray[$get['iSortCol_0']]) : $default_sort_column;  
		$order = isset($get['sSortDir_0']) ? strval($get['sSortDir_0']) : $default_sort_order;

		for($i=0;$i<7;$i++)
		{
			if(isset($get['sSearch_'.$i]) && $get['sSearch_'.$i]!='')
			{
				$condition .= " && $colArray[$i] like '%".$_GET['sSearch_'.$i]."%'";
			}
			
		}
		
		//echo "Condition: ".$condition;
		//exit;
		$this -> db -> select('i.*, z.zone_name, c.center_name');
		$this -> db -> from('tbl_feedbacks as i');
		$this -> db -> join('tbl_zones as z', 'i.zone_id  = z.zone_id', 'left');
		$this -> db -> join('tbl_centers as c', 'i.center_id  = c.center_id', 'left');
		$this->db->where("($condition)");
		$this->db->order_by($sort, $order);
		$this->db->limit($rows,$page);
		
		$query = $this -> db -> get();
		
		//print_r($this->db->last_query());
		//exit;
		
		$this -> db -> select('i.*, z.zone_name, c.center_name');
		$this -> db -> from('tbl_feedbacks as i');
		$this -> db -> join('tbl_zones as z', 'i.zone_id  = z.zone_id', 'left');
		$this -> db -> join('tbl_centers as c', 'i.center_id  = c.center_id', 'left');
		$this->db->where("($condition)");
		$this->db->order_by($sort, $order);
		
		$query1 = $this -> db -> get();
		//echo "total: ".$query1 -> num_rows();
		//exit;
		
		if($query -> num_rows() >= 1)
		{
			$totcount = $query1 -> num_rows();
			return array("query_result" => $query->result(),"totalRecords"=>$totcount);
		}
		else
		{
			//return false;
			return array("totalRecords"=>0);
		}
		
		
		//exit;
	}
	
	function getFormdata($ID){
		
	   $this -> db -> select('i.*');
	   $this -> db -> from('tbl_feedbacks as i');
	   $this -> db -> where('i.feedback_id', $ID);
	
	   $query = $this -> db -> get();
	   
	   //print_r($this->db->last_query());
	   //exit;
	   
	   if($query -> num_rows() >= 1)
	   {
	     return $query->result();
	   }
	   else
	   {
	     return false;
	   }
		
	}
		
	function checkRecord($tbl_name,$POST,$condition){
		
		//print_r($POST);
		//exit;
		$this -> db -> select('*');
		$this -> db -> from($tbl_name);
		$this->db->where("($condition)");
	
		$query = $this -> db -> get();
	   
		//print_r($this->db->last_query());
		//exit;
	   
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
		
	}
	
	 function getExportRecords($get){
		$table = "tbl_feedbacks";
		$table_id = 'i.feedback_id';
		$default_sort_column = 'i.feedback_id';
		$default_sort_order = 'desc';
		$condition = "";
		$condition .= "  1=1 ";
		
		//$colArray = array('feedback_name','feedback_email','feedback_contact','created_on');
		$colArray = array('z.zone_name','c.center_name','i.feedback_name', 'i.feedback_email', 'i.feedback_contact', 'i.feedback_msg','i.created_on');
		$searchArray = array('z.zone_name','c.center_name','i.feedback_name', 'i.feedback_email', 'i.feedback_contact', 'i.feedback_msg','i.created_on');
		
		// sort order by column
		$sort = $default_sort_column;  
		$order = $default_sort_order;
		
		for($i=0;$i<4;$i++){
			if(isset($get['sSearch_'.$i]) && $get['sSearch_'.$i]!=''){
				$condition .= " AND $colArray[$i] like '%".$get['sSearch_'.$i]."%'";
			}
		}
		
		$this -> db -> select('i.*, z.zone_name, c.center_name');
		$this -> db -> from('tbl_feedbacks as i');
		$this -> db -> join('tbl_zones as z', 'i.zone_id  = z.zone_id', 'left');
		$this -> db -> join('tbl_centers as c', 'i.center_id  = c.center_id', 'left');
		
		$this->db->where("($condition)");
		$query = $this -> db -> get();
		if($query -> num_rows() >= 1){
			$totcount = $query -> num_rows();
			return array("query_result" => $query->result(), "totalRecords" => $totcount);
		}else{
			return array("totalRecords" => 0);
		}
	}
}
?>