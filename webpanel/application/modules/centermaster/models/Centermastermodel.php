<?PHP
class Centermastermodel extends CI_Model
{
	function insertData($tbl_name,$data_array,$sendid = NULL)
	{
		$this->db->insert($tbl_name,$data_array);
	 	//print_r($this->db->last_query());
	    //exit;
		$result_id = $this->db->insert_id();
		
		if($sendid == 1)
	 	{
	 		return $result_id;
	 	}
	}
	 
	function insertSize($tbl_name, $data_array, $sendid = NULL)
	{
		$this->db->insert($tbl_name, $data_array);
		$result_id = $this->db->insert_id();
		if ($sendid == 1) {
			return $result_id;
		}
	}
	
	
	function getRecords($get=null){
		$table = "tbl_centers";
		$table_id = 'i.center_id';
		$default_sort_column = 'i.center_id';
		$default_sort_order = 'desc';
		
		$condition = "";
		$condition .= "  1=1 ";
		
		$colArray = array('z.zone_name','i.center_name','i.center_address','i.center_spoc','i.center_contact_no', 'i.center_email_id','i.can_see_timetable_before','i.can_see_timetable_after','i.status');
		$searchArray = array('z.zone_name','i.center_name','i.center_address','i.center_spoc','i.center_contact_no', 'i.center_email_id','i.can_see_timetable_before','i.can_see_timetable_after','i.status');
		
		$page = $get['iDisplayStart'];											// iDisplayStart starting offset of limit funciton
		$rows = $get['iDisplayLength'];											// iDisplayLength no of records from the offset
		
		// sort order by column
		$sort = isset($get['iSortCol_0']) ? strval($colArray[$get['iSortCol_0']]) : $default_sort_column;  
		$order = isset($get['sSortDir_0']) ? strval($get['sSortDir_0']) : $default_sort_order;

		for($i=0;$i<10;$i++)
		{
			
			if(isset($get['sSearch_'.$i]) && $get['sSearch_'.$i]!='')
			{
				$condition .= " && $searchArray[$i] like '%".$_GET['sSearch_'.$i]."%'";
			}
			
		}
		
		//echo "condition ".$condition;exit;
		
		$this -> db -> select('i.*,z.zone_name');
		$this -> db -> from('tbl_centers as i');
		$this -> db -> join('tbl_zones as z', 'i.zone_id  = z.zone_id', 'left');

		$this->db->where("($condition)");
		$this->db->order_by($sort, $order);
		$this->db->limit($rows,$page);		
		$query = $this -> db -> get();
		
		$this -> db -> select('i.*,z.zone_name');
		$this -> db -> from('tbl_centers as i');
		$this -> db -> join('tbl_zones as z', 'i.zone_id  = z.zone_id', 'left');

		$this->db->where("($condition)");
		$this->db->order_by($sort, $order);		
		$query1 = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			$totcount = $query1 -> num_rows();
			return array("query_result" => $query->result(),"totalRecords"=>$totcount);
		}
		else
		{
			//return false;
			return array("totalRecords"=>0);
		}
		
		
		//exit;
	}
	
	
	function getDropdown($tbl_name,$tble_flieds){
	   
		$this -> db -> select($tble_flieds);
		$this -> db -> from($tbl_name);
		$status="Active";
		$this -> db -> where("status",$status);
	
		$query = $this -> db -> get();
	
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
			
	}
	
	function getDropdownSelval($tbl_name,$tbl_id,$tble_flieds,$rec_id=NULL){
		
		$this -> db -> select($tble_flieds);
		$this -> db -> from($tbl_name);
		$this -> db -> where($tbl_id, $rec_id);
	
		$query = $this -> db -> get();
		
		//print_r($this->db->last_query());
		//exit;
	   
	   
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}
	
	function getFormdata($ID){
		
		//$condition = "1=1 && i.product_id= ".$ID." ";
		$this -> db -> select('i.*');
		$this -> db -> from('tbl_centers as i');
		$this->db->where('i.center_id', $ID);
		//$this->db->where("($condition)");
	
		$query = $this -> db -> get();
	   
		//print_r($this->db->last_query());
		//exit;
	   
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
		
	}
	
	function getOptions($tbl_name,$tbl_id,$comp_id){
		//echo "Cat ID: ".$category_id;
		//exit;
		$this -> db -> select('*');
		$this -> db -> from($tbl_name);
		$this -> db -> where($comp_id,$tbl_id);
	
		$query = $this -> db -> get();
	
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}
	
	function getdata($table,$condition = '1=1'){
        $sql = $this->db->query("Select * from $table where $condition");
        if($sql->num_rows()>0){
            return $sql->result_array();
        }else{
            return false;
        }
    }
	
	function getRecords1($tbl_name, $tbl_id, $comp_field, $comp_record){
		$this -> db -> select($tbl_id);
		$this -> db -> from($tbl_name);
		$this -> db -> where($comp_field, $comp_record);
		$query = $this -> db -> get();

		if($query -> num_rows() >= 1){
			return $query->result();
		}
		else{
			return false;
		}	
	}
	
	function getRecords2($tbl_name, $tbl_id, $condition= '1=1'){
		$this -> db -> select($tbl_id);
		$this -> db -> from($tbl_name);
		$this -> db -> where("($condition)");
		$query = $this -> db -> get();

		if($query -> num_rows() >= 1){
			return $query->result();
		}
		else{
			return false;
		}	
	}
	
	
	function updateRecord($tableName, $data, $column, $value)
	{
		$this->db->where("$column", $value);
		$this->db->update($tableName, $data);
		//print_r($this->db->last_query());
		//exit;
		if ($this->db->affected_rows() > 0) {
			return true;
		}
		else {
			return true;
		}
	} 
	 
	function delrecord($tbl_name,$tbl_id,$record_id,$status)
	{
		$data = array('status' => $status);
		 
		$this->db->where($tbl_id, $record_id);
		$this->db->update($tbl_name,$data); 
		 
		if($this->db->affected_rows() >= 1)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function delrecord12($tbl_name,$tbl_id,$record_id,$status)
	{
		$data = array('status' => $status);
		 
		$this->db->where($tbl_id, $record_id);
		$this->db->update($tbl_name,$data); 
		 
		if($this->db->affected_rows() >= 1)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function delrecord1($tbl_name,$tbl_id,$record_id)
	{
		$this->db->where($tbl_id, $record_id);
	    $this->db->delete($tbl_name); 
		if($this->db->affected_rows() >= 1)
		{
			return true;
	    }
	    else
	    {
			return false;
	    }
	}
	 
	
	
}
?>
