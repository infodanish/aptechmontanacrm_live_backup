<style>
	.appended .appenddia{
		border-bottom: 1px solid black;
	}
</style>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/adapters/jquery.js"></script>
<div id="content" class="content-wrapper">
				<div class="page-title">
                  <div>
                    <h1>Academic Year Master</h1>            
                  </div>
                  <div>
                    <ul class="breadcrumb">
                      <li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
                      <li><a href="<?php echo base_url();?>academicyear">Academic Year Master</a></li>
                    </ul>
                  </div>
                </div>
                <div class="card">       
                 <div class="card-body">             
                    <div class="box-content">
                        <div class="col-sm-8 col-md-12">
							<form class="form-horizontal" id="form-validate" method="post" enctype="multipart/form-data">
								<input type="hidden" id="academic_year_master_id" name="academic_year_master_id" value="<?php if(!empty($details[0]->academic_year_master_id)){echo $details[0]->academic_year_master_id;}?>" />
							
								<div class="control-group form-group">
									<label class="control-label"><span>Academic Year Name*</span></label>
									<div class="controls">
										<input type="text" class="form-control required" placeholder="Enter Academic Year Name" id="academic_year_master_name" name="academic_year_master_name" onchange="dateAgo('1986-04-11')" value="<?php if(!empty($details[0]->academic_year_master_name)){echo $details[0]->academic_year_master_name;}?>" >
									</div>
								</div>
								
								<div class="control-group form-group">
									<label class="control-label"><span>Start Date*</span></label>
									<div class="controls">
										<input type="text" class="form-control required datepicker" placeholder="Select Start Date" id="start_date" name="start_date" value="<?php if(!empty($details[0]->start_date)){echo date("d-m-Y", strtotime($details[0]->start_date));}?>" >
									</div>
								</div>
								
								<div class="control-group form-group">
									<label class="control-label"><span>End Date*</span></label>
									<div class="controls">
										<input type="text" class="form-control required datepicker" placeholder="Select End Date" id="end_date" name="end_date" value="<?php if(!empty($details[0]->end_date)){echo date("d-m-Y", strtotime($details[0]->end_date));}?>" >
									</div>
								</div>
								
								<div class="form-actions form-group">
									<button type="submit" class="btn btn-primary">Submit</button>
									<a href="<?php echo base_url();?>academicyear" class="btn btn-primary">Cancel</a>
								</div>
							</form>
                        </div>
                    <div class="clearfix"></div>
                    </div>
                 </div>
                </div>        
			</div><!-- end: Content -->								
<script>

function getAge(dateString) {
    var today = new Date();
    var birthDate = new Date(dateString);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
    }
    //return m;
    //return {years: age, months: m};
    return [age, m];
}
 
$( document ).ready(function() {
	var res = getAge('1986/05/11');

	//alert(res[0]);
	//alert(Math.abs(res[1]));

$(".datepicker").datetimepicker({
		format: 'DD-MM-YYYY'
	});

	var config = {enterMode : CKEDITOR.ENTER_BR, height:200, filebrowserBrowseUrl: '../js/ckeditor/filemanager/index.html', scrollbars:'yes',
			toolbar_Full:
			[
						//['Source', 'Templates'],
						['Cut','Copy','Paste','PasteText','-','Print', 'SpellChecker', 'Scayt'],
						['Subscript','Superscript'],
						['NumberedList','BulletedList'],
						['BidiLtr', 'BidiRtl' ],
						//['Maximize', 'ShowBlocks'],
						['Undo','Redo'],['Bold','Italic','Underline','Strike'],			
						['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],			
						//['SelectAll','RemoveFormat'],'/',
						['Styles','Format','Font','FontSize'],
						['TextColor','BGColor'],								
						//['Image','Flash','Table','HorizontalRule','Smiley'],
					],
					 width: "620px"
			};
	$('.editor').ckeditor(config);
	
	$.validator.addMethod("needsSelection", function(value, element) {
        return $(element).multiselect("getChecked").length > 0;
    });
	
});

var vRules = {
	academic_year_master_name:{required:true},
	start_date:{required:true},
	end_date:{required:true}
};
var vMessages = {
	academic_year_master_name:{required:"Please enter name."},
	start_date:{required:"Please select start date."},
	end_date:{required:"Please select end date."}
};

$("#form-validate").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{
		var act = "<?php echo base_url();?>academicyear/submitForm";
		$("#form-validate").ajaxSubmit({
			url: act, 
			type: 'post',
			cache: false,
			clearForm: false,
			success: function (response) {
				var res = eval('('+response+')');
				if(res['success'] == "1")
				{
					displayMsg("success",res['msg']);
					setTimeout(function(){
						window.location = "<?php echo base_url();?>academicyear";
					},2000);

				}
				else
				{	
					//$("#error_msg").show();
					displayMsg("error",res['msg']);
					return false;
				}
			}
		});
	}
});

document.title = "AddEdit - Academic Year";

 
</script>					
