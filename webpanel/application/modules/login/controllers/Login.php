<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Login extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		
		if(!empty($_SESSION["stars_cosmetics_webadmin"]))
		{
			redirect('home/index', 'refresh');
		}
		$this->load->model('loginmodel','',TRUE);
	}
 
	function index()
	{
		$this->load->view('template/login_header.php');
		$this->load->view('login');
		$this->load->view('template/login_footer.php');
	}
 
	/*function loginvalidate()
	{
		$result = $this->loginmodel->login($_POST['username'],md5($_POST['password']));
		//echo "<pre>";print_r($result);exit;
		if($result){
			$_SESSION["webadmin"] = $result;
			$this->privilegeduser->getByUsername($_SESSION["webadmin"][0]->user_id);		
				echo json_encode(array("success"=>"1","msg"=>'You are successfully logged In.'));
				exit;		
		}else{
			echo json_encode(array("success"=>"0","msg"=>'Username or Password incorrect.'));
			exit;
		}
	}*/
	
	function loginvalidate()
	{
		$result = $this->loginmodel->login($_POST['username'],md5($_POST['password']));
		//echo "<pre>";print_r($result);exit;
		if($result){
			$zone_id = array();
			if($result[0]->user_type == 1){
				$get_zones = $this->loginmodel->getdata("tbl_user_zones", "zone_id","user_id='".$result[0]->user_id."' ");
				//echo "<pre>";print_r($get_zones);
				//echo $get_zones[0]['zone_id'];
				//exit;
				if(!empty($get_zones)){
					for($i=0; $i < sizeof($get_zones); $i++){
						$zone_id[] = $get_zones[$i]['zone_id'];
					}
				}
			}else{
				//echo $result[0]->zone_id;
				$zone_id[] = $result[0]->zone_id;
			}
			
			$result[0]->user_zones = $zone_id;
			//echo "<pre>";print_r($result);exit;
			$_SESSION["webadmin"] = $result;
			
			$this->privilegeduser->getByUsername($_SESSION["webadmin"][0]->user_id);		
				echo json_encode(array("success"=>"1","msg"=>'You are successfully logged In.'));
				exit;		
		}else{
			echo json_encode(array("success"=>"0","msg"=>'Username or Password incorrect.'));
			exit;
		}
	}

	function forgotpassword()
	{
		if(!empty($_POST['email_id']))
		{
			$result = $this->loginmodel->forgotpass($_POST['email_id']);	
			if($result)
			{
				// $result_content = $this->loginmodel->getContentForgetPass(1);
				$result_content = $this->loginmodel->getEmailContent("ADMIN_FORGOT_PASSWORD");
				
				$url = base_url()."forgetchangepassword?text=".rtrim(strtr(base64_encode("eid=".$_POST['email_id']), '+/', '-_'), '=')."/".rtrim(strtr(base64_encode("uid=".$result[0]->user_id ), '+/', '-_'), '=')."/".rtrim(strtr(base64_encode("dt=".date("Y-m-d")), '+/', '-_'), '=');
				// echo $url; exit;
				
				$message = str_replace(array('{username}','{url}','{link}'), array($result[0]->user_name,$url,base_url()), $result_content['content']);
				
				$this->email->from(FROM_EMAIL); // change it to yours
				$this->email->to($_POST['email_id']);// change it to yours
				$this->email->subject($result_content['subject']);
				$this->email->message($message);		
				$checkemail = $this->email->send();
				
				if($checkemail)
				{
					echo json_encode(array("success"=>true, "msg"=>'Mail sent successfully, Please check your mail.'));
					exit;
				}
				else
				{
					echo json_encode(array("success"=>false, "msg"=>'Problem while sending mail..'));
					exit;
				}			
			}
			else
			{		
				echo json_encode(array("success"=>false, "msg"=>'Invalid Email ID.'));
				exit;
			}
		}
		else
		{		
			echo json_encode(array("success"=>false, "msg"=>'Invalid Email ID.'));
			exit;
		}	
	}
}

?>