<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Documentfoldermaster extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

		// $this->load->helper('erp_setting');
		ini_set( 'memory_limit', '25M' );
		ini_set('upload_max_filesize', '25M');  
		ini_set('post_max_size', '25M');  
		ini_set('max_input_time', 3600);  
		ini_set('max_execution_time', 3600);

		$this->load->model('documentfoldermastermodel', '', TRUE);
	}

	function index()
	{
		if (!empty($_SESSION["webadmin"])) {
			$this->load->view('template/header.php');
			$this->load->view('documentfoldermaster/index');
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
	function addEdit($id = NULL)
	{
		if (!empty($_SESSION["webadmin"])) {
			$record_id = "";

			// print_r($_GET);

			if (!empty($_GET['text']) && isset($_GET['text'])) {
				$varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
				parse_str($varr, $url_prams);
				$record_id = $url_prams['id'];
			}
			$result = "";
			$result['zones'] = $this->documentfoldermastermodel->getDropdown("tbl_zones","zone_id,zone_name");
			//$result['courses'] = $this->documentfoldermastermodel->getDropdown("tbl_courses","course_id,course_name");
			$result['categories'] = $this->documentfoldermastermodel->getDropdown("tbl_categories","category_id,categoy_name");
			$result['details'] = $this->documentfoldermastermodel->getFormdata($record_id);
			
			
			if(!empty($result['details'])){
				$result['selcourses'] = $this->documentfoldermastermodel->getDropdownSelval('tbl_center_courses', 'center_id', 'course_id',$record_id);
			}
			
			//echo "<pre>";print_r($result['selcourses']);exit;
			
			$this->load->view('template/header.php');
			$this->load->view('documentfoldermaster/addEdit', $result);
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
	public function getCourses(){
		$result = $this->documentfoldermastermodel->getOptions("tbl_courses",$_REQUEST['category_id'],"category_id");
		/*echo "<pre>";
		print_r($result);
		exit;*/
		
		$option = '';
		$selcourses1 = array();
		
		if(isset($_REQUEST['document_folder_id']) && !empty($_REQUEST['document_folder_id'])){
			$selcourses = $this->documentfoldermastermodel->getDropdownSelval('tbl_folder_courses', 'document_folder_id', 'course_id',$_REQUEST['document_folder_id']);
			if(!empty($selcourses)){
				for($i=0; $i < sizeof($selcourses); $i++){
					$selcourses1[] = $selcourses[$i]->course_id;
				}
			}
		}
		
		
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				//$sel = ($result[$i]->course_id == $course_id) ? 'selected="selected"' : '';
				$sel = (in_array($result[$i]->course_id, $selcourses1)) ? 'selected="selected"' : '';
				$option .= '<option value="'.$result[$i]->course_id.'" '.$sel.' >'.$result[$i]->course_name.'</option>';
			}
		}
		
		
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}
	
	public function getCenters(){
		$result = $this->documentfoldermastermodel->getOptions("tbl_centers",$_REQUEST['zone_id'],"zone_id");
		/*echo "<pre>";
		print_r($result);
		exit;*/
		
		$option = '';
		$selcenters1 = array();
		
		if(isset($_REQUEST['document_folder_id']) && !empty($_REQUEST['document_folder_id'])){
			$selcenters = $this->documentfoldermastermodel->getDropdownSelval('tbl_folder_centers', 'document_folder_id', 'center_id',$_REQUEST['document_folder_id']);
			if(!empty($selcenters)){
				for($i=0; $i < sizeof($selcenters); $i++){
					$selcenters1[] = $selcenters[$i]->center_id;
				}
			}
		}
		
		
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$sel = (in_array($result[$i]->center_id, $selcenters1)) ? 'selected="selected"' : '';
				$option .= '<option value="'.$result[$i]->center_id.'" '.$sel.' >'.$result[$i]->center_name.'</option>';
			}
		}
		
		
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}

	function submitForm()
	{ 
		//echo "<pre>";print_r($_POST);exit;
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			
			$condition = " folder_name= '".$_POST['folder_name']."' ";
			if(isset($_POST['document_folder_id']) && $_POST['document_folder_id'] > 0)
			{
				$condition .= " &&  document_folder_id != ".$_POST['document_folder_id'];
			}
			
			$check_name = $this->documentfoldermastermodel->getdata("tbl_document_folders",$condition);
			if(!empty($check_name)){
				echo json_encode(array("success"=>"0",'msg'=>'Record Already Present!'));
				exit;
			}
			
			$thumnail_value = "";
			if(isset($_FILES) && isset($_FILES["cover_image"]["name"]))
			{
				$config['upload_path'] = DOC_ROOT_FRONT."/images/document_folder_images/";
				$config['max_size']    = '1000000';
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
				$config['file_name']     = md5(uniqid("100_ID", true));
				//$config['file_name']     = $_FILES["cover_image"]["name"].;
				
				$this->load->library('upload', $config);
				if (!$this->upload->do_upload("cover_image"))
				{
					$image_error = array('error' => $this->upload->display_errors());
					echo json_encode(array("success"=>false, "msg"=>$image_error['error']));
					exit;
				}
				else
				{
					$image_data = array('upload_data' => $this->upload->data());
					$thumnail_value = $image_data['upload_data']['file_name'];
				}
				
				/* Unlink previous category image */
				if(!empty($_POST['document_folder_id']))
				{
					$image = $this->documentfoldermastermodel->getFormdata($_POST['document_folder_id']);
					if(is_array($image) && !empty($image[0]->cover_image) && file_exists(DOC_ROOT_FRONT."/images/document_folder_images/".$image[0]->cover_image))
					{
						@unlink(DOC_ROOT_FRONT."/images/document_folder_images/".$image[0]->cover_image);
					}
				}
				
			}
			else
			{
				$thumnail_value = $_POST['input_cover_image'];
			}
			
			//exit;
			if (!empty($_POST['document_folder_id'])) {
				$data_array = array();			
				$document_folder_id = $_POST['document_folder_id'];
		 		
				//$data_array['zone_id'] = (!empty($_POST['zone_id'])) ? $_POST['zone_id'] : '';
				//$data_array['category_id'] = (!empty($_POST['category_id'])) ? $_POST['category_id'] : '';
				$data_array['folder_name'] = (!empty($_POST['folder_name'])) ? $_POST['folder_name'] : '';				
				$data_array['cover_image'] = $thumnail_value;
				
				$data_array['updated_on'] = date("Y-m-d H:i:s");
				$data_array['updated_by'] = $_SESSION["webadmin"][0]->user_id;
				
				$result = $this->documentfoldermastermodel->updateRecord('tbl_document_folders', $data_array,'document_folder_id',$document_folder_id);
				
				/*
				if(!empty($_POST['course_id'])){
					$this->documentfoldermastermodel->delrecord1('tbl_folder_courses', 'document_folder_id',$document_folder_id);
					for($i=0; $i < sizeof($_POST['course_id']); $i++){
						$data = array();
						$data['document_folder_id'] = $document_folder_id;
						$data['course_id'] = $_POST['course_id'][$i];
						$this->documentfoldermastermodel->insertData('tbl_folder_courses', $data, '1');
					}
				}
				
				if(!empty($_POST['center_id'])){
					$this->documentfoldermastermodel->delrecord1('tbl_folder_centers', 'document_folder_id',$document_folder_id);
					for($i=0; $i < sizeof($_POST['center_id']); $i++){
						$data = array();
						$data['document_folder_id'] = $document_folder_id;
						$data['center_id'] = $_POST['center_id'][$i];
						$this->documentfoldermastermodel->insertData('tbl_folder_centers', $data, '1');
					}
				}
				*/
				
			}else {
				
				$data_array = array();
				
				//$data_array['zone_id'] = (!empty($_POST['zone_id'])) ? $_POST['zone_id'] : '';
				//$data_array['category_id'] = (!empty($_POST['category_id'])) ? $_POST['category_id'] : '';
				$data_array['folder_name'] = (!empty($_POST['folder_name'])) ? $_POST['folder_name'] : '';				
				$data_array['cover_image'] = $thumnail_value;
				
				$data_array['created_on'] = date("Y-m-d H:i:s");
				$data_array['created_by'] = $_SESSION["webadmin"][0]->user_id;
				$data_array['updated_on'] = date("Y-m-d H:i:s");
				$data_array['updated_by'] = $_SESSION["webadmin"][0]->user_id;
				//echo "<pre>";print_r($data_array);exit;
				
				$result = $this->documentfoldermastermodel->insertData('tbl_document_folders', $data_array, '1');
				
				
				/*if(!empty($_POST['course_id'])){
					for($i=0; $i < sizeof($_POST['course_id']); $i++){
						$data = array();
						$data['document_folder_id'] = $result;
						$data['course_id'] = $_POST['course_id'][$i];
						$this->documentfoldermastermodel->insertData('tbl_folder_courses', $data, '1');
					}
				}
				
				if(!empty($_POST['center_id'])){
					for($i=0; $i < sizeof($_POST['center_id']); $i++){
						$data = array();
						$data['document_folder_id'] = $result;
						$data['center_id'] = $_POST['center_id'][$i];
						$this->documentfoldermastermodel->insertData('tbl_folder_centers', $data, '1');
					}
				}*/
				
				
			}
		
		
			if (!empty($result)) {
				echo json_encode(array(
					'success' => '1',
					'msg' => 'Record Added/Updated Successfully.'
				));
				exit;
			}
			else{
				echo json_encode(array(
					'success' => '0',
					'msg' => 'Problem in data update.'
				));
				exit;
			}
			
		}
		else {
			return false;
		}
	
	}
	
	/*new code end*/
	function fetch($id=null)
	{
		//$_GET['album_id'] = $_GET['id'];
		//echo $id;exit;
		$get_result = $this->documentfoldermastermodel->getRecords($_GET);
		$result = array();
		$result["sEcho"] = $_GET['sEcho'];
		
		$result["iTotalRecords"] = $get_result['totalRecords']; //	iTotalRecords get no of total recors
		$result["iTotalDisplayRecords"] = $get_result['totalRecords']; //  iTotalDisplayRecords for display the no of records in data table.
		$items = array();
		if(!empty($get_result['query_result'])){
			for ($i = 0; $i < sizeof($get_result['query_result']); $i++) {
				$temp = array();
				array_push($temp, $get_result['query_result'][$i]->folder_name);
				//array_push($temp, $get_result['query_result'][$i]->categoy_name);
				//array_push($temp, $get_result['query_result'][$i]->folder_name);
				
				$viewCourses="";
				if($this->privilegeduser->hasPrivilege("DocumentFolderCategoryCoursesList")){
					$viewCourses.= '<a href="foldercourses/index?text=' . rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->document_folder_id) , '+/', '-_') , '=') . '" title="Courses Mapping">View Courses Mapping</a>';
				}	
				
				
				$viewCenters="";
				if($this->privilegeduser->hasPrivilege("DocumentFolderZoneCenterList")){
					$viewCenters.= '<a href="foldercenters/index?text=' . rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->document_folder_id) , '+/', '-_') , '=') . '" title="Center Mapping">View Center Mapping</a>';
				}	
				
				$viewDocuments="";
				if($this->privilegeduser->hasPrivilege("FolderDocumentList")){
					$viewDocuments.= '<a href="folderdocumentsmaster/index?text=' . rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->document_folder_id) , '+/', '-_') , '=') . '" title="Center Mapping">View Documents</a>';
				}	
				
				array_push($temp, $viewCourses);
				array_push($temp, $viewCenters);
				array_push($temp, $viewDocuments);
				
				$actionCol21="";
				if($this->privilegeduser->hasPrivilege("DocumentFolderAddEdit")){
					$actionCol21 .= '<a href="javascript:void(0);" onclick="deleteData1(\'' . $get_result['query_result'][$i]->document_folder_id. '\',\'' . $get_result['query_result'][$i]->status. '\');" title="">'.$get_result['query_result'][$i]->status.'</a>';
				}	
				
				$actionCol1 = "";
				if($this->privilegeduser->hasPrivilege("DocumentFolderAddEdit")){
					$actionCol1.= '<a href="documentfoldermaster/addEdit?text=' . rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->document_folder_id) , '+/', '-_') , '=') . '" title="Edit"><i class="fa fa-edit"></i></a> ';
				}	
				
				array_push($temp, $actionCol21);
				array_push($temp, $actionCol1);
				
				
				array_push($items, $temp);
			}
		}	

		$result["aaData"] = $items;
		echo json_encode($result);
		exit;
	}
	
	
	function changeapproval(){
		$result = "";
		$condition = "1=1 && course_id='".$_GET['id']."' ";
		$result['details'] = $this->documentfoldermastermodel->getdata('tbl_courses',$condition);
		//echo "<pre>";print_r($result["details"]);exit;
		$view = $this->load->view('routinesactions/changeapproval',$result,true);
		echo $view;
	}
	
	function view($id = NULL)
	{
		if (!empty($_SESSION["webadmin"])) {
			$record_id = "";

			// print_r($_GET);

			if (!empty($_GET['text']) && isset($_GET['text'])) {
				$varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
				parse_str($varr, $url_prams);
				$record_id = $url_prams['id'];
			}
			$result = "";
			$result['details'] = $this->documentfoldermastermodel->getFormdata($record_id);
			
			$this->load->view('template/header.php');
			$this->load->view('centermaster/view', $result);
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
	
	function delRecord()
	{
		
		$id=$_POST['id'];
		$status=$_POST['status'];
		
		$appdResult = $this->documentfoldermastermodel->delrecord("tbl_document_folders","document_folder_id",$id,$status);
		if($appdResult)
		{
			echo "1";
		}
		else
		{
			echo "2";	
		}	
	}
	
	function delrecord12()
	{
		//echo $_POST['status'];exit;
		$id=$_POST['id'];
		$status=$_POST['status'];
		
		$appdResult = $this->documentfoldermastermodel->delrecord12("tbl_document_folders","document_folder_id",$id,$status);
		if($appdResult)
		{
			echo "1";
		}
		else
		{
			echo "2";	
		}	
	}

	function logout()
	{
		$this->session->unset_userdata('logged_in');
		session_destroy();
		redirect('auth/login', 'refresh');
	}
	
}

?>
