<?php 
//session_start();
//print_r($_SESSION["webadmin"]);
//echo "<pre>";
//print_r($zones);exit;
?>
<!-- start: Content -->
<div id="content" class="content-wrapper">
	 <div class="page-title">
      <div>
        <h1>Timetables</h1>            
      </div>
      <div>
        <ul class="breadcrumb">
          <li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="<?php echo base_url();?>centertimetables">Timetables</a></li>
        </ul>
      </div>
    </div> 
    <div class="card">
    	<div class="page-title-border">
        	<div class="col-sm-12 col-md-12 left-button-top">
            		
            <div class="clearfix"></div>
            </div>
        </div>     
		
        <div class="col-sm-12">
         	<div class="box-content form-horizontal product-filter">            	
            	
				<div class="col-sm-2 col-xs-12">
					<div class="dataTables_filter searchFilterClass form-group">
						<label for="firstname" class="control-label">Timetable Title</label>
						<input id="sSearch_0" name="sSearch_0" type="text" class="searchInput form-control"/>
					</div>
				</div>
				
				<?php if($_SESSION["webadmin"][0]->user_type == 1 ){?>
					<div class="col-sm-2 col-xs-12">
						<div class="dataTables_filter searchFilterClass form-group">
							<label for="firstname" class="control-label">Zone</label>
							<!--<input id="sSearch_1" name="sSearch_1" type="text" class="searchInput form-control"/>-->
							<select id="zone_id" name="sSearch_1" class="searchInput form-control"  onchange="getCenters(this.value);" >
								<option value="">Select Zone</option>
								<?php 
									if(isset($zones) && !empty($zones)){
										foreach($zones as $cdrow){
								?>
									<option value="<?php echo $cdrow->zone_id;?>"><?php echo $cdrow->zone_name;?></option>
								<?php }}?>
							</select>
							
							
						</div>
					</div>
					
					<div class="col-sm-2 col-xs-12">
						<div class="dataTables_filter searchFilterClass form-group">
							<label for="firstname" class="control-label">Center</label>
							<!--<input id="sSearch_2" name="sSearch_2" type="text" class="searchInput form-control"/>-->
							<select id="center_id" name="sSearch_2" class="searchInput form-control" >
								<option value="">Select Center</option>
							</select>
						</div>
					</div>
				<?php }?>	
				
				<div class="control-group clearFilter">
					<div class="controls">
						<a href="#" onclick="clearSearchFilters();"><button class="btn" style="margin:32px 10px 10px 10px;">Clear Search</button></a>
					</div>
				</div>
				
				
            </div>
         </div>
		 
		 
         <div class="clearfix"></div>
         <div class="card-body">
          	<div class="box-content">
            	 <div class="table-responsive scroll-table">
                    <table class="dynamicTable display table table-bordered non-bootstrap" >
                        <thead>
                          <tr>
							<th>Timetable Title</th>
							<?php if($_SESSION["webadmin"][0]->user_type == 1 ){?>
								<th>Zone</th>
								<th>Center</th>
							<?php }?>	
							<th data-bSortable="false">Action</th>
						</tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                        </tfoot>
                    </table>           
                </div>
            </div>
         </div>
         <div class="clearfix"></div>
    </div>
</div><!-- end: Content -->
			
<script>

	function getCenters(zone_id,center_id = null)
	{
		//alert("Val: "+val);return false;
		if(zone_id != "" )
		{
			$.ajax({
				url:"<?php echo base_url();?>centertimetables/getCenters",
				data:{zone_id:zone_id, center_id:center_id},
				dataType: 'json',
				method:'post',
				success: function(res)
				{
					if(res['status']=="success")
					{
						if(res['option'] != "")
						{
							$("#center_id").html("<option value=''>Select</option>"+res['option']);
						}
						else
						{
							$("#center_id").html("<option value=''>Select</option>");
						}
					}
					else
					{	
						$("#center_id").html("<option value=''>Select</option>");
					}
				}
			});
		}
	}

	$( document ).ready(function() {

		clearSearchFilters();
	});	
	
	
	function deleteData1(id,status)
	{
		var sta="";
		var sta1=" ";
		
		if(status=='Active')
		{
		
			sta="In-active";
			
		}
		else{
			
			sta="Active";
		}
		
		
		
    	var r=confirm("Are you sure to " +sta);
    	if (r==true)
   		{
    		//window.location.href="users/delete?id="+id;
			$.ajax({
				url: "<?php echo base_url().$this->router->fetch_module();?>/delrecord12/",
				data:{"id":id,"status":sta},
				async: false,
				type: "POST",
				success: function(data2){
					data2 = $.trim(data2);
					if(data2 == "1")
					{
						displayMsg("success","Record has been Updated!");
						setTimeout("location.reload(true);",1000);
						
					}
					else
					{
						displayMsg("error","Oops something went wrong!");
						setTimeout("location.reload(true);",1000);
					}
				}
			});
    	}
    }
	
	document.title = "Timetables";
</script>