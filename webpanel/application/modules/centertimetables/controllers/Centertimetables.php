<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Centertimetables extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		// $this->load->helper('erp_setting');
		ini_set( 'memory_limit', '25M' );
		ini_set('upload_max_filesize', '25M');  
		ini_set('post_max_size', '25M');  
		ini_set('max_input_time', 3600);  
		ini_set('max_execution_time', 3600);

		$this->load->model('centertimetablesmodel', '', TRUE);
	}

	function index()
	{
		
		if (!empty($_SESSION["webadmin"])) {
		
			$result = array();
			//get zone user zones
			$user_id = $_SESSION["webadmin"][0]->user_id;
			$getUserZones = $this->centertimetablesmodel->getUserZones("user_id='".$user_id."' ");
			//echo "<pre>";print_r($getUserZones);exit;
			$result['zones'] = (!empty($getUserZones)) ? $getUserZones : '';
			$this->load->view('template/header.php');
			$this->load->view('centertimetables/index', $result);
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
	public function getCenters(){
		$result = $this->centertimetablesmodel->getOptions("tbl_centers",$_REQUEST['zone_id'],"zone_id");
		//echo "<pre>";
		//print_r($result);
		//exit;
		
		$option = '';
		$center_id = '';
		
		if(isset($_REQUEST['center_id']) && !empty($_REQUEST['center_id'])){
			$center_id = $_REQUEST['center_id'];
		}
		
		
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$sel = ($result[$i]->center_id == $center_id) ? 'selected="selected"' : '';
				$option .= '<option value="'.$result[$i]->center_id.'" '.$sel.' >'.$result[$i]->center_name.'</option>';
			}
		}
		
		
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}
	
	function viewDoc($id = NULL)
	{
		if (!empty($_SESSION["webadmin"])) {
			$record_id = $_GET['text'];

			// print_r($_GET);

			
			$result = "";
			$result['details'] = $this->centertimetablesmodel->getdata("tbl_timetable_documents", "timetable_document_id='".$record_id."' ");
			//echo "<pre>";print_r($result);exit;
			
			$this->load->view('template/header.php');
			$this->load->view('centertimetables/viewDoc', $result);
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
	function addEdit($id = NULL)
	{
		if (!empty($_SESSION["webadmin"])) {
			$record_id = "";

			// print_r($_GET);

			if (!empty($_GET['text']) && isset($_GET['text'])) {
				$varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
				parse_str($varr, $url_prams);
				$record_id = $url_prams['id'];
			}
			$result = "";
			$result['categories'] = $this->centertimetablesmodel->getDropdown("tbl_categories","category_id,categoy_name");
			$result['details'] = $this->centertimetablesmodel->getFormdata($record_id);
			$result['videos'] = $this->centertimetablesmodel->getdata("tbl_timetable_videos", "timetable_id='".$record_id."' ");
			$result['documents'] = $this->centertimetablesmodel->getdata("tbl_timetable_documents", "timetable_id='".$record_id."' ");
			//echo "<pre>";print_r($result['videos']);exit;
			
			$this->load->view('template/header.php');
			$this->load->view('centertimetables/addEdit', $result);
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	

	/*new code end*/
	function fetch($id=null)
	{
		//$_GET['album_id'] = $_GET['id'];
		
		$user_id = $_SESSION["webadmin"][0]->user_id;
		
		if($_SESSION["webadmin"][0]->user_type == 1 ){
			//get zones
			//echo "<pre>";print_r($_SESSION["webadmin"][0]->user_zones);exit;
			
			$zones = implode(",", $_SESSION["webadmin"][0]->user_zones);
			//echo  $zones;exit;
			$_GET['zones'] = $zones;
			
		}else{
			//get courses & categories
			$getCoursesCategories = $this->centertimetablesmodel->getdata("tbl_center_user_courses", "user_id='".$user_id."' ");
			$course_array = array();
			$category_array = array();
			for($i=0; $i < sizeof($getCoursesCategories); $i++){
				$course_array[] = $getCoursesCategories[$i]['course_id'];
				$category_array[] = $getCoursesCategories[$i]['category_id'];
			}
			//echo "<pre>";print_r($category_array);exit;
			$course_implode = implode(",", $course_array);
			$category_implode = implode(",", $category_array);
			$_GET['courses'] = $course_implode;
			$_GET['categories'] = $category_implode;
		}
		
		
		
		$get_result = $this->centertimetablesmodel->getRecords($_GET);
		$result = array();
		$result["sEcho"] = $_GET['sEcho'];
		
		$result["iTotalRecords"] = $get_result['totalRecords']; //	iTotalRecords get no of total recors
		$result["iTotalDisplayRecords"] = $get_result['totalRecords']; //  iTotalDisplayRecords for display the no of records in data table.
		$items = array();
		if(!empty($get_result['query_result'])){
			for ($i = 0; $i < sizeof($get_result['query_result']); $i++) {
				$temp = array();
				array_push($temp, $get_result['query_result'][$i]->timetable_title);
				if($_SESSION["webadmin"][0]->user_type == 1 ){
					array_push($temp, $get_result['query_result'][$i]->zone_name);
					array_push($temp, $get_result['query_result'][$i]->center_name);
				}	
				
				$getSeeBeforeAfter = $this->centertimetablesmodel->getdata("tbl_centers", "center_id='".$get_result['query_result'][$i]->center_id."' ");
				//echo "<pre>";print_r($getSeeBeforeAfter);exit;
				$see_before = $getSeeBeforeAfter[0]['can_see_timetable_before'];
				$see_after = $getSeeBeforeAfter[0]['can_see_timetable_after'];
				
				$see_before_date = date('Y-m-d', strtotime($get_result['query_result'][$i]->start_date. ' - '.$see_before.' days'));
				$see_after_date = date('Y-m-d', strtotime($get_result['query_result'][$i]->end_date. ' + '.$see_after.' days'));
				//echo "see_before_date: ".$see_before_date." see_after_date: ".$see_after_date;exit;
				$current_date = date("Y-m-d");
				$actionCol1 = "";
				if (($current_date >= $see_before_date) && ($current_date <= $see_after_date)){
					$actionCol1.= '<a href="centertimetables/addEdit?text=' . rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->timetable_id) , '+/', '-_') , '=') . '" title="View">View Timetable</a> ';
				}
				
				array_push($temp, $actionCol1);
				
				
				array_push($items, $temp);
			}
		}	

		$result["aaData"] = $items;
		echo json_encode($result);
		exit;
	}
	
	function view($id = NULL)
	{
		if (!empty($_SESSION["webadmin"])) {
			$record_id = "";

			// print_r($_GET);

			if (!empty($_GET['text']) && isset($_GET['text'])) {
				$varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
				parse_str($varr, $url_prams);
				$record_id = $url_prams['id'];
			}
			$result = "";
			$result['details'] = $this->centertimetablesmodel->getFormdata($record_id);
			
			$this->load->view('template/header.php');
			$this->load->view('coursesmaster/view', $result);
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
	
	function delRecord()
	{
		
		$id=$_POST['id'];
		
		$appdResult = $this->centertimetablesmodel->delrecord1("tbl_timetable_videos","timetable_video_id",$id);
		if($appdResult)
		{
			echo "1";
		}
		else
		{
			echo "2";	
		}	
	}
	
	function delDocRecord()
	{
		
		$id=$_POST['id'];
		$get_name = $this->centertimetablesmodel->getdata("tbl_timetable_documents","timetable_document_id='".$id."' ");
		
		if(!empty($get_name[0]['doc_file'])){
			$del_path = DOC_ROOT."/images/timetable_images/".$get_name[0]['doc_file'];
			if(file_exists($del_path)){
				@unlink($del_path);
			}
		}
		
		$appdResult = $this->centertimetablesmodel->delrecord1("tbl_timetable_documents","timetable_document_id",$id);
		if($appdResult)
		{
			echo "1";
		}
		else
		{
			echo "2";	
		}	
	}
	
	function delrecord12()
	{
		//echo $_POST['status'];exit;
		$id=$_POST['id'];
		$status=$_POST['status'];
		
		$appdResult = $this->centertimetablesmodel->delrecord12("tbl_timetable_master","timetable_id",$id,$status);
		if($appdResult)
		{
			echo "1";
		}
		else
		{
			echo "2";	
		}	
	}
	
	private function set_upload_options()
	{   
		//upload an image options //products
		$config = array();
		$config['upload_path'] = "images/timetable_images/";
		//$config['allowed_types'] = '*';
		$config['allowed_types'] = 'gif|jpg|png|jpeg|pdf';
		$config['max_size']      = '0';
		$config['overwrite']     = FALSE;

		return $config;
	}

	function logout()
	{
		$this->session->unset_userdata('logged_in');
		session_destroy();
		redirect('auth/login', 'refresh');
	}
	
}

?>
