<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Feesmaster extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

		ini_set( 'memory_limit', '25M' );
		ini_set('upload_max_filesize', '25M');  
		ini_set('post_max_size', '25M');  
		ini_set('max_input_time', 3600);  
		ini_set('max_execution_time', 3600);

		$this->load->model('feesmastermodel', '', TRUE);
	}

	function index()
	{
		if (!empty($_SESSION["webadmin"])) {
			$this->load->view('template/header.php');
			$this->load->view('feesmaster/index');
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
	function addEdit($id = NULL)
	{
		if (!empty($_SESSION["webadmin"])) {
			$record_id = "";
			if (!empty($_GET['text']) && isset($_GET['text'])) {
				$varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
				parse_str($varr, $url_prams);
				$record_id = $url_prams['id'];
				// $record_array = explode(',',$record_id1);
				// $record_id = $record_array[0];
				// $componentid = $record_array[1];
			}
			$result = [];
			$result['academic_year'] = $this->feesmastermodel->getDropdown("tbl_academic_year_master","academic_year_master_id,academic_year_master_name");
			$result['categories'] = $this->feesmastermodel->getDropdownCategory("tbl_categories","category_id,categoy_name");
			$result['courses'] = $this->feesmastermodel->getDropdown("tbl_courses","course_id,course_name");
			$result['groups'] = $this->feesmastermodel->getDropdown("tbl_group_master","group_master_id,group_master_name");
			$result['fees_components'] = $this->feesmastermodel->getDropdown("tbl_fees_component_master","fees_component_master_id,fees_component_master_name");
			$result['fees_level'] = $this->feesmastermodel->getDropdown("tbl_fees_level_master","fees_level_id,fees_level_name");
			$result['details'] = $this->feesmastermodel->getFormdata($record_id);
			$result['details1'] = $this->feesmastermodel->getFormdata1($record_id);
			// print_r(implode(',',$result['details1']));exit();
			$result['c_id_array'] = [];
			if(sizeof($result['details1']) > 0){
				for ($i=0; $i < sizeof($result['details1']); $i++) { 
					$result['c_id_array'][] = (!empty($result['details1'])) ? $result['details1'][$i]->fees_component_id : '';;
				}
			}
			// print_r($result['c_id_array']);exit();
			
			$this->load->view('template/header.php');
			$this->load->view('feesmaster/addEdit', $result);
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
	public function getCourses(){
		$result = $this->feesmastermodel->getOptions("tbl_courses",$_REQUEST['category_id'],"category_id");
		
		$option = '';
		$course_id = '';
		
		if(isset($_REQUEST['course_id']) && !empty($_REQUEST['course_id'])){
			$course_id = $_REQUEST['course_id'];
		}
		
		
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$sel = ($result[$i]->course_id == $course_id) ? 'selected="selected"' : '';
				$option .= '<option value="'.$result[$i]->course_id.'" '.$sel.' >'.$result[$i]->course_name.'</option>';
			}
		}
		
		
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}	

	function getCalcamount(){
		$result = $this->feesmastermodel->getOptions1("tbl_fees_component_master",$_REQUEST['components_id'],"fees_component_master_id");
		if(!empty($result)){
			$sum = 0;
			for($i=0;$i<sizeof($result);$i++){
				$sum = $sum + $result[$i]->amount;
			}
		}
		echo json_encode(array("status"=>"success","sum"=>$sum));
		exit();
	}

	function submitForm()
	{ 
		// $components_id_string = implode(',',$_REQUEST['fees_component_id']);
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
		


			if(!isset($_POST['fees_component_id'])){
				echo json_encode(array("success"=>"0",'msg'=>'Please select component!'));
				exit;
			}
			// if(empty($_POST['fees_id'])){
				if(!empty($_POST['fees_component_id'])){
					for($i=0; $i < sizeof($_POST['fees_component_id']); $i++){
						// print_r($_POST['fees_component_id'][$i]);exit();
						if($_POST['fees_selection_type'] == 'Class'){
							$condition = "academic_year_id='".$_POST['academic_year_id']."' && category_id='".$_POST['category_id']."' && course_id='".$_POST['course_id']."' && fees_name= '".$_POST['fees_name']."' ";
						}
						if($_POST['fees_selection_type'] == 'Group'){
							$condition = "academic_year_id='".$_POST['academic_year_id']."' && group_id='".$_POST['group_id']."' && fees_name= '".$_POST['fees_name']."' ";
						}
						
						// $condition1 = "fees_component_id='".$_POST['fees_component_id'][$i]."' ";

						if(isset($_POST['fees_id']) && $_POST['fees_id'] > 0)
						{
							$condition .= " &&  fees_id != ".$_POST['fees_id'];
						}

						// if(isset($_POST['fees_component_data_id']) && $_POST['fees_component_data_id'] > 0)
						// {
						// 	$condition1 .= " &&  fees_component_data_id != ".$_POST['fees_component_data_id'];
						// }
						
						$check_name = $this->feesmastermodel->getdata("tbl_fees_master",$condition);
						// $check_name1 = $this->feesmastermodel->getdata("tbl_fees_component_data",$condition1);
						// // print_r($check_name);
						// // // echo "<pre>";
						// print_r($check_name1);
						// exit();
						if(!empty($check_name)){
							// print_r($_POST['fees_component_id'][$i]);
							$getFeesName = $this->feesmastermodel->getdata("tbl_fees_master", "fees_id='".$_POST['fees_id']."' ");
							echo json_encode(array("success"=>"0",'msg'=>' '.$getFeesName[0]['fees_name'].' Already assign to selected fees!'));
							exit;
						}
						// exit();
						
					}
				}
			// }
			
			if(strtotime($_POST['start_date']) > strtotime($_POST['end_date'])){
				echo json_encode(array("success"=>"0",'msg'=>'Start date not greater than end date!'));
				exit;
			}
			
			
			if (!empty($_POST['fees_id'])) {

				// $condition = "academic_year_id='".$_POST['academic_year_id']."' && category_id='".$_POST['category_id']."' && course_id='".$_POST['course_id']."' && group_id='".$_POST['group_id']."' && fees_component_id='".$_POST['fees_component_id']."' &&  fees_level_id='".$_POST['fees_level_id']."' &&  fees_type='".$_POST['fees_type']."' && fees_name= '".$_POST['fees_name']."' && amount= '".$_POST['amount']."' && receipt_print_name= '".$_POST['receipt_print_name']."' && installment_no= '".$_POST['installment_no']."' ";
				// if(isset($_POST['fees_id']) && $_POST['fees_id'] > 0)
				// {
				// 	$condition .= " &&  fees_id != ".$_POST['fees_id'];
				// }
				
				// $check_name = $this->feesmastermodel->getdata("tbl_fees_master",$condition);
				// if(!empty($check_name)){
				// 	$getComponentName = $this->feesmastermodel->getdata("tbl_fees_component_master", "fees_component_master_id='".$_POST['fees_component_id']."' ");
				// 	echo json_encode(array("success"=>"0",'msg'=>' '.$getComponentName[0]['center_name'].' Already assign to selected component!'));
				// 	exit;
				// }

				$data_array = array();			
				$fees_id = $_POST['fees_id'];
		 		
				$data_array['academic_year_id'] = (!empty($_POST['academic_year_id'])) ? $_POST['academic_year_id'] : '';
				$data_array['fees_selection_type'] = (!empty($_POST['fees_selection_type'])) ? $_POST['fees_selection_type'] : '';
				$data_array['category_id'] = (!empty($_POST['category_id'])) ? $_POST['category_id'] : 0;
				$data_array['course_id'] = (!empty($_POST['course_id'])) ? $_POST['course_id'] : 0;
				$data_array['group_id'] = (!empty($_POST['group_id'])) ? $_POST['group_id'] : 0;
				$data_array['fees_level_id'] = (!empty($_POST['fees_level_id'])) ? $_POST['fees_level_id'] : '';
				// $data_array['fees_type'] = (!empty($_POST['fees_type'])) ? $_POST['fees_type'] : '';
				$data_array['fees_name'] = (!empty($_POST['fees_name'])) ? $_POST['fees_name'] : '';
				$data_array['amount'] = (!empty($_POST['amount'])) ? $_POST['amount'] : '';
				$data_array['description'] = (!empty($_POST['description'])) ? $_POST['description'] : '';
				$data_array['receipt_print_name'] = (!empty($_POST['receipt_print_name'])) ? $_POST['receipt_print_name'] : '';
				$data_array['installment_no'] = (!empty($_POST['installment_no'])) ? $_POST['installment_no'] : 0;
				$data_array['start_date'] = (!empty($_POST['start_date'])) ? date("Y-m-d", strtotime($_POST['start_date'])) : '';
				$data_array['end_date'] = (!empty($_POST['end_date'])) ? date("Y-m-d", strtotime($_POST['end_date'])) : '';
				
				$data_array['updated_on'] = date("Y-m-d H:i:s");
				$data_array['updated_by'] = $_SESSION["webadmin"][0]->user_id;


				$result = $this->feesmastermodel->updateRecord('tbl_fees_master', $data_array,'fees_id',$fees_id);
				$getComponentId = $this->feesmastermodel->deletedata("tbl_fees_component_data", 'fees_master_id',$_POST['fees_id']);
				// print_r($getComponentId);exit();
				
				$data_array1 = array();
				for($i=0;$i<sizeof($_POST['fees_component_id']);$i++){
					$data_array1['fees_master_id'] = $_POST['fees_id'];
					// $fees_component_data_id = $getComponentId[$i]['fees_component_data_id'];
					// print_r($fees_component_data_id);
					// $data_array1['fees_component_data_id'] = $fees_component_data_id;
					$fees_component_id = (!empty($_POST['fees_component_id'])) ? $_POST['fees_component_id'][$i] : '';
					// print_r($_POST['fees_component_id']);exit();
					$data_array1['fees_component_id'] = $fees_component_id;

					$data_array1['created_on'] = date("Y-m-d H:i:s");
					$data_array1['updated_on'] = date("Y-m-d H:i:s");
					$result = $this->feesmastermodel->insertData('tbl_fees_component_data', $data_array1, '1');
				}
				
				
			}else {
				
				$data_array = array();
				$data_array['academic_year_id'] = (!empty($_POST['academic_year_id'])) ? $_POST['academic_year_id'] : '';
				$data_array['fees_selection_type'] = (!empty($_POST['fees_selection_type'])) ? $_POST['fees_selection_type'] : '';
				$data_array['category_id'] = (!empty($_POST['category_id'])) ? $_POST['category_id'] : 0;
				$data_array['course_id'] = (!empty($_POST['course_id'])) ? $_POST['course_id'] : 0;
				$data_array['group_id'] = (!empty($_POST['group_id'])) ? $_POST['group_id'] : 0;
				$data_array['fees_level_id'] = (!empty($_POST['fees_level_id'])) ? $_POST['fees_level_id'] : '';
				// $data_array['fees_type'] = (!empty($_POST['fees_type'])) ? $_POST['fees_type'] : '';
				$data_array['fees_name'] = (!empty($_POST['fees_name'])) ? $_POST['fees_name'] : '';
				$data_array['amount'] = (!empty($_POST['amount'])) ? $_POST['amount'] : '';
				$data_array['description'] = (!empty($_POST['description'])) ? $_POST['description'] : '';
				$data_array['receipt_print_name'] = (!empty($_POST['receipt_print_name'])) ? $_POST['receipt_print_name'] : '';
				$data_array['installment_no'] = (!empty($_POST['installment_no'])) ? $_POST['installment_no'] : 0;
				$data_array['start_date'] = (!empty($_POST['start_date'])) ? date("Y-m-d", strtotime($_POST['start_date'])) : '';
				$data_array['end_date'] = (!empty($_POST['end_date'])) ? date("Y-m-d", strtotime($_POST['end_date'])) : '';
				
				$data_array['created_on'] = date("Y-m-d H:i:s");
				$data_array['created_by'] = $_SESSION["webadmin"][0]->user_id;
				$data_array['updated_on'] = date("Y-m-d H:i:s");
				$data_array['updated_by'] = $_SESSION["webadmin"][0]->user_id;
				
				$result = $this->feesmastermodel->insertData('tbl_fees_master', $data_array, '1');
				$data_array1 = array();
				$data_array1['fees_master_id'] = $result;
				for($i=0;$i<sizeof($_POST['fees_component_id']);$i++){
					$fees_component_id = (!empty($_POST['fees_component_id'])) ? $_POST['fees_component_id'][$i] : '';
					$data_array1['fees_component_id'] = $fees_component_id;

					$data_array1['created_on'] = date("Y-m-d H:i:s");
					$data_array1['updated_on'] = date("Y-m-d H:i:s");
					$result = $this->feesmastermodel->insertData('tbl_fees_component_data', $data_array1, '1');
				}
				
			}
		
		
			if (!empty($result)) {
				echo json_encode(array(
					'success' => '1',
					'msg' => 'Record Added/Updated Successfully.'
				));
				exit;
			}
			else{
				echo json_encode(array(
					'success' => '0',
					'msg' => 'Problem in data update.'
				));
				exit;
			}
			
		}
		else {
			return false;
		}
	
	}
	
	/*new code end*/
	function fetch($id=null)
	{
		$get_result = $this->feesmastermodel->getRecords($_GET);
		// print_r($get_result);
		// exit();
		$result = array();
		$result["sEcho"] = $_GET['sEcho'];
		
		$result["iTotalRecords"] = $get_result['totalRecords']; //	iTotalRecords get no of total recors
		$result["iTotalDisplayRecords"] = $get_result['totalRecords']; //  iTotalDisplayRecords for display the no of records in data table.
		$items = array();
		if(!empty($get_result['query_result'])){
			for ($i = 0; $i < sizeof($get_result['query_result']); $i++) {
				$temp = array();
				array_push($temp, $get_result['query_result'][$i]->academic_year_master_name);
				array_push($temp, $get_result['query_result'][$i]->fees_selection_type);
				array_push($temp, $get_result['query_result'][$i]->fees_level_name);
				// array_push($temp, $get_result['query_result'][$i]->fees_type);
				array_push($temp, $get_result['query_result'][$i]->fees_name);
				array_push($temp, $get_result['query_result'][$i]->amount);
				array_push($temp, $get_result['query_result'][$i]->receipt_print_name);
				array_push($temp, $get_result['query_result'][$i]->start_date);
				array_push($temp, $get_result['query_result'][$i]->end_date);
				
				$actionCol21="";
				if($this->privilegeduser->hasPrivilege("FeesMasterAddEdit")){
					$actionCol21 .= '<a href="javascript:void(0);" onclick="deleteData1(\'' . $get_result['query_result'][$i]->fees_id. '\',\'' . $get_result['query_result'][$i]->status. '\');" title="">'.$get_result['query_result'][$i]->status.'</a>';
				}	
				
				$actionCol1 = "";
				if($this->privilegeduser->hasPrivilege("FeesMasterAddEdit")){
					$actionCol1.= '<a href="feesmaster/addEdit?text=' . rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->fees_id) , '+/', '-_') , '=') . '" title="Edit"><i class="fa fa-edit"></i></a> ';
				}	
				
				array_push($temp, $actionCol21);
				array_push($temp, $actionCol1);
				
				
				array_push($items, $temp);
			}
		}	

		$result["aaData"] = $items;
		echo json_encode($result);
		exit;
	}

	
	function delrecord12()
	{
		$id=$_POST['id'];
		$status=$_POST['status'];
		
		$appdResult = $this->feesmastermodel->delrecord12("tbl_fees_master","fees_id",$id,$status);
		if($appdResult)
		{
			echo "1";
		}
		else
		{
			echo "2";	
		}	
	}
	
}

?>
