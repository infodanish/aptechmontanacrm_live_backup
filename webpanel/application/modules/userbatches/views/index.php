<?php 

if (!empty($_GET['text']) && isset($_GET['text'])) {
	$varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
	parse_str($varr, $url_prams);
	$record_id = $url_prams['id'];
}
?>


<!-- start: Content -->
<div id="content" class="content-wrapper">
	 <div class="page-title">
      <div>
        <h1>Center User -Batches</h1> 
      </div>
      <div>
        <ul class="breadcrumb">
          <li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="<?php echo base_url();?>centerusers">Center User</a></li>
        </ul>
      </div>
    </div> 
    <div class="card">
    	<div class="page-title-border">
        	<div class="col-sm-12 col-md-12 left-button-top">
					<p>
						<a href="<?php  echo base_url();?>userbatches/addEdit?id=<?php echo $_GET['text'];?>" class="btn btn-primary icon-btn"><i class="fa fa-plus"></i>Add Batch</a>
            <div class="clearfix"></div>
            </div>
        </div>     
		
        <div class="col-sm-12">
         	<div class="box-content form-horizontal product-filter">            	
            	
				<div class="col-sm-2 col-xs-12">
					<div class="dataTables_filter searchFilterClass form-group">
						<label for="firstname" class="control-label">User First Name</label>
						<input id="sSearch_0" name="sSearch_0" type="text" class="searchInput form-control"/>
					</div>
				</div>
				
				<div class="col-sm-2 col-xs-12">
					<div class="dataTables_filter searchFilterClass form-group">
						<label for="firstname" class="control-label">User Last Name</label>
						<input id="sSearch_1" name="sSearch_1" type="text" class="searchInput form-control"/>
					</div>
				</div>
				
				<div class="col-sm-2 col-xs-12">
					<div class="dataTables_filter searchFilterClass form-group">
						<label for="type" class="control-label">Selection Type</label>
						<input id="sSearch_2" name="sSearch_2" type="text" class="searchInput form-control"/>
					</div>
				</div>
				
				<div class="col-sm-2 col-xs-12">
					<div class="dataTables_filter searchFilterClass form-group">
						<label for="firstname" class="control-label">Category</label>
						<input id="sSearch_3" name="sSearch_3" type="text" class="searchInput form-control"/>
					</div>
				</div>
				
				<div class="col-sm-2 col-xs-12">
					<div class="dataTables_filter searchFilterClass form-group">
						<label for="firstname" class="control-label">Course</label>
						<input id="sSearch_4" name="sSearch_4" type="text" class="searchInput form-control"/>
					</div>
				</div>

				<div class="col-sm-2 col-xs-12">
					<div class="dataTables_filter searchFilterClass form-group">
						<label for="groupname" class="control-label">Group</label>
						<input id="sSearch_4" name="sSearch_4" type="text" class="searchInput form-control"/>
					</div>
				</div>

				<div class="col-sm-2 col-xs-12">
					<div class="dataTables_filter searchFilterClass form-group">
						<label for="batchname" class="control-label">Batch</label>
						<input id="sSearch_5" name="sSearch_5" type="text" class="searchInput form-control"/>
					</div>
				</div>
				
				<div class="control-group clearFilter">
					<div class="controls">
						<a href="#" onclick="clearSearchFilters();"><button class="btn" style="margin:32px 10px 10px 10px;">Clear Search</button></a>
					</div>
				</div>
				
				
            </div>
         </div>
		 
		 
         <div class="clearfix"></div>
         <div class="card-body">
          	<div class="box-content">
            	 <div class="table-responsive scroll-table">
                    <table class="dynamicTable display table table-bordered non-bootstrap" callfunction="<?php echo base_url();?>userbatches/fetch/<?php echo $record_id;?>">
                        <thead>
                          <tr>
							<th>User First Name</th>
							<th>User Last Name</th>
							<th>Selection Type</th>
							<th>Category</th>
							<th>Course</th>
							<th>Group</th>
							<th>Batch</th>
							<th data-bSortable="false">Action</th>
						</tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                        </tfoot>
                    </table>           
                </div>
            </div>
         </div>
         <div class="clearfix"></div>
	    
	    
    </div>
</div><!-- end: Content -->
			
<script>
    $(document).ready(function(){
        clearSearchFilters()
    })
	function deleteData(id)
	{
		var r=confirm("Are you sure to delete selected record?");
		if (r==true){
				$.ajax({
					url: "<?php echo base_url().$this->router->fetch_module();?>/delRecord/",
					data:{"id":id},
					async: false,
					type: "POST",
					success: function(data2){
						data2 = $.trim(data2);
						if(data2 == "1")
						{
						    clearSearchFilters()
							displayMsg("success","Record has been Deleted!");
							
						}
						else
						{
							displayMsg("error","Oops something went wrong!");
						}
					}
				});
		}
    }
	
	
	document.title = "Center User - Batches";
</script>