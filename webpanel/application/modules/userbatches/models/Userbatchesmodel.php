<?PHP
class Userbatchesmodel extends CI_Model
{
	function insertData($tbl_name,$data_array,$sendid = NULL)
	{
		$this->db->insert($tbl_name,$data_array);
		$result_id = $this->db->insert_id();
		
		if($sendid == 1)
	 	{
	 		return $result_id;
	 	}
	}
	
	function getRecords($get){
		$table = "tbl_center_user_batches";
		$table_id = 'i.center_user_batch_id';
		$default_sort_column = 'i.center_user_batch_id';
		$default_sort_order = 'desc';
		
		$condition = "1=1 ";
		
		$colArray = array('u.first_name','u.last_name','b.batch_selection_type','ct.categoy_name', 'cn.course_name','g.group_master_name','b.batch_name');
		$searchArray = array('u.first_name','u.last_name','b.batch_selection_type','ct.categoy_name', 'cn.course_name','g.group_master_name','b.batch_name');
		
		$page = $get['iDisplayStart'];						
		$rows = $get['iDisplayLength'];					
		
		$sort = isset($get['iSortCol_0']) ? strval($colArray[$get['iSortCol_0']]) : $default_sort_column;  
		$order = isset($get['sSortDir_0']) ? strval($get['sSortDir_0']) : $default_sort_order;

		for($i=0;$i<7;$i++)
		{
			
			if(isset($get['sSearch_'.$i]) && $get['sSearch_'.$i]!='')
			{
				$condition .= " && $searchArray[$i] like '%".$_GET['sSearch_'.$i]."%'";
			}
			
		}
		
		$this -> db -> select('i.*,u.first_name, u.last_name, ct.categoy_name, cn.course_name,g.group_master_name,b.batch_name');
		$this -> db -> from('tbl_center_user_batches as i');
		$this -> db -> join('tbl_admin_users as u', 'i.user_id  = u.user_id', 'left');
		$this -> db -> join('tbl_categories as ct', 'i.category_id  = ct.category_id', 'left');
		$this -> db -> join('tbl_courses as cn', 'i.course_id  = cn.course_id', 'left');
		$this -> db -> join('tbl_group_master as g', 'i.group_id  = g.group_master_id', 'left');
		$this -> db -> join('tbl_batch_master as b', 'i.batch_id  = b.batch_id', 'left');
		$this-> db -> where('i.user_id',$get['user_id']);
		$this->db->where("($condition)");
		
		$this->db->order_by($sort, $order);
		$this->db->limit($rows,$page);		
		$query = $this -> db -> get();

		$this -> db -> select('i.*,u.first_name, u.last_name, ct.categoy_name, cn.course_name,g.group_master_name,b.batch_name');
		$this -> db -> from('tbl_center_user_batches as i');
		$this -> db -> join('tbl_admin_users as u', 'i.user_id  = u.user_id', 'left');
		$this -> db -> join('tbl_categories as ct', 'i.category_id  = ct.category_id', 'left');
		$this -> db -> join('tbl_courses as cn', 'i.course_id  = cn.course_id', 'left');
		$this -> db -> join('tbl_group_master as g', 'i.group_id  = g.group_master_id', 'left');
		$this -> db -> join('tbl_batch_master as b', 'i.batch_id  = b.batch_id', 'left');
		$this-> db -> where('i.user_id',$get['user_id']);
		$this->db->where("($condition)");

		$this->db->order_by($sort, $order);		
		$query1 = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			$totcount = $query1 -> num_rows();
			return array("query_result" => $query->result(),"totalRecords"=>$totcount);
		}
		else
		{
			return array("totalRecords"=>0);
		}
	}
	
	function getdata($table,$condition = '1=1'){
        $sql = $this->db->query("Select * from $table where $condition");
        if($sql->num_rows()>0){
            return $sql->result_array();
        }else{
            return false;
        }
    }
	
	function delrecord1($tbl_name,$tbl_id,$record_id)
	{
		$this->db->where($tbl_id, $record_id);
	    $this->db->delete($tbl_name); 
		if($this->db->affected_rows() >= 1)
		{
			return true;
	    }
	    else
	    {
			return false;
	    }
	}
	 
	function getCatrgoryOptions($ID){
		$this->db->distinct();
		$this -> db -> select('i.category_id,ct.categoy_name');
		$this -> db -> from('tbl_center_user_courses as i');
		$this -> db -> join('tbl_categories as ct', 'i.category_id  = ct.category_id', 'left');
		$status="Active";
		$category_id = 1;
		$this -> db -> where("ct.status",$status);
		$this -> db -> where("i.category_id",$category_id);
		$this-> db -> where('i.user_id',$ID);
		$query = $this -> db -> get();
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}

	function getCourseOptions($ID,$UserID){
		$this->db->distinct();
		$this -> db -> select('cn.course_id,cn.course_name');
		$this -> db -> from('tbl_center_user_courses as i');
		$this -> db -> join('tbl_courses as cn', 'i.course_id  = cn.course_id', 'left');
		$status="Active";
		$this -> db -> where("cn.status",$status);
		$this-> db -> where('i.category_id',$ID);
		$this-> db -> where('i.user_id',$UserID);
		$query = $this -> db -> get();
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}
	
	function getBatchesOptions($ID,$CourseID,$UserID){
		$this->db->distinct();
		$this -> db -> select('b.batch_id,b.batch_name');
		$this -> db -> from('tbl_batch_master as b');
		$this -> db -> join('tbl_admin_users as u', 'b.zone_id  = u.zone_id and b.center_id  = u.center_id ',  'left');
		$status="Active";
		$this -> db -> where("b.status",$status);
		$this-> db -> where('b.category_id',$ID);
		$this-> db -> where('b.course_id',$CourseID);
		$this-> db -> where('u.user_id',$UserID);
		$query = $this -> db -> get();
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}

	function getGroupBatchesOptions($GroupID,$UserID){
		$this->db->distinct();
		$this -> db -> select('b.batch_id,b.batch_name');
		$this -> db -> from('tbl_batch_master as b');
		$this -> db -> join('tbl_admin_users as u', 'b.zone_id  = u.zone_id and b.center_id  = u.center_id ',  'left');
		$status="Active";
		$this -> db -> where("b.status",$status);
		$this-> db -> where('b.group_id',$GroupID);
		$this-> db -> where('u.user_id',$UserID);
		$query = $this -> db -> get();
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}

	function getDropdown($tbl_name,$tble_flieds){
	   
		$this -> db -> select($tble_flieds);
		$this -> db -> from($tbl_name);
		$status="Active";
		$this -> db -> where("status",$status);
	
		$query = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
			
	}
}
?>
