<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Assigncamera extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

		// $this->load->helper('erp_setting');
		ini_set( 'memory_limit', '25M' );
		ini_set('upload_max_filesize', '25M');  
		ini_set('post_max_size', '25M');  
		ini_set('max_input_time', 3600);  
		ini_set('max_execution_time', 3600);

		$this->load->model('assigncameramodel', '', TRUE);
	}

	function index()
	{
		if (!empty($_SESSION["webadmin"])) {
			$this->load->view('template/header.php');
			$this->load->view('assigncamera/index');
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
	function addEdit($id = NULL)
	{
		if (!empty($_SESSION["webadmin"])) {
			$record_id = "";

			// print_r($_GET);

			if (!empty($_GET['text']) && isset($_GET['text'])) {
				$varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
				parse_str($varr, $url_prams);
				$record_id = $url_prams['id'];
			}
			$result = [];
			$result['categories'] = $this->assigncameramodel->getDropdownCategory("tbl_categories","category_id,categoy_name");
			$result['zones'] = $this->assigncameramodel->getDropdown("tbl_zones","zone_id,zone_name");
			$result['groups'] = $this->assigncameramodel->getDropdown("tbl_group_master","group_master_id,group_master_name");
			$result['batch'] = $this->assigncameramodel->getDropdown("tbl_batch_master","batch_id,batch_name");
			$result['camera'] = $this->assigncameramodel->getDropdown("tbl_camera","camera_id,camera_name");
			$result['details'] = $this->assigncameramodel->getFormdata($record_id);
			// print_r($result['details']);
			// exit();

			$this->load->view('template/header.php');
			$this->load->view('assigncamera/addEdit', $result);
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
	public function getCourses(){
		$result = $this->assigncameramodel->getOptions("tbl_courses",$_REQUEST['category_id'],"category_id");
		/*echo "<pre>";
		print_r($result);
		exit;*/
		
		$option = '';
		$course_id = '';
		
		if(isset($_REQUEST['course_id']) && !empty($_REQUEST['course_id'])){
			$course_id = $_REQUEST['course_id'];
		}
		
		
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$sel = ($result[$i]->course_id == $course_id) ? 'selected="selected"' : '';
				$option .= '<option value="'.$result[$i]->course_id.'" '.$sel.' >'.$result[$i]->course_name.'</option>';
			}
		}
		
		
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}

	public function getBatch(){
		// print_r($_REQUEST);
		// exit();
		$result = $this->assigncameramodel->getOptions1("tbl_batch_master",$_REQUEST['center_id'],"center_id",$_REQUEST['course_id'],"course_id");
		
		$option = '';
		$batch_id = '';
		
		if(isset($_REQUEST['batch_id']) && !empty($_REQUEST['batch_id'])){
			$batch_id = $_REQUEST['batch_id'];
		}
		
		
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$sel = ($result[$i]->batch_id == $batch_id) ? 'selected="selected"' : '';
				$option .= '<option value="'.$result[$i]->batch_id.'" '.$sel.' >'.$result[$i]->batch_name.'</option>';
			}
		}
		
		
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}

	function getGroupBatch(){
		$result = $this->assigncameramodel->getOptions1("tbl_batch_master",$_REQUEST['center_id'],"center_id",$_REQUEST['group_id'],"group_id");
		
		$option = '';
		$batch_id = '';
		
		if(isset($_REQUEST['batch_id']) && !empty($_REQUEST['batch_id'])){
			$batch_id = $_REQUEST['batch_id'];
		}
		
		
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$sel = ($result[$i]->batch_id == $batch_id) ? 'selected="selected"' : '';
				$option .= '<option value="'.$result[$i]->batch_id.'" '.$sel.' >'.$result[$i]->batch_name.'</option>';
			}
		}
		
		
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}
	
	public function getCenters(){
		$result = $this->assigncameramodel->getOptions("tbl_centers",$_REQUEST['zone_id'],"zone_id");
		//echo "<pre>";
		//print_r($result);
		//exit;
		
		$option = '';
		$center_id = '';
		
		if(isset($_REQUEST['center_id']) && !empty($_REQUEST['center_id'])){
			$center_id = $_REQUEST['center_id'];
		}
		
		
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$sel = ($result[$i]->center_id == $center_id) ? 'selected="selected"' : '';
				$option .= '<option value="'.$result[$i]->center_id.'" '.$sel.' >'.$result[$i]->center_name.'</option>';
			}
		}
		
		
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}

	function submitForm()
	{ 
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			
			// if(empty($_POST['assign_camera_id'])){
			
				if(empty($_POST['camera_id'])){
					echo json_encode(array("success"=>"0",'msg'=>'Please select camera!'));
					exit;
				}	
			// }
			if(empty($_POST['assign_camera_id'])){
				if(!empty($_POST['camera_id'])){
					for($i=0; $i < sizeof($_POST['camera_id']); $i++){
						if($_POST['fees_selection_type'] == 'Class'){
							$condition = "category_id='".$_POST['category_id']."' && course_id='".$_POST['course_id']."' &&  zone_id='".$_POST['zone_id']."' &&  center_id='".$_POST['center_id']."' &&  batch_id='".$_POST['batch_id']."' && camera_id='".$_POST['camera_id'][$i]."'  ";
						}
						if($_POST['fees_selection_type'] == 'Group'){
							$condition = "zone_id='".$_POST['zone_id']."' &&  center_id='".$_POST['center_id']."' &&  group_id='".$_POST['group_id']."' &&  batch_id='".$_POST['batch_id']."' && camera_id='".$_POST['camera_id'][$i]."'  ";
						}
						
						if(isset($_POST['assign_camera_id']) && $_POST['assign_camera_id'] > 0){
							$condition .= " &&  assign_camera_id != ".$_POST['assign_camera_id'];
						}
						
						$check_name = $this->assigncameramodel->getdata("tbl_assign_camera",$condition);
						
						if(!empty($check_name)){
							$getCameraName = $this->assigncameramodel->getdata("tbl_camera", "camera_id='".$_POST['camera_id'][$i]."' ");
							echo json_encode(array("success"=>"0",'msg'=>' '.$getCameraName[0]['camera_name'].' Already assign to selected camera!'));
							exit;
						}
						
					}
				}
			}
			//exit;
			if (!empty($_POST['assign_camera_id'])) {
				if($_POST['fees_selection_type'] == 'Class'){
					$condition = "category_id='".$_POST['category_id']."' && course_id='".$_POST['course_id']."' &&  zone_id='".$_POST['zone_id']."' &&  center_id='".$_POST['center_id']."' &&  batch_id='".$_POST['batch_id']."' && camera_id='".$_POST['camera_id']."'  ";
				}
				if($_POST['fees_selection_type'] == 'Group'){
					$condition = "zone_id='".$_POST['zone_id']."' &&  center_id='".$_POST['center_id']."' &&  group_id='".$_POST['group_id']."' &&  batch_id='".$_POST['batch_id']."' && camera_id='".$_POST['camera_id']."'  ";
				}
					
				if(isset($_POST['assign_camera_id']) && $_POST['assign_camera_id'] > 0){
					$condition .= " &&  assign_camera_id != ".$_POST['assign_camera_id'];
				}
				
				$check_name = $this->assigncameramodel->getdata("tbl_assign_camera",$condition);
				
				if(!empty($check_name)){
					$getCameraName = $this->assigncameramodel->getdata("tbl_camera", "camera_id='".$_POST['camera_id']."' ");
					echo json_encode(array("success"=>"0",'msg'=>' '.$getCameraName[0]['camera_name'].' Already assign to selected camera!'));
					exit;
				}
				$data_array = array();			
				$assign_camera_id = $_POST['assign_camera_id'];
		 		
				
				$data_array['zone_id'] = (!empty($_POST['zone_id'])) ? $_POST['zone_id'] : '';
				$data_array['center_id'] = (!empty($_POST['center_id'])) ? $_POST['center_id'] : '';
				$data_array['fees_selection_type'] = (!empty($_POST['fees_selection_type'])) ? $_POST['fees_selection_type'] : '';
				$data_array['category_id'] = (!empty($_POST['category_id'])) ? $_POST['category_id'] : 0;
				$data_array['course_id'] = (!empty($_POST['course_id'])) ? $_POST['course_id'] : 0;
				$data_array['batch_id'] = (!empty($_POST['batch_id'])) ? $_POST['batch_id'] : 0;
				$data_array['group_id'] = (!empty($_POST['group_id'])) ? $_POST['group_id'] : 0;
				$data_array['camera_id'] = (!empty($_POST['camera_id'])) ? $_POST['camera_id'] : '';
				
				$data_array['updated_on'] = date("Y-m-d H:i:s");
				$data_array['updated_by'] = $_SESSION["webadmin"][0]->user_id;
				
				$result = $this->assigncameramodel->updateRecord('tbl_assign_camera', $data_array,'assign_camera_id',$assign_camera_id);
				
				
			}else {
				
				$data_array = array();
				$data_array['zone_id'] = (!empty($_POST['zone_id'])) ? $_POST['zone_id'] : '';
				$data_array['center_id'] = (!empty($_POST['center_id'])) ? $_POST['center_id'] : '';
				$data_array['fees_selection_type'] = (!empty($_POST['fees_selection_type'])) ? $_POST['fees_selection_type'] : '';
				$data_array['category_id'] = (!empty($_POST['category_id'])) ? $_POST['category_id'] : 0;
				$data_array['course_id'] = (!empty($_POST['course_id'])) ? $_POST['course_id'] : 0;
				$data_array['batch_id'] = (!empty($_POST['batch_id'])) ? $_POST['batch_id'] : 0;
				$data_array['group_id'] = (!empty($_POST['group_id'])) ? $_POST['group_id'] : 0;
				
				
				$data_array['created_on'] = date("Y-m-d H:i:s");
				$data_array['created_by'] = $_SESSION["webadmin"][0]->user_id;
				$data_array['updated_on'] = date("Y-m-d H:i:s");
				$data_array['updated_by'] = $_SESSION["webadmin"][0]->user_id;
				//echo "<pre>";print_r($data_array);exit;
				
				if(!empty($_POST['camera_id'])){
					for($i=0; $i < sizeof($_POST['camera_id']); $i++){
						$data_array['camera_id'] = (!empty($_POST['camera_id'][$i])) ? $_POST['camera_id'][$i] : '';
						$result = $this->assigncameramodel->insertData('tbl_assign_camera', $data_array, '1');
						
					}
				}
				
			}
		
		
			if (!empty($result)) {
				echo json_encode(array(
					'success' => '1',
					'msg' => 'Record Added/Updated Successfully.'
				));
				exit;
			}
			else{
				echo json_encode(array(
					'success' => '0',
					'msg' => 'Problem in data update.'
				));
				exit;
			}
			
		}
		else {
			return false;
		}
	
	}
	
	/*new code end*/
	function fetch($id=null)
	{
		//$_GET['album_id'] = $_GET['id'];
		//echo $id;exit;
		$get_result = $this->assigncameramodel->getRecords($_GET);
		$result = array();
		$result["sEcho"] = $_GET['sEcho'];
		
		$result["iTotalRecords"] = $get_result['totalRecords']; //	iTotalRecords get no of total recors
		$result["iTotalDisplayRecords"] = $get_result['totalRecords']; //  iTotalDisplayRecords for display the no of records in data table.
		$items = array();
		if(!empty($get_result['query_result'])){
			for ($i = 0; $i < sizeof($get_result['query_result']); $i++) {
				$temp = array();
				array_push($temp, $get_result['query_result'][$i]->zone_name);
				array_push($temp, $get_result['query_result'][$i]->center_name);
				array_push($temp, $get_result['query_result'][$i]->fees_selection_type);
				array_push($temp, $get_result['query_result'][$i]->camera_name);
				
				array_push($temp, $get_result['query_result'][$i]->course_name);
				array_push($temp, $get_result['query_result'][$i]->batch_name);
				array_push($temp, $get_result['query_result'][$i]->group_master_name);
				
				
				$actionCol21="";
				if($this->privilegeduser->hasPrivilege("AssignCameraAddEdit")){
					$actionCol21 .= '<a href="javascript:void(0);" onclick="deleteData1(\'' . $get_result['query_result'][$i]->assign_camera_id. '\',\'' . $get_result['query_result'][$i]->status. '\');" title="">'.$get_result['query_result'][$i]->status.'</a>';
				}	
				
				$actionCol1 = "";
				if($this->privilegeduser->hasPrivilege("AssignCameraAddEdit")){
					$actionCol1.= '<a href="assigncamera/addEdit?text=' . rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->assign_camera_id) , '+/', '-_') , '=') . '" title="Edit"><i class="fa fa-edit"></i></a> ';
				}	
				
				array_push($temp, $actionCol21);
				array_push($temp, $actionCol1);
				
				
				array_push($items, $temp);
			}
		}	

		$result["aaData"] = $items;
		echo json_encode($result);
		exit;
	}
	
	function delrecord12()
	{
		//echo $_POST['status'];exit;
		$id=$_POST['id'];
		$status=$_POST['status'];
		
		$appdResult = $this->assigncameramodel->delrecord12("tbl_assign_camera","assign_camera_id",$id,$status);
		if($appdResult)
		{
			echo "1";
		}
		else
		{
			echo "2";	
		}	
	}
	
	
}

?>
