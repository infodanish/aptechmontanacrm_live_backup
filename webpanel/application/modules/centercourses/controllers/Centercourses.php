<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Centercourses extends CI_Controller

{
	function __construct()
	{
		parent::__construct();

		// $this->load->helper('erp_setting');
		ini_set( 'memory_limit', '25M' );
		ini_set('upload_max_filesize', '25M');  
		ini_set('post_max_size', '25M');  
		ini_set('max_input_time', 3600);  
		ini_set('max_execution_time', 3600);

		$this->load->model('centercoursesmodel', '', TRUE);
	}

	function index()
	{
		if (!empty($_SESSION["webadmin"])) {
			//echo "here";exit;
			$this->load->view('template/header.php');
			$this->load->view('centercourses/index');
			$this->load->view('template/footer.php');
		}
		else {
			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}

	function addEdit($id = NULL)
	{
		if (!empty($_SESSION["webadmin"])) {
			$record_id = "";

			// print_r($_GET);

			if (!empty($_GET['text']) && isset($_GET['text'])) {
				$varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
				parse_str($varr, $url_prams);
				$record_id = $url_prams['id'];
			}
			$result = [];
			$result['categories'] = $this->centercoursesmodel->getDropdownCategory("tbl_categories","category_id,categoy_name");
			$result['details'] = $this->centercoursesmodel->getFormdata($record_id);
			
			$this->load->view('template/header.php');
			$this->load->view('centercourses/addEdit', $result);
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
	/*new code end*/
	function fetch($id=null)
	{
		$_GET['center_id'] = $id;
		//echo $id;exit;
		$get_result = $this->centercoursesmodel->getRecords($_GET);
		$result = array();
		$result["sEcho"] = $_GET['sEcho'];
		
		$result["iTotalRecords"] = $get_result['totalRecords']; //	iTotalRecords get no of total recors
		$result["iTotalDisplayRecords"] = $get_result['totalRecords']; //  iTotalDisplayRecords for display the no of records in data table.
		$items = array();
		if(!empty($get_result['query_result'])){
			for ($i = 0; $i < sizeof($get_result['query_result']); $i++) {
				$temp = array();
				array_push($temp, $get_result['query_result'][$i]->center_name);
				array_push($temp, $get_result['query_result'][$i]->categoy_name);
				array_push($temp, $get_result['query_result'][$i]->course_name);
				
				$actionCol21="";
				if($this->privilegeduser->hasPrivilege("CenterCategoryCoursesDelete")){
					$actionCol21 .= '<a href="javascript:void(0);" onclick="deleteData(\'' . $get_result['query_result'][$i]->center_course_id. '\');" title="">Delete</a>';
				}
				
				array_push($temp, $actionCol21);
				
				array_push($items, $temp);
			}
		}	

		$result["aaData"] = $items;
		echo json_encode($result);
		exit;
	}
	
	public function getCourses(){
		$result = $this->centercoursesmodel->getOptions("tbl_courses",$_REQUEST['category_id'],"category_id");
		/*echo "<pre>";
		print_r($result);
		exit;*/
		$option = '';
		
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$option .= '<option value="'.$result[$i]->course_id.'" >'.$result[$i]->course_name.'</option>';
			}
		}
		
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}
	
	
	function submitForm()
	{ 
		//echo "<pre>";print_r($_POST);exit;
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			
			/*$condition = "center_id='".$_POST['center_id']."' && category_id= '".$_POST['category_id']."' && course_id= '".$_POST['course_id']."' ";
			
			$check_name = $this->centercoursesmodel->getdata("tbl_center_courses",$condition);
			if(!empty($check_name)){
				echo json_encode(array("success"=>"0",'msg'=>'Record Already Present!'));
				exit;
			}
			
			*/
			
			if(!empty($_POST['course_id'])){
				//$this->centercoursesmodel->delrecord1("tbl_center_courses", "category_id", $_POST['category_id']);
				$this->centercoursesmodel->delrecord_condition("tbl_center_courses", "category_id='".$_POST['category_id']."' && center_id='".$_POST['center_id']."' ");
				for($i=0; $i < sizeof($_POST['course_id']); $i++){
					$data_array = array();
					$data_array['center_id'] = (!empty($_POST['center_id'])) ? $_POST['center_id'] : '';
					$data_array['category_id'] = (!empty($_POST['category_id'])) ? $_POST['category_id'] : '';
					$data_array['course_id'] = $_POST['course_id'][$i];
					
					$result = $this->centercoursesmodel->insertData('tbl_center_courses', $data_array, '1');
				}
			}else{
				echo json_encode(array("success"=>"0",'msg'=>'Kindly select atleast one record!'));
				exit;
			}
			
			
			
			if (!empty($result)) {
				echo json_encode(array(
					'success' => '1',
					'msg' => 'Record Added/Updated Successfully.'
				));
				exit;
			}
			else{
				echo json_encode(array(
					'success' => '0',
					'msg' => 'Problem in data update.'
				));
				exit;
			}
			
		}
		else {
			return false;
		}
	
	}
	
	private function set_upload_options()
	{   
		//upload an image options //products
		$config = array();
		//$config['file_name']     = md5(uniqid("100_ID", true));
		$config['upload_path'] = DOC_ROOT_FRONT."/images/teacher_album_images";
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['max_size']      = '0';
		$config['overwrite']     = FALSE;

		return $config;
	}
	
	function delRecord()
	{
		$id=$_POST['id'];
		$appdResult = $this->centercoursesmodel->delrecord1("tbl_center_courses","center_course_id",$id);
		if($appdResult)
		{
			echo "1";
		}
		else
		{
			echo "2";	
		}	
	}
	
	function logout()
	{
		$this->session->unset_userdata('logged_in');
		session_destroy();
		redirect('auth/login', 'refresh');
	}
	
}

?>
