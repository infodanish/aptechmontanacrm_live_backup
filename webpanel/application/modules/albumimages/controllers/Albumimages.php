<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
require_once './vendor/autoload.php';
use Google\Cloud\Vision\VisionClient;

function detect_safe_search($projectId, $path)
{
	$vision = new VisionClient([
		'projectId' => 'montana-191907',
		// 'keyFile' => $apiJson
		'keyFilePath' => './api.json'
	]);
	
	$image = $vision->image(file_get_contents($path), [
		'SAFE_SEARCH_DETECTION'/*,
		'LABEL_DETECTION'*/
	]);
   
	$result = $vision->annotate($image);
	$safe = $result->safeSearch();
	
	/*printf("Adult: %s\n", $safe->isAdult() ? 'yes' : 'no');
	printf("Spoof: %s\n", $safe->isSpoof() ? 'yes' : 'no');
	printf("Medical: %s\n", $safe->isMedical() ? 'yes' : 'no');
	printf("Violence: %s\n\n", $safe->isViolent() ? 'yes' : 'no');
	*/
	
	$isAdult = $safe->isAdult() ? 'yes' : 'no';
	$isSpoof = $safe->isSpoof() ? 'yes' : 'no';
	$isMedical = $safe->isMedical() ? 'yes' : 'no';
	$isViolent = $safe->isViolent() ? 'yes' : 'no';
	//echo "isAdult: ".$isAdult." isSpoof: ".$isSpoof." isMedical: ".$isMedical." isViolent: ".$isViolent;
	//exit;
	
	$check_labels_array = array('face','toddler','human','children','kids','team','community','child');
	$label_check = 'no';
	/*foreach ($result->labels() as $label) {
		//echo "<br/>description: ".$label->description();
		//echo " <br/>Percentage: ".$label->score()."/n";
		if(in_array($label->description(), $check_labels_array)){
			$label_check = 'yes';
			break;
		}
	}*/
	//exit;
	return array($isAdult,$isSpoof,$isMedical,$isViolent,$label_check);
	
}

class Albumimages extends CI_Controller

{
	function __construct()
	{
		parent::__construct();

		// $this->load->helper('erp_setting');
		ini_set( 'memory_limit', '25M' );
		ini_set('upload_max_filesize', '25M');  
		ini_set('post_max_size', '25M');  
		ini_set('max_input_time', 3600);  
		ini_set('max_execution_time', 3600);

		$this->load->model('albumimagesmodel', '', TRUE);
	}

	function index()
	{
		if (!empty($_SESSION["webadmin"])) {
			$this->load->view('template/header.php');
			$this->load->view('index');
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}

	function addEdit($id = NULL)
	{
		if (!empty($_SESSION["webadmin"])) {
			$record_id = "";

			// print_r($_GET);

			if (!empty($_GET['text']) && isset($_GET['text'])) {
				$varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
				parse_str($varr, $url_prams);
				$record_id = $url_prams['id'];
			}
			$result = "";
			$result['details'] = $this->albumimagesmodel->getFormdata($record_id);
			
			
			$zone_data = array('action'=>'FetchZone', 'BrandID'=>'112');
			$getZones = curlFunction(SERVICE_URL, $zone_data);
			//echo "<pre>";print_r($getZones);exit;
			$getZones = json_decode($getZones, true);
			$result['zones'] = $getZones;
			
			/*for($i=0; $i < sizeof($getZones);$i++){
				echo $getZones[$i]['Zone'];
			}
			exit;
			*/
            
			$this->load->view('template/header.php');
			$this->load->view('albumimages/addEdit', $result);
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
	/*new code end*/
	function fetch($id=null)
	{
		$_GET['album_id'] = $id;
		//echo $id;exit;
		$get_result = $this->albumimagesmodel->getRecords($_GET);
		$result = array();
		$result["sEcho"] = $_GET['sEcho'];
		
		$result["iTotalRecords"] = $get_result['totalRecords']; //	iTotalRecords get no of total recors
		$result["iTotalDisplayRecords"] = $get_result['totalRecords']; //  iTotalDisplayRecords for display the no of records in data table.
		$items = array();
		if(!empty($get_result['query_result'])){
			for ($i = 0; $i < sizeof($get_result['query_result']); $i++) {
				$temp = array();
				array_push($temp, $get_result['query_result'][$i]->album_name);
				
				$img="";
				//$img .= '<a href="'.FRONT_URL.'/images/teacher_album_images/'.$get_result['query_result'][$i]->album_pic.' " class="thumbnail" data-toggle="modal" data-toggle="lightbox"> <img  src="'.FRONT_URL.'/images/teacher_album_images/'.$get_result['query_result'][$i]->album_pic.' " width="100" height="100" alt="..." class="img-responsive" ><a>';
				
				$img .= '<a href="javascript:void(0);" onclick="openwindow(\'' .$get_result['query_result'][$i]->album_pic. '\');" title=""> <img  src="'.FRONT_URL.'/images/teacher_album_images/'.$get_result['query_result'][$i]->album_pic.' " width="100" height="100" alt="..." class="img-responsive" ></a>';
				
				$actionCol21="";
				$actionCol21 .= '<a href="javascript:void(0);" onclick="deleteData1(\'' . $get_result['query_result'][$i]->album_image_id. '\',\'' . $get_result['query_result'][$i]->status. '\');" title="">'.$get_result['query_result'][$i]->status.'</a>';
				
				array_push($temp, $img);
				array_push($temp, $actionCol21);
				
				array_push($items, $temp);
			}
		}	

		$result["aaData"] = $items;
		echo json_encode($result);
		exit;
	}
	
	
	function submitForm()
	{ 
		//echo "<pre>";print_r($_POST);exit;
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			
			if(empty($_FILES['product_image'])){
				echo json_encode(array(
					'success' => '0',
					'msg' => 'Please select atleast one image to upload.'
				));
				exit;
			}
			
			//echo "<pre>";print_r($_FILES);exit;
			$this->load->library('upload');
			$files = $_FILES;
			
			if(!empty($_POST['mhid']) && isset($_POST['mhid'])){
				$files = $_FILES;
				foreach($_POST['mhid'] as $key=>$value){
					if(!empty($files['product_image']['name'][$key]) && isset($files['product_image']['name'][$key])){
						$file_name_val = false;
						$file_name = md5(uniqid("100_ID", true)).str_replace(' ', '-', $files['product_image']['name'][$key]);
						
						$_FILES['product_image']['name']= $file_name;
						$_FILES['product_image']['type']= $files['product_image']['type'][$key];
						$_FILES['product_image']['tmp_name']= $files['product_image']['tmp_name'][$key];
						$_FILES['product_image']['error']= $files['product_image']['error'][$key];
						$_FILES['product_image']['size']= $files['product_image']['size'][$key];    
						
						$this->upload->initialize($this->set_upload_options());
					
						$this->upload->do_upload('product_image');
						$productImage = array();
						
						$img_path = DOC_ROOT_FRONT."/images/teacher_album_images/".$file_name;
						$safe_search_response = detect_safe_search('montana-191907', $img_path);
						
						/*echo $safe_search_response[0]."<br/>";
						echo $safe_search_response[1]."<br/>";
						echo $safe_search_response[2]."<br/>";
						echo $safe_search_response[3]."<br/>";
						echo $safe_search_response[4]."<br/>";
						exit;
						*/
						
						if($safe_search_response[0] == "yes"){
							@unlink(DOC_ROOT_FRONT."/images/teacher_album_images/".$file_name);
							$file_name_val = true;
						}
						
						if($safe_search_response[1] == "yes"){
							@unlink(DOC_ROOT_FRONT."/images/teacher_album_images/".$file_name);
							$file_name_val = true;
						}
						
						if($safe_search_response[2] == "yes"){
							@unlink(DOC_ROOT_FRONT."/images/teacher_album_images/".$file_name);
							$file_name_val = true;
						}
						
						if($safe_search_response[3] == "yes"){
							@unlink(DOC_ROOT_FRONT."/images/teacher_album_images/".$file_name);
							$file_name_val = true;
						}
						
						/*if($safe_search_response[4] == "no"){
							@unlink(DOC_ROOT_FRONT."/images/teacher_album_images/".$file_name);
							$file_name_val = true;
						}*/
						
						if($file_name_val == false){
							$data = array();
							$data['teacher_id'] = (!empty($_SESSION["webadmin"][0]->username)) ? $_SESSION["webadmin"][0]->username : '';
							$data['center_id'] = (!empty($_SESSION["webadmin"][0]->center_id)) ? $_SESSION["webadmin"][0]->center_id : '';
							$data['album_id'] = (!empty($_POST["album_id"])) ? $_POST["album_id"] : '';
							$data['album_pic'] = $file_name;
							$data['created_on'] = date("Y-m-d H:i:s");
							$data['created_by'] = (!empty($_SESSION["webadmin"][0]->username)) ? $_SESSION["webadmin"][0]->username : '';
							$data['updated_by'] = (!empty($_SESSION["webadmin"][0]->username)) ? $_SESSION["webadmin"][0]->username : '';
							
							$this->albumimagesmodel->insertData('tbl_album_images', $data, 1);
							
						}
						
						
						
					}
					
				}
				
				echo json_encode(array(
					'success' => '1',
					'msg' => 'Record Added/Updated Successfully.'
				));
				exit;
				
			}
			
		}
		else {
			return false;
		}
	
	}
	
	private function set_upload_options()
	{   
		//upload an image options //products
		$config = array();
		//$config['file_name']     = md5(uniqid("100_ID", true));
		$config['upload_path'] = DOC_ROOT_FRONT."/images/teacher_album_images";
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['max_size']      = '0';
		$config['overwrite']     = FALSE;

		return $config;
	}
	
	function delRecord()
	{
		
		$id=$_POST['id'];
		$status=$_POST['status'];
		
		$appdResult = $this->albumimagesmodel->delrecord("tbl_album_images","album_image_id",$id,$status);
		if($appdResult)
		{
			echo "1";
		}
		else
		{
			echo "2";	
		}	
	}
	
	function delrecord12()
	{
		//echo $_POST['status'];exit;
		$id=$_POST['id'];
		$status=$_POST['status'];
		
		$appdResult = $this->albumimagesmodel->delrecord12("tbl_album_images","album_image_id",$id,$status);
		if($appdResult)
		{
			echo "1";
		}
		else
		{
			echo "2";	
		}	
	}

	
	

	function logout()
	{
		$this->session->unset_userdata('logged_in');
		session_destroy();
		redirect('auth/login', 'refresh');
	}
	
}

?>
