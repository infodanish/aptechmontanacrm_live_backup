<?php 
error_reporting(0);
if (!empty($_GET['text']) && isset($_GET['text'])) {
	$varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
	//echo $_GET['text'];exit;
	parse_str($varr, $url_prams);
	$record_id = $url_prams['id'];
}
//echo $record_id;exit;
//echo DOC_ROOT_FRONT;exit;
?>
<style>
	.appended .appenddia{
		border-bottom: 1px solid black;
	}
</style>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/adapters/jquery.js"></script>
<div id="content" class="content-wrapper">
				<div class="page-title">
                  <div>
                    <h1>Newsletters</h1>            
                  </div>
                  <div>
                    <ul class="breadcrumb">
                      <li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
                      <li><a href="<?php echo base_url();?>newsletters">Newsletters</a></li>
                    </ul>
                  </div>
                </div>
                <div class="card">       
                 <div class="card-body">             
                    <div class="box-content">
                        <div class="col-sm-8 col-md-12">
							<form class="form-horizontal" id="form-validate" method="post" enctype="multipart/form-data">
								<input type="hidden" id="album_id" name="album_id" value="<?php if(!empty($record_id)){echo $record_id;}?>" />
								
								
								<div class="control-group form-group">
									<label class="control-label"></label><br/>
									<label class="control-label" for="selectError">Upload Images</label>
									<div class="controls">
										<button class="addProductImage btn btn-primary" type="button">Add More</button><br/><br/>
										<div id="divProductImage">
											<strong>Image</strong>: <input type="hidden" name="mhid[1]" id="mhid1" value="1" />
											<input type="file" name="product_image[1]" />
										<br/></div>
									</div>
								</div>
								<div class="form-actions form-group">
									<button type="submit" class="btn btn-primary">Submit</button>
									<a href="#" onclick="window.history.back();" class="btn btn-primary">Cancel</a>
								</div>
							</form>
                        </div>
                    <div class="clearfix"></div>
                    </div>
                 </div>
                </div>        
			</div><!-- end: Content -->								
<script>

 
$( document ).ready(function() {
	
	<?php 
		if(!empty($details[0]->newsletter_id)){
	?>
		//getRegion('<?php echo $details[0]->zone_text; ?>', '<?php echo $details[0]->region_text; ?>');
		//getArea('<?php echo $details[0]->region_text; ?>', '<?php echo $details[0]->area_text; ?>');
		//getCenter('<?php echo $details[0]->area_text; ?>', '<?php echo $details[0]->center_id; ?>');
	<?php }?>

	
});


var vRules = {
	zone_text:{required:true},
	region_text:{required:true},
	area_text:{required:true},
	center_id:{required:true},
	newsletter_title:{required:true, alphanumericwithspace:true},
	newsletter_description:{required:true},
	cover_image:{extension: "gif,jpg,png,jpeg",filesize: 2}
	
};
var vMessages = {
	zone_text:{required:"Please select zone."},
	region_text:{required:"Please select region."},
	area_text:{required:"Please select area."},
	center_id:{required:"Please select center."},
	newsletter_title:{required:"Please enter title."},
	newsletter_description:{required:"Please enter contents."}
	
};

$("#form-validate").validate({
	success: function(error) { 
        error.removeClass("error");  // <- no, no, no!!
    },
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{
		var act = "<?php echo base_url();?>albumimages/submitForm";
		$("#form-validate").ajaxSubmit({
			url: act, 
			type: 'post',
			cache: false,
			clearForm: false,
			success: function (response) {
				var res = eval('('+response+')');
				if(res['success'] == "1")
				{
					displayMsg("success",res['msg']);
					setTimeout(function(){
						//window.location = "<?php echo base_url();?>albumimages";
						location.reload();
					},2000);

				}
				else
				{	
					//$("#error_msg").show();
					displayMsg("error",res['msg']);
					return false;
				}
			}
		});
	}
});



$(document).ready(function(){
		
 	var count = 2;
	$(".addProductImage").click(function(){
		$("#divProductImage").append("<input type='hidden' name='mhid["+count+"]' value='1' /><strong>Image</strong>: <input type='file' name='product_image["+count+"]' /> <br/>");
		count++;
	});
	
	
});


document.title = "AddEdit - Album Images";

 
</script>					
