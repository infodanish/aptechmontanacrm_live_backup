<style>
	.appended .appenddia{
		border-bottom: 1px solid black;
	}
</style>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/adapters/jquery.js"></script>
<div id="content" class="content-wrapper">
				<div class="page-title">
                  <div>
                    <h1>Center Event</h1>            
                  </div>
                  <div>
                    <ul class="breadcrumb">
                      <li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
                      <li><a href="<?php echo base_url();?>centerevents">Center Event</a></li>
                    </ul>
                  </div>
                </div>
                <div class="card">       
                 <div class="card-body">             
                    <div class="box-content">
                        <div class="col-sm-8 col-md-12">
							<form class="form-horizontal" id="form-validate" method="post" enctype="multipart/form-data">
								<input type="hidden" id="event_id" name="event_id" value="<?php if(!empty($details[0]->event_id)){echo $details[0]->event_id;}?>" />
							
							<?php if($_SESSION["webadmin"][0]->role_id == 1){?>
								<div class="control-group form-group">
									<label class="control-label"><span>Zone*</span></label> 
									<div class="controls">
										<select id="zone_text" name="zone_text" class="form-control" onchange="getRegion(this.value);">
											<option value="">Select Zone</option>
											<?php 
												if(isset($zones) && !empty($zones)){
												//foreach($zones as $cdrow){
												for($i=0; $i < sizeof($zones);$i++){
													$sel = ($zones[$i]['Zone'] == $details[0]->zone_text) ? 'selected="selected"' : '';
												?>
											<option value="<?php echo $zones[$i]['Zone'];?>" <?php echo $sel; ?>><?php echo $zones[$i]['Zone'];?></option>
											<?php }}?>
										</select>
									</div>
								</div>
								
								<div class="control-group form-group">
									<label class="control-label"><span>Region*</span></label> 
									<div class="controls">
										<select id="region_text" name="region_text" class="form-control" onchange="getArea(this.value);">
											<option value="">Select Region</option>
										</select>
									</div>
								</div>
								
								<div class="control-group form-group">
									<label class="control-label"><span>Area*</span></label> 
									<div class="controls">
										<select id="area_text" name="area_text" class="form-control" onchange="getCenter(this.value);">
											<option value="">Select Area</option>
										</select>
									</div>
								</div>
								
								<div class="control-group form-group">
									<label class="control-label"><span>Center*</span></label> 
									<div class="controls">
										<select id="center_id" name="center_id" class="form-control" >
											<option value="">Select Center</option>
										</select>
									</div>
								</div>
							<?php }?>	
								
								<div class="control-group form-group">
									<label class="control-label"><span>Title*</span></label>
									<div class="controls">
										<input type="text" class="form-control required" placeholder="Enter Title" id="event_title" name="event_title" value="<?php if(!empty($details[0]->event_title)){echo $details[0]->event_title;}?>" >
									</div>
								</div>
								
								
								<div class="control-group form-group">
									<label class="control-label" for="cover_image">Image*</label>
									<div class="controls">
										
										<input class="input-xlarge" id="input_cover_image" value="<?php if(!empty($details[0]->cover_image)){echo $details[0]->cover_image;}?>" name="input_cover_image" type="hidden" >
										
										<input class="input-xlarge" id="cover_image" name="cover_image" type="file">
										Only (gif, jpg, png, jpeg) and Max Size 2 MB allowed.<br/>
										
										<?php if(!empty($details[0]->cover_image) && file_exists(DOC_ROOT_FRONT."/images/events_images/".$details[0]->cover_image))
										{
										?>
											<img style="width: 150px;height:150px;padding-top:5px;" src="<?php echo FRONT_URL; ?>/images/events_images/<?php echo $details[0]->cover_image; ?>">
											</img>
											
										<?php 
										}
										?>
									</div>
								</div>
								
								
								<div class="control-group form-group">
									<label class="control-label"><span>Date & Time*</span></label>
									<div class="controls">
										
										
										<input type="text" class="input-xlarge form-control datepicker required" placeholder="Select date & time" id="event_datetime" name="event_datetime" value="<?php echo(isset($details[0]->event_datetime) && $details[0]->event_datetime != "0000-00-00 00:00:00")? $details[0]->event_datetime : "";?>" format = "dd/MM/yyyy hh:mm:ss">
										
									</div>
								</div>
								
								<div class="control-group form-group">
									<label class="control-label"><span>Place/Venue*</span></label>
									<div class="controls">
										<input type="text" class="form-control required" placeholder="Enter Place/Venue" id="event_place" name="event_place" value="<?php if(!empty($details[0]->event_place)){echo $details[0]->event_place;}?>" >
									</div>
								</div>
				
								<div class="control-group form-group">
									<label class="control-label"><span>Description</span></label>
									<br/>(Editor is for text contens only kindly don't put any image)
									<div class="controls">
										<textarea name="event_description" id="event_description" placeholder="Enter Description" class="form-control editor"><?php if(!empty($details[0]->event_description)){echo $details[0]->event_description;}?></textarea>
									</div>
								</div>
								
								
								<div class="form-actions form-group">
									<button type="submit" class="btn btn-primary">Submit</button>
									<a href="<?php echo base_url();?>centerevents" class="btn btn-primary">Cancel</a>
								</div>
							</form>
                        </div>
                    <div class="clearfix"></div>
                    </div>
                 </div>
                </div>        
			</div><!-- end: Content -->								
<script>

 
$( document ).ready(function() {

	$(".datepicker").datetimepicker({
		format: 'YYYY-MM-DD H:m:s'
	});
	
	<?php 
		if(!empty($details[0]->event_id)){
	?>
		getRegion('<?php echo $details[0]->zone_text; ?>', '<?php echo $details[0]->region_text; ?>');
		getArea('<?php echo $details[0]->region_text; ?>', '<?php echo $details[0]->area_text; ?>');
		getCenter('<?php echo $details[0]->area_text; ?>', '<?php echo $details[0]->center_id; ?>');
	<?php }?>

	var config = {enterMode : CKEDITOR.ENTER_BR, height:200, filebrowserBrowseUrl: '../js/ckeditor/filemanager/index.html', scrollbars:'yes',
			toolbar_Full:
			[
						//['Source', 'Templates'],
						['Cut','Copy','Paste','PasteText','-','Print', 'SpellChecker', 'Scayt'],
						['Subscript','Superscript'],
						['NumberedList','BulletedList'],
						['BidiLtr', 'BidiRtl' ],
						//['Maximize', 'ShowBlocks'],
						['Undo','Redo'],['Bold','Italic','Underline','Strike'],			
						['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],			
						//['SelectAll','RemoveFormat'],'/',
						['Styles','Format','Font','FontSize'],
						['TextColor','BGColor'],								
						//['Image','Flash','Table','HorizontalRule','Smiley'],
					],
					 width: "620px"
			};
	$('.editor').ckeditor(config);
	
	$.validator.addMethod("needsSelection", function(value, element) {
        return $(element).multiselect("getChecked").length > 0;
    });
	
});

function getRegion(zone_text,region_text = null)
{
	//alert("Val: "+val);return false;
	if(zone_text != "" )
	{
		$.ajax({
			url:"<?php echo base_url();?>centerevents/getRegion",
			data:{zone_text:zone_text, region_text:region_text},
			dataType: 'json',
			method:'post',
			success: function(res)
			{
				if(res['status']=="success")
				{
					if(res['option'] != "")
					{
						$("#region_text").html("<option value=''>Select</option>"+res['option']);
						//$("#region_text").select2();
					}
					else
					{
						$("#region_text").html("<option value=''>Select</option>");
						//$("#region_text").select2();
					}
				}
				else
				{	
					$("#region_text").html("<option value=''>Select</option>");
					//$("#region_text").select2();
				}
			}
		});
	}
}

function getArea(region_text,area_text = null)
{
	//alert("Val: "+val);return false;
	if(region_text != "" )
	{
		$.ajax({
			url:"<?php echo base_url();?>centerevents/getArea",
			data:{region_text:region_text, area_text:area_text},
			dataType: 'json',
			method:'post',
			success: function(res)
			{
				if(res['status']=="success")
				{
					if(res['option'] != "")
					{
						$("#area_text").html("<option value=''>Select</option>"+res['option']);
						//$("#area_text").select2();
					}
					else
					{
						$("#area_text").html("<option value=''>Select</option>");
						//$("#area_text").select2();
					}
				}
				else
				{	
					$("#area_text").html("<option value=''>Select</option>");
					//$("#area_text").select2();
				}
			}
		});
	}
}


function getCenter(area_text,center_id = null)
{
	//alert("Val: "+val);return false;
	if(area_text != "" )
	{
		$.ajax({
			url:"<?php echo base_url();?>centerevents/getCenter",
			data:{area_text:area_text, center_id:center_id},
			dataType: 'json',
			method:'post',
			success: function(res)
			{
				if(res['status']=="success")
				{
					if(res['option'] != "")
					{
						$("#center_id").html("<option value=''>Select</option>"+res['option']);
						//$("#center_id").select2();
					}
					else
					{
						$("#center_id").html("<option value=''>Select</option>");
						//$("#center_id").select2();
					}
				}
				else
				{	
					$("#center_id").html("<option value=''>Select</option>");
					//$("#center_id").select2();
				}
			}
		});
	}
}


var vRules = {
	
	zone_text:{required:true},
	region_text:{required:true},
	area_text:{required:true},
	center_id:{required:true},
	event_title:{required:true, alphanumericwithspace:true},
	event_datetime:{required:true},
	event_place:{required:true},
	event_description:{required:true}
	
};
var vMessages = {
	
	zone_text:{required:"Please select zone."},
	region_text:{required:"Please select region."},
	area_text:{required:"Please select area."},
	center_id:{required:"Please select center."},
	event_title:{required:"Please enter title."},
	event_datetime:{required:"Please select date & time."},
	event_place:{required:"Please enter place/venue."},
	event_description:{required:"Please enter description."}
	
};

$("#form-validate").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{
		var act = "<?php echo base_url();?>centerevents/submitForm";
		$("#form-validate").ajaxSubmit({
			url: act, 
			type: 'post',
			cache: false,
			clearForm: false,
			success: function (response) {
				var res = eval('('+response+')');
				if(res['success'] == "1")
				{
					displayMsg("success",res['msg']);
					setTimeout(function(){
						window.location = "<?php echo base_url();?>centerevents";
					},2000);

				}
				else
				{	
					//$("#error_msg").show();
					displayMsg("error",res['msg']);
					return false;
				}
			}
		});
	}
});



$(document).ready(function(){
		
 	var count = 2;
	$(".addProductImage").click(function(){
		$("#divProductImage").append("<input type='hidden' name='mhid["+count+"]' value='1' /><strong>Product Image</strong>: <input type='file' name='product_image["+count+"]' /> <strong>Thumbnail:</strong><input type='file' name='product_thumbnail_image["+count+"]' /><br/>");
		count++;
	});
	
	$(".setDefaultProductImage").click(function(){
		var image_id = $(this).attr("alt");
		if(image_id !== ""){
			$.ajax({
				url: '<?php echo base_url();?>centerevents/setDefaultProductImage',
				data:{"image_id":image_id},
				type: 'post',
				cache: false,
				clearForm: false,
				success: function () {
					alert("Image is set as default");
					location.reload();
				}
			});
		}
	});
	
	$(".deleteProductImage").click(function(){
		if(confirm("Are you sure you want to delete this product image ?")){
			var image_id = $(this).attr("alt");
			if(image_id !== ""){
				$.ajax({
					url: '<?php echo base_url();?>centerevents/deleteProductImage',
					data:{"image_id":image_id},
					type: 'post',
					cache: false,
					clearForm: false,
					success: function () {
						alert("Image is deleted");
						location.reload();
					}
				});
			}
		}
	});
});


document.title = "AddEdit - Center Events";

 
</script>					
