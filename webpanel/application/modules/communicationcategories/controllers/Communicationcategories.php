<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Communicationcategories extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

		ini_set( 'memory_limit', '25M' );
		ini_set('upload_max_filesize', '25M');  
		ini_set('post_max_size', '25M');  
		ini_set('max_input_time', 3600);  
		ini_set('max_execution_time', 3600);

		$this->load->model('communicationcategoriesmodel', '', TRUE);
	}

	function index()
	{
		if (!empty($_SESSION["webadmin"])) {
			$this->load->view('template/header.php');
			$this->load->view('communicationcategories/index');
			$this->load->view('template/footer.php');
		}
		else {
			redirect('login', 'refresh');
		}
	}

	function addEdit($id = NULL)
	{
		if (!empty($_SESSION["webadmin"])) {
			$record_id = "";
			if (!empty($_GET['text']) && isset($_GET['text'])) {
				$varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
				parse_str($varr, $url_prams);
				$record_id = $url_prams['id'];
			}
			$result = [];
			$result['details'] = $this->communicationcategoriesmodel->getFormdata($record_id);
			
			$this->load->view('template/header.php');
			$this->load->view('communicationcategories/addEdit', $result);
			$this->load->view('template/footer.php');
		}
		else {
			redirect('login', 'refresh');
		}
	}
	
	// /*new code*/
	function fetch()
	{
		$get_result = $this->communicationcategoriesmodel->getRecords($_GET);
		$result = array();
		$result["sEcho"] = $_GET['sEcho'];
		$result["iTotalRecords"] = $get_result['totalRecords']; 
		$result["iTotalDisplayRecords"] = $get_result['totalRecords']; 
		$items = array();
		if(!empty($get_result['query_result'])){
			for ($i = 0; $i < sizeof($get_result['query_result']); $i++) {
				$temp = array();
				array_push($temp, $get_result['query_result'][$i]->communication_categoy_name);
				
				$actionCol21="";
				if($this->privilegeduser->hasPrivilege("CommunicationCategoriesAddEdit")){
					$actionCol21 .= '<a href="javascript:void(0);" onclick="deleteData1(\'' . $get_result['query_result'][$i]->communication_category_id . '\',\'' . $get_result['query_result'][$i]->status. '\');" title="Status">'.$get_result['query_result'][$i]->status .'</a>';
				}
				
				$actionCol = "";
				if($this->privilegeduser->hasPrivilege("CommunicationCategoriesAddEdit")){
					$actionCol.= '<a href="communicationcategories/addEdit?text=' . rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->communication_category_id) , '+/', '-_') , '=') . '" title="Edit"><i class="fa fa-edit"></i></a>';
				}	
				array_push($temp, $actionCol21);
				array_push($temp, $actionCol);
				
				
				array_push($items, $temp);
			}
		}	

		$result["aaData"] = $items;
		echo json_encode($result);
		exit;
	}
	
	
	function submitForm()
	{ 
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
		
			$condition = "communication_categoy_name='".$_POST['communication_categoy_name']."' ";
			if(isset($_POST['communication_category_id']) && $_POST['communication_category_id'] > 0)
			{
				$condition .= " &&  communication_category_id != ".$_POST['communication_category_id'];
			}
			
			$check_name = $this->communicationcategoriesmodel->getdata("tbl_communication_categories",$condition);
			if(!empty($check_name)){
				echo json_encode(array("success"=>"0",'msg'=>'Record Already Present!'));
				exit;
			}
			
			if (!empty($_POST['communication_category_id'])) {
				$data_array=array();			
				$communication_category_id = $_POST['communication_category_id'];
				$data_array['communication_categoy_name'] = (!empty($_POST['communication_categoy_name'])) ? $_POST['communication_categoy_name'] : '';
				$data_array['updated_on'] = date("Y-m-d H:i:s");
				$data_array['updated_by']=$_SESSION["webadmin"][0]->user_id;
				
				$result = $this->communicationcategoriesmodel->updateRecord('tbl_communication_categories', $data_array,'communication_category_id',$communication_category_id);
				
			}else {
				$data_array=array();
				$data_array['communication_categoy_name'] = (!empty($_POST['communication_categoy_name'])) ? $_POST['communication_categoy_name'] : '';
				
				$data_array['created_on'] = date("Y-m-d H:i:s");
				$data_array['created_by'] = $_SESSION["webadmin"][0]->user_id;
				$data_array['updated_on'] = date("Y-m-d H:i:s");
				$data_array['updated_by'] = $_SESSION["webadmin"][0]->user_id;
				
				$result = $this->communicationcategoriesmodel->insertData('tbl_communication_categories', $data_array, '1');
				$result = 1;
				
			}
		
		
			if (!empty($result)) {
				echo json_encode(array(
					'success' => '1',
					'msg' => 'Record Added/Updated Successfully.'
				));
				exit;
			}
			else{
				echo json_encode(array(
					'success' => '0',
					'msg' => 'Problem in data update.'
				));
				exit;
			}
			
		}
		else {
			return false;
		}
	
	}
	
	function delrecord12()
	{
		$id=$_POST['id'];
		$status=$_POST['status'];
		
		$appdResult = $this->communicationcategoriesmodel->delrecord12("tbl_communication_categories","communication_category_id",$id,$status);
		if($appdResult)
		{
			echo "1";
		}
		else
		{
			echo "2";	
		}	
	}
	
	function logout()
	{
		$this->session->unset_userdata('logged_in');
		session_destroy();
		redirect('auth/login', 'refresh');
	}
	
}

?>
