<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Coursesmaster extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

		// $this->load->helper('erp_setting');
		ini_set( 'memory_limit', '25M' );
		ini_set('upload_max_filesize', '25M');  
		ini_set('post_max_size', '25M');  
		ini_set('max_input_time', 3600);  
		ini_set('max_execution_time', 3600);

		$this->load->model('coursesmastermodel', '', TRUE);
	}

	function index()
	{
		if (!empty($_SESSION["webadmin"])) {
			$this->load->view('template/header.php');
			$this->load->view('coursesmaster/index');
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
	function addEdit($id = NULL)
	{
		if (!empty($_SESSION["webadmin"])) {
			$record_id = "";

			// print_r($_GET);

			if (!empty($_GET['text']) && isset($_GET['text'])) {
				$varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
				parse_str($varr, $url_prams);
				$record_id = $url_prams['id'];
			}
			$result = "";
			$result['categories'] = $this->coursesmastermodel->getDropdown("tbl_categories","category_id,categoy_name");
			$result['details'] = $this->coursesmastermodel->getFormdata($record_id);
			//echo "<pre>";print_r($result);exit;
			
			$this->load->view('template/header.php');
			$this->load->view('coursesmaster/addEdit', $result);
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}

	function submitForm()
	{ 
		//echo "<pre>";print_r($_POST);exit;
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			
			$condition = "category_id='".$_POST['category_id']."' && course_name= '".$_POST['course_name']."' ";
			if(isset($_POST['course_id']) && $_POST['course_id'] > 0)
			{
				$condition .= " &&  course_id != ".$_POST['course_id'];
			}
			
			$check_name = $this->coursesmastermodel->getdata("tbl_courses",$condition);
			//echo "<pre>";print_r($check_name);exit;
			//echo $_SESSION["webadmin"][0]->user_name;exit;
			if(!empty($check_name)){
				echo json_encode(array("success"=>"0",'msg'=>'Record Already Present!'));
				exit;
			}
			
			//exit;
			if (!empty($_POST['course_id'])) {
				$data_array = array();			
				$course_id = $_POST['course_id'];
		 		
				$data_array['category_id'] = (!empty($_POST['category_id'])) ? $_POST['category_id'] : '';
				$data_array['course_name'] = (!empty($_POST['course_name'])) ? $_POST['course_name'] : '';
				$data_array['updated_on'] = date("Y-m-d H:i:s");
				$data_array['updated_by'] = $_SESSION["webadmin"][0]->user_id;
				
				$result = $this->coursesmastermodel->updateRecord('tbl_courses', $data_array,'course_id',$course_id);
				
			}else {
				
				$data_array = array();
				$data_array['category_id'] = (!empty($_POST['category_id'])) ? $_POST['category_id'] : '';
				$data_array['course_name'] = (!empty($_POST['course_name'])) ? $_POST['course_name'] : '';
				
				$data_array['created_on'] = date("Y-m-d H:i:s");
				$data_array['created_by'] = $_SESSION["webadmin"][0]->user_id;
				$data_array['updated_on'] = date("Y-m-d H:i:s");
				$data_array['updated_by'] = $_SESSION["webadmin"][0]->user_id;
				//echo "<pre>";print_r($data_array);exit;
				
				$result = $this->coursesmastermodel->insertData('tbl_courses', $data_array, '1');
			}
		
		
			if (!empty($result)) {
				echo json_encode(array(
					'success' => '1',
					'msg' => 'Record Added/Updated Successfully.'
				));
				exit;
			}
			else{
				echo json_encode(array(
					'success' => '0',
					'msg' => 'Problem in data update.'
				));
				exit;
			}
			
		}
		else {
			return false;
		}
	
	}
	
	/*new code end*/
	function fetch($id=null)
	{
		//$_GET['album_id'] = $_GET['id'];
		//echo $id;exit;
		$get_result = $this->coursesmastermodel->getRecords($_GET);
		$result = array();
		$result["sEcho"] = $_GET['sEcho'];
		
		$result["iTotalRecords"] = $get_result['totalRecords']; //	iTotalRecords get no of total recors
		$result["iTotalDisplayRecords"] = $get_result['totalRecords']; //  iTotalDisplayRecords for display the no of records in data table.
		$items = array();
		if(!empty($get_result['query_result'])){
			for ($i = 0; $i < sizeof($get_result['query_result']); $i++) {
				$temp = array();
				array_push($temp, $get_result['query_result'][$i]->categoy_name);
				array_push($temp, $get_result['query_result'][$i]->course_name);
				
				$actionCol21="";
				if($this->privilegeduser->hasPrivilege("CourseAddEdit")){
					$actionCol21 .= '<a href="javascript:void(0);" onclick="deleteData1(\'' . $get_result['query_result'][$i]->course_id. '\',\'' . $get_result['query_result'][$i]->status. '\');" title="">'.$get_result['query_result'][$i]->status.'</a>';
				}	
				
				$actionCol1 = "";
				if($this->privilegeduser->hasPrivilege("CourseAddEdit")){
					$actionCol1.= '<a href="coursesmaster/addEdit?text=' . rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->course_id) , '+/', '-_') , '=') . '" title="Edit"><i class="fa fa-edit"></i></a> ';
				}	
				
				array_push($temp, $actionCol21);
				array_push($temp, $actionCol1);
				
				
				array_push($items, $temp);
			}
		}	

		$result["aaData"] = $items;
		echo json_encode($result);
		exit;
	}
	
	
	function changeapproval(){
		$result = "";
		$condition = "1=1 && course_id='".$_GET['id']."' ";
		$result['details'] = $this->coursesmastermodel->getdata('tbl_courses',$condition);
		//echo "<pre>";print_r($result["details"]);exit;
		$view = $this->load->view('routinesactions/changeapproval',$result,true);
		echo $view;
	}
	
	function view($id = NULL)
	{
		if (!empty($_SESSION["webadmin"])) {
			$record_id = "";

			// print_r($_GET);

			if (!empty($_GET['text']) && isset($_GET['text'])) {
				$varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
				parse_str($varr, $url_prams);
				$record_id = $url_prams['id'];
			}
			$result = "";
			$result['details'] = $this->coursesmastermodel->getFormdata($record_id);
			
			$this->load->view('template/header.php');
			$this->load->view('coursesmaster/view', $result);
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
	
	function delRecord()
	{
		
		$id=$_POST['id'];
		$status=$_POST['status'];
		
		$appdResult = $this->coursesmastermodel->delrecord("tbl_courses","course_id",$id,$status);
		if($appdResult)
		{
			echo "1";
		}
		else
		{
			echo "2";	
		}	
	}
	
	function delrecord12()
	{
		//echo $_POST['status'];exit;
		$id=$_POST['id'];
		$status=$_POST['status'];
		
		$appdResult = $this->coursesmastermodel->delrecord12("tbl_courses","course_id",$id,$status);
		if($appdResult)
		{
			echo "1";
		}
		else
		{
			echo "2";	
		}	
	}

	function logout()
	{
		$this->session->unset_userdata('logged_in');
		session_destroy();
		redirect('auth/login', 'refresh');
	}
	
}

?>
