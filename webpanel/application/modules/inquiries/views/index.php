<?php 
//session_start();
//print_r($_SESSION["webadmin"]);
//echo "<pre>";
//print_r($roles);exit;
?>
<!-- start: Content -->
<div id="content" class="content-wrapper">
	 <div class="page-title">
      <div>
        <h1>Inquiries</h1>            
      </div>
      <div>
        <ul class="breadcrumb">
          <li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="<?php echo base_url();?>inquiries">Inquiries</a></li>
        </ul>
      </div>
    </div> 
    <div class="card">
    	<div class="page-title-border">
	<?php 
					if ($this->privilegeduser->hasPrivilege("InquiryExport")) {
				?>
			<a style="margin-left:10px;" class="btn btn-primary icon-btn export_contactus"><i class="fa fa-plus"></i>Export</a>
			<?php }?>	
         </div>     
		
        <div class="col-sm-12">
			<form name="filter_form" method="post" id="filter_form">
				<div class="box-content form-horizontal product-filter">            	
				
					<div class="col-sm-2 col-xs-12">
						<div class="dataTables_filter searchFilterClass form-group">
							<label for="firstname" class="control-label">Academic Year</label>
							<input id="sSearch_0" name="sSearch_0" type="text" class="searchInput form-control"/>
						</div>
					</div>
					
					<div class="col-sm-2 col-xs-12">
						<div class="dataTables_filter searchFilterClass form-group">
							<label for="firstname" class="control-label">Zone</label>
							<input id="sSearch_1" name="sSearch_1" type="text" class="searchInput form-control"/>
						</div>
					</div>
					
					<div class="col-sm-2 col-xs-12">
						<div class="dataTables_filter searchFilterClass form-group">
							<label for="firstname" class="control-label">Center</label>
							<input id="sSearch_2" name="sSearch_2" type="text" class="searchInput form-control"/>
						</div>
					</div>

					<div class="col-sm-2 col-xs-12">
						<div class="dataTables_filter searchFilterClass form-group">
							<label for="Email ID" class="control-label">Category</label>
							<input id="sSearch_3" name="sSearch_3" type="text" class="searchInput form-control"/>
						</div>
					</div>
					
					<div class="col-sm-2 col-xs-12">
						<div class="dataTables_filter searchFilterClass form-group">
							<label for="Email ID" class="control-label">Course</label>
							<input id="sSearch_4" name="sSearch_4" type="text" class="searchInput form-control"/>
						</div>
					</div>
					
					<div class="col-sm-2 col-xs-12">
						<div class="dataTables_filter searchFilterClass form-group">
							<label for="Email ID" class="control-label">Inquiry No.</label>
							<input id="sSearch_5" name="sSearch_5" type="text" class="searchInput form-control"/>
						</div>
					</div>
					
					<div class="col-sm-2 col-xs-12">
						<div class="dataTables_filter searchFilterClass form-group">
							<label for="Email ID" class="control-label">Student First Name</label>
							<input id="sSearch_6" name="sSearch_6" type="text" class="searchInput form-control"/>
						</div>
					</div>
					
					<div class="col-sm-2 col-xs-12">
						<div class="dataTables_filter searchFilterClass form-group">
							<label for="Email ID" class="control-label">Student Last Name</label>
							<input id="sSearch_7" name="sSearch_7" type="text" class="searchInput form-control"/>
						</div>
					</div>
					
					<div class="col-sm-2 col-xs-12">
						<div class="dataTables_filter searchFilterClass form-group">
							<label for="firstname" class="control-label">Inquiry Status</label>
							<select name="sSearch_8" id="sSearch_8" class="searchInput form-control">
								<option value="">Select Status</option>
								<option value="Hot" >Hot</option>
								<option value="Cold" >Cold</option>
								<option value="Warm" >Warm</option>
							</select>
						</div>
					</div>
					
					<div class="col-sm-2 col-xs-12">
						<div class="dataTables_filter searchFilterClass form-group">
							<label for="firstname" class="control-label">Inquiry Progress</label>
							<select name="sSearch_9" id="sSearch_9" class="searchInput form-control">
								<option value="">Select Progress</option>
								<option value="Meeting" >Meeting</option>
								<option value="Call" >Call</option>
								<option value="Walk inn" >Walk inn</option>
							</select>
						</div>
					</div>
					
					
					<div class="control-group clearFilter" style="clear: right;float:left;">
						<div class="controls">
							<a href="#" onclick="clearSearchFilters();"><button class="btn" style="margin:32px 10px 10px 10px;">Clear Search</button></a>
						</div>
					</div>
					
				</div>
			</form>
        </div>
		 
		 
         <div class="clearfix"></div>
         <div class="card-body">
          	<div class="box-content">
            	 <div class="table-responsive scroll-table">
                   <table class="dynamicTable display table table-bordered non-bootstrap">
                        <thead>
                          <tr>
							<th>Academic Year</th>
							<th>Zone</th>
							<th>Center</th>
							<th>Category</th>
							<th>Course</th>
							<th>Inquiry No.</th>
							<th>Student First Name</th>
							<th>Student Last Name</th>
							<th>Inquiry Status</th>
							<th>Inquiry Progress</th>
							<th>Action</th>
							<!-- <th data-bSortable="false">Convert To Admission</th> -->
						</tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                        </tfoot>
                    </table>           
                </div>
            </div>
         </div>
         <div class="clearfix"></div>
    </div>
</div><!-- end: Content -->
			
<script>
	
	$( document ).ready(function() {
		clearSearchFilters();
		$(".datepicker").datepicker({
			//format: 'YYYY-MM-DD'
		});
	});	

	
	// $("#sSearch_3").datepicker({format: 'yyyy-dd-mm '});
	function changestatus(id){
		$.ajax({
			url: "<?php echo base_url(); ?>feedbacks/changestatus/"+id,
			dataType:'json',
			beforeSend:function(){
			},
			success: function(res){
				if(res['success']){
					displayMsg("success",res['msg']);
					setTimeout(function(){
						window.location = "<?php echo base_url();?>feedbacks";
					},2000);

				}
				else{
					if(res['msg']=='redirect'){
						displayMsg("error",'Session has expired.');
						setTimeout(function(){
							window.location = "<?php echo base_url();?>login";
						},2000)
					}
					else{
						displayMsg("error",res['msg']);
						return false;
					}
				}
			},
			error: function(){
				displayMsg("error",'Error Occured,Please reload the page.');
				return false;
			}
		});
	}
	
	$(".export_contactus").on("click", function()
	{
		$('.export_contactus').attr("disabled","disabled");
		var act = "<?php  echo base_url();?>inquiries/export";
		$("#filter_form").attr("action",act);
		$("#filter_form").submit();
	});
	
	$("#filter_form").on("submit", function()
	{
		$('.export_contactus').removeAttr("disabled");
		
		$('.export_contactus_sizes').removeAttr("disabled");
		
		setTimeout("location.reload(true);",1000);
	});
	
	<?php
	if(isset($_SESSION['contactus_export_success']))
	{
		?>
		displayMsg("<?php echo $_SESSION['contactus_export_success']; ?>", "<?php echo $_SESSION['contactus_export_msg']; ?>");
		<?php
		unset($_SESSION['contactus_export_success']);
		unset($_SESSION['contactus_export_msg']);
	}
	?>
	
	document.title = "Inquiries";
</script>