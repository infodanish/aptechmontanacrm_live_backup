<!DOCTYPE html>
<html lang="en">
<head>
	<!-- start: Meta -->
	<meta charset="utf-8">
	<title>Montana - CRM</title>
	<meta name="description" content="Admin Panel">
	<!-- end: Meta -->
	<!-- start: Mobile Specific -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- end: Mobile Specific -->
	<!-- start: CSS -->	
	<link id="base-style-responsive" href="<?PHP echo base_url();?>css/main.css" rel="stylesheet">			
	<link href="https://fonts.googleapis.com/css?family=Niconne" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=PT+Sans" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700" rel="stylesheet">
    <!-- end: CSS -->	
    <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<link id="ie-style" href="<?PHP echo base_url();?>css/ie.css" rel="stylesheet">
	<![endif]-->
	
	<!--[if IE 9]>
		<link id="ie9style" href="<?PHP echo base_url();?>css/ie9.css" rel="stylesheet">
	<![endif]-->
	<!-- start: JavaScript -->	
	<script src="<?PHP echo base_url();?>js/jquery-2.1.4.min.js"></script>
	<script src="<?PHP echo base_url();?>js/essential-plugins.js"></script>
	<script src="<?PHP echo base_url();?>js/bootstrap.min.v3.3.6.js"></script>
	<script src="<?PHP echo base_url();?>js/plugins/pace.min.js"></script>
	<script src="<?PHP echo base_url();?>js/main.js"></script>
	<script src="<?PHP echo base_url();?>js/jquery.form.js"></script>
	<script src="<?PHP echo base_url();?>js/jquery.validate.js"></script>
	<!-- end: JavaScript-->
</head>
<body>